// const elixir = require('laravel-elixir');

// require('laravel-elixir-vue');

// elixir(mix => {
//     // elixir(function(mix) {
//     mix.sass('app.scss')
//        .webpack('app.js')
//        .webpack('surveys/questions_handler.js')
//        .webpack('surveys/respond_survey_handler.js')
//        .sass('survey.scss')
//        .webpack('survey.js');
//        // .webpack('paysheet.js');
// });

// code for gulp 4

let gulp = require('gulp');
let sass = require('gulp-sass');
let rename = require('gulp-rename');

sass.compiler = require('node-sass');

gulp.task('styles', () => {
    return gulp.src('resources/assets/sass/app.scss')
        .pipe(sass())
        .pipe(rename('app.css'))
        .pipe(gulp.dest('public/css'));
});

// gulp.task('assets', () => {
//     gulp.src('assets/*')
//         .pipe(gulp.dest('public'));
// })

gulp.task('watch', () => {
    gulp.watch('app.scss', gulp.series('styles'));
    // gulp.watch('asset/*', gulp.series('assets'));
});

// gulp.task('default', gulp.parallel('styles', 'assets'));
gulp.task('default', gulp.parallel('styles'));