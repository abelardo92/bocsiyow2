<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;
use Illuminate\Notifications\Notification;

class DiarySchedule extends Notification
{
    use Queueable;

    public $diary;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($diary)
    {
        $this->diary = $diary;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['nexmo'];
    }

    public function toNexmo($notifiable)
    {
        $confirmation_code = is_numeric($this->diary->confirmation_code) ? $this->diary->confirmation_code : "";
        $name = $this->diary->customer_name ? $this->diary->customer_name : ($this->diary->customer ? $this->diary->customer->name : $this->diary->comments);
        return (new NexmoMessage)->content("Hola {$name}, Se agendo una cita en bocsiYow Barbershop para el {$this->diary->date} a las {$this->diary->time} en la sucursal {$this->diary->subsidiary->name}, tu numero de confirmacion es: $confirmation_code");
    }
}
