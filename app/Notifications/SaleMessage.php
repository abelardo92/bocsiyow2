<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\NexmoMessage;
use Illuminate\Notifications\Notification;

class SaleMessage extends Notification
{
    use Queueable;

    public $sale;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($sale)
    {
        $this->sale = $sale;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['nexmo'];
    }

    public function toNexmo($notifiable)
    {
        // $subtotal = 0;
        // $hasPromotion = false;
        // $wash_quantity = $this->sale->washQuantity();
        // $ticket_link = route('sale.print_customer',['sale_id' => base64_encode($this->sale->id)]);
        $ticket_link = "www.bocsiyowbarbershop.com.mx/ticket/".base64_encode($this->sale->id);
        $note_content = "Gracias por venir a BOCSIYOW {$this->sale->subsidiary->name}, consulta tu nota en $ticket_link";
        // $note_content = "Gracias por venir a BOCSIYOW {$this->sale->subsidiary->name}\n\n";
        
        // foreach($this->sale->services as $sale_service) {

        //     // if($this->sale->subsidiary->is_laundry && $sale_service->service_id == 23 && $wash_quantity <= 5){
        //     //     $note_content .= "1-5 kg";
        //     // } else {
        //     //     $note_content .= $sale_service->qty;
        //     // }
        //     $note_content .= $this->sale->subsidiary->is_laundry && $sale_service->service_id == 23 && $wash_quantity <= 5 ? "1-5 kg" : "";
        //     $note_content .= "{$sale_service->service->name}: $";
            
        //     if($sale_service->has_promotion || $this->sale->is_courtesy) {
        //         $note_content .= number_format(($sale_service->original_price * $sale_service->qty), 2, '.', '');
        //     } else {
        //         if($sale_service->service_id == 23 && $wash_quantity <= 5) {
        //             $note_content .= number_format($sale_service->price * 5, 2, '.', '');
        //         } else {
        //             $note_content .= number_format($sale_service->price * $sale_service->qty, 2, '.', '');
        //         }
        //     }
        //     $subtotal += $sale_service->original_price;
        //     $note_content .= "\n";
        //     if($sale_service->has_promotion && $sale_service->original_price > $sale_service->price) {
        //         $hasPromotion = true;
        //         $note_content .= "Desc. promoción: - $".number_format(($sale_service->original_price - $sale_service->price) * $sale_service->qty, 2, '.', '')."\n";  
        //     }  
        // }

        // foreach($this->sale->products as $sale_product) {
            
        //     $note_content .= $sale_product->qty;
        //     $note_content .= $sale_product->product->name;
        //     $note_content .= $sale_product->price * $sale_product->qty;
        //     $note_content .= "\n";
        //     if($sale_product->price < $sale_product->product->sell_price) {
        //         $hasPromotion = true;
        //         $note_content .= "Desc. promoción - $".number_format(($sale_product->product->sell_price - $sale_product->price) * $sale_product->qty, 2, '.', '')."\n";
        //     }
        //     $subtotal += $sale_product->original_price;
        // }

        // foreach($this->sale->packages as $sale_package) {
        //     $note_content .= $sale_package->quantity;
        //     $note_content .= $sale_package->package->name;
        //     $note_content .= $sale_package->package->price * $sale_package->quantity;
        //     $subtotal += $sale_package->price;
        // }

        // if($this->sale->is_courtesy) {
        //     $note_content .= "Subtotal: $".number_format($subtotal, 2, '.', '')."\n";
        // } else {
        //     $note_content .= "Subtotal: $".number_format($this->sale->subtotal, 2, '.', '')."\n";
        // }
        // if($this->sale->tip > 0) {
        //     $note_content .= "Propina: $".number_format($this->sale->tip, 2, '.', '')."\n";
        // }

        // if(!$hasPromotion && $this->sale->has_birthday && !$this->sale->kids_promotion) {
        //     $note_content .= "Desc. Cumple: $".number_format($this->sale->servicesMaxPrice(), 2, '.', '')."\n"; 
        // }

        // if($this->sale->promotion && $this->sale->promotion->discount_percentaje > 0) {
        //     $services_promotion_price_total = $this->sale->services->where('discount_percentaje', $this->sale->discount_percentaje)->sum('price'); 
        //     $note_content .= "Desc({$this->sale->discount_percentaje}%): $".number_format(($services_promotion_price_total * $this->sale->discount_percentaje) / 100, 2, '.', '')."\n";
        // }

        // if((!$hasPromotion || ($this->sale->promotion && $this->sale->promotion->mix_with_diaries_discount)) && 
        // !$this->sale->payments()->where('method','Monedero')->first() && $this->sale->has_discount && 
        // !$this->sale->is_promo_ut && !$this->sale->kids_promotion) {
        //     $services_promotion_price_total = $this->sale->services->where('include_in_diaries_discount', true)->sum('price'); 
        //     $note_content .= "Desc({$this->sale->discount_percentaje}%): $".number_format(($services_promotion_price_total * $this->sale->discount_percentaje) / 100, 2, '.', '')."\n";
        // }
        
        // if($this->sale->is_promo_ut && !$this->sale->has_birthday && !$this->sale->payments()->where('method','Monedero')->first() && !$this->sale->kids_promotion) {
        //     $note_content .= "Desc. UT(20%): $".number_format(($this->sale->servicesTotalPrice() * 20) / 100, 2, '.', '')."\n";
        // }
        
        // if(!$hasPromotion && $this->sale->kids_promotion > 0) {
        //     if($this->sale->kids_promotion == 50) {
        //         $amount = (($this->sale->getKidsPromotionService()->price * 50) / 100) * $this->sale->getKidsPromotionService()->qty;
        //     } else {
        //         $amount = (($this->sale->getKidsPromotionService()->price * 100) / 100) * $this->sale->getKidsPromotionService()->qty;
        //     }
        //     $note_content .= "Desc. Niño: $".number_format($amount, 2, '.', '')."\n";
        // }
        // $note_content .= "Total: $".($this->sale->is_courtesy ? ($subtotal + $this->sale->tip) : $this->sale->total);

        
        return (new NexmoMessage)->content($note_content);
    }
}