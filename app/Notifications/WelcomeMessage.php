<?php

namespace App\Notifications;

use App\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;
use Illuminate\Notifications\Notification;

class WelcomeMessage extends Notification
{
    use Queueable;
    public $customer;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($customer)
    {
        $this->customer = $customer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['nexmo'];
    }

    public function toNexmo($notifiable)
    {
        if($this->customer->related_brand == 1) {
            $message = "Bienvenido {$this->customer->name} a bocsiyow barbershop, ha sido registrado como cliente frecuente. Proporcione su nombre o número telefónico al agendar sus citas y tendrá los beneficios de ser cliente frecuente";
            return (new NexmoMessage)->content($message)->unicode();
        }
    }
}