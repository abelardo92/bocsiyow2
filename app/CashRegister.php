<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CashRegister extends Model
{
    protected $fillable = [
        'date' ,'time' ,'subsidiary_id' ,'imprest', 
        'employee_id', 'turn_id', 'exchange_rate_id', 'is_delayed'
    ];

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function movements()
    {
        return $this->hasMany(CashRegisterMovement::class);
    }

    public function lastMovement()
    {
        return $this->hasOne(CashRegisterMovement::class)->where('type', 'corte')->latest();
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function turn()
    {
        return $this->belongsTo(Turn::class);
    }

    public function sales()
    {
        return $this->hasMany(Sale::class);
    }

    public function salesFinished()
    {
        return $this->hasMany(Sale::class)->finished()->notCanceled()->forCustomers();
    }

    public function payments()
    {
        return $this->hasMany(SalePayment::class);
    }

    public function laundryServices()
    {
        return $this->hasMany(LaundryService::class);
    }

    public function exchangeRate()
    {
        return $this->belongsTo(ExchangeRate::class);
    }

    public function totalFromOtherCashRegisters()
    {
        $subtotal = 0;
        $cash_register_id = $this->id;
        //$sales_additional = $this->subsidiary->sales()->with(['payments'])
        //->AdditionalSalesByCashRegister($this->id)->get();
        $sales_additional = $this->additionalSales();
        
        foreach ($sales_additional as $sale) {
            $sale_total = $sale->payments->where('cash_register_id', $this->id)->sum('total');
            if($sale->finished_cash_register_id != null && $sale->finished_cash_register_id == $this->id) {
                $sale_total -= $sale->money_change;
            }
            $subtotal += $sale_total;
        }
        return $subtotal;
    }

    public function totalLaundry()
    {
        $subtotal = 0;
        $cash_register_sales = $this->sales->where('paid', true)->where('finish', true)->where('canceled_by',null);

        foreach ($cash_register_sales as $cash_register_sale) {
            if($cash_register_sale->cash_register_id == $cash_register_sale->finished_cash_register_id) {
                $subtotal += $cash_register_sale->payments->sum('total') - $cash_register_sale->money_change;
            } else {
                $subtotal += $cash_register_sale->payments->where('cash_register_id', $this->id)->sum('total');
            }
        }

        $subtotal += $this->totalFromOtherCashRegisters();
        return $subtotal;
    }

    public function additionalSales()
    {
        $cash_register_id = $this->id;
        /*
        $sales_additional = $this->subsidiary->sales->where('paid', true)->where('finish', true)
        ->where('canceled_by',null)->where('cash_register_id', '!=', $this->id)
        ->filter(function($sale) use ($cash_register_id)
        {
            $payments = $sale->payments->where('cash_register_id', $cash_register_id); 
            return !$payments->isEmpty(); 
        });
        return $sales_additional;
        */
        
        return $this->subsidiary->sales()->with(['payments'])->finished()->notCanceled()
        ->where('cash_register_id', '!=', $this->id)
        ->whereHas('payments', function($q) use ($cash_register_id) {
            $q->where('cash_register_id', $cash_register_id);
        })->get();
    }

    public function scopeToday($query)
    {
        $today = Carbon::now()->format('Y-m-d');
        return $query->where('date', $today);
    }

    public function scopeByRange($query, $start, $end)
    {
        return $query->whereBetween('date', [$start, $end]);
    }

    public function scopeByDate($query, $start)
    {
        return $query->where('date', $start);
    }

    public function scopeDelayed($query)
    {
        return $query->where('is_delayed', true);
    }

    public function scopeNotDelayed($query)
    {
        return $query->where('is_delayed', false);
    }

    public function scopeYesterday($query)
    {
        $yesterday = Carbon::now()->subDay()->format('Y-m-d');
        return $query->where('date', $yesterday);
    }

    public function scopeOpen($query)
    {
        return $query->where('close', false);
    }

    public function scopeClose($query)
    {
        return $query->where('close', true);
    }
}
