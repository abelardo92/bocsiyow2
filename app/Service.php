<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public $current_promotion;

    protected $fillable = [
        'key', 'name', 'sell_price', 'second_sell_price', 'third_sell_price', 'commission', 'cost', 'cost2',
        'cost3', 'kids_promotion', 'subsidiary_id', 'is_laundry', 'type', 'include_in_diaries_discount',
        'include_in_birthday_discount','show_for_customer_diaries'
    ];

    // protected $attributes = [
    //     'current_promotion' => null,
    // ];

    public function getActivePromotionAttribute() {
        return $this->current_promotion;
    }

    public function setActivePromotionAttribute($subsidiary_id) {
        $this->current_promotion = $this->activePromotion($subsidiary_id);
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function getCost($subsidiary_id, $currentPromo = null) {

        // check if current promotions contains the service with different cost...
        if($currentPromo) {
            $promoService = $currentPromo->promotionservices->where('service_id', $this->id)->first();
            if($promoService && $promoService->cost > 0) {
                return $promoService->cost;
            }
        }

        $subsidiary_price = $this->subsidiaryPrices->where('subsidiary_id', $subsidiary_id)->first();
        if($subsidiary_price && $subsidiary_price->cost > 0) {
            return $subsidiary_price->cost;
        }
        return $this->cost;
    }

    public function promotionservices()
    {
        $promotionservices = $this->hasMany(PromotionService::class)->whereHas('promotion', function ($query) {
            $query->where('is_active', true)
                ->whereDate('start_date','<=', date('Y-m-d'))
                ->whereDate('end_date','>=', date('Y-m-d'))
                ->where('start_time','<=',date('H:i:s'))
                ->where('end_time','>=',date('H:i:s'))
                ->where(strtolower(date('l')),true);
        });
        return $promotionservices;
    }

    public function promotionServices2()
    {
        return $this->hasMany(PromotionService::class);
    }

    public static function filterPromotionServices($subsidiary_id) // might be static or instance method
    {

        return function ($query) use ($subsidiary_id) {
            $query->whereHas('promotion', function ($query) use ($subsidiary_id) {
                $query->where('is_active', true)
                    ->whereDate('start_date','<=', date('Y-m-d'))
                    ->whereDate('end_date','>=', date('Y-m-d'))
                    ->where('start_time','<=',date('H:i:s'))
                    ->where('end_time','>=',date('H:i:s'))
                    ->where(function ($q) use ($subsidiary_id) {
                        $q->where('subsidiary_id', null)
                        ->orWhere('subsidiary_id', $subsidiary_id);
                    })
                    ->where(strtolower(date('l')),true);
            });
        };
    }

    public function activePromotion($subsidiary_id = null)
    {
        if($subsidiary_id) {
            $promotion_service = $this->promotionservices()->with('promotion')->whereHas('promotion', function ($query) use ($subsidiary_id) {
                $query->where('is_active', true)
                    ->whereDate('start_date','<=',date('Y-m-d'))
                    ->whereDate('end_date','>=',date('Y-m-d'))
                    ->where('start_time','<=',date('H:i:s'))
                    ->where('end_time','>=',date('H:i:s'))
                    ->where('subsidiary_id', $subsidiary_id)
                    ->where(strtolower(date('l')),true);
            })->get()->first();

            if($promotion_service) {
                return $promotion_service;
            }
        }

        return $this->promotionservices()->with('promotion')->whereHas('promotion', function ($query) {
            $query->where('is_active', true)
                ->whereDate('start_date','<=',date('Y-m-d'))
                ->whereDate('end_date','>=',date('Y-m-d'))
                ->where('start_time','<=',date('H:i:s'))
                ->where('end_time','>=',date('H:i:s'))
                ->where('subsidiary_id', null)
                ->where(strtolower(date('l')),true);
        })->get()->first();
    }

    // this will work to not set the discount price immediately when a cash only product is set in the sale...
    public function isValidPromotion($activePromotion, $sale_with_cash = 0) {
        return !$activePromotion->promotion->cash_only || $sale_with_cash;
    }

    public function currentPrice($subsidiary_id, $sale_with_cash = 0)
    {
        if(($activePromotion = $this->activePromotion($subsidiary_id)) && $this->isValidPromotion($activePromotion, $sale_with_cash)) {
            return $activePromotion->price;
        } else {
            $subsidiaryPrice = $this->subsidiaryPrice($subsidiary_id);
            if($subsidiaryPrice != null){
                return $subsidiaryPrice;
            }
            return $this->sell_price;
        }
    }

    public function subsidiaryPrice($subsidiary_id)
    {
        foreach ($this->subsidiaryPrices as $subsidiaryPrice) {
            if($subsidiaryPrice->subsidiary_id == $subsidiary_id){
                return $subsidiaryPrice->price;
            }
        }
        return null;
    }

    public function includeInDiariesdiscount($subsidiary_id, $sale_with_cash = 0) {

        // if there is an active promotion with this service...
        if($promotionService = $this->activePromotion($subsidiary_id)) {
            if($this->isValidPromotion($promotionService, $sale_with_cash)) {
                return $promotionService->include_in_diaries_discount;
            }
        }

        if($subsidiaryPrice = $this->subsidiaryPrices->where('subsidiary_id', $subsidiary_id)->where('service_id', $this->id)->first()) {
            return $subsidiaryPrice->include_in_diaries_discount;
        }
        return $this->include_in_diaries_discount;
    }

    public function originalPrice($subsidiary_id) {
        $subsidiaryPrice = $this->subsidiaryPrice($subsidiary_id);
        if($subsidiaryPrice != null) {
            return $subsidiaryPrice;
        }
        return $this->sell_price;
    }

    public function hasPromotion($subsidiary_id, $cash_only = 0)
    {
        return $this->originalPrice($subsidiary_id) != $this->currentPrice($subsidiary_id, $cash_only);
    }

    public function subsidiaryPrices()
    {
        return $this->hasMany(SubsidiaryServicePrice::class);
    }

    public function scopeLaundry($query)
    {
        return $query->where('is_laundry', true);
    }

    public function scopeNotLaundry($query)
    {
        return $query->where('is_laundry', false);
    }

    public function scopeKilo($query)
    {
        return $query->where('type', 1);
    }

    public function scopePiece($query)
    {
        return $query->where('type', 2);
    }

    public function scopeRent($query)
    {
        return $query->where('type', 3);
    }

    public function scopeForCustomerDiaries($query)
    {
        return $query->where('show_for_customer_diaries', true);
    }

    public function scopeBySubsidiary($query, $subsidiary_id) {
        return $query->where(function ($q) use ($subsidiary_id) {
            $q->where('subsidiary_id', null)
            ->orWhere('subsidiary_id', $subsidiary_id);
        });
    }
}
