<?php

namespace App\Surveys;

use App\Subsidiary;
use App\Surveys\Survey;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    const INPUT_TYPES = [
        'text', 'textarea', 'checkbox', 'radio', 'satisfaction'
    ];

    protected $fillable = [
        'title', 'input_type', 'subsidiary_id', 'order', 'active', 'required', 'options', 'for_barber'
    ];

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function surveys()
    {
        return $this->belongsToMany(Survey::class)->withTimestamps()->withPivot('answer');
    }

    public function options()
    {
        return explode('|', $this->options);
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeNoSubsidiary($query)
    {
        return $query->where('subsidiary_id', 0);
    }

    public function scopeSatisfactions($query)
    {
        return $query->where('input_type', 'satisfaction');
    }
}
