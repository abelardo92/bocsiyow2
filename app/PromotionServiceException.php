<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromotionServiceException extends Model
{
    protected $fillable = [
        'promotion_id', 'service_id'
    ];

    public function service()
    {
        return $this->belongsTo(Service::class);
    }
    
    public function promotion()
    {
        return $this->belongsTo(Promotion::class);
    }
}