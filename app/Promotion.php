<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $table = 'promotions';
    protected $fillable = [
        'name', 'start_date', 'end_date', 'start_time', 'end_time', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 
        'apply_on_other_promotions', 'is_active', 'must_buy_a_service', 'must_buy_a_product', 'subsidiary_id', 'services_discount_percentaje', 
        'products_discount_percentaje', 'must_buy_specific_service', 'must_buy_specific_product', 'is_permanent', 'discount_percentaje', 
        'mix_with_diaries_discount', 'cash_only'
    ];

    public function promotionservices()
    {
        return $this->hasMany(PromotionService::class);
    }

    public function promotionProducts()
    {
        return $this->hasMany(PromotionProduct::class);
    }

    public function promotionProductExceptions()
    {
    	return $this->hasMany(PromotionProductException::class);
    }

    public function promotionServiceExceptions()
    {
    	return $this->hasMany(PromotionServiceException::class);
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    public function scopeNotActive($query)
    {
        return $query->where('is_active', false);
    }

    public function scopePermanent($query)
    {
        return $query->where('is_permanent', true);
    }

    public function scopeNotPermanent($query)
    {
        return $query->where('is_permanent', false);
    }

    public function scopeBySubsidiary($query, $subsidiary_id) 
    {
        return $query->where('subsidiary_id', $subsidiary_id);
    }


}
