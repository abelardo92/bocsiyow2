<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerPackage extends Model
{
    protected $fillable = [
        'customer_id', 'package_id', 'code', 'quantity', 'price', 'package_content_price', 'is_disabled', 'disabled_by', 'disabled_at', 'expiration_date'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }
}
