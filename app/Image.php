<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    protected $fillable = [
        'name', 'path', 'full_path',
    ];
    public function imageable()
    {
        return $this->morphTo();
    }

    public function getPathAttribute($value)
    {
        return Storage::url($value);
    }

    public function isMp3()
    {
        $arr = explode('.', $this->path);
        $ext = end($arr);
        return !!in_array($ext, ['mp3', 'mpga']);
    }
}
