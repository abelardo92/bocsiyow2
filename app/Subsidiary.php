<?php

namespace App;

use App\Product;
use App\Surveys\{Question, Survey};
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Subsidiary extends Model
{
    protected $fillable = [
        'name', 'key', 'razon_social', 'rfc', 'address', 'imprest', 'chairs_number', 'configuration_id',
        'is_active', 'is_laundry', 'discount_percentaje', 'barber_can_sell', 'related_brand', 'frequent_customers_discount',
        'diaries_limit', 'show_for_customer_diaries', 'timezone'
    ];

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    protected function setKeyAttribute($key)
    {
        $this->attributes['key'] = str_slug($key);
    }

    public function activateSubsidiary()
    {
        $this->is_active = true;
        $this->save();
    }

    public function deactivateSubsidiary()
    {
        $this->is_active = false;
        $this->save();
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function laundryServices()
    {
        return $this->hasMany(LaundryService::class, 'customer_subsidiary_id', 'id');
    }

    public function originLaundryServices()
    {
        return $this->hasMany(LaundryService::class, 'subsidiary_id', 'id');
    }

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }

    public function turns()
    {
        return $this->hasMany(Turn::class);
    }

    public function entries()
    {
        return $this->hasMany(Entry::class);
    }

    public function departures()
    {
        return $this->hasMany(Departure::class);
    }

    public function requests()
    {
        return $this->hasMany(WarehouseRequest::class);
    }

    public function adjustments()
    {
        return $this->hasMany(Adjustment::class);
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class);
    }

    public function attendances()
    {
        return $this->hasMany(Attendance::class);
    }

    public function cashRegisters()
    {
        return $this->hasMany(CashRegister::class);
    }

    public function cashRegistersByDate($date)
    {
        return $this->hasMany(CashRegister::class)->where('date', $date);
    }

    public function cashRegistersByDates($start, $end)
    {
        return $this->hasMany(CashRegister::class)->whereBetween('date', [$start, $end]);
    }

    public function sales()
    {
        return $this->hasMany(Sale::class);
    }

    public function todaySales()
    {
        $today = Carbon::now()->format('Y-m-d');
        return $this->hasMany(Sale::class)->where('date', $today)->finished()->notCanceled();
    }

    public function finishedSalesByDates($start, $end)
    {
        return $this->hasMany(Sale::class)->whereBetween('date', [$start, $end])->finished()->notCanceled();
    }

    public function services()
    {
        return $this->hasMany(Service::class);
    }

    public function diaries()
    {
        return $this->hasMany(Diary::class);
    }

    public function devices()
    {
        return $this->hasMany(Device::class);
    }

    public function miniCutPetitions()
    {
        return $this->hasMany(MiniCutPetition::class);
    }

    public function configuration()
    {
        return $this->belongsTo(Configuration::class);
    }

    public function eats()
    {
        return $this->hasMany(Eat::class);
    }

    public function surveys()
    {
        return $this->hasMany(Survey::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function expenditures()
    {
        return $this->hasMany(Expenditure::class);
    }

    public function laundrySalesTotalByRange($start,$end)
    {
        $subtotal = 0;
        if($this->is_laundry) {
            $cash_registers = $this->cashRegisters
            ->filter(function($cash_register) use ($start, $end)
            {
                return $cash_register->date >= $start && $cash_register->date <= $end;
            });
            //->whereDate('date','=>', $start)->whereDate('date','<=', $end);

            foreach ($cash_registers as $cr) {
                $subtotal += $cr->totalFromOtherCashRegisters();
            }
        }
        return $subtotal;
    }

    public function getTranscurredTimeAverage()
    {
        $now = Carbon::now();
        $currentDay = $now->format('l');

        // get start and end of week
        if($currentDay == "Saturday" || !$currentDay == "Sunday") {
            $start = Carbon::parse("last monday");
            $end = Carbon::parse("last friday");
        } else {
            $start = Carbon::now()->modify('-7 days')->modify('monday this week');
            $end = Carbon::now()->modify('-7 days') ->modify('friday this week');
        }

        // get seconds average of sales
        $sales = $this->finishedSalesByDates($start, $end)->with('services')->get();
        $transcurredTime = 0;
        $salesTotal = 0;
        foreach($sales as $sale) {
            if($sale->services && !($sale->services->count() == 1 && $sale->services->first()->service_id == 19)) {
                $transcurredTime += $sale->transcurred_time;
                $salesTotal += 1;
            }
        }
        $secondsAverage = $salesTotal > 0 ? (int)($transcurredTime / $salesTotal) : 0;

        // format seconds in minutes - seconds
        $minutes = (int)($secondsAverage / 60);
        $seconds = $secondsAverage - ($minutes * 60);
        $seconds = $seconds < 10 ? "0" . $seconds : $seconds;
        $time = $minutes . ":" . $seconds;
        return $time;
    }

    public function getEntriesFolio()
    {
        if($lastEntrie = $this->entries->last()){
            return $lastEntrie->folio + 1;
        }
        return 1;
    }

    public function getDeparturesFolio()
    {
        if($lastDeparture = $this->departures->last()){
            return $lastDeparture->folio + 1;
        }
        return 1;
    }

    public function getAdjustmentsFolio()
    {
        if($lastAdjustment = $this->adjustments->last()){
            return $lastAdjustment->folio + 1;
        }
        return 1;
    }

    public function products()
    {
        return $this->belongsToMany(Product::class)->where('active', true)->withPivot('existence');
    }

    public function articles()
    {
        return $this->belongsToMany(Article::class)->withPivot('existence');
    }

    public function getServices($start, $end)
    {
        $sale_ids = $this->sales()->whereBetween('date', [$start, $end])
            ->finished()->notCanceled()->get()->pluck('id');
        return SaleService::whereIn('sale_id', $sale_ids)->notCanceled()->notWaiting()->get();
    }

    public function getServicesReport($start, $end)
    {
        $sale_ids = $this->sales()->whereBetween('date', [$start, $end])
            ->finished()->notCanceled()->get()->pluck('id');;
        $sale_services = SaleService::with('sale')->whereIn('sale_id', $sale_ids)->notCanceled()->notWaiting()->get();

        $report = [];
        foreach($sale_services as $sale_service) {
            $minutes = $sale_service->sale->attended_at->diffInMinutes($sale_service->sale->finished_at);
            $quantity = $sale_service->qty;
            $service_id = $sale_service->service_id;
            if(!isset($report[$service_id])) {
                $report[$service_id] = [
                    'name' => $sale_service->service->name,
                    'minutes' => $minutes,
                    'quantity' => $quantity,
                ];
            } else {
                $report[$service_id]['minutes'] += $minutes;
                $report[$service_id]['quantity'] += $quantity;
            }
        }
        sort($report);

        foreach($report as &$service_report) {
            $average = $service_report['minutes'] / $service_report['quantity'];
            $service_report['average'] = number_format($average, 2, '.', '');
        }
        return $report;
    }

    public function getProductsReport($start, $end)
    {
        $sale_ids = $this->sales()
            ->whereBetween('date', [$start, $end])
            ->finished()->notCanceled()->get()->pluck('id');
        return SaleProduct::select(DB::raw('*, count(*) as products_count'))->whereIn('sale_id', $sale_ids)->notCanceled()->groupBy('product_id')->get();
    }

    public function getServicesNumber($start, $end)
    {
        $services = $this->getServices($start, $end);
        $services_count = 0;
        foreach($services as $service) {
            $services_count += $service->qty;
        }
        return $services_count;
    }

    public function getServicesToday()
    {
        if($this->todaySales) {
            $services_count = 0;
            foreach($this->todaySales as $sale) {
                foreach($sale->services as $service) {
                    // not waiting
                    if($service->service_id != 19) {
                        $services_count += $service->qty;
                    }
                }
            }
            return $services_count;
        }

        $today = Carbon::now()->format('Y-m-d');
        $sale_ids = $this->sales()->where('date', $today)
        ->finished()->notCanceled()->get()->pluck('id');
        return SaleService::select(DB::raw('*, SUM(qty) as services_count'))->whereIn('sale_id', $sale_ids)->notCanceled()->notWaiting()->groupBy('service_id')->get();
    }

    public function getAttendedTimeAverage($start, $end)
    {
        $time = [];
        foreach ($this->finishedSalesByDates($start, $end)->with('services')->get() as $sale) {
            $time[] = $sale->attended_at->diffInMinutes($sale->finished_at);
        }
        if (count($time) == 0) {
            return 0;
        }
        return number_format((array_sum($time) / count($time)), 2, '.', '');
    }

    public function getWaitTimeAverage($start, $end)
    {
        $time = [];
        foreach ($this->finishedSalesByDates($start, $end)->with('services')->get() as $sale) {
            $time[] = $sale->created_at->diffInMinutes($sale->attended_at);
        }
        if (count($time) == 0) {
            return 0;
        }
        return number_format((array_sum($time) / count($time)), 2, '.', '');
    }

    public function isBocsiyow() {
        return $this->related_brand == 1;
    }

    public function isMerida() {
        return $this->id == 14;
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    public function scopeForCustomerDiaries($query)
    {
        return $query->where('show_for_customer_diaries', true);
    }

    public function scopeLaundry($query)
    {
        return $query->where('is_laundry', true);
    }

    public function scopeNotLaundry($query)
    {
        return $query->where('is_laundry', false);
    }

    public function scopeByKey($query, $key)
    {
        return $query->where('key', $key);
    }

    public function servicePrices()
    {
        return $this->hasMany(SubsidiaryServicePrice::class);
    }
}
