<?php

namespace App\Providers;

use App;
use App\Employee;
use App\Subsidiary;
use Bouncer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        ini_set('max_input_vars', 1000000);
        ini_set('memory_limit', '512M');
        setLocale(LC_ALL, 'es_ES.UTF-8', 'spanish');
        Bouncer::seeder(function () {
            Bouncer::allow('super-admin')->to('all');
            Bouncer::allow('manager')->to('all-sucursale');
            Bouncer::allow('cashier')->to('sell');
            Bouncer::allow('barber')->to('cut-hair');
            Bouncer::allow('agenda')->to('agend');
            Bouncer::allow('horarios')->to('horario');
            Bouncer::allow('employee')->to('employee');
            Bouncer::allow('nomina')->to('nomina');
            Bouncer::allow('accountant')->to('accountant');
            Bouncer::allow('supervisor')->to('supervisor');
            Bouncer::allow('subsidiary-admin')->to('subsidiary-admin');
            Bouncer::allow('cashier-admin')->to('cashier-admin');
            Bouncer::allow('super-admin-restringido')->to('all');
        });
        $bool = false;
        if (isset($_SERVER['HTTP_HOST'])) {
            $arr = explode(".", $_SERVER['HTTP_HOST']);
            $domain = array_shift($arr);
            $bool = true;
            if (in_array($domain, ['bocsiyow', 'bocsiyow2','bocsiyowbarbershop', 'bocsiyowbarbers', 'www', 'bocsi'])) {
                $bool = false;
            }
        }

        View::share('subdomain', $bool);
        $barbers = collect([]);
        if ($bool) {
            $subsidiary = Subsidiary::whereKey($domain)->firstOrFail();
            View::share('current_subsidiary', $subsidiary);
            $barbers = $this->getBarbers($subsidiary);
        }
        View::share('schedules_barbers', $barbers);

        if (!ini_get("auto_detect_line_endings")) {
            ini_set("auto_detect_line_endings", '1');
        }
    }

    protected function getBarbers($subsidiary)
    {
        return Employee::barbers()->active()->get();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (App::environment('staging', 'production')) {
            $this->app->alias('bugsnag.logger', \Illuminate\Contracts\Logging\Log::class);
            $this->app->alias('bugsnag.logger', \Psr\Log\LoggerInterface::class);
        }
    }
}
