<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryArticle extends Model
{
    protected $fillable = [
        'article_id', 'subsidiary_id', 'type', 'qty', 'kardex_type', 'folio', 'concepto', 'current_existence', 'real_existence'
    ];

    public function scopeEntries($query)
    {
        return $query->where('kardex_type', 'entrada');
    }

    public function scopeOuts($query)
    {
        return $query->where('kardex_type', 'salida');
    }

    public function scopeAdjusts($query)
    {
        return $query->where('kardex_type', 'ajuste');
    }
    
    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    public static function getFolio($kardex_type)
    {
        if ((new static)->where('kardex_type', $kardex_type)->count() == 0) {
            return 1;
        }
        return (new static)->where('kardex_type', $kardex_type)->orderBy('folio')->first()->folio + 1;
    }
}