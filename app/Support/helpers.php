<?php

if (!function_exists('startltqend')) {
    function startltqend($start, $end)
    {
        /*
        $start_parse = explode(':', $start);
        $end_parse = explode(':', $end);

        $start = "{$start_parse[0]}:$start_parse[1]:00";
        $minute_end = $end_parse[1];
        $end = \Carbon\Carbon::parse("{$end_parse[0]}:{$minute_end}:00")->addMinutes(16)->format('H:i:s');
        */
        return $start <= $end;
    }
}