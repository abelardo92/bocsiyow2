<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class DiariesException extends Model
{
    protected $fillable = [
        'active', 'subsidiary_id','monday','monday_from_time','monday_to_time','tuesday','tuesday_from_time',
        'tuesday_to_time','wednesday','wednesday_from_time','wednesday_to_time','thursday','thursday_from_time',
        'thursday_to_time','friday','friday_from_time','friday_to_time','saturday','saturday_from_time',
        'saturday_to_time','sunday','sunday_from_time','sunday_to_time'
    ];

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function isTimeInRange($date, $time)
    {
        $current_day = strtolower(Carbon::parse($date)->format('l'));
        return $this->{"{$current_day}_from_time"} <= $time && $this->{"{$current_day}_to_time"} >= $time;
    }

    public function scopeBySubsidiary($query, $subsidiary_id)
    {
        return $query->where('subsidiary_id', $subsidiary_id);
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeDayActive($query, $date) {

        $current_day = strtolower(Carbon::parse($date)->format('l'));
        return $query->where($current_day, true);
    }

    public function scopeCurrentDay($query)
    {
        $current_day = strtolower(date('l'));
        return $query->where($current_day, true);
    }

    public function scopeCurrentTimeInRange($query)
    {
        $current_time = Carbon::now()->format('H:i:s');
        $current_day = strtolower(date('l'));

        return $query->where("{$current_day}_from_time", '<=', $current_time)
        ->where("{$current_day}_to_time", '>=', $current_time);
    }
}