<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleFacturaRequest extends Model
{
    protected $fillable = [
        'subsidiary_id', 'folio', 'folio_date', 'subtotal', 'rfc', 'full_name', 'postal_code', 'road_name',
        'external_number', 'internal_number', 'suburb_name', 'locality_name', 'municipality_name', 'state_id', 'between_street_1',
        'between_street_2', 'factura_email', 'phone', 'payment_type', 'cfdi_use_id', 'tax_regime_id', 'status', 'sale_id',
        'is_active',
    ];

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function sale()
    {
        return $this->belongsTo(Sale::class);
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function cfdiUse()
    {
        return $this->belongsTo(CfdiUse::class);
    }

    public function taxRegime()
    {
        return $this->belongsTo(TaxRegime::class);
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }
}
