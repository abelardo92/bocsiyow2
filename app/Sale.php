<?php

namespace App;

use App\Surveys\Survey;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = [
        'customer_id', 'subsidiary_id', 'employee_id', 'turn_id',
        'exchange_rate_id', 'subtotal', 'tip', 'total', 'status',
        'payment_gateway', 'date', 'attend', 'attended_at', 'finish',
        'finished_at', 'pay_at', 'card_brand', 'last_four', 'coin', 'money_change', 'qty', 
        'diary_id', 'cash_register_id', 'folio', 'tip_in', 'repaired_by', 'comments', 'has_discount', 
        'has_birthday', 'kids_promotion', 'employee2_id', 'suggested_employee_id', 'customer2', 
        'delivered_by', 'customer_subsidiary_id', 'is_courtesy', 'finished_cash_register_id', 
        'is_promo_ut', 'canceled_description', 'authorized_by', 'discount_percentaje', 'promotion_id',
        'frequent_customers_discount', 'is_for_employee', 'employee_sale_comments',
    ];

    protected $dates = ['attended_at', 'finished_at', 'pay_at', 'canceled_at', 'repaired_at'];
    protected $appends = array('transcurred_time');

    public function getTranscurredTimeAttribute()
    {
        if($this->status == 'atendiendo') {
            $now = Carbon::now();
            $difference = $now->diffInSeconds($this->attended_at);
            return $difference;
        }
        if($this->status == 'finalizado' || $this->status == 'paid') {

            $difference = $this->finished_at ? $this->finished_at->diffInSeconds($this->attended_at) : 0;
            return $difference;
        }
        return 0;
    }

    public function getTranscurredTimeFormatted() {
        // format seconds in minutes - seconds
        $minutes = (int)($this->transcurred_time / 60);
        $seconds = $this->transcurred_time - ($minutes * 60);
        $seconds = $seconds < 10 ? "0" . $seconds : $seconds;  
        $time = $minutes . ":" . $seconds;
        return $time;
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function customerSubsidiary()
    {
        return $this->belongsTo(Subsidiary::class, 'customer_subsidiary_id', 'id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function employee2()
    {
        return $this->belongsTo(Employee::class, 'employee2_id', 'id');
    }

    public function suggestedEmployee()
    {
        return $this->belongsTo(Employee::class, 'suggested_employee_id', 'id');
    }

    public function repaired()
    {
        return $this->belongsTo(Employee::class, 'repaired_by');
    }

    public function canceler()
    {
        return $this->belongsTo(User::class, 'canceled_by');
    }

    public function authorized()
    {
        return $this->belongsTo(User::class, 'authorized_by');
    }

    public function delivered()
    {
        return $this->belongsTo(User::class);   
    }

    public function turn()
    {
        return $this->belongsTo(Turn::class);
    }

    public function exchangeRate()
    {
        return $this->belongsTo(ExchangeRate::class);
    }

    public function services()
    {
        return $this->hasMany(SaleService::class)->notCanceled();
    }

    public function canceledServices()
    {
        return $this->hasMany(SaleService::class)->canceled();
    }

    public function allServices()
    {
        return $this->hasMany(SaleService::class);
    }

    public function servicesWithoutWaiting()
    {
        return $this->hasMany(SaleService::class)->notCanceled()->where('service_id', '!=', 19);
    }

    public function products()
    {
        return $this->hasMany(SaleProduct::class)->notCanceled();
    }

    public function canceledProducts()
    {
        return $this->hasMany(SaleProduct::class)->canceled();
    }

    public function allProducts()
    {
        return $this->hasMany(SaleProduct::class);
    }

    public function packages()
    {
        return $this->hasMany(SalePackage::class);
    }    

    public function diary()
    {
        return $this->belongsTo(Diary::class);
    }

    public function promotion()
    {
        return $this->belongsTo(Promotion::class);
    }

    public function cashRegister()
    {
        return $this->belongsTo(CashRegister::class, 'cash_register_id', 'id');
    }

    public function finishedCashRegister()
    {
        return $this->belongsTo(CashRegister::class, 'finished_cash_register_id', 'id');
    }

    public function payments()
    {
        return $this->hasMany(SalePayment::class);
    }

    public function survey()
    {
        return $this->hasOne(Survey::class);
    }

    public function scopeByRange($query, $start, $end)
    {
        return $query->whereBetween('date', [$start, $end]);
    }

    public function scopeWithPromotion($query)
    {
        return $query->where('has_birthday', true)->orWhere('payment_gateway', 'Monedero')->orWhere('is_courtesy', true)->orWhere('is_promo_ut', true);
        /*
        return $query->where('has_birthday', true)
        ->orWhereHas('payments', function($q) {
            $q->where('method', 'Monedero');
        });
        */
    }

    public function scopeToday($query)
    {
        $today = Carbon::now()->format('Y-m-d');
        return $query->where('date', $today);
    }

    public function scopeFinishedOnSameCashRegister($query)
    {
        return $query->whereRaw('cash_register_id = finished_cash_register_id');
    }

    public function scopeAdditionalSalesByCashRegister($query,$cash_register_id, $excluded_sale_ids = null)
    {
        $sales = $this->finished()->notCanceled()->where('cash_register_id', '!=', $cash_register_id);
        if($excluded_sale_ids != null) {
            $sales = $sales->whereNotIn('id', $excluded_sale_ids);
        }
        
        $sales = $sales->whereHas('payments', function($q) use ($cash_register_id) {
            $q->where('cash_register_id', $cash_register_id);
        });
        return $sales;
    }

    public function scopeForCustomers($query) {
        return $query->where('is_for_employee', false);
    }

    public function scopeForEmployees($query) {
        return $query->where('is_for_employee', true);
    }

    public function scopeBeforeToday($query)
    {
        $today = Carbon::now()->format('Y-m-d');
        return $query->whereDate('date','<', $today);
    }

    public function scopeActive($query)
    {
        return $query->where('paid', false);
    }

    public function scopeFinish($query, $bool = true)
    {
        return $query->where('finish', $bool);
    }

    public function scopeFinished($query)
    {
        return $query->where('paid', true)->where('finish', true);
    }

     public function scopeNotFinished($query)
    {
        return $query->where('paid', false)->orWhere('finish', false);
    }

    public function scopeEfectivo($query)
    {
        return $query->where('payment_gateway', 'Efectivo');
    }

    public function scopeCard($query)
    {
        return $query->where('payment_gateway', 'Tarjeta');
    }

    public function scopeStatus($query, $status = 'en espera')
    {
        return $query->where('status', $status);
    }

    public function scopeNotCanceled($query)
    {
        return $query->whereNull('canceled_by');
    }

    public function scopeCanceled($query)
    {
        return $query->whereNotNull('canceled_by');
    }

    public function scopeRepaireds($query)
    {
        return $query->whereNotNull('repaired_by');
    }

    public function scopeCourtesy($query)
    {
        return $query->where('is_courtesy', true);
    }

    public function hasTwoBarbers()
    {
        return $this->employee2_id != null && $this->employee_id != $this->employee2_id;
    }

    public function isBocsiBarber() 
    {
        return $this->employee_id == 46;
    }

    public function isSunday()
    {
        return date('w', strtotime($this->date)) == 0;
    }

    public function scopeBocsiBarber($query)
    {
        return $query->where('employee_id', 46);
    }

    public function containsHaircutAndKids()
    {
        $haircut = $this->services->where('service_id', 1)->first();
        $kids = $this->services->where('service_id', 16)->first();
        return $haircut && $kids;
    }

    public function containsHairCut()
    {
        $haircut = $this->services->where('service_id', 1)->first();
        return $haircut;
    }

    public function customerNotCreatedToday() {
        $days_diff = 0;
        if($this->customer) {
            $created = new Carbon($this->customer->created_at);
            $now = Carbon::now();
            $days_diff = $created->startOfDay()->diff($now)->days;
        }
        return $days_diff >= 1;
    }

    public function hasBirthday() {
        return $this->customerNotCreatedToday() && $this->has_birthday;
    }

    public function hasFrequentCustomerDiscount() {
        return $this->frequent_customers_discount == 1 && $this->customer_id && 
        $this->customer_id != 7 && ($this->customerNotCreatedToday() || $this->has_discount);
    }

    public function updateTotals()
    {
        $total = 0;
        $wash_quantity = 0;
        if($this->subsidiary != null && $this->subsidiary->is_laundry) {
            $wash_quantity = $this->washQuantity();
        }
        foreach ($this->services as $sale_service) {
            if($this->subsidiary != null && $this->subsidiary->is_laundry 
                && $sale_service->service_id == 23 && $wash_quantity <= 5) {
                $total += 5 * $sale_service->price;
            } else {
                $total += $sale_service->qty * $sale_service->price;
            }
        }
        foreach ($this->products as $sale_product) {
            $total += $sale_product->qty * $sale_product->price;
        }
        foreach ($this->packages as $sale_package) {
            $total += $sale_package->quantity * $sale_package->price;
        }
        $this->subtotal = $total;
        $this->total = $this->subtotal + $this->tip;
        $this->save();
    }

    public function washQuantity()
    {
        $total = 0;
        foreach ($this->services as $sale_service) {
            if($sale_service->service_id == 23) {
                $total += $sale_service->qty;
            }
        }

        foreach ($this->packages as $sale_package) {
            foreach ($sale_package->services as $package_service) {
                if($package_service->service_id == 23) {
                    $total += $package_service->quantity * count($sale_package->services);
                }
            }
        }
        return $total;
    }

    public function totalByCashRegister($cash_register_id)
    {
        if( !$this->subsidiary->is_laundry || 
            ($this->cash_register_id == $cash_register_id && $this->finished_cash_register_id == $cash_register_id)) {
            return $this->total;
        } 
        $total = $this->payments->where('cash_register_id', $cash_register_id)->sum('total');
        $total_different = $this->payments->where('cash_register_id','!=', $cash_register_id)->sum('total');

        if($this->finished_cash_register_id != null && $this->finished_cash_register_id == $cash_register_id) {
            $total -= $this->money_change;
        } else
        if($this->finished_cash_register_id != null && $this->finished_cash_register_id != $cash_register_id) {
            $total = $this->total - $total_different;
        }

        return $total;    
    }

    public function totalByOriginCashRegister()
    {
        return $this->totalByCashRegister($this->cash_register_id);
    }

    public function paidAmount()
    {
        $total = $this->payments->where('cash_register_id', $this->cash_register_id)->sum('total');
        return $total;
    }

    public function markAsPaid($barber_id = '', $barber2_id = '')
    {
        if (!$this->attend) {
            $this->attend = true;
            $this->attended_at = Carbon::now();
        }
        if (!$this->finish) {
            $this->finish = true;
            $this->finished_at = Carbon::now();
        }
        if (!empty($barber_id)) {
            $this->employee_id = $barber_id;
        }
        if (!empty($barber2_id)) {
            $this->employee2_id = $barber2_id;
        }
        $this->status = 'pagado';
        $this->paid = true;
        $this->pay_at = Carbon::now();
        $this->save();
    }

    public function markAsAttend($barber_id = '', $barber2_id = '')
    {
        $this->status = 'atendiendo';
        $this->attend = true;
        $this->attended_at = Carbon::now();
        if (!empty($barber_id)) {
            $this->employee_id = $barber_id;
        }
        if (!empty($barber2_id)) {
            $this->employee2_id = $barber2_id;
        }
        $this->save();
    }

    public function markAsFinish($barber_id = '', $barber2_id = '')
    {
        $this->status = 'finalizado';
        $this->finish = true;
        $this->finished_at = Carbon::now();
        if (!empty($barber_id)) {
            $this->employee_id = $barber_id;
        }
        if (!empty($barber2_id)) {
            $this->employee2_id = $barber2_id;
        }
        $this->save();
    }

    public function servicesTotalPrice()
    {
        $total = 0;
        foreach ($this->services as $service) {
            $total += $service->qty * $service->price;
        }
        return $total;
    }

    public function servicesMaxPrice()
    {
        $serives_price_arr = [];
        foreach ($this->services->where('include_in_birthday_discount', true) as $service) {
            $serives_price_arr[] = $service->price;
        }
        return max($serives_price_arr);
    }

    public function scopeDates($query, array $dates)
    {
        return $query->whereBetween('date', $dates);
    }

    public function getKidsPromotionService()
    {
        foreach ($this->services as $service) {
            if ($service->service->kids_promotion) {
                return $service;
            }
        }
    }

    public function setOrder()
    {
        if($this->status == 'en espera') {
            $this->{"order"} = 1;
        } else if($this->status == 'atendiendo') {
            $this->{"order"} = 2;
        } else if($this->status == 'finalizado') {
            $this->{"order"} = 3;
        } else if($this->status == 'paid') {
            $this->{"order"} = 4;
        } else {
            $this->{"order"} = 5;
        }
    }
}
