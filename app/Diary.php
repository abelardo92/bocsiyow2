<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Diary extends Model
{
    use Notifiable;
    protected $fillable = [
        'date', 'time', 'customer_id', 'subsidiary_id', 'employee_id', 'confirmation_code', 'folio', 
        'user_id', 'cashier_id', 'has_discount', 'comments', 'service_id', 'customer_name', 'phone',
        'email', 'suggested_customer_id', 'created_by_customer'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function cashier()
    {
        return $this->belongsTo(Employee::class, 'cashier_id');
    }

    public function sale()
    {
        return $this->hasOne(Sale::class);
    }

    public function routeNotificationForNexmo()
    {
        return '52'.($this->customer ? $this->customer->phone : $this->phone);
    }

    public function scopeToday($query)
    {
        return $query->where('date', Carbon::now()->format('Y-m-d'));
    }

    public function scopeAfterCurrentTime($query)
    {
        $current = Carbon::now()->subMinutes(15);
        return $query->whereTime('time', '>=', $current->format('H:i:s'));   
    }

    public function scopeDate($query, $date)
    {
        return $query->where('date', $date);
    }

    public function scopeActive($query)
    {
        return $query->where('attend', false);
    }

    public function scopeIsAttend($query)
    {
        return $query->where('attend', true);
    }

    public function scopeCancel($query)
    {
        return $query->where('canceled', true);
    }

    public function scopeNotCancel($query)
    {
        return $query->where('canceled', false);
    }

    public function scopeRead($query, $bool = true)
    {
        return $query->where('read', $bool);
    }

    public function scopeDates($query, array $dates)
    {
        return $query->whereBetween('date', $dates);
    }

    public function scopeCallCenter($query)
    {
        return $query->where('has_discount',true);
    }

    public function scopeBySubsidiary($query, $subsidiary_id)
    {
        return $query->where('subsidiary_id', $subsidiary_id);
    }

    public function scopeFromNow($query)
    {
        return $query->where('date', '>=' , Carbon::now()->format('Y-m-d'));
    }
}
