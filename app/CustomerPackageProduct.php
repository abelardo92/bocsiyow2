<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerPackageProduct extends Model
{
    protected $fillable = ['customer_package_id', 'product_id', 'quantity', 'price'];

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function customerPackage()
    {
        return $this->belongsTo(SalePackage::class);
    }
}
