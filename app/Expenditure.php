<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expenditure extends Model
{
    protected $fillable = [
        'subsidiary_id', 'account_id', 'date', 'description', 'total', 'warehouse_movement_id'
    ];

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function movement()
    {
        return $this->belongsTo(WarehouseMovement::class, 'warehouse_movement_id', 'id');
    }

    public function scopeDates($query, array $dates)
    {
        return $query->whereBetween('date', $dates);
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
