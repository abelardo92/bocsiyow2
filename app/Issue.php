<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{
    protected $fillable = [
        'employee_id', 'area', 'date', 'action', 'origin_user_id', 'origin_subsidiary_id', 
        'destiny_subsidiary_id', 'subject', 'opened_by', 'related_issue_id', 'type', 'destiny_user_id'
    ];

    protected $enumTypes = [
        1 => 'Incidencia',
        2 => 'Mensaje (Caja)',
        3 => 'Mensaje (Empleados)'
    ];

    public function scopeIncidences($query)
    {
        return $query->where('type', 1);
    }

    public function scopeCashierMessages($query)
    {
        return $query->where('type', 2);
    }

    public function scopeEmployeeMessages($query)
    {
        return $query->where('type', 3);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function originUser()
    {
        return $this->belongsTo(User::class, 'origin_user_id', 'id');
    }

    public function openedBy()
    {
        return $this->belongsTo(User::class, 'opened_by', 'id');
    }

    public function relatedIssue()
    {
        return $this->belongsTo(Issue::class, 'related_issue_id', 'id');
    }

    public function originSubsidiary()
    {
        return $this->belongsTo(Subsidiary::class, 'origin_subsidiary_id', 'id');
    }

    public function destinySubsidiary()
    {
        return $this->belongsTo(Subsidiary::class, 'destiny_subsidiary_id', 'id');
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function scopeDates($query, array $dates)
    {
        return $query->whereBetween('date', $dates);
    }

    public function scopeUnreadedCashierMessagesBySubsidiary($query, $subsidiary_id, $user)
    {
        return $query->where('destiny_subsidiary_id', $subsidiary_id)->where('origin_user_id', '!=', $user->id)->where('opened_by', null)->cashierMessages();
    }

    public function scopeUnreadedCashierMessagesByAdmon($query, $user)
    {
        if($user->isA('subsidiary-admin')) {
            return $this->scopeUnreadedCashierMessagesBySubsidiary($query, $user->access_subsidiary_id, $user)->cashierMessages();
        } 
        return $this->scopeUnreadedCashierMessagesBySubsidiary($query, 7, $user)->cashierMessages();
    }

    public function scopeUnreadedEmployeeMessagesBySubsidiary($query, $subsidiary_id, $user)
    {
        return $query->where('destiny_subsidiary_id', $subsidiary_id)->where('origin_user_id', '!=', $user->id)->where('opened_by', null)->employeeMessages();
    }

    public function scopeUnreadedEmployeeMessagesByAdmon($query, $user)
    {
        if($user->isA('subsidiary-admin')) {
            return $this->scopeUnreadedCashierMessagesBySubsidiary($query, $user->access_subsidiary_id, $user)->cashierMessages();
        }
        return $this->scopeUnreadedEmployeeMessagesBySubsidiary($query, 7, $user)->employeeMessages();
    }

}
