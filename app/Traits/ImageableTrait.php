<?php namespace App\Traits;

use App\Image;

trait ImageableTrait
{
    protected function CreateFile($model, $request, $path)
    {
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            
            if($image2 = $model->image()->get()->first()){
                $image2->path = $file->store("{$path}", 'public');
                $image2->name = $file->getClientOriginalName();
                $image2->save();
            } else {
                $image = Image::create([
                    'name' => $file->getClientOriginalName(),
                    'path' => $file->store("{$path}", 'public'),
                ]);
                $model->image()->save($image);
            }
        }
    }

    protected function CreateFiles($model, $request, $path)
    {
        if ($request->hasFile('image')) {
            $files = $request->file('image');
            foreach ($files as $file) {
                $image = Image::create([
                    'name' => $file->getClientOriginalName(),
                    'path' => $file->store("{$path}", 'public'),
                ]);
                $model->images()->save($image);
            }
        }
    }
}
