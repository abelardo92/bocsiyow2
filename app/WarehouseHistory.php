<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseHistory extends Model
{
    protected $fillable = [
        'name', 'date', 'is_active'
    ];

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    public function scopeNotActive($query)
    {
        return $query->where('is_active', false);
    }
}