<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleProduct extends Model
{
    protected $fillable = ['qty', 'product_id', 'sale_id', 'price', 'original_price', 'cost', 'canceled_at', 'canceled_by', 'canceled_description', 'discount_percentaje'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function sale()
    {
        return $this->belongsTo(Sale::class);
    }

    public function canceledBy()
    {
        return $this->belongsTo(User::class, 'canceled_by', 'id');
    }

    public function scopeNotCanceled($query)
    {
        return $query->whereNull('canceled_by');
    }

    public function scopeCanceled($query)
    {
        return $query->whereNotNull('canceled_by');
    }
}