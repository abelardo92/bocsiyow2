<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseMovementProduct extends Model
{
    protected $fillable = [
        'warehouse_movement_id', 'warehouse_product_id', 'quantity', 'amount', 'provider_price' 
    ];

    public function movement()
    {
        return $this->belongsTo(WarehouseMovement::class, 'warehouse_movement_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(WarehouseProduct::class, 'warehouse_product_id', 'id');
    }
}
