<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departure extends Model
{
    protected $fillable = ['qty', 'user_id', 'product_id', 'concept_id', 'folio', 'sale_id'];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function concept()
    {
        return $this->belongsTo(Concept::class);
    }

    public function sale()
    {
        return $this->belongsTo(Sale::class);
    }
}
