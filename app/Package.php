<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = [
        'name', 'price', 'subsidiary_id', 'is_laundry'
    ];

    public function packageServices()
    {
        return $this->hasMany(PackageService::class);
    }

    public function packageProducts()
    {
        return $this->hasMany(PackageProduct::class);
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function scopeLaundry($query)
    {
        return $query->where('is_laundry', true);
    }

    public function scopeNotLaundry($query)
    {
        return $query->where('is_laundry', false);
    }

    public function contentPrice()
    {
    	$totalServices = 0;
    	$totalProducts = 0;
    	
    	$packageServices = $this->packageServices();
    	$packageProducts = $this->packageProducts();
    	foreach($packageServices as $packageService){
    		$totalServices += $packageService->service()->total;
    	}
    	foreach($packageProducts as $packageProduct){
    		$totalProducts += $packageProduct->product()->total;
    	}
    	return $totalServices + $totalProducts;
    }

    public function containsProduct($id){
        $product_ids = $this->packageProducts->pluck('product_id')->toArray();
        return in_array($id, $product_ids);
    }

    public function containsService($id){
        $service_ids = $this->packageServices->pluck('service_id')->toArray();
        return in_array($id, $service_ids);
    }
}
