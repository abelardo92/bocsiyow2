<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class LaundryService extends Model
{
    protected $fillable = [
        'subsidiary_id', 'customer_subsidiary_id', 'amount', 'date', 'folio', 'cash_register_id', 'canceled_at', 'canceled_by', 'customer_name' 
    ];

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class, 'subsidiary_id', 'id');
    }

    public function customerSubsidiary()
    {
        return $this->belongsTo(Subsidiary::class, 'customer_subsidiary_id', 'id');
    }

    public function cashRegister()
    {
        return $this->belongsTo(CashRegister::class);
    }

    public function scopeToday($query)
    {
        $today = Carbon::now()->format('Y-m-d');
        return $query->where('date', $today);
    }

    public function canceler()
    {
        return $this->belongsTo(User::class, 'canceled_by');
    }

    public function scopeNotCanceled($query)
    {
        return $query->whereNull('canceled_by');
    }
}
