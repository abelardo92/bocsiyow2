<?php

namespace App\Events;

use App\Sale;
use App\Subsidiary;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;

class AcceptSurvey implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;

    public $sale;

    public $subsidiary;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Sale $sale, Subsidiary $subsidiary)
    {
        $this->sale = $sale;
        $this->subsidiary = $subsidiary;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel("survey.{$this->subsidiary->id}.answered.{$this->sale->id}");
    }

    public function broadcastWith()
    {
        return [
            'sale_id' => $this->sale->id,
            'customer_name' => trim($this->sale->customer->name),
            'subsidiary_id' => $this->subsidiary->id,
        ];
    }

     public function failed(Exception $exception)
    {
        \Log::error($exception->getMessage());
    }
}
