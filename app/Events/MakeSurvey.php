<?php

namespace App\Events;

use App\Sale;
use App\Subsidiary;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;

class MakeSurvey implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;

    public $sale;

    public $subsidiary;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Sale $sale, Subsidiary $subsidiary)
    {
        $this->sale = $sale;
        $this->subsidiary = $subsidiary;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel("survey.{$this->subsidiary->key}");
    }

    public function broadcastWith()
    {
        $customer_name = $this->sale->customer->name;
        if ($this->sale->customer_id == 7) {
            $customer_name = $this->sale->comments === null ? '' : $this->sale->comments;
        }
        return [
            'sale_id' => $this->sale->id,
            'customer_name' => trim($customer_name),
            'subsidiary_id' => $this->subsidiary->id,
        ];
    }

     public function failed(Exception $exception)
    {
        \Log::error($exception->getMessage());
    }
}
