<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Product extends Model
{
    protected $fillable = [
        'key', 'name', 'stock_max', 'stock_min', 'sell_price', 'buy_price', 'commission', 'has_commission', 
        'is_laundry', 'include_in_reports', 'is_for_employee', 'employee_discount_percentaje',
    ];

    //protected $appends = ['current_price'];
    /*
    public function __construct()
    {
        //dd($this);
        //$this->current_price = $this->currentPrice();
    }
    */
        

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function subsidiaries()
    {
        return $this->belongsToMany(Subsidiary::class)->withPivot('existence');
    }

    public function subsidiaryPrices()
    {
        return $this->hasMany(SubsidiaryProductPrice::class);
    }

    public function updateExistence($qty, $type = 'increment')
    {
        $this->{$type}('existence', $qty);
    }

    public function addExistence($qty)
    {
        $this->existence = $this->existence + $qty;
        $this->save();
    }

    public function subtrackExistence($qty)
    {
        $this->existence = $this->existence - $qty;
        $this->save();
    }
    
    public function scopeLaundry($query)
    {
        return $query->where('is_laundry', true);
    }

    public function scopeNotLaundry($query)
    {
        return $query->where('is_laundry', false);
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeNotActive($query)
    {
        return $query->where('active', false);
    }

    public function currentPrice($sale)
    {
        $day = Carbon::now()->format('l');
        //dd($day);
        // 60% value - 40% off...
        // if(!$sale->has_discount && ($day == 'Tuesday' || $sale->subsidiary_id == 7) && $this->id == 22) {
        //     return $this->sell_price * (0.6);  
        // }

        return $this->sell_price;
    }

    public function currentPriceBySubsidiary($subsidiary_id)
    {
        $subsidiaryPrice = $this->subsidiaryPrice($subsidiary_id);
        if($subsidiaryPrice != null){
            return $subsidiaryPrice;
        }
        return $this->sell_price;
    }

    public function subsidiaryPrice($subsidiary_id)
    {
        foreach ($this->subsidiaryPrices as $subsidiaryPrice) {
            if($subsidiaryPrice->subsidiary_id == $subsidiary_id){
                return $subsidiaryPrice->price;
            }
        }
        return null;
    }

    public function permanentPromotionByProduct() 
    {
        $promotion = Promotion::where('is_active', true)
            ->where('is_permanent', true)
            ->where(strtolower(date('l')), true)
            ->where('must_buy_specific_product', $this->id)
            ->get();
    }

    
}
