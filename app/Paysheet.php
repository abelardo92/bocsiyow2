<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paysheet extends Model
{
    protected $fillable = [
        'key', 'start', 'end', 'employee_id', 'total', 'asistencia', 'asistencia_pay',
        'puntualidad', 'puntualidad_pay', 'productividad', 'productividad_pay',
        'extra', 'extra_pay', 'total_ingreso',
        'prestamo', 'faltas', 'faltas_pay', 'uniforme', 'infonavit', 'otras',
        'total_deducciones', 'neto', 'depositado', 'diferencia', 'status', 'otros_ingresos',
        'repairs', 'discount_repairs', 'cardtips', 'products', 'excellence', 'excellence_pay', 'note',
        'sunday', 'sunday_pay'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function makeTotals()
    {
        $this->total_ingreso = array_sum([
                    $this->total,
                    $this->asistencia,
                    $this->puntualidad,
                    $this->productividad,
                    $this->extra,
                    $this->otros_ingresos,
                    $this->cardtips,
                    $this->products,
                    $this->excellence,
                    $this->repairs,
                ]);
        $this->total_deducciones = array_sum([
                    $this->prestamo,
                    $this->faltas,
                    $this->uniforme,
                    $this->infonavit,
                    $this->otras,
                    $this->discount_repairs,
                ]);
        $this->neto = $this->total_ingreso - $this->total_deducciones;
        $this->diferencia = $this->neto - $this->depositado;
        $this->save();
    }

    public function scopeByDateRange($query, $start, $end) {
        return $query->where('key', "$start-$end");
    }

}
