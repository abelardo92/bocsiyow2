<?php

namespace App\Listeners;

use App\Events\MakeSurvey;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ShowSurveyForm implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MakeSurvey  $event
     * @return void
     */
    public function handle(MakeSurvey $event)
    {
        //
    }
}
