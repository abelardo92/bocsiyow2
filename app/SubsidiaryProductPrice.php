<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubsidiaryProductPrice extends Model
{
    protected $fillable = [
        'subsidiary_id', 'product_id', 'price'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function scopeBySubsidiary($subsidiary_id)
    {
    	return $query->where('subsidiary_id', $subsidiary_id);
    }
}
