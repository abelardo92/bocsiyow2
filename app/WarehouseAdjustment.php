<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseAdjustment extends Model
{
    protected $fillable = ['qty', 'user_id', 'warehouse_product_id', 'current_existence', 'real_existence', 'folio'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(WarehouseProduct::class, 'warehouse_product_id', 'id');
    }
}
