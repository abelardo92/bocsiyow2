<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Enums;

class WarehouseHistoryProduct extends Model
{
	use Enums;

    protected $fillable = [
        'warehouse_history_id', 'warehouse_product_id', 'code', 'name', 'use', 'amount', 'accounting', 'in_storage', 'product_id', 'is_active', 'type', 'warehouse_desired_existence'
    ];

    protected $enumUses = [
        1 => 'Para servicio',
        2 => 'Aseo y limpieza',
        3 => 'Area de café',
        4 => 'Ventas',
        5 => 'Papelería',
        6 => 'Botiquin',
    ];

    protected $enumAccountings = [
        1 => 'Suministros',
        2 => 'Aseo y limpieza',
        4 => 'Ventas',
        5 => 'Papelería'
    ];

    protected $enumTypes = [
        1 => 'Barberia',
        2 => 'Lavanderia',
        3 => 'Todos'
    ];

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function warehouseProduct()
    {
        return $this->belongsTo(WarehouseProduct::class, 'product_id', 'id');
    }

    public function warehouseHistory()
    {
        return $this->belongsTo(WarehouseHistory::class, 'history_id', 'id');
    }

    public function existences()
    {
        return $this->hasMany(SubsidiaryWarehouseProduct::class);
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    public function scopeNotActive($query)
    {
        return $query->where('is_active', false);
    }

    public function initExistence()
    {
        $this->{"existence"} = "";
    }
}