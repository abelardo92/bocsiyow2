<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Traits\Enums;

class WarehouseMovement extends Model
{
    use Enums;
    
    protected $fillable = [
        'subsidiary_id', 'folio', 'date', 'type', 'warehouse_request_id', 'is_confirmed', 'confirmed_by', 'is_canceled', 'canceled_by', 'created_by', 'discount', 'provider', 'note_folio', 'is_finalized', 'finalized_by'
    ];

    protected $enumTypes = [
        1 => 'Entrada',
        2 => 'Salida',
    ];

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function request()
    {
        return $this->belongsTo(WarehouseRequest::class, 'warehouse_request_id', 'id');
    }

    public function confirmedBy()
    {
        return $this->belongsTo(User::class, 'confirmed_by', 'id');
    }

    public function canceledBy()
    {
        return $this->belongsTo(User::class, 'canceled_by', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function finalizedBy()
    {
        return $this->belongsTo(User::class, 'finalized_by', 'id');
    }

    public function products()
    {
    	return $this->hasMany(WarehouseMovementProduct::class);
    }

    public function expenditures()
    {
    	return $this->hasMany(Expenditure::class);
    }

    public function containsProduct($id) {
        $product_ids = $this->products->pluck('warehouse_product_id')->toArray();
        return in_array($id, $product_ids);
    }

    public function deleteProducts()
    {
        $expenditures = WarehouseMovementProduct::where('warehouse_movement_id', $this->id)->delete();
    }

    public function deleteExpenditures()
    {
        $expenditures = Expenditure::where('warehouse_movement_id', $this->id)->delete();
    }

    public function createExpenditures() 
    {
        $date = Carbon::now()->format('Y-m-d');
        $this->createExpendituresByDate($date);
    }

    public function createExpendituresByDate($date) 
    {
        $suministros = 0;
        $aseo = 0;
        $ventas = 0;
        $papeleria = 0;

        foreach ($this->products as $index => $product) {

            $amount = 0;
            if($this->type == 1) {
                if($product->provider_price > 0) {
                    $amount = $product->provider_price;
                } else {
                    $amount = $product->amount;    
                }
            } else {
                $amount = $product->amount;
            }
            
            switch ($product->product->accounting) {
                case 1:
                    $suministros += $amount * $product->quantity;
                break;
                case 2:
                    $aseo += $amount * $product->quantity;
                break;
                case 4:
                    $ventas += $amount * $product->quantity;
                break;
                case 5:
                    $papeleria += $amount * $product->quantity;
                break;
                default:
                    $suministros += $amount * $product->quantity;
                break;
            }
        }

        if($this->type == 1) { //entrada
            $subsidiary_id = 7; //Sucursal "Administrador"
            $prefix = "E";
            $prefix2 = "Entrada";
            if($this->discount > 0) {
                $expendidure = Expenditure::create([
                    'subsidiary_id' => $subsidiary_id,
                    'account_id' => 40,
                    'date' => $date,
                    'description' => "Descuento " . $prefix . "-" . $this->folio,
                    'total' => $this->discount * (-1),
                    'warehouse_movement_id' => $this->id 
                ]);
            }
        } else { //pedido
            $prefix = "F";
            $prefix2 = "Pedido";
            $subsidiary_id = $this->subsidiary_id;
        }

        if($suministros > 0) {
            $expendidure = Expenditure::create([
                'subsidiary_id' => $subsidiary_id,
                'account_id' => 40,
                'date' => $date,
                'description' => $prefix2 . " (suministros) " . $prefix . "-" . $this->folio,
                'total' => $suministros,
                'warehouse_movement_id' => $this->id 
            ]);
            if($this->type != 1) {
                $expendidure = Expenditure::create([
                    'subsidiary_id' => 7,
                    'account_id' => 40,
                    'date' => $date,
                    'description' => $prefix2 . " (suministros) " . $prefix . "-" . $this->folio,
                    'total' => $suministros * (-1),
                    'warehouse_movement_id' => $this->id 
                ]);
            }
        }

        if($aseo > 0) {
            $expendidure = Expenditure::create([
                'subsidiary_id' => $subsidiary_id,
                'account_id' => 51,
                'date' => $date,
                'description' => $prefix2 . " (aseo y limpieza) " . $prefix . "-" . $this->folio,
                'total' => $aseo,
                'warehouse_movement_id' => $this->id
            ]);
            if($this->type != 1) {
                $expendidure = Expenditure::create([
                    'subsidiary_id' => 7,
                    'account_id' => 51,
                    'date' => $date,
                    'description' => $prefix2 . " (aseo y limpieza) " . $prefix . "-" . $this->folio,
                    'total' => $aseo * (-1),
                    'warehouse_movement_id' => $this->id
                ]);
            }
        }

        if($ventas > 0) {
            $expendidure = Expenditure::create([
                'subsidiary_id' => $subsidiary_id,
                'account_id' => 28,
                'date' => $date,
                'description' => $prefix2 . " (ventas) " . $prefix . "-" . $this->folio,
                'total' => $ventas,
                'warehouse_movement_id' => $this->id
            ]);
            if($this->type != 1) {
                $expendidure = Expenditure::create([
                    'subsidiary_id' => 7,
                    'account_id' => 28,
                    'date' => $date,
                    'description' => $prefix2 . " (ventas) " . $prefix . "-" . $this->folio,
                    'total' => $ventas * (-1),
                    'warehouse_movement_id' => $this->id
                ]);
            }
        }

        if($papeleria > 0) {
            $expendidure = Expenditure::create([
                'subsidiary_id' => $subsidiary_id,
                'account_id' => 6,
                'date' => $date,
                'description' => $prefix2 . " (papeleria) " . $prefix . "-" . $this->folio,
                'total' => $papeleria,
                'warehouse_movement_id' => $this->id
            ]);
            if($this->type != 1) {
                $expendidure = Expenditure::create([
                    'subsidiary_id' => 7,
                    'account_id' => 6,
                    'date' => $date,
                    'description' => $prefix2 . " (papeleria) " . $prefix . "-" . $this->folio,
                    'total' => $papeleria * (-1),
                    'warehouse_movement_id' => $this->id
                ]);
            }
        }
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function scopeEntry($query)
    {
        return $query->where('type', 1);
    }

    public function scopeExit($query)
    {
        return $query->where('type', 2);
    }

    public function scopeNotCanceled($query)
    {
        return $query->where('is_canceled', false);
    }

    public function scopeCanceled($query)
    {
        return $query->where('is_canceled', true);
    }

    public function scopeConfirmed($query)
    {
        return $query->where('is_confirmed', true);
    }

    public function scopeNotConfirmed($query)
    {
        return $query->where('is_confirmed', false);
    }
}
