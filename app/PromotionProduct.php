<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromotionProduct extends Model
{
    protected $table = 'promotion_products';
    protected $fillable = [
        'promotion_id', 'product_id', 'price'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    
    public function promotion()
    {
        return $this->belongsTo(Promotion::class);
    }
}
