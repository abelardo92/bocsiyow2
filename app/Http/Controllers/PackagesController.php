<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\SubDomainHelper;
use App\Package;
use App\PackageProduct;
use App\PackageService;
use App\Product;
use App\Service;
use App\Subsidiary;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use SubDomainHelper;

    private function isLaundry() {
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            if($subsidiary->is_laundry){
                return true;
            }
        }
        return false;
    }

    public function index()
    {
        if ($this->isLaundry()) {
            $packages = Package::laundry()->paginate(15);    
        } else {
            $packages = Package::notLaundry()->paginate(15);
        }
        return view('admin.packages.index', compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
        if ($this->isLaundry()) {
            $products = Product::laundry()->get();
            $services = Service::laundry()->get();    
        } else {
            $products = Product::notLaundry()->get();
            $services = Service::notLaundry()->get();
        }
        return view('admin.packages.create', compact('products', 'services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        if(!$request->has('product_ids') && !$request->has('service_ids')){
            $request->session()->flash('error', "El paquete debe incluir un servicio y/o un producto");
            return redirect()->back();
        }

        $subsidiary_code = $this->hasSubdomain();
        if($subsidiary_code == null) {
            $request->session()->flash('error', "Para agregar un paquete debe hacerlo desde una sucursal");
            return redirect()->back();
        }
        
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        
        $package = Package::create([
            'name' => $data["name"],
            'price' => $data["price"],
            'subsidiary_id' => $subsidiary->id
        ]);

        if($request->has('product_ids')){
            $product_ids = $data['product_ids'];
            $product_quantities = $data['product_quantities'];
            foreach($product_ids as $index => $product_id){
                $product = Product::find($product_id);
                $package_product = PackageProduct::create([
                    'package_id' => $package->id,
                    'product_id' => $product_id,
                    'price' => $product->sell_price,
                    'quantity' => (int)$product_quantities[$index]
                ]);
            }
        }

        if($request->has('service_ids')){
            $service_ids = $data['service_ids'];
            $service_quantities = $data['service_quantities'];
            foreach($service_ids as $index2 => $service_id){
                $service = Service::find($service_id);
                $package_service = PackageService::create([
                    'package_id' => $package->id,
                    'service_id' => $service_id,
                    'price' => $service->sell_price,
                    'quantity' => (int)$service_quantities[$index2]
                    ]);
            }
        }
        $request->session()->flash('success', 'Paquete creado con éxito');
        return redirect()->to('/home/packages');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = Package::with(['packageProducts.product', 'packageServices.service'])->where('id',$id)->get()->first();
        if ($this->isLaundry()) {
            $products = Product::laundry()->get();
            $services = Service::laundry()->get();    
        } else {
            $products = Product::notLaundry()->get();
            $services = Service::notLaundry()->get();
        }
        return view('admin.packages.edit', compact('package', 'products', 'services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        if(!$request->has('product_ids') && !$request->has('service_ids')){
            $request->session()->flash('error', "El paquete debe incluir un servicio y/o un producto");
            return redirect()->back();
        }

        $package = Package::with(['packageProducts.product', 'packageServices.service'])->find($id);
        $package->name = $data['name'];
        $package->price = $data['price'];
        $package->save();

        //dd($package);

        if($request->has('product_ids')){
            $product_ids = $data['product_ids'];
            $product_quantities = $data['product_quantities'];

            foreach($product_ids as $index => $product_id){
                if($package->containsProduct($product_id)) {
                    $package_product = $package->packageProducts->filter(function($item) use ($product_id) {
                        return $item->product_id == (int)$product_id;
                    })->first();
                    $package_product->quantity = (int)$product_quantities[$index];
                    $package_product->price = $package_product->product->sell_price;
                    $package_product->save();
                } else {
                    $product = Product::find($product_id);
                    //dd(floatval($product->sell_price));
                    $package_product = PackageProduct::create([
                    'package_id' => $package->id,
                    'product_id' => $product_id,
                    'quantity' => (int)$product_quantities[$index],
                    'price' => $product->sell_price,
                    ]);
                }
            }
        }
        
        if($request->has('service_ids')) {
            $service_ids = $data['service_ids'];
            $service_quantities = $data['service_quantities'];

            foreach($service_ids as $index => $service_id){
                if($package->containsService($service_id)){
                    $package_service = $package->packageServices->filter(function($item) use ($service_id) {
                        return $item->service_id == (int)$service_id;
                    })->first();
                    $package_service->quantity = (int)$service_quantities[$index];
                    $package_service->price = $package_service->service->sell_price;
                    $package_service->save();
                } else {
                    $service = Service::find($service_id);
                    $package_service = PackageService::create([
                    'package_id' => $package->id,
                    'service_id' => $service_id,
                    'quantity' => (int)$service_quantities[$index],
                    'price' => $service->sell_price
                    ]);
                }
            }
        }

        $request->session()->flash('success', 'Paquete actualizado con éxito');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function destroyProduct($id)
    {
        if($packageProduct = PackageProduct::find($id)){
            $packageProduct->delete();
            return response()->json(['is_deleted' => true], 200);
        }
        return response()->json([
            'is_deleted' => false,
            'message' => 'Ocurrió un problema al eliminar el producto',
        ], 404);
    }

    public function destroyService($id)
    {
        if($packageService = PackageService::find($id)){
            $packageService->delete();
            return response()->json(['is_deleted' => true], 200);
        }
        return response()->json([
            'is_deleted' => false,
            'message' => 'Ocurrió un problema al eliminar el servicio',
        ], 404);
    }
}
