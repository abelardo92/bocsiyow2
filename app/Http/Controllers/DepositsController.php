<?php

namespace App\Http\Controllers;

use App\Deposit;
use App\Employee;
use App\Paysheet;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DepositsController extends Controller
{
    public function index(Request $request)
    {
        $now = Carbon::now();
        if ($request->has('start')) {
            $start = $request->start;
            $end = $request->end;
        } else {
            $start = $now->format('Y-m-d');
            $end = $now->format('Y-m-d');
        }

        $deposits = Deposit::where('start', $start)
                ->where('end', $end)
                ->orderBy('start', 'desc')
                ->get();

        return view('admin.deposits.index', compact('deposits', 'start', 'end'));
    }

    public function create()
    {
        $employees = Employee::where('active', true)->orderBy('short_name', 'asc')->get();
        return view('admin.deposits.create', compact('employees'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['period'] = $data['start'].'-'.$data['end'];
        foreach ($data['id'] as $key => $employee_id) {
            $employee = Employee::find($employee_id);
            if ($paysheet = Paysheet::where('key', $data['start'] . '-' . $data['end'])->where('employee_id', $employee_id)->first()) {
               $paysheet->depositado = $data['amount'][$key];
               $paysheet->diferencia = $paysheet->neto - $data['amount'][$key];
               $paysheet->save();
            }
            if($employee->deposits()->where('start', $data['start'])->where('end', $data['end'])->count() > 0 ){
                continue;
            }
            $deposit = Deposit::create([
                'period' => $data['period'],
                'start' => $data['start'],
                'end' => $data['end'],
                'employee_id' => $employee_id,
                'amount' => $data['amount'][$key],
            ]);
        }

        $request->session()->flash('success', 'Deposito creado con exito.');
        return redirect()->route('deposits.index');
    }

    public function edit($id)
    {
        $employees = Employee::where('active', true)->get();
        $deposit = Deposit::find($id);
        return view('admin.deposits.edit', compact('employees', 'deposit'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['period'] = $data['start'].'-'.$data['end'];
        $deposit = Deposit::find($id);
        $deposit->update($data);

        if ($paysheet = Paysheet::where('key', $data['start'] . '-' . $data['end'])->where('employee_id', $deposit->employee_id)->first()) {
           $paysheet->depositado = $deposit->amount;
           $paysheet->diferencia = $paysheet->neto - $deposit->amount;
           $paysheet->save();
        }

        $request->session()->flash('success', 'Deposito actualizado con exito.');
        return redirect()->back();
    }
}
