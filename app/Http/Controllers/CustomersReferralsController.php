<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Referral;
use Illuminate\Http\Request;

class CustomersReferralsController extends Controller
{
    public function store(Request $request, Customer $customer)
    {
        $data = $request->all();

        $wallet = preg_split('/ ?- ?/', $data['wallet_number']);

        if (count($wallet) == 2) {
            $start = $wallet[0];
            $end = $wallet[1];
        }

        if (count($wallet) == 1) {
            $start = $data['wallet_number'];
            $end = $data['wallet_number'];
        }

        $this->createReferrals($customer, $start, $end);

        $request->session()->flash('success', 'Monedero de referencia creado con exito.');
        return response()->json([
            'success' => true,
        ]);
    }

    protected function createReferrals($customer, $start, $end)
    {
        foreach (range($start, $end) as $val) {
            $customer->referrals()->create([
                'wallet_number' => $val,
            ]);
        }
    }

    public function destroy(Request $request, Customer $customer, $referral_id)
    {
        $referrals = Referral::find($referral_id);
        $referrals->delete();

        $request->session()->flash('success', 'Monedero de referencia eliminado con exito.');
        return back();
    }

}
