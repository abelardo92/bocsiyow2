<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeRequestRequest;
// use App\Mail\EmployeeNotification;
use App\Sale;
use App\EmployeeRequest;
use App\State;
use App\Subsidiary;
use App\TaxRegime;
use Illuminate\Support\Facades\Auth;
use App\Traits\ImageableTrait;
use Illuminate\Support\Facades\Mail;

class EmployeeRequestController extends Controller
{
    use ImageableTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee_requests = EmployeeRequest::active()->orderBy('created_at', 'desc')->get();
        return view('admin.employee-requests.index', compact('employee_requests'));
    }

    public function createCustomer()
    {
        $subsidiaries = Subsidiary::active()->forCustomerDiaries()->get();
        return view('admin.employee-requests.createCustomer', compact('subsidiaries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequestRequest $request)
    {
        $data = $request->all();

        $data['status'] = 'pendiente';

        $employee_request = EmployeeRequest::create($data);
        $this->CreateFiles($employee_request, $request, "employee_requests/{$employee_request->id}");

        $request->session()->flash('success', 'Su solicitud ya ha sido recibida, nos comunicaremos con usted una vez revisemos su solicitud.');
        return redirect()->back();
    }

    public function storeCustomer(EmployeeRequestRequest $request)
    {
        $data = $request->all();

        $data['status'] = 'pendiente';

        $employee_request = EmployeeRequest::create($data);
        $this->CreateFiles($employee_request, $request, "employee_requests/{$employee_request->id}");

        $request->session()->flash('success', 'Su solicitud ya ha sido recibida, nos comunicaremos con usted una vez revisemos su solicitud.');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee_request = EmployeeRequest::find($id);
        $subsidiaries = Subsidiary::active()->forCustomerDiaries()->get();
        return view('admin.employee-requests.edit', compact('employee_request', 'subsidiaries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequestRequest $request, $id)
    {
        $data = $request->all();

        $employee_request = EmployeeRequest::find($id);
        $employee_request->update($data);

        $this->CreateFiles($employee_request, $request, "employee_requests/{$employee_request->id}");

        $request->session()->flash('success', 'Solicitud de empleo actualizada con éxito');
        return redirect()->route('employee-requests.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $employee_request = EmployeeRequest::find($id);
        $employee_request->is_active = false;
        $employee_request->save();

        $request->session()->flash('success', 'Solicitud de empleo dada de baja con éxito');
        return redirect()->route('employee-requests.index');
    }
}
