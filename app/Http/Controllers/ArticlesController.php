<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Article;
use App\Subsidiary;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    public function index()
    {
        $articles = Article::where('type', request('type', 'activo'))->get();

        return view('admin.articles.index', compact('articles'));
    }

    public function create()
    {
        return view('admin.articles.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $article = Article::create($data);
        
        $request->session()->flash('success', 'Articulo dado de alta con exito.');
        return redirect()->route('articles.edit', $article->id);
    }

    public function edit(Request $request, $id)
    {
        $article = Article::find($id);
        return view('admin.articles.edit', compact('article'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $article = Article::find($id);
        $article->update($data);
        $request->session()->flash('success', 'Articulo actualizado con exito.');
        return redirect()->route('articles.edit', $article->id);
    }

    public function getByKey($key)
    {
        $article = Article::whereKey($key)->first();
        if ($article) {
            $article->existence = 0;
            if (request()->has('subsidiary_id')) {
                $subsidiary = Subsidiary::find(request('subsidiary_id'));
                $article_sub = $subsidiary->articles()->whereKey($key)->first();
                if ($article_sub) {
                    $article->existence = $article_sub->pivot->existence;
                }
            }
            return response()->json([
                'success' => true,
                'article' => $article,
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'No encontramos ni un articulo con esta clave.',
        ], 404);
        
    }
}
