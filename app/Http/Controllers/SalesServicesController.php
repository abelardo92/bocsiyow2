<?php
namespace App\Http\Controllers;

use App\Sale;
use App\Service;
use App\User;
use App\Employee;
use App\Promotion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SalesServicesController extends Controller
{
    public function index($subsidiary_code, $sale_id)
    {
        return response()->json(Service::all());
    }

    public function store(Request $request, $subsidiary_code, $sale_id)
    {
        $data = $request->all();
        $sale = Sale::with('employee','products.product')->find($sale_id);
        $service_id = $data['id'];
        $service = $sale->services()->where('service_id', $service_id)->first();
        if ($service) {
            $service->increment('qty');
        } else {

            $currentService = Service::with(['subsidiaryPrices'])->find($service_id);

            $current_promotion = $this->activePromotion($sale->subsidiary_id);
            $discount_percentaje = null;

            if($current_promotion != null && $current_promotion->discount_percentaje > 0) {
                // if there are no service exceptions or service is not in exception, then apply discount percentaje
                if(!$current_promotion->promotionServiceExceptions ||
                !$current_promotion->promotionServiceExceptions->where('service_id', $service_id)->first()) {
                    $discount_percentaje = $current_promotion->discount_percentaje;
                }
            }

            $original_price = $currentService->originalPrice($sale->subsidiary_id);
            $current_price = $currentService->currentPrice($sale->subsidiary_id);
            // $service_price = $discount_percentaje > 0 ? $original_price * $discount_percentaje / 100 : $currentService->currentPrice($sale->subsidiary_id);

            $service = $sale->services()->create([
                'service_id' => $service_id,
                'price' => $current_price,
                'qty' => 1,
                'has_promotion' => $current_price < $original_price,
                'original_price' => $original_price,
                'cost' => $currentService->getCost($sale->subsidiary_id, $current_promotion),
                'discount_percentaje' => $discount_percentaje,
                'include_in_diaries_discount' => $currentService->includeInDiariesdiscount($sale->subsidiary_id),
                'include_in_birthday_discount' => $currentService->include_in_birthday_discount
            ]);
        }

        $day = Carbon::now()->format('l');
        $product = $sale->products->where('product_id', 22)->first();
        // if(
        //     $product && !$sale->has_discount && ($day == 'Tuesday' || $sale->subsidiary_id == 7) && $sale->containsHairCut() &&
        //     $product->price == $product->product->sell_price
        // ) {
        //     $productPrice = $product->product->sell_price * (0.6); //40% off
        //     $product->price = $productPrice;
        //     $product->save();
        // }

        $sale->updateTotals();
        $sale = Sale::with(['employee', 'customer', 'services.service.subsidiaryPrices', 'products.product.subsidiaryPrices', 'diary'])->find($sale_id);
        $message = 'Servicio agregado a la nota con éxito.';
        return response()->json(compact('sale', 'message'));
    }

    public function destroy2(Request $request, $subsidiary_code, $sale_id, $sale_service_id)
    {
        $user = Auth::user();

        if(isset($request->code) && isset($request->password)) {

            if(strpos($request->code, '@')) {
                if ($auth_user = User::where('email', $request->code)->first()) {
                    $user_id = $auth_user->id;
                }
            }
            else if (!$employee = Employee::with('user')->whereCode($request->code)->wherePassword($request->password)->first()) {
                return response()->json([
                    'message' => 'Verifica tus datos de acceso.',
                ], 404);
            } else {
                $user_id = $employee->user ? $employee->user->id : $user->id;
            }
        } else {
            $user_id = $user->id;
        }

        $data = $request->all();
        $canceled_description = $data['canceled_description'];
        $sale = Sale::find($sale_id);
        $service = $sale->services()->find($sale_service_id);

        $service->canceled_by = $user_id;
        $service->canceled_at = Carbon::now();
        $service->canceled_description = $canceled_description;
        $service->save();

        $sale = Sale::with(['employee', 'customer', 'services.service.subsidiaryPrices', 'products.product.subsidiaryPrices', 'packages.package', 'diary', 'payments'])->find($sale_id);
        $sale->updateTotals();
        $message = 'Haz eliminado el servicio.';
        return response()->json(compact('sale', 'message'));
    }

    public function activePromotion($subsidiary_id = null)
    {
        $promotion_subsidiary = Promotion::with('promotionServiceExceptions', 'promotionProductExceptions')
            ->where('is_active', true)
            ->whereDate('start_date','<=',date('Y-m-d'))
            ->whereDate('end_date','>=',date('Y-m-d'))
            ->where('start_time','<=',date('H:i:s'))
            ->where('end_time','>=',date('H:i:s'))
            ->where('subsidiary_id', $subsidiary_id)
            ->where(strtolower(date('l')),true)->get()->first();

        if($promotion_subsidiary) {
            return $promotion_subsidiary;
        }

        $promotion = Promotion::with('promotionServiceExceptions', 'promotionProductExceptions')
            ->where('is_active', true)
            ->whereDate('start_date','<=',date('Y-m-d'))
            ->whereDate('end_date','>=',date('Y-m-d'))
            ->where('start_time','<=',date('H:i:s'))
            ->where('end_time','>=',date('H:i:s'))
            ->whereNull('subsidiary_id')
            ->where(strtolower(date('l')),true)->get()->first();
        return $promotion;
    }

}
