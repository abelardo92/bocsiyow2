<?php

namespace App\Http\Controllers;

use App\Adjustment;
use App\WarehouseAdjustment;
use App\WarehouseProduct;
use App\Concept;
use App\Departure;
use App\Entry;
use App\Product;
use App\Subsidiary;
use App\Traits\SubDomainHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class InventoriesController extends Controller
{
    use SubDomainHelper;

    var $user = null;

    public function __construct() {
        $this->middleware(function ($request, $next){
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $data = $this->getInventoryData();
        return view('admin.inventories.index', $data);
    }

    public function show($type, $folio, $subsidiary_id)
    {
        $singular = str_singular($type);
        $class = studly_case($singular);
        $types = call_user_func("\App\\$class::whereFolio", $folio);
        $types->where('subsidiary_id', $subsidiary_id);
        $title = $this->getTitle($type);
        $models = $types->get();
        $model = $models->first();
        return view("admin.inventories.{$type}.ticket", compact('model', 'title', 'models'));
    }

    public function getTitle($type)
    {
        if ($type == 'entries') {
            return 'Ticket de entrada.';
        }

        if ($type == 'departures') {
            return 'Ticket de salida.';
        }

        if ($type == 'adjustments') {
            return 'Ticket de ajuste.';
        }

        if ($type == 'existences') {
            return 'Ticket de existencia.';
        }
    }

    public function createEntry($subsidiary_code)
    {
        return view('admin.inventories.entries.create');
    }

    public function storeEntry(Request $request, $subsidiary_code)
    {
        $products = $this->getProductsByRequest($request);
        // $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        $subsidiary = $this->getCurrentSubsidiary();
        $folio = $subsidiary->getEntriesFolio();
        foreach ($products as $product) {
            $pid = $product['id'];
            $entry = $subsidiary->entries()->create([
                'user_id' => $request->user()->id,
                'product_id' => $product['id'],
                'qty' => $product['qty'],
                'folio' => $folio,
                'employee_id' => session()->get('employee_id'),
            ]);
            $existence = $product['qty'];
            if ($product_find = $subsidiary->products()->find($pid)) {
                $existence = $product_find->pivot->existence + $product['qty'];
            }
            $subsidiary->products()->syncWithoutDetaching([
                $pid => [
                    'existence' => $existence,
                ],
            ]);

            $entry->product->updateExistence($product['qty']);
        }

        $request->session()->flash('success', 'Entradas guardadas con exito.');
        $request->session()->flash('ticket_url', route('inventories.ticket', [
            'type' => 'entries',
            'folio' => $folio,
            'subsidiary_id' => $subsidiary->id,
        ])
        );
        return redirect()->route('inventories.index');
    }

    public function createDeparture($subsidiary_code)
    {
        $concepts = Concept::whereType('departure')->get();
        return view('admin.inventories.departures.create', compact('concepts'));
    }

    public function storeDeparture(Request $request, $subsidiary_code)
    {
        $products = $this->getProductsByRequest($request);
        // $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        $subsidiary = $this->getCurrentSubsidiary();
        $folio = $subsidiary->getDeparturesFolio();

        foreach ($products as $product) {
            $pid = $product['id'];
            $departure = $subsidiary->departures()->create([
                'user_id' => $request->user()->id,
                'concept_id' => $this->getConceptByRequest($request),
                'product_id' => $product['id'],
                'qty' => $product['qty'],
                'folio' => $folio,
                'employee_id' => session()->get('employee_id'),
            ]);

            $existence = $product['qty'];
            if ($product_find = $subsidiary->products()->find($pid)) {
                $existence = $product_find->pivot->existence - $product['qty'];
            }
            $subsidiary->products()->syncWithoutDetaching([
                $pid => [
                    'existence' => $existence,
                ],
            ]);

            $departure->product->updateExistence($product['qty'], 'decrement');
        }

        $request->session()->flash('success', 'Salidas guardadas con exito.');
        $request->session()->flash('ticket_url', route('inventories.ticket', [
            'type' => 'departures',
            'folio' => $folio,
            'subsidiary_id' => $subsidiary->id,
        ])
        );

        return redirect()->route('inventories.index');
    }

    public function createTransfer($subsidiary_code)
    {
        return view('admin.inventories.transfers.create');
    }

    public function createAdjustment($subsidiary_code)
    {

        return view('admin.inventories.adjustments.create');
    }

    public function storeAdjustment(Request $request, $subsidiary_code)
    {
        $products = $this->getProductsByRequest($request);
        // $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        $subsidiary = $this->getCurrentSubsidiary();
        $folio = $subsidiary->getAdjustmentsFolio();

        foreach ($products as $product) {
            $type = $this->getType($product['qty']);
            $pid = $product['id'];

            $adjustment = $subsidiary->adjustments()->create([
                'user_id' => $request->user()->id,
                'product_id' => $product['id'],
                'qty' => $product['qty'],
                'folio' => $folio,
                'current_existence' => $product['current_existence'],
                'real_existence' => $product['real_existence'],
                'employee_id' => session()->get('employee_id'),
            ]);

            $subsidiary->products()->syncWithoutDetaching([
                $pid => [
                    'existence' => $product['real_existence'],
                ],
            ]);

            $adjustment->product->updateExistence(abs($product['qty']), $type);
        }

        $request->session()->flash('success', 'Ajustes creados con exito.');
        $request->session()->flash('ticket_url', route('inventories.ticket', [
            'type' => 'adjustments',
            'folio' => $folio,
            'subsidiary_id' => $subsidiary->id,
        ])
        );

        return redirect()->route('inventories.index');
    }

    public function indexWarehouseAdjustments(Request $request)
    {
        $filter = '';
        if($request->has('filter')) {
            $filter = $request->filter;    
            $adjustments = WarehouseAdjustment::where('folio', 'LIKE', '%'.$filter.'%')->orderBy('folio', 'DESC'); 
        } else {
            $adjustments = WarehouseAdjustment::orderBy('folio', 'DESC');    
        }
        $adjustments = $adjustments->paginate(15);
        return view('admin.inventories.warehouse.adjustments.index', compact('adjustments'));
    }

    public function printWarehouseAdjustment($folio)
    {
        $models = WarehouseAdjustment::where('folio',$folio)->get();
        $model = $models->first();
        return view('admin.inventories.warehouse.adjustments.ticket', compact('model', 'models')); 
    }

    public function createWarehouseAdjustment()
    {
        $warehouse_products = WarehouseProduct::active()->get();
        return view('admin.inventories.warehouse.adjustments.create', compact('warehouse_products'));
    }

    public function storeWarehouseAdjustment(Request $request)
    {
        $products = $request->products;

        $folio = 1;
        if ($last = WarehouseAdjustment::orderBy('folio', 'DESC')->first()) {
            $folio = $last->folio + 1;
        }

        foreach ($products as $product) {
            $pid = $product['id'];
            $adjustment = WarehouseAdjustment::create([
                'user_id' => $request->user()->id,
                'warehouse_product_id' => $product['id'],
                'qty' => $product['qty'],
                'folio' => $folio,
                'current_existence' => $product['existence'],
                'real_existence' => $product['real_existence'],
            ]);
            $warehouseProduct = WarehouseProduct::find($product['id']);
            $warehouseProduct->in_storage = $product['real_existence'];
            $warehouseProduct->save();
        }

        $request->session()->flash('success', 'Ajustes creados con exito.');
        return redirect()->route('home.inventories.warehouse.adjustments.index');
    }

    public function createStock($subsidiary_code)
    {
        return view('admin.inventories.stocks.create');
    }

    public function printExistence($subsidiary_code)
    {
        $subsidiary = $this->getCurrentSubsidiary();
        $products = $subsidiary->products->sortBy('key');
        $title = $this->getTitle('existences');
        return view('admin.inventories.existences.ticket', compact('products', 'title', 'subsidiary'));
    }

    protected function getProductsByRequest($request)
    {
        return array_filter($request->products, function ($product) {
            $product['id'] = (int) $product['id'];
            return $product['id'] != false;
        });
    }

    protected function getConceptByRequest($request)
    {
        if ($request->concept_id == 'new') {
            $concept = Concept::create([
                'name' => $request->concept,
                'type' => 'departure',
            ]);

            return $concept->id;
        }

        return $request->concept_id;
    }

    protected function getType($qty)
    {
        return ($qty >= 0) ? 'increment' : 'decrement';
    }

    public function getInventoryData()
    {
        $threeMonthsBefore = Carbon::now()->subDays(60);        
        $subsidiary = $this->getCurrentSubsidiary();

        if ($subsidiary) {
            $entries = $subsidiary->entries()->where('created_at', '>=', $threeMonthsBefore)->orderBy('created_at', 'desc')->get();
            $departures = $subsidiary->departures()->where('created_at', '>=', $threeMonthsBefore)->orderBy('created_at', 'desc')->get();
            $adjustments = $subsidiary->adjustments()->where('created_at', '>=', $threeMonthsBefore)->orderBy('created_at', 'desc')->get();
        } else {
            $entries = Entry::where('created_at', '>=', $threeMonthsBefore)->orderBy('created_at', 'desc')->get();
            $departures = Departure::where('created_at', '>=', $threeMonthsBefore)->orderBy('created_at', 'desc')->get();
            $adjustments = Adjustment::where('created_at', '>=', $threeMonthsBefore)->orderBy('created_at', 'desc')->get();
        }
        return compact('entries', 'departures', 'adjustments', 'subsidiary');
    }

    public function getCurrentSubsidiary() {
        $subsidiary = null;

        if($this->user->access_subsidiary_id != null) {
            $subsidiary = Subsidiary::find($this->user->access_subsidiary_id);
        } else if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        }
        return $subsidiary;
    }
}
