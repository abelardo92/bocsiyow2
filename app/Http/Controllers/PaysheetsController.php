<?php

namespace App\Http\Controllers;

use App\Paysheet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use League\Csv\Writer;
use SplTempFileObject;

class PaysheetsController extends Controller
{
    public function index()
    {
        $paysheets = Paysheet::orderBy('start', 'desc')->get()->groupBy('key');
        return view('admin.paysheets.index', compact('paysheets'));
    }


    public function store(Request $request)
    {
        $data = $request->all();
        $data['asistencia'] = $data['asistencia_pay'] == "1" ? $data['asistencia'] : 0; 
        $data['puntualidad'] = $data['puntualidad_pay'] == "1" ? $data['puntualidad'] : 0; 
        $data['productividad'] = $data['productividad_pay'] == "1" ? $data['productividad'] : 0; 
        $data['sunday'] = $data['sunday_pay'] == "1" ? $data['sunday'] : 0; 
        $data['excellence'] = $data['excellence_pay'] == "1" ? $data['excellence'] : 0; 
        $data['extra'] = $data['extra_pay'] == "1" ? $data['extra'] : 0; 
        $paysheet = Paysheet::create($data);
        return back();
    }

    public function show($key)
    {
        $paysheets = Paysheet::where('key', $key)->get();
        $cajeros = $paysheets->filter(function ($paysheet) {
            return $paysheet->employee->hasJob('Cajero');
        })->sortBy(function ($paysheet) {
            return $paysheet->employee->short_name;
        });
        $barberos = $paysheets->filter(function ($paysheet) {
            return $paysheet->employee->hasJob('Barbero');
        })->sortBy(function ($paysheet) {
            return $paysheet->employee->short_name;
        });
        $otros = $paysheets->filter(function ($paysheet) {
            return !$paysheet->employee->hasJobs(['Cajero', 'Barbero']);
        })->sortByDesc(function ($paysheet) {
            return $paysheet->employee->job;
        })->sortBy(function ($paysheet) {
            return $paysheet->employee->short_name;
        });
        $status = $paysheets->first()->status == 'archived' ? 1 : 0;
        $start = Carbon::parse($paysheets->first()->start);
        $end = Carbon::parse($paysheets->first()->end);

        $copy = $start->copy();
        $dates = [];
        while ($copy->lte($end)) {
            $dates[] = $copy->format('Y-m-d');
            $copy->addDay();
        }
        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');
        // dd($cajeros_barberos);
        $cajeros_barberos = $cajeros->merge($barberos);
        $paysheets = $cajeros_barberos->merge($otros);

        return view('admin.paysheets.show', compact('paysheets', 'dates', 'start', 'end', 'status'));
    }

    public function update(Request $request, $key)
    {
        $data = $request->all();

        foreach ($data['employee'] as $paysheet) {
            $paysheet_update = Paysheet::find($paysheet['paysheet_id']);
            $paysheet_update->update([
                'total' => $paysheet['total'] ?? 0,
                'asistencia' => $paysheet['asistencia'] ?? 0,
                'asistencia_pay' =>  (($paysheet['asistencia'] ?? 0) > 0 ? true : false),
                'puntualidad' => $paysheet['puntualidad'] ?? 0,
                'puntualidad_pay' => (($paysheet['puntualidad'] ?? 0) > 0 ? true : false),
                'productividad' => $paysheet['productividad'] ?? 0,
                'productividad_pay' => (($paysheet['productividad'] ?? 0) > 0 ? true : false),
                'sunday' => $paysheet['sunday'] ?? 0,
                'sunday_pay' => (($paysheet['sunday'] ?? 0) > 0 ? true : false),
                'excellence' => $paysheet['excellence'] ?? 0,
                'excellence_pay' => (($paysheet['excellence'] ?? 0) > 0 ? true : false),
                'extra' => $paysheet['extra'] ?? 0,
                'repairs' => $paysheet['repairs'] ?? 0,
                'cardtips' => $paysheet['cardtips'] ?? 0,
                'products' => $paysheet['products'] ?? 0,
                'extra_pay' => (($paysheet['extra'] ?? 0) > 0 ? true : false),
                'otros_ingresos' => $paysheet['otros_ingresos'] ?? 0,
                'total_ingreso' => $paysheet['total_ingresos'] ?? 0,
                'prestamo' => $paysheet['prestamo'] ?? 0,
                'faltas' => $paysheet['total_faltas'] ?? 0,
                'faltas_pay' => (($paysheet['total_faltas'] ?? 0) > 0 ? true : false),
                'uniforme' => $paysheet['uniforme'] ?? 0,
                'infonavit' => $paysheet['monto_infonavit'] ?? 0,
                'otras' => $paysheet['otras'] ?? 0,
                'discount_repairs' => $paysheet['discount-repairs'] ?? 0,
                'total_deducciones' => $paysheet['desctotal'] ?? 0,
                'neto' => $paysheet['neto'] ?? 0,
                'depositado' => $paysheet['depositado'] ?? 0,
                'diferencia' => $paysheet['diferencia'] ?? 0,
            ]);
        }

        return back();
    }

    public function updateById(Request $request)
    {
        $data = $request->all();
        $paysheet = Paysheet::find($data['id']);

        if ($paysheet) {
            $paysheet->update($data);
            $message = 'La nomina ha sido actualizada.';
        } else {
            $data['key'] = "{$data['start']}-{$data['end']}";
            $paysheet = Paysheet::create($data);
            $message = 'La nomina ha sido guardada.';
        }

        return response()->json([
            'success' => true,
            'paysheet_id' => $paysheet->id,
            'message' => $message,
        ], 200);
    }

    public function archiveById(Request $request)
    {
        $data = $request->all();
        $data['status'] = "archived";
        $paysheet = Paysheet::find($data['id']);
        $paysheet->update($data);
        return response()->json([
            'success' => true,
            'message' => 'La nomina ha sido archivada, ya no se podrá editar.',
        ], 200);
    }

    public function refresh(Request $request, $id)
    {
        $data = $request->all();
        $paysheet = Paysheet::find($id);
        $data['asistencia'] = $data['asistencia_pay'] == "1" ? $data['asistencia'] : 0; 
        $data['puntualidad'] = $data['puntualidad_pay'] == "1" ? $data['puntualidad'] : 0; 
        $data['productividad'] = $data['productividad_pay'] == "1" ? $data['productividad'] : 0; 
        $data['sunday'] = $data['sunday_pay'] == "1" ? $data['sunday'] : 0; 
        $data['excellence'] = $data['excellence_pay'] == "1" ? $data['excellence'] : 0; 
        $data['extra'] = $data['extra_pay'] == "1" ? $data['extra'] : 0; 

        $paysheet->update($data);
        return back();
    }

    public function archive($key)
    {
        foreach (Paysheet::where('key', $key)->get() as $paysheet_update) {
            $paysheet_update->update([
                'status' => 'archived',
            ]);
        }

        return back();
    }

    public function nomina($key)
    {
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne(['No. empleado', 'Nombre', 'Importe', 'No. banco receptor', 'Tipo de cuenta', 'Cuenta']);
        foreach (Paysheet::where('key', $key)->get() as $paysheet) {
            $csv->insertOne([$paysheet->employee->no_banco, strtoupper(str_slug($paysheet->employee->name, ' ')), number_format($paysheet->depositado, 2, '.', ''), '072', '01', $paysheet->employee->account_number]);

        }
        $csv->output("nomina-{$key}.csv");
        die;
    }

    public function banco($key)
    {
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne(['Columna', 'Cuenta', 'Nombre', 'Importe a depositar', 'No. tarjeta']);
        $loop = 1;
        foreach (Paysheet::where('key', $key)->get() as $paysheet) {
            $csv->insertOne([$loop, $paysheet->employee->account_number, strtoupper(str_slug($paysheet->employee->name, ' ')), number_format(($paysheet->neto - $paysheet->depositado), 2, '.', ''), "_{$paysheet->employee->no_tarjeta}"]);
            $loop++;

        }
        $csv->output("banco-{$key}.csv");
        die;
    }

    public function original($key)
    {
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne([
            'Nombre', 'Total', 'Asistencia', 'Puntualidad', 'Productividad',
            'Dia extra', 'Otros ingresos', 'Total ingresos', 'Prestamo', 'Faltas',
            'Uniforme', 'Infonavit', 'Otras', 'Total deducciones', 'Neto a pagar',
            'Depositado', 'Diferencia',
        ]);
        foreach (Paysheet::where('key', $key)->get() as $paysheet) {
            $csv->insertOne([
                strtoupper(str_slug($paysheet->employee->name, ' ')), $paysheet->total, $paysheet->asistencia, $paysheet->puntualidad, $paysheet->productividad, $paysheet->extra, $paysheet->otros_ingresos, $paysheet->total_ingreso, $paysheet->prestamo, $paysheet->faltas, $paysheet->uniforme, $paysheet->infonavit, $paysheet->otras, $paysheet->total_deducciones, $paysheet->neto, $paysheet->depositado, $paysheet->diferencia,
            ]);

        }
        $csv->output("original-{$key}.csv");
        die;
    }
}
