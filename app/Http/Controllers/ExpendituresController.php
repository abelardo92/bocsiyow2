<?php

namespace App\Http\Controllers;

use App\Account;
use App\Expenditure;
use Carbon\Carbon;
use App\Traits\ImageableTrait;
use Illuminate\Http\Request;

class ExpendituresController extends Controller
{
    use ImageableTrait;

    public function index(Request $request)
    {
        $date = Carbon::now();
        $filter = $request->get('filter', 'all');
        if ($request->has('date')) {
            $date = Carbon::parse($request->date);
        }
        $start = $date->copy()->startOfMonth();
        $end = $date->copy()->endOfMonth();
        $expenditures = Expenditure::dates([$start, $end])->whereHas('subsidiary', function ($query) {
            $query->where('is_active', true);
        });
        if ($filter != 'all') {
            $expenditures->whereAccountId($filter);
        }
        $expenditures = $expenditures->orderBy('date')->get();
        $accounts = Account::all();

        return view('admin.expenditures.index', compact('expenditures', 'start', 'end', 'date', 'filter', 'accounts'));
    }

    public function create(Request $request)
    {
        return view('admin.expenditures.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $expenditure = Expenditure::create($data);

        $this->CreateFile($expenditure, $request, "expenditures/{$expenditure->id}");

        $request->session()->flash('success', 'Erogación dada de alta con éxito.');
        return redirect()->route('expenditures.edit', $expenditure->id);
    }

    public function edit($id)
    {
        $expenditure = Expenditure::with(['movement','image'])->find($id);
        return view('admin.expenditures.edit', compact('expenditure'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $expenditure = Expenditure::find($id);
        $expenditure->update($data);

        $this->CreateFile($expenditure, $request, "expenditures/{$expenditure->id}");

        $request->session()->flash('success', 'Erogación actualizada con exito.');
        return back();
    }

    public function destroy($id)
    {
        $diary = Expenditure::find($id);
        $diary->delete();
        session()->flash('success', "Erogación ha sido eliminada con éxito");
        return back();
    }
}
