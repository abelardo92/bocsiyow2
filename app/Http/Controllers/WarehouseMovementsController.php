<?php

namespace App\Http\Controllers;

use App\WarehouseMovement;
use App\WarehouseMovementProduct;
use App\WarehouseRequest;
use App\WarehouseProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Traits\ImageableTrait;
use App\Image;
use Carbon\Carbon;
use File;

class WarehouseMovementsController extends Controller
{
    use ImageableTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $movements = WarehouseMovement::with(['createdBy'])->entry()->notCanceled()->orderBy('id','desc')->paginate(10);
        return view('admin.warehouse.movements.index', compact('movements', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $warehouseRequest = WarehouseRequest::find($id);
        $warehouseProducts = WarehouseProduct::active()->get();
        return view('admin.warehouse.movements.create', compact('warehouseRequest', 'warehouseProducts'));
    }

    public function createEntry()
    {
        $warehouseProducts = WarehouseProduct::active()->get();
        return view('admin.warehouse.movements.createEntry', compact('warehouseProducts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        if(!$request->has('product_ids')) {
            $request->session()->flash('error', 'Debe seleccionar al menos un producto.');
            return redirect()->back();
        }
        
        $has_storage = true;
        $storage_error_message = "Mercancía de almacen insuficiente: <br/>";
        foreach ($data['product_ids'] as $index => $id) {
            $product = WarehouseProduct::find($id); 
            if($product != null && $product->in_storage < $data['product_quantities'][$index]) {
                $storage_error_message .= $product->name . " (". $product->in_storage .")<br/>"; 
                $has_storage = false;
            }
        }

        if(!$has_storage) {
            $request->session()->flash('error', $storage_error_message);
                return redirect()->back();
        }
        
        $warehouseRequest = WarehouseRequest::with(['subsidiary','products','products.product'])->find($data['request_id']);
        $folio = 1;
        if ($last = WarehouseMovement::exit()->orderBy('folio', 'DESC')->first()) {
            $folio = $last->folio + 1;
        }
        
        $warehouseMovement = WarehouseMovement::create([
            'subsidiary_id' => $warehouseRequest->subsidiary->id,
            'folio' => $folio,
            'date' => Carbon::now()->format('Y-m-d'),
            'type' => 2,
            'warehouse_request_id' => $warehouseRequest->id,
            'created_by' => $user->id,
        ]);

        foreach ($data['product_ids'] as $index => $id) {
            $warehouseMovementProduct = $warehouseMovement->products()->create([
            'warehouse_product_id' => $id,
            'quantity' => $data['product_quantities'][$index],
            'amount' => $data['product_amounts'][$index],
            ]); 
        }

        $request->session()->flash('success', 'Solicitud atendida con éxito.');
        return redirect()->route('warehouse.requests.pending');
    }

    public function storeEntry(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $folio = 1;
        if ($last = WarehouseMovement::entry()->orderBy('folio', 'DESC')->first()) {
            $folio = $last->folio + 1;
        }
        
        $warehouseMovement = WarehouseMovement::create([
            'subsidiary_id' => null,
            'folio' => $folio,
            'date' => Carbon::now()->format('Y-m-d'),
            'type' => 1,
            'warehouse_request_id' => null,
            'discount' => $data['discount'],
            'provider' => $data['provider'],
            'note_folio' => $data['note_folio'],
            'created_by' => $user->id,
        ]);

        $this->createWarehouseMovementProducts($data, $warehouseMovement);

        $this->CreateFile($warehouseMovement, $request, "warehouseEntries/{$warehouseMovement->id}");

        $movement = WarehouseMovement::with(['products.product.product', 'subsidiary.products'])
        ->find($warehouseMovement->id);
        $movement->createExpenditures();

        $request->session()->flash('success', 'Entrada agregada con éxito.');
        return redirect()->route('warehouse.movements.index');
    }

    public function storeEntry2(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $folio = 1;
        if ($last = WarehouseMovement::entry()->orderBy('folio', 'DESC')->first()) {
            $folio = $last->folio + 1;
        }
        
        $warehouseMovement = WarehouseMovement::create([
            'subsidiary_id' => null,
            'folio' => $folio,
            'date' => Carbon::now()->format('Y-m-d'),
            'type' => 1,
            'warehouse_request_id' => null,
            'discount' => $data['warehouse_movement']['discount'],
            'provider' => $data['warehouse_movement']['provider'],
            'note_folio' => $data['warehouse_movement']['note_folio'],
            'created_by' => $user->id,
        ]);
        
        $products = $data['products'];
        foreach ($products as $index => $product) {

            $warehouseMovementProduct = $warehouseMovement->products()->create([
            'warehouse_product_id' => $product['id'],
            'quantity' => $product['qty'],
            'amount' => $product['amount'],
            'provider_price' => $product['price'],
            ]); 

            $product = WarehouseProduct::find($product['id']);
            $product->in_storage = $product->in_storage + $warehouseMovementProduct->quantity;
            $product->amount = $product['price'];
            $product->save();
        }

        $movement = WarehouseMovement::with(['products.product.product', 'subsidiary.products'])
        ->find($warehouseMovement->id);
        $movement->createExpenditures();

        //$this->CreateFile($warehouseMovement, $request, "warehouseEntries/{$warehouseMovement->id}");
        //$request->file('image')->store("warehouseEntries/{$warehouseMovement->id}");

        $request->session()->flash('success', 'Entrada agregada con éxito.');
        return response()->json([
                'message' => 'Solicitud dada de alta con éxito.',
            ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $warehouseMovement = WarehouseMovement::with(['subsidiary', 'products', 'products.product', 'request', 'request.products'])->find($id);
        return view('admin.warehouse.movements.show', compact('warehouseMovement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $warehouseMovement = WarehouseMovement::with(['subsidiary', 'products', 'products.product', 'request', 'request.products'])->find($id);
        $warehouseProducts = WarehouseProduct::active()->get();
        return view('admin.warehouse.movements.edit', compact('warehouseMovement', 'warehouseProducts'));
    }

    public function editEntry($id)
    {
        $warehouseMovement = WarehouseMovement::with(['subsidiary', 'products', 'products.product', 'request', 'request.products'])->find($id);
        $warehouseProducts = WarehouseProduct::active()->get();
        return view('admin.warehouse.movements.editEntry', compact('warehouseMovement', 'warehouseProducts'));
    }

    public function confirm($id)
    {
        $warehouseMovement = WarehouseMovement::with(['subsidiary', 'products', 'products.product', 'request', 'request.products'])->find($id);
        $warehouseProducts = WarehouseProduct::active()->get();
        return view('admin.warehouse.movements.confirm', compact('warehouseMovement', 'warehouseProducts'));
    }

    public function print($id)
    {
        $warehouseMovement = WarehouseMovement::with(['subsidiary', 'products', 'products.product', 'request', 'request.products', 'request.employee', 'request.confirmedBy', 'createdBy'])->find($id);
        //dd($warehouseMovement->request->confirmedBy->name);
        $uses = WarehouseProduct::getEnum('uses');
        $accountings = WarehouseProduct::getEnum('accountings');
        return view('admin.warehouse.movements.tickets.movement', compact('warehouseMovement', 'uses', 'accountings'));
    }

    public function printUnsupplied($id)
    {
        $warehouseMovement = WarehouseMovement::with(['subsidiary', 'products', 'products.product', 'request', 'request.products.product', 'request.employee', 'createdBy'])->find($id);
        $uses = WarehouseProduct::getEnum('uses');
        $accountings = WarehouseProduct::getEnum('accountings');
        return view('admin.warehouse.movements.tickets.movement_unsupplied', compact('warehouseMovement', 'uses', 'accountings'));
    }

    public function printSubsidiary($id)
    {
        $warehouseMovement = WarehouseMovement::with(['subsidiary', 'products', 'products.product', 'request', 'request.products'])->find($id);
        $title = "Ticket de pedido";
        return view('admin.warehouse.movements.tickets.movement_subsidiary', compact('warehouseMovement', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user();

        if(!$request->has('product_ids')){
            $request->session()->flash('error', "El paquete debe incluir un servicio y/o un producto");
            return redirect()->back();
        }

        $movement = WarehouseMovement::with(['products'])->find($id);

        $product_ids = $data['product_ids'];
        $product_quantities = $data['product_quantities'];
        $product_amounts = $data['product_amounts'];

        foreach($product_ids as $index => $product_id) {
            if($movement->containsProduct($product_id)) {
                $movement_product = $movement->products->filter(function($item) use ($product_id) {
                    return $item->warehouse_product_id == (int)$product_id;
                })->first();
                $movement_product->quantity = (int)$product_quantities[$index];
                $movement_product->save();
            } else {
                $movement_product = $movement->products()->create([
                'warehouse_product_id' => $product_id,
                'quantity' => (int)$product_quantities[$index],
                'amount' => $product_amounts[$index],
                ]);
            }
        }

        if($request->submit == "finalize") {
            $movement->is_finalized = true;
            $movement->finalized_by = $user->id;
            $movement->save();
            $movement->createExpenditures();
            return redirect()->route('warehouse.requests.pending');
        }

        $request->session()->flash('success', 'movimiento actualizado con éxito');
        return redirect()->back();
    }

    public function updateEntry(Request $request, $id)
    {
        $data = $request->all();
        $movement = WarehouseMovement::with(['products'])->find($id);
        $movement->update($data);
        $this->CreateFile($movement, $request, "warehouseEntries/{$movement->id}");

        foreach ($movement->products as $product) {
            $warehouse_product = WarehouseProduct::find($product->warehouse_product_id);
            $warehouse_product->in_storage = $warehouse_product->in_storage - $product->quantity; 
            $warehouse_product->save();
        }
        $movement->deleteProducts();
        $this->createWarehouseMovementProducts($data, $movement);

        $request->session()->flash('success', 'Entrada actualizada con éxito');
        return redirect()->back();
    }

    public function updateConfirm(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user();

        if(!$request->has('product_ids')){
            $request->session()->flash('error', "El paquete debe incluir un servicio y/o un producto");
            return redirect()->back();
        }

        $movement = WarehouseMovement::with(['products.product','subsidiary'])->find($id);
        $subsidiary = $movement->subsidiary;
        $product_ids = $data['product_ids'];
        $product_quantities = $data['product_quantities'];
        $product_amounts = $data['product_amounts'];

        foreach($product_ids as $index => $product_id) {
            if($movement->containsProduct($product_id)) {
                $movement_product = $movement->products->filter(function($item) use ($product_id) {
                    return $item->warehouse_product_id == (int)$product_id;
                })->first();
                $movement_product->quantity = (int)$product_quantities[$index];
                $movement_product->save();
            } else {
                $movement_product = $movement->products()->create([
                'warehouse_product_id' => $product_id,
                'quantity' => (int)$product_quantities[$index],
                'amount' => $product_amounts[$index],
                ]);
            }
        }

        $movement = WarehouseMovement::with(['products.product.product', 'subsidiary.products'])->find($id);
        $subsidiaryProducts = $subsidiary->products;

        //ciclo previo para verificar que hay productos suficientes en almacen para distribuir
        $has_storage = true;
        $storage_error_message = "Mercancía de almacen insuficiente: <br/>";
        foreach ($movement->products as $index => $product) {
            if($product->product->in_storage < $product->quantity) {
                $storage_error_message .= $product->product->name . " (". $product->product->in_storage .")<br/>"; 
                $has_storage = false;
            }
        }        

        if(!$has_storage) {
            $request->session()->flash('error', $storage_error_message);
                return redirect()->back();
        }
        
        foreach ($movement->products as $index => $product) {
            if($product->product->product != null) {
                $folio = $subsidiary->getEntriesFolio();
                $entry = $subsidiary->entries()->create([
                    'user_id' => $user->id,
                    'product_id' => $product->product->product->id,
                    'qty' => $product->quantity,
                    'folio' => $folio,
                    'employee_id' => session()->get('employee_id'),
                ]);
                $pid = $product->product->product->id;

                //Agregar existencia a la sucursal..
                $subsidiaryProduct = $subsidiaryProducts->find($pid); 
                if(isset($subsidiaryProduct->pivot)) {
                    $current_existence = $subsidiaryProduct->pivot->existence;
                    $subsidiary->products()->syncWithoutDetaching([
                        $pid => [
                            'existence' => $current_existence + $product->quantity,
                        ],
                    ]);
                $product->product->product->updateExistence($product->quantity, 'increment');
                }  
            }
            if($product->product != null) {
                //restar la existencia del almacen
                $warehouseProduct = $product->product;
                $warehouseProduct->in_storage = $warehouseProduct->in_storage - $product->quantity;
                $warehouseProduct->save();
            }
        }
        
        $movement->is_confirmed = true;
        $movement->confirmed_by = $user->id;
        $movement->save();
        $movement->createExpenditures();

        $request->session()->flash('success', 'movimiento confirmado con éxito');
        return redirect()->route('warehouse.requests.index');
    }

    private function createWarehouseMovementProducts($data, $warehouseMovement, $add = true) {

        foreach ($data['product_ids'] as $index => $id) {
    
            $warehouseMovementProduct = $warehouseMovement->products()->create([
            'warehouse_product_id' => $id,
            'quantity' => $data['product_quantities'][$index],
            'amount' => $data['product_amounts'][$index],
            'provider_price' => $data['product_prices'][$index],
            ]); 
    
            $product = WarehouseProduct::find($id);
            if($add) {
                $product->in_storage = $product->in_storage + $data['product_quantities'][$index]; 
            } else {
                $product->in_storage = $product->in_storage - $data['product_quantities'][$index]; 
            }
            $product->amount = $data['product_prices'][$index];
            $product->save();
        }
    }

    public function createMissingExpenditures() {
        $movements = WarehouseMovement::with('expenditures','products.product')->whereDoesntHave('expenditures')->where('created_by', 182)->get();
        // dd($movements);
        foreach($movements as $movement) {
            $movement->createExpendituresByDate($movement->updated_at);
        }
        dd("terminado");
    }

    public function destroy($id)
    {
        if($warehouseMovement = WarehouseMovement::with(['products', 'products.product'])->find($id)) {
            $user = Auth::user();
            $warehouseMovement->is_canceled = true;
            $warehouseMovement->canceled_by = $user->id;
            $warehouseMovement->save();
            foreach($warehouseMovement->products as $product) {
                $product->product->in_storage = $product->product->in_storage - $product->quantity;
                $product->product->save(); 
            }
            $warehouseMovement->deleteExpenditures();
            session()->flash('success', "El movimiento ha sido cancelado.");
            return back();
        }
        session()->flash('success', "Ocurrió un problema al cancelar el movimiento.");
        return back();
    }

    public function destroyProduct($id)
    {
        if($movementProduct = WarehouseMovementProduct::find($id)){
            $movementProduct->delete();
            return response()->json(['is_deleted' => true], 200);
        }
        return response()->json([
            'is_deleted' => false,
            'message' => 'Ocurrió un problema al eliminar el producto',
        ], 404);
    }
}
