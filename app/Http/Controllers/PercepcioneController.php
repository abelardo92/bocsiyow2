<?php

namespace App\Http\Controllers;

use App\Paysheet;
use App\Percepcione;
use Illuminate\Http\Request;

class PercepcioneController extends Controller
{

    public function store(Request $request)
    {
        $data = $request->all();
        $percepcione = Percepcione::create($data);
        $paysheet = Paysheet::find($data['paysheet_id']);

        if($paysheet) {
            $total= Percepcione::dates([$paysheet->start, $paysheet->end])->sum('amount');
            $paysheet->otros_ingresos = $total;
            $paysheet->save();
            $paysheet->makeTotals();
        }

        return response()->json([
            'success' => true,
            'paysheet_id' => $paysheet ? $paysheet->id : null,
            'message' => 'Percepcion agregada exitosamente',
        ], 200);
    }

    public function destroy(Request $request, $id)
    {
        $percepcione = Percepcione::find($id);
        $percepcione->delete();

        foreach (Paysheet::where('employee_id', $percepcione->employee_id)->get() as $paysheet) {
            $total= Percepcione::dates([$paysheet->start, $paysheet->end])->sum('amount');
            $paysheet->otros_ingresos = $total;
            $paysheet->save();
            $paysheet->makeTotals();
        }

        $request->session()->flash('success', 'Percepción eliminada con exito');
        return redirect()->back();
    }

}
