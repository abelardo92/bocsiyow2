<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Pending;
use App\Subsidiary;
use Carbon\Carbon;
use App\Traits\SubDomainHelper;
use App\Traits\ImageableTrait;

class PendingsController extends Controller
{
	use ImageableTrait;
	use SubDomainHelper;

    var $user = null;

    public function __construct() {
        $this->middleware(function ($request, $next){
            $this->user = Auth::user();
            return $next($request);
        });
    }

	public function create()
    {
        $user = Auth::user();
        $is_admin = false;
        return view('admin.pendings.create', compact('user', 'is_admin'));
    }

    public function createAdmin()
    {
        $user = Auth::user();
        $is_admin = true;
        return view('admin.pendings.create', compact('user', 'is_admin'));
    }

    public function show($id)
    {
        $user = Auth::user();
        $pending = Pending::with('subsidiary', 'createdBy')->find($id);
        $this->updateOpenedBy($user, $pending);
        $status = Pending::getEnum('status');
        return view('admin.pendings.show', compact('pending', 'user', 'status'));
    }

    private function updateOpenedBy($user, $pending) {
        if($user->id != $pending->created_by && $pending->opened_by == null) {
            $pending->opened_by = $user->id;
            $pending->update(); 
        }
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $data['created_by'] = $user->id;
        $data['date'] = Carbon::now()->format('Y-m-d');
        $data['status'] = 1;
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            $data['subsidiary_id'] = $subsidiary->id;
        } 
        $pending = Pending::create($data);
        $this->CreateFiles($pending, $request, "pendings/{$pending->id}");

        $request->session()->flash('success', 'Pendiente creado con éxito.');

        if($user->isA('cashier')) {
        	return redirect()->route('pendings.index');	
        }

        if(intval($data['is_admin']) == 1) {
            return redirect()->route('pendings.admin.nonFinished');
        }

        return redirect()->route('pendings.nonFinished');
    }

    public function edit($id)
    {
        $user = Auth::user();
        $pending = Pending::with('subsidiary', 'createdBy')->find($id);
        $is_admin = false;
        return view('admin.pendings.edit', compact('user', 'pending', 'is_admin'));
    }

    public function editAdmin($id)
    {
        $user = Auth::user();
        $pending = Pending::with('subsidiary', 'createdBy')->find($id);
        $is_admin = true;
        return view('admin.pendings.edit', compact('user', 'pending', 'is_admin'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user();
        $data['finished_by'] = $user->id;
        $data['finished_at'] = Carbon::now();
        $data['status'] = 3; 
        $pending = Pending::find($id); 
        $pending->update($data);
        $this->CreateFiles($pending, $request, "pendings/{$pending->id}");
        $request->session()->flash('success', 'Pendiente actualizado con éxito.');

        if($user->isA('cashier')) {
        	return redirect()->route('pendings.index');	
        }

        if(intval($data['is_admin']) == 1) {
            return redirect()->route('pendings.admin.finished');
        }

        return redirect()->route('pendings.finished');
    }

    public function index()
    {
        $user = Auth::user();
        $status = Pending::getEnum('status');
        $subsidiary_code = $this->hasSubdomain();
        $subsidiary = Subsidiary::byKey($subsidiary_code)->firstOrFail();
        $pendings = Pending::with('subsidiary', 'createdBy')->notAdmin()->where('subsidiary_id', $subsidiary->id)
        ->orderBy('id','desc')->paginate(8);
        $is_admin = false;
        return view('admin.pendings.index', compact('user', 'pendings', 'status', 'is_admin'));
    }

    public function indexAdmin()
    {
        $user = Auth::user();
        $status = Pending::getEnum('status');
        $subsidiary_code = $this->hasSubdomain();
        $subsidiary = Subsidiary::byKey($subsidiary_code)->firstOrFail();
        $pendings = Pending::with('subsidiary', 'createdBy')->admin()->where('subsidiary_id', $subsidiary->id)
        ->orderBy('id','desc')->paginate(8);
        $is_admin = true;
        return view('admin.pendings.index', compact('user', 'pendings', 'status', 'is_admin'));
    }

    public function nonFinished()
    {
        $user = $this->user;
        $status = Pending::getEnum('status');

        $pendings = Pending::with('subsidiary', 'createdBy')->notAdmin()->nonFinished();

        if($user->isA('cashier')) {
            $subsidiary_code = $this->hasSubdomain();
            $subsidiary = Subsidiary::byKey($subsidiary_code)->firstOrFail();
            $pendings = $pendings->where('subsidiary_id', $subsidiary->id);
        }

        if($user->isA('subsidiary-admin')) {
            $subsidiary = Subsidiary::find($this->user->access_subsidiary_id);
            $pendings = $pendings->where('subsidiary_id', $subsidiary->id);
        }

        $pendings = $pendings->orderBy('id','desc')->paginate(8);
        $is_admin = false;
        return view('admin.pendings.nonFinished', compact('user', 'pendings', 'status', 'is_admin'));
    }

    public function nonFinishedAdmin()
    {
        $user = $this->user;
        $status = Pending::getEnum('status');
        $pendings = Pending::with('subsidiary', 'createdBy')->admin()->nonFinished()->orderBy('id','desc')->paginate(8);
        $is_admin = true;
        return view('admin.pendings.nonFinished', compact('user', 'pendings', 'status', 'is_admin'));
    }

    public function finished()
    {
        $user = $this->user;
        $status = Pending::getEnum('status');
        $pendings = Pending::with('subsidiary', 'createdBy')->notAdmin()->finished();

        if($user->isA('cashier')) {
            $subsidiary_code = $this->hasSubdomain();
            $subsidiary = Subsidiary::byKey($subsidiary_code)->firstOrFail();
            $pendings = $pendings->where('subsidiary_id', $subsidiary->id);
        }

        if($user->isA('subsidiary-admin')) {
            $subsidiary = Subsidiary::find($this->user->access_subsidiary_id);
            $pendings = $pendings->where('subsidiary_id', $subsidiary->id);
        }

        $pendings = $pendings->orderBy('id','desc')->paginate(8);
        $is_admin = false;
        return view('admin.pendings.finished', compact('user', 'pendings', 'status', 'is_admin'));
    }

    public function finishedAdmin()
    {
        $user = Auth::user();
        $status = Pending::getEnum('status');
        $pendings = Pending::with('subsidiary', 'createdBy')->admin()->finished();
        if($user->isA('cashier')) {
            $subsidiary_code = $this->hasSubdomain();
            $subsidiary = Subsidiary::byKey($subsidiary_code)->firstOrFail();
            $pendings = $pendings->where('subsidiary_id', $subsidiary->id);
        }
        $pendings = $pendings->orderBy('id','desc')->paginate(8);
        $is_admin = true;
        return view('admin.pendings.finished', compact('user', 'pendings', 'status', 'is_admin'));
    }
}
