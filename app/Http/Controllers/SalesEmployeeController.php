<?php
namespace App\Http\Controllers;

use App\Attendance;
use App\Customer;
use App\Diary;
use App\Employee;
use App\Events\MakeSurvey;
use App\ExchangeRate;
use App\MiniCutPetition;
use App\Sale;
use App\SalePayment;
use App\SalePackage;
use App\SaleProduct;
use App\SaleService;
use App\Service;
use App\Subsidiary;
use App\Promotion;
use App\Product;
use App\Package;
use App\CashRegister;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Traits\SubDomainHelper;
use App\Image;
use App\Traits\ImageableTrait;
use App\Notifications\SaleMessage;

class SalesEmployeeController extends Controller
{
    use SubDomainHelper;
    use ImageableTrait;

    public function index()
    {
        $user = Auth::user();
        $pending_sales = Sale::with('products','employee','payments')->forEmployees()->notFinished()->notCanceled()->get();
        $paid_sales = Sale::with('products','employee','payments')->forEmployees()->finished()->notCanceled()->get();
        return view('admin.sales.employees.index', compact('pending_sales', 'paid_sales', 'sale_id'));
    }

    public function create()
    {
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        }

        if($subsidiary->id != 7) {
            $cash_register = $subsidiary->cashRegisters()->notDelayed()->open()->today()->first();           
        } else {
            $cash_register = CashRegister::with(['subsidiary'])->open()->delayed()->orderBy('id', 'desc')->first();
        }

        if ($cash_register == null) {
            session()->flash('success', 'No puedes crear una venta hasta que abras tu caja.');
            return redirect()->back();
        }

        $employees = Employee::active()->barbers()->get();
        $products = $cash_register->subsidiary->products->where('is_for_employee', 1);
        $user = Auth::user();

        return view('admin.sales.employees.create', compact('subsidiary', 'employees', 'products', 'user'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $success = false;

        if (empty($data['product_id'])) {
            $message = 'Necesitas seleccionar al menos un producto.';
            return response()->json(compact('success', 'message'));
        }

        foreach ($data['product_id'] as $product_id) {
            if($data['product_qty'][$product_id] == '0') {
                $message = 'Los productos seleccionado deben tener una cantidad mayor o igual a 1.';
                return response()->json(compact('success', 'message'));
            }
        }

        $subsidiary_code = $this->hasSubdomain();

        if(!$subsidiary_code) {
            $message = 'Debe estar dentro de una sucursal.';
            return response()->json(compact('success', 'message'));
        }
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();

        if (empty($data['employee_id'])) {
            $message = 'Necesitas seleccionar un empleado.';
            return response()->json(compact('success', 'message'));
        }

        $data['exchange_rate_id'] = ExchangeRate::latest()->first()->id;

        $data['diary_id'] = 0;
        $data['status'] = 'finalizado';
        $data['is_for_employee'] = true;

        if($subsidiary->id != 7) {
            $cash_register = $subsidiary->cashRegisters()->notDelayed()->open()->today()->first(); 
            $data['date'] = Carbon::now()->format('Y-m-d');          
        } else {
            $cash_register = CashRegister::with(['subsidiary'])->open()->delayed()->orderBy('id', 'desc')->first();
            $data['date'] = $cash_register->date;
        }

        $data['cash_register_id'] = $cash_register->id;

        $folio = 1;
        if ($last = $cash_register->subsidiary->sales()->orderBy('folio', 'DESC')->first()) {
            $folio = $last->folio + 1;
        }
        $data['folio'] = $folio;
        $sale = $cash_register->subsidiary->sales()->create($data);

        foreach ($data['product_id'] as $product_id) {

            $product = Product::with(['subsidiaryPrices'])->find($product_id);
            $productPrice = $product->currentPriceBySubsidiary($cash_register->subsidiary->id);

            //* Descuento del 20% para empleados unicamente
            $productPrice = ($productPrice * (100 - $product->employee_discount_percentaje)) / 100;

            $sale->products()->create([
                'product_id' => $product_id,
                'qty' => $data['product_qty'][$product_id],
                'price' => $productPrice,
                'original_price' => $product->sell_price,
                'discount_percentaje' => $product->employee_discount_percentaje,
            ]);
        }

        $sale->updateTotals();

        $success = true;
        $message = 'Producto(s) vendido(s) con exito';
        $sale_id = $sale->id;
        return response()->json(compact('success', 'message', 'sale_id'));
    }

    public function editPayments($id)
    {
        $sale = Sale::with(['customer','payments','products.product','services.service','subsidiary.products','employee'])->find($id);
        $products = $sale->subsidiary->products;
        return view('admin.sales.employees.editPayments', compact('sale', 'products'));
    }

    public function updatePayments(Request $request, $id)
    {
        $data = $request->all();
        $sale = Sale::with('employee')->find($id);
        $sale->update($data);

        $payments_total = 0;

        if (isset($data['methods'])) {
            $methods = $data['methods'];
            $coins = $data['coins']; 
            $totals = $data['totals']; 
            $dates = $data['dates']; 
            foreach ($data['payment_ids'] as $index => $id) {
                if($id != 0) {
                    $payment = SalePayment::find($id);
                    $payment->method = $methods[$index];
                    $payment->coin = $coins[$index]; 
                    $payment->total = $totals[$index]; 
                    $payment->cash_register_id = $sale->cash_register_id; 
                    $payment->save();
                } else {
                    $sale->payments()->create([
                        'method' => $methods[$index],
                        'coin' => $coins[$index],
                        'total' => $totals[$index],
                        'date' => $dates[$index],
                        'cash_register_id' => $sale->cash_register_id,
                    ]);
                }
                $payments_total += $totals[$index];
            }
        }
        
        if(isset($data['product_ids'])) {
            $product_ids = $data['product_ids'];
            $qtys = $data['product_qtys'];
            foreach($data['saleproduct_ids'] as $index => $id) {
                $product = Product::find($product_ids[$index]);

                $new_sell_price = $product->sell_price * 0.8; // 20% off 

                if($id != 0) {
                    $saleproduct = SaleProduct::find($id);
                    $saleproduct->product_id = $product->id;
                    $saleproduct->qty = $qtys[$index]; 
                    $saleproduct->price = $new_sell_price;
                    $saleproduct->original_price = $product->sell_price;
                    $saleproduct->save();
                } else {
                    $sale->products()->create([
                        'product_id' => $product->id,
                        'qty' => $qtys[$index],
                        'disabled' => 0,
                        'price' => $new_sell_price,
                        'original_price' => $product->sell_price,
                    ]);
                }
            }
        }

        $sale->updateTotals();
        if($payments_total >= $sale->total) {
            $sale->paid = true;
            $sale->finish = true;
            $sale->status = 'paid';
            $sale->save();
        }

        $request->session()->flash('success', 'Folio actualizado con éxito');
        return redirect()->back();
    }

    public function destroy(Request $request, $folio)
    {
        $user = Auth::user();
        $subsidiary_code = $this->hasSubdomain();
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();

        if (!$sale = $subsidiary->sales()->with(['customer'])->where('folio', $folio)->first()) {
            $request->session()->flash('error', 'No encontramos ventas con este folio.');
            return redirect()->route('sales.employees', $subsidiary_code);
        }

        if($sale->canceled_at != null || $sale->canceled_by != null){
            $request->session()->flash('error', 'La nota '.$sale->folio. ' ya está cancelada.');
            return redirect()->route('sales.employees', $subsidiary_code);
        }

        $sale->canceled_at = Carbon::now();
        $user_id = $user->id;
        $sale->canceled_by = $user_id;

        $sale->save();

        $request->session()->flash('success', 'Venta cancelada con exito');
        return redirect()->route('sales.employees', $subsidiary_code);
    }
}