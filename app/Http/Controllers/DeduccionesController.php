<?php

namespace App\Http\Controllers;

use App\Deduccione;
use App\Paysheet;
use Illuminate\Http\Request;

class DeduccionesController extends Controller
{

    public function store(Request $request)
    {
        $data = $request->all();
        $deduccione = Deduccione::create($data);
        $paysheet = Paysheet::find($data['paysheet_id']);

        if($paysheet) {
            $total= Deduccione::dates([$paysheet->start, $paysheet->end])->sum('amount');
            $paysheet->otras = $total;
            $paysheet->save();
            $paysheet->makeTotals();
        }

        return response()->json([
            'success' => true,
            'paysheet_id' => $paysheet ? $paysheet->id : null,
            'message' => 'Deduccion agregada exitosamente',
        ], 200);
    }

    public function destroy(Request $request, $id)
    {
        $deduccione = Deduccione::find($id);
        $deduccione->delete();
        foreach (Paysheet::where('employee_id', $deduccione->employee_id)->get() as $paysheet) {
            $total= Deduccione::dates([$paysheet->start, $paysheet->end])->sum('amount');
            $paysheet->otras = $total;
            $paysheet->save();
            $paysheet->makeTotals();
        }

        $request->session()->flash('success', 'Deduccion eliminada con exito');
        return redirect()->back();
    }

}
