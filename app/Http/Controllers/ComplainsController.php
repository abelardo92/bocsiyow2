<?php

namespace App\Http\Controllers;

use App\Complain;
use App\Subsidiary;
use App\Sale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ComplainsController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        if($user->isA('super-admin')){
            $complains = Complain::orderBy('date', 'desc')->get();
        } else {
            $complains = Complain::where('date',date('Y-m-d'))->where('user_id', $user->id)->get();
        }
        return view('admin.complains.index', compact('complains'));
    }

    public function create()
    {
        return view('admin.complains.create');
    }

    public function createCustomer()
    {
        return view('admin.complains.createCustomer');
    }

    public function store2(Request $request)
    {
        $data = $request->all();
        if (!preg_match('/[0-9]{2}+-+[0-9]{1,8}/', $data['folio'])){
            $request->session()->flash('error', 'Formato de folio incorrecto, Ej: 04-1234');
            return redirect()->back();
        }

        list($subsidiary_key,$folio) = explode("-",$data['folio']);
        $subsidiary = Subsidiary::where('key',$subsidiary_key)->get()->first();
        $sale = Sale::where('folio',$folio)->get()->first();

        if($subsidiary == null || $sale == null){
            $request->session()->flash('error', 'El folio proporcionado no existe');
            return redirect()->back();
        }

        $data['folio'] = $folio;
        $data['subsidiary_id'] = $subsidiary->id;

        $complain = Complain::create($data);
        $request->session()->flash('success', 'Queja dada de alta con exito.');
        return redirect()->back();
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();

        if (!preg_match('/[0-9]{2}+-+[0-9]{1,8}/', $data['folio'])){
            $request->session()->flash('error', 'Formato de folio incorrecto, Ej: 04-1234');
            return redirect()->back();
        }

        list($subsidiary_key,$folio) = explode("-",$data['folio']);
        $subsidiary = Subsidiary::where('key',$subsidiary_key)->get()->first();
        $sale = Sale::where('folio',$folio)->get()->first();

        if($subsidiary == null || $sale == null){
            $request->session()->flash('error', 'El folio proporcionado no existe');
            return redirect()->back();
        }

        $data['folio'] = $folio;
        $data['subsidiary_id'] = $subsidiary->id;
        $data['user_id'] = $user->id;
        $complain = Complain::create($data);
        $request->session()->flash('success', 'Queja dada de alta con exito.');
        return redirect()->route('complains.edit', $complain->id);
    }

    public function edit(Request $request, $id)
    {
        $complain = Complain::find($id);
        $subsidiary = Subsidiary::find($complain->subsidiary_id);
        $folio = $subsidiary->key . "-" . $complain->folio;
        return view('admin.complains.edit', compact('complain','folio'));
    }

    public function show(Request $request, $id)
    {
        $user = Auth::user();
        $complain = Complain::find($id);
        if($complain->opened_by == null) {
            $complain->opened_by = $user->id;
            $complain->update(); 
        }
        $subsidiary = Subsidiary::find($complain->subsidiary_id);
        $folio = $subsidiary->key . "-" . $complain->folio;
        return view('admin.complains.show', compact('complain','folio'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        if (!preg_match('/[0-9]{2}+-+[0-9]{1,8}/', $data['folio'])){
            $request->session()->flash('error', 'Formato de folio incorrecto, Ej: 04-1234');
            return redirect()->back();
        }

        list($subsidiary_key,$folio) = explode("-",$data['folio']);
        $subsidiary = Subsidiary::where('key',$subsidiary_key)->get()->first();
        $sale = Sale::where('folio',$folio)->get()->first();

        if($subsidiary == null || $sale == null){
            $request->session()->flash('error', 'El folio proporcionado no existe');
            return redirect()->back();
        }

        $data['folio'] = $folio;
        $data['subsidiary_id'] = $subsidiary->id;
        $complain = Complain::find($id); 
        $complain->update($data);
        $request->session()->flash('success', 'queja actualizada con éxito.');
        return redirect()->route('complains.edit', $complain->id);
    }

    public function destroy(Request $request, $id)
    {
        $complain = Complain::find($id);
        $complain->delete();
        $request->session()->flash('success', 'Queja eliminada con exito.');
        return back();
    }
}
