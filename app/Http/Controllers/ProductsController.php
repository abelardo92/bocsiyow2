<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests;
use App\Image;
use App\Product;
use App\Subsidiary;
use App\Traits\ImageableTrait;
use App\Traits\SubDomainHelper;
use Illuminate\Http\Request;
use App\SubsidiaryProductPrice;
use Silber\Bouncer\Database\Role;

class ProductsController extends Controller
{
    use ImageableTrait;
    use SubDomainHelper;

    public function index(Request $request)
    {
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            if($subsidiary->is_laundry){
                $products = Product::laundry()->active()->paginate(15);
                $products_deleted = Product::laundry()->notActive()->paginate(15);
                return view('admin.products.index', compact('products', 'products_deleted'));
            }
        }
        $products = Product::notLaundry()->active()->paginate(15);
        $products_deleted = Product::notLaundry()->notActive()->paginate(15);
        return view('admin.products.index', compact('products', 'products_deleted'));
    }

    public function create()
    {
        return view('admin.products.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            if($subsidiary->is_laundry) {
                $data['is_laundry'] = true;
                $subsidiaries = Subsidiary::active()->laundry()->get();
            } else {
                $subsidiaries = Subsidiary::active()->notLaundry()->get();
            }
        } else {
            $subsidiaries = Subsidiary::active()->notLaundry()->get();
        }
        $product = Product::create($data);

        $this->CreateFile($product, $request, "products/{$product->id}");

        foreach ($subsidiaries as $subsidiary) {
            $pid = $product->id;
            $subsidiary->products()->syncWithoutDetaching([
                $pid => [
                    'existence' => 0
                ]
            ]);
        }
        
        $request->session()->flash('success', 'Producto dado de alta con exito.');
        return redirect()->route('products.edit', $product->id);
    }

    public function edit(Request $request, $id)
    {
        $product = Product::with('subsidiaryPrices.subsidiary')->find($id);
        $subsidiaries = $this->getSubsidiaries();
        return view('admin.products.edit', compact('product', 'subsidiaries'));
    }

    public function updateSubsidiaryPrices(Request $request)
    {
        $product = Product::with('subsidiaryPrices')->find($request->product_id);

        // delete all subsidiary prices
        foreach($product->subsidiaryPrices as $subsidiary_price) {
            $subsidiary_price->delete();
        }

        // insert all new information
        foreach($request->subsidiary_prices as $subsidiary_price) {
            SubsidiaryProductPrice::create($subsidiary_price);
        }

        $message = 'Precios actualizados con éxito.';
        return response()->json(compact('message'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $product = Product::find($id);
        $product->update($data);
        $this->CreateFile($product, $request, "products/{$product->id}");

        $request->session()->flash('success', 'Producto actualizado con éxito.');
        return redirect()->route('products.edit', $product->id);
    }

    public function getByKey($key)
    {
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            $product = $subsidiary->products()->whereKey($key)->first();
            if(!$product){
                $product = Product::whereKey($key)->first();
                $product->existence = 0;
            }
        }else {
            $product = Product::whereKey($key)->first();     
        }
        
        if ($product) {
            if ($subsidiary_code = $this->hasSubdomain()) {
                if($productPivot = $product->pivot){
                    $product->existence = $productPivot->existence;
                }            
            }
            return response()->json([
                'success' => true,
                'product' => $product,
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'No encontramos ni un producto con esta clave.',
        ], 404);
        
    }

    public function search($subsidiary_code, $id)
    {
        $product = Product::find($id);
        return $product;
    }

    private function getSubsidiaries() {
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            if($subsidiary->is_laundry){
                return Subsidiary::active()->laundry()->get();
            }
        }
        return Subsidiary::active()->notLaundry()->get();
    }

    public function destroy(Request $request, $id)
    {
        $product = Product::find($id);
        $product->active = false;
        $product->save();

        $request->session()->flash('success', 'Producto dado de baja con éxito');
        return redirect()->route('products.index');
    }

    public function active(Request $request, $id)
    {
        $product = Product::find($id);
        $product->active = true;
        $product->save();

        $request->session()->flash('success', 'Producto dado de alta con éxito.');
        return back();
    }
}
