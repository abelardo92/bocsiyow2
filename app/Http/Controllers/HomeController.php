<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\AuthToken;
use App\CashRegister;
use App\Customer;
use App\Departure;
use App\Employee;
use App\ExchangeRate;
use App\Manual;
use App\Product;
use App\Package;
use App\Diary;
use App\Sale;
use App\Service;
use App\LaundryService;
use App\Subsidiary;
use App\Traits\SubDomainHelper;
use App\Turn;
use App\Pending;
use App\Promotion;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\BirthdayNotification;
use App\Mail\FacturaNotification;
use App\Mail\NewClientNotification;
use App\Notifications\DiaryBirtdayMessage;
use App\Notifications\WelcomeMessage;
use App\Notifications\DiarySchedule;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Notifications\SaleMessage;

class HomeController extends Controller
{
    use SubDomainHelper;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exchange_rate = ExchangeRate::latest()->first();
        $barbers = collect([]);
        $sales = collect([]);
        $cuts = collect([]);
        $laundry_services = null;
        $packages = null;
        $now = Carbon::now();
        $turns = Turn::whereIn('identifier', [1, 2])->get();
        $subsidiaries = $this->getSubsidiaries();
        $user = Auth::user();

        if ($subsidiary = $this->getSubsidiary()) {
            $time_average = $subsidiary->getTranscurredTimeAverage();

            if($subsidiary->id != 7) {
                $cash_register = $subsidiary->cashRegisters()->with(['movements'])->notDelayed()->open()->today()->first();
            } else {
                $cash_register = CashRegister::with(['movements', 'subsidiary'])->open()->delayed()->orderBy('id', 'desc')->first();
            }

            if ($cash_register != null) {
                $sales = $cash_register->sales()->with(['subsidiary','employee2','packages', 'customer', 'customerSubsidiary', 'image'])->finished()->orderBy('folio', 'desc')->get();

                if($subsidiary->is_laundry) {
                    //optener ventas que contengan algun pago en esa caja
                    $cash_register_id = $cash_register->id;
                    $sales_additional = Sale::with('subsidiary','payments','finishedCashRegister')
                    ->finished()->notCanceled()->where('cash_register_id', '!=', $cash_register_id)
                    ->whereHas('payments', function($q) use ($cash_register_id) {
                        $q->where('cash_register_id', $cash_register_id);
                    })->get();
                    $sales = $sales->merge($sales_additional);
                }

                if($subsidiary && $subsidiary->id == 11) {
                    $laundry_services = $cash_register->laundryServices()->with(['customerSubsidiary'])->where('date',$now->toDateString())->get();
                }
            }

            $cuts = $subsidiary->cashRegisters()->notDelayed()->close()->today()
            ->get()->map(function ($cash_register) {
                return $cash_register->movements()->with('cashRegister.turn')->get()->last();
            });

            $employee_ids = $subsidiary->sales()->with(['employee', 'customer', 'services.service', 'products.product', 'packages.package', 'diary', 'customerSubsidiary'])->today()->active()->finish(false)->get()->map(function ($sale) {
                return $sale->employee_id;
            })->toArray();

            if($subsidiary->is_laundry) {

                $attendances = Attendance::select('employee_id', DB::raw('COUNT(*) as count_attend'))
                ->where('subsidiary_id', $subsidiary->id)
                ->today()
                ->groupBy('employee_id')
                ->get()
                ->whereIn('count_attend', [1, 3]);

                $services = Service::with(['image','promotionservices','promotionservices.promotion'])->laundry()->get();
                $employees = $attendances->where('employee.job', 'Lavador');
                $employees = $employees->map(function ($key) {
                return $key->employee;
                })->filter(function ($employee) use ($employee_ids) {
                return !in_array($employee->id, $employee_ids);
                });
            } else {
                $services = Service::with(['image',
                'promotionServices2' => Service::filterPromotionServices($subsidiary->id),
                'promotionServices2.promotion',
                'subsidiaryPrices'])->bySubsidiary($subsidiary->id)->notLaundry()->get();

                $employees = Employee::with('todayAttendances')->where('job', 'Barbero')->get();
                $subsidiary_id = $subsidiary->id;

                foreach($services as $service) {
                    $service->setActivePromotionAttribute($subsidiary_id);
                }

                $employees = $employees->filter(function ($employee) use ($subsidiary_id) {

                    $todayAttendances = $employee->todayAttendances->where('subsidiary_id', $subsidiary_id);
                    $hasAttendances = $todayAttendances->count() > 0;

                    $isResting = false;
                    if($hasAttendances) {
                        $isResting = $todayAttendances->where('type', 'entrada descanso')->first() && !$todayAttendances->where('type', 'salida descanso')->first();
                    }

                    return $hasAttendances && $todayAttendances->first()->subsidiary_id == $subsidiary_id &&
                    !$todayAttendances->where('type', 'salida')->first() && !$isResting;
                });
                $employees = $employees->filter(function ($employee) use ($employee_ids) {
                    return !in_array($employee->id, $employee_ids);
                });
                if($subsidiary->isBocsiyow()) {
                    $employees = $employees->merge([46 => Employee::find(46)]);
                }
            }

            if($subsidiary->is_laundry) {
                $packages = Package::all();
            }
            $current_promotion = $this->activePromotion($subsidiary->id);
        } else {
            $current_promotion = $this->activePromotion();
            $services = Service::with(['image','promotionservices','promotionservices.promotion'
            // 'promotionServices2' => Service::filterPromotionServices($subsidiary->id),
            // 'promotionServices2.promotion',
            ])->laundry();
            if($subsidiary) {
                $services = $services->bySubsidiary($subsidiary->id);
            }
            $services = $services->get();
        }

        $cash_registers = CashRegister::with(['sales','subsidiary'])->today();
        $cash_registers_yesterday = CashRegister::yesterday();

        if($user->access_subsidiary_id != null){
            $cash_registers = $cash_registers->where('subsidiary_id',$user->access_subsidiary_id);
            $cash_registers_yesterday = $cash_registers_yesterday->where('subsidiary_id',$user->access_subsidiary_id);
        }

        $cash_registers = $cash_registers->get()->sortBy(function ($cash_regiter, $key) {
            return $cash_regiter->subsidiary->name;
        });

        $cash_registers_yesterday = $cash_registers_yesterday->get()->sortBy(function ($cash_regiter, $key) {
            return $cash_regiter->subsidiary->name;
        });

        $pendings = Pending::with('subsidiary')->nonFinished()->orderBy('id', 'desc')->get();
        // $current_promotion = $this->activePromotion();
        return view('home', compact('services', 'exchange_rate', 'employees', 'sales', 'subsidiaries', 'cash_registers', 'cash_registers_yesterday', 'cuts', 'turns', 'laundry_services','user', 'packages', 'pendings', 'time_average', 'current_promotion'));
    }

    public function activePromotion($subsidiary_id = null)
    {
        $promotion_subsidiary = Promotion::with('promotionservices','promotionProducts','promotionServiceExceptions', 'promotionProductExceptions')->where('is_active', true)
                ->whereDate('start_date','<=',date('Y-m-d'))
                ->whereDate('end_date','>=',date('Y-m-d'))
                ->where('start_time','<=',date('H:i:s'))
                ->where('end_time','>=',date('H:i:s'))
                ->where('subsidiary_id', $subsidiary_id)
                ->where(strtolower(date('l')),true)->get()->first();

        if($promotion_subsidiary) {
            return $promotion_subsidiary;
        }

        $promotion = Promotion::with('promotionservices','promotionProducts','promotionServiceExceptions', 'promotionProductExceptions')->where('is_active', true)
                ->whereDate('start_date','<=',date('Y-m-d'))
                ->whereDate('end_date','>=',date('Y-m-d'))
                ->where('start_time','<=',date('H:i:s'))
                ->where('end_time','>=',date('H:i:s'))
                ->whereNull('subsidiary_id')
                ->where(strtolower(date('l')),true)->get()->first();
        return $promotion;
    }

    public function testMessage() {
        $user = Auth::user();
        if($user->isA('super-admin')) {
            $date = Carbon::now()->addDay()->format('Y-m-d');
            //$customers = Customer::where('birthday', $date)->get();
            //$customers = Customer::get()->first();
            $customer = Customer::where('id', 18162)->get()->first();
            //dd($customer);
            //foreach($customers as $customer) {
                Mail::to('abelardo-0@hotmail.com')->send(new FacturaNotification(123456));
                Mail::to('facturasbocsiyow@outlook.es')->send(new FacturaNotification(123456));
                // Mail::to('abelardo-0@hotmail.com')->send(new BirthdayNotification($customer, $date));
                // Mail::to('abelardinii@gmail.com')->send(new BirthdayNotification($customer, $date));
                // Mail::to('facturasbocsiyow@outlook.es')->send(new NewClientNotification($customer, $date));
                // Mail::to('abelardo-0@hotmail.com')->send(new NewClientNotification($customer, $date));
                //Mail::to('abelardo-0@hotmail.com')->send(new BirthdayNotification($customer, $date));
            //}
            //$request->session()->flash('success', 'mensaje enviado.');
        }
        return redirect()->to("/home/");

    }

    public function testNexmoMessage() {
        $user = Auth::user();
        if($user->isA('super-admin')) {
            $date = Carbon::now()->addDay()->format('Y-m-d');
            $customer = Customer::where('id', 18162)->get()->first();

            try {
                // $customer->notify(new DiaryBirtdayMessage);
                // $customer->notify(new WelcomeMessage($customer));
                // if($sale = Sale::with(['services','products','packages','customer'])->find(100000)) {
                //     $customer->notify(new SaleMessage($sale));
                // }
                if ($customer->validPhone()) {
                    $diary = Diary::with('customer','subsidiary')->find(53932);
                    // dd($customer);
                    $customer->notify(new DiarySchedule($diary));
                    echo 'Mensaje de prueba enviado.';
                }
            } catch (\Exception $e) {
                echo 'Error al enviar mensaje de prueba: '.$e->getMessage();
            }
            exit;
        }
    }

    public function sales()
    {
        $subsidiaries = $this->getSubsidiaries();
        return view('home.sales', compact('subsidiaries'));
    }

    public function getSubsidiary()
    {
        if ($subsidiary_code = $this->hasSubdomain()) {
            return Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        }
        return false;
    }

    public function searchForSale(Request $request, $folio)
    {
        $subsidiary = Subsidiary::find($request->subsidiary_id);
        if (!$sale = $subsidiary->sales()->with(['employee', 'customer', 'services.service', 'products.product', 'diary'])->where('folio', $folio)->first()) {
            return response()->json(['message' => 'No encontramos ventas con este folio en esta sucursal.'], 404);
        }
        return response()->json([
            'message' => '',
            'sale' => $sale,
        ]);
    }

    public function destroySale(Request $request, $id)
    {
        $sale = Sale::find($id);
        $canceled_description = $request->canceled_description;
        if($sale->canceled_at != null || $sale->canceled_by != null){
            return response()->json([
            'message' => 'La nota '.$sale->folio. ' ya está cancelada.',
            'sale' => $sale,
            ]);
        }

        $sale->canceled_at = Carbon::now();
        $sale->canceled_by = $request->user()->id;
        $sale->canceled_description = $canceled_description;

        if($sale->customer->visits == 0 && $sale->customer->services_free > 0){
            $sale->customer->setVisits(9);
            $sale->customer->removeServicesFree();
        } else{
            $sale->customer->removeVisit();
        }

        if($sale->payments()->where('method','Monedero')->first()){
            $sale->customer->addServicesFree();
        }

        $sale->save();

        $subsidiary = $sale->subsidiary;
        foreach ($sale->products as $sale_product) {
            $pid = $sale_product->product_id;
            $departure = Departure::where('sale_id', $sale->id)->first();
            $departure->delete();

            $product_find = $subsidiary->products()->find($pid);
            $existence = $product_find->pivot->existence + $sale_product->qty;
            $subsidiary->products()->syncWithoutDetaching([
                $pid => [
                    'existence' => $existence,
                ],
            ]);

            $sale_product->product->updateExistence($sale_product->qty, 'increment');
        }
        if ($payment = $sale->payments()->where('method', 'Efectivo')->first()) {
            $cashRegister = $sale->cashRegister;
            $toto = $payment->total - $sale->money_change;
            $cashRegister->total = $cashRegister->total - $toto;
            $cashRegister->save();
        }

        return response()->json([
            'message' => 'Nota cancelada con éxito',
            'sale' => $sale,
        ]);
    }

    public function getSubsidiaryUrl(Request $request, $id)
    {
        $subsidiary = Subsidiary::find($id);
        $token = AuthToken::create([
            'token' => str_random(60),
            'user_id' => $request->user()->id,
        ]);

        return response()->json([
            'message' => '',
            'url' => route('home.sucursal.login', $subsidiary->key) . "?token={$token->token}",
        ]);
    }

    public function authSubsidiaryAsSuperUser(Request $request)
    {
        try {
            $token = AuthToken::whereToken($request->token)->where('used', false)->firstOrFail();
            $token->used = true;
            $token->save();
            Auth::login($token->user);

            return redirect('/home');
        } catch (ModelNotFoundException $e) {
            return redirect('/');
        }
    }

    public function operationsManual()
    {
        $type = request('type', 'Cajero');
        $user_manuals = Manual::whereNull('cashier_id')->where('type', $type)->orderBy('user_id')->groupBy('user_id')->get();
        $cashier_manuals = Manual::whereNull('user_id')->where('type', $type)->orderBy('cashier_id')->groupBy('cashier_id')->get();
        $creators = $user_manuals->merge($cashier_manuals);

        return view('operations-manual', compact('creators', 'type'));
    }

    public function postOperationsManual(Request $request)
    {
        $data = $request->all();
        if ($request->user()->isA('super-admin')) {
            $data['user_id'] = $request->user()->id;
        } else {
            $subsidiary_code = $this->hasSubdomain();
            $subsi = Subsidiary::whereKey($subsidiary_code)->first();
            if ($subsi) {
                if ($cash_register = $subsi->cashRegisters()->open()->today()->first()) {
                    $data['cashier_id'] = $cash_register->employee->id;
                } else {
                    $data['user_id'] = $request->user()->id;
                }
            } else {
                $data['user_id'] = $request->user()->id;
            }
        }
        $manual = Manual::create($data);
        return back();
    }

    public function deleteOperationsManual(Request $request)
    {
        $manual = Manual::find($request->id);
        $type = $manual->type;
        $manual->delete();
        return redirect()->to("/home/operations-manual?type={$type}");
    }

    public function editOperationsManual($id)
    {
        $manual = Manual::find($id);
        return view('operations-manual-edit', compact('manual'));
    }

    public function updateOperationsManual(Request $request, $id)
    {
        $data = $request->all();
        $manual = Manual::find($id);
        $manual->update($data);
        return redirect()->to("/home/operations-manual?type={$manual->type}");
    }

    private function getSubsidiaries() {
        $subsidiaries = Subsidiary::with('todaySales.services')->active();
        $user = Auth::user();
        if($user->access_subsidiary_id != null){
            $subsidiaries = $subsidiaries->where('id',$user->access_subsidiary_id);
        }
        return $subsidiaries->get();
    }

    public function infoo()
    {
        $current_date_time = Carbon::now();
        // echo print_r(openssl_get_cert_locations()); exit;
        echo "GIT: 20240416" . shell_exec("git log -1 --pretty=format:'%h - %s (%ci)' --abbrev-commit");
        echo phpinfo();
    }
}
