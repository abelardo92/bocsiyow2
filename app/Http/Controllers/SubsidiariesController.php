<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests;
use App\Subsidiary;
use App\Configuration;
use App\Traits\ImageableTrait;
use App\User;
use Illuminate\Http\Request;

class SubsidiariesController extends Controller
{
    use ImageableTrait;

    public function index(Request $request)
    {
        $subsidiaries = Subsidiary::orderBy('key', 'asc')->paginate(15);
        return view('admin.subsidiaries.index', compact('subsidiaries'));
    }

    public function create()
    {
        $configurations = Configuration::all();
        return view('admin.subsidiaries.create', compact('configurations'));
    }

    public function store(Request $request)
    {
        $data = $request->all();      
        $subsidiary = Subsidiary::create($data);
        $this->CreateFile($subsidiary, $request, "subsidiaries/{$subsidiary->id}"); 
        $request->session()->flash('success', 'Sucursal dada de alta con exito.');
        return redirect()->route('subsidiaries.edit', $subsidiary->id);
    }

    public function edit(Request $request, $id)
    {
        $subsidiary = Subsidiary::find($id);
        $configurations = Configuration::all();
        $users = User::noInSubsidiary()->get();
        $employees = Employee::noInSubsidiary()->get();

        return view('admin.subsidiaries.edit', compact('subsidiary', 'users', 'employees', 'configurations'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $subsidiary = Subsidiary::find($id);
        $subsidiary->update($data);
        $this->CreateFile($subsidiary, $request, "subsidiaries/{$subsidiary->id}");
        $request->session()->flash('success', 'Sucursal actualizada con exito.');
        return redirect()->route('subsidiaries.edit', $subsidiary->id);
    }

    public function asignUsers(Request $request, $id)
    {
        $data = $request->all();
        foreach ($data['users'] as $userId) {
            $user = User::find($userId);
            $user->subsidiary_id = $id;
            $user->save();
        }

        $request->session()->flash('success', 'Usuarios asignados con exito.');
        return back();
    }

    public function asignEmployees(Request $request, $id)
    {
        $data = $request->all();
        foreach ($data['employees'] as $employeeId) {
            $employee = Employee::find($employeeId);
            $employee->subsidiary_id = $id;
            $employee->save();
        }

        $request->session()->flash('success', 'Usuarios asignados con exito.');
        return back();
    }
}
