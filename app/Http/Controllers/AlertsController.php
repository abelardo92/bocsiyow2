<?php

namespace App\Http\Controllers;

use App\Alert;
use App\Subsidiary;
use App\Traits\SubDomainHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AlertsController extends Controller
{
    use SubDomainHelper;

    public function index()
    {
        $alerts = Alert::active()->get();
        $inactive_alerts = Alert::notActive()->get();
        return view('admin.alerts.index', compact('alerts','inactive_alerts'));
    }

    public function todayAlerts()
    {
        $alerts = Alert::active()
        ->where(strtolower(date('l')), true);

        if ($subsidiary = $this->getSubsidiary()) {
            $subsidiary_id = $subsidiary->id;
            $alerts = $alerts->where(function ($q) use ($subsidiary_id) {
                $q->where('subsidiary_id', null)
                ->orWhere('subsidiary_id', $subsidiary_id);
            });
        }

        $alerts = $alerts->get();

        return response()->json([
            'success' => true,
            'alerts' => $alerts
        ]);
    }


    public function create()
    {
        $subsidiaries = $this->getSubsidiaries();
        return view('admin.alerts.create', compact('subsidiaries'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if(empty($request->subsidiary_id)) {
            $data['subsidiary_id'] = null;
        }
        $alert = Alert::create($data);
        $request->session()->flash('success', 'Alerta creada con éxito.');
        return redirect()->route('alerts.edit', $alert->id);
    }

    public function edit(Request $request, $id)
    {
        $alert = Alert::find($id);
        $subsidiaries = $this->getSubsidiaries();
        return view('admin.alerts.edit', compact('alert', 'subsidiaries'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        if(empty($request->subsidiary_id)) $data['subsidiary_id'] = null;

        // validate days
        $data['monday'] = $request->monday ? $request->monday : 0; 
        $data['tuesday'] = $request->tuesday ? $request->tuesday : 0; 
        $data['wednesday'] = $request->wednesday ? $request->wednesday : 0; 
        $data['thursday'] = $request->thursday ? $request->thursday : 0; 
        $data['friday'] = $request->friday ? $request->friday : 0; 
        $data['saturday'] = $request->saturday ? $request->saturday : 0; 
        $data['sunday'] = $request->sunday ? $request->sunday : 0; 

        $data['time'] = date("H:i", strtotime($data['time']));

        $alert = Alert::find($id);
        $alert->update($data);

        $request->session()->flash('success', 'Alerta actualizada con éxito.');
        return redirect()->route('alerts.edit', $alert->id);
    }

    public function destroy(Request $request, $id)
    {
        $alert = Alert::find($id);
        $alert->update(['is_active' => false]);
        $request->session()->flash('success', 'Alerta dada de baja con éxito');
        return redirect()->route('alerts.index');
    }

    public function active(Request $request, $id)
    {
        $alert = Alert::find($id);
        $alert->update(['is_active' => true]);
        $request->session()->flash('success', 'Alerta dada de alta con éxito.');
        return back();
    }

    public function getSubsidiary()
    {
        if ($subsidiary_code = $this->hasSubdomain()) {
            return Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        }
        return false;
    }

    private function getSubsidiaries(){
        $subsidiaries = Subsidiary::where('is_active', true);
        $user = Auth::user();
        if($user->access_subsidiary_id != null){
            $subsidiaries = $subsidiaries->where('id',$user->access_subsidiary_id);
        }
        return $subsidiaries->get();
    }
}
