<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests;
use App\Subsidiary;
use App\User;
use Illuminate\Http\Request;
use Silber\Bouncer\Database\Role;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    var $user = null;

    public function __construct() {
        $this->middleware(function ($request, $next){
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $data = $request->all();
        $users = null;

        if($request->has('filter')) {
            $filter = $data['filter'];
            $users = User::where('name', 'LIKE', '%'.$filter.'%')
            ->orWhere('email', 'LIKE', '%'.$filter.'%')
            ->orWhereHas('subsidiary', function($q) use ($filter) {
                $q->where('name', 'LIKE', '%'.$filter.'%');
            })
            ->orWhereHas('roles', function($q) use ($filter) {
                $q->where('name', 'LIKE', '%'.$filter.'%');
            });
        }

        if($this->user->access_subsidiary_id != null) {
            $users = $users ? $users->where('subsidiary_id', $this->user->access_subsidiary_id) : User::where('subsidiary_id', $this->user->access_subsidiary_id);
        }

        if(!$users) {
            $users = User::paginate(15);
        } else {
            $users = $users->paginate(15);
        }

        $currentUser = $this->user;
        return view('admin.users.index', compact('users', 'currentUser', 'filter'));
    }

    public function create()
    {
        if($this->user->isA('super-admin')) {
            $roles = Role::all();
        } else {
            $roles = Role::where('name', '!=', 'super-admin')->get();
        }
        $subsidiaries = $this->getSubsidiaries();
        return view('admin.users.create', compact('roles', 'subsidiaries'));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        if($user = $this->findAnUser($data)){
            $request->session()->flash('error', 'El correo escrito ya existe, escriba un correo diferente');
            return back();
        }

        $user = $this->createUser($data);
        $user->assign($data['role_id']);
        if($data['role_id'] == 'subsidiary-admin') {
            $user->access_subsidiary_id = $data['subsidiary_id'];
            $user->save();
        }
        $request->session()->flash('success', 'Usuario dado de alta con exito.');

        if ($request->has('from_employee')) {
            $employee = Employee::find($data['employee_id']);
            $employee->user_id = $user->id;
            $employee->save();
            return back();
        }

        return redirect()->route('users.edit', $user->id);
    }

    public function edit(Request $request, $id)
    {
        $user = User::find($id);
        $roles = Role::all();
        $subsidiaries = Subsidiary::active()->get();

        if($this->user->isA('super-admin')) {
            $roles = Role::all();
        } else {
            $roles = Role::where('name', '!=', 'super-admin')->get();
        }

        if($this->user->isA('manager','super-admin-restringido') && $user->isA('super-admin')) {
            $request->session()->flash('error', 'No puedes editar la informacion de un super administrador');
            return back();
        }
        return view('admin.users.edit', compact('user', 'roles', 'subsidiaries'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $used_user = User::where('id','!=',$id)->where('email', $data['email'])->first();
        if($used_user) {
            $request->session()->flash('error', 'El correo ya esta en uso');
            return back();
        }

        if (empty($data['password'])) {
            $key = array_search('password', $data);
            unset($key);
        }else{
            $data['password'] = bcrypt($data['password']);
        }
        $user = User::find($id);
        if (!isset($data['from_employee'])) {
            $rol = Role::whereName($user->roles()->first()->name)->first();
            $user->retract($rol->name);
            $user->assign($data['role_id']);
        }

        if(isset($data['role_id']) && $data['role_id'] == 'subsidiary-admin') {
            $user->access_subsidiary_id = $data['subsidiary_id'];
        } else {
            $user->access_subsidiary_id = null;
        }

        $user->update($data);
        $request->session()->flash('success', 'Usuario actualizado con exito.');
        return redirect()->back();
    }

    protected function createUser($data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'subsidiary_id' => $data['subsidiary_id'] ?? 0,
            'api_token' => str_random(60),
        ]);
    }

    protected function findAnUser($data)
    {
        if(!$user = User::where('email', $data['email'])->first()) {
            return false;
        }
        $user->update([
            'email' => $data['email'],
            'password' => bcrypt($data['password'])
        ]);
        return $user;
    }

    private function getSubsidiaries() {
        $subsidiaries = Subsidiary::active();
        if($this->user->access_subsidiary_id != null) {
            $subsidiaries = $subsidiaries->where('id',$this->user->access_subsidiary_id);
        }
        return $subsidiaries->get();
    }
}
