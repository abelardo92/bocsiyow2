<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\WarehouseRequest;
use App\WarehouseRequestProduct;
use App\WarehouseProduct;
use App\Subsidiary;
use App\Employee;
use App\Traits\SubDomainHelper;
use Illuminate\Support\Facades\Auth;

class WarehouseRequestsController extends Controller
{
    use SubDomainHelper;
    var $user = null;

    public function __construct() {
        $this->middleware(function ($request, $next){
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            $requests = WarehouseRequest::with(['movements', 'createdBy', 'employee'])
            ->where('subsidiary_id',$subsidiary->id)->orderBy('folio','desc')->get();
        } else {
            $requests = WarehouseRequest::with(['movements', 'createdBy', 'employee'])
            ->orderBy('folio','desc')->get();
        }
        return view('admin.warehouse.requests.index', compact('requests'));
    }

    public function pending()
    {
        $requests = WarehouseRequest::with(['subsidiary','movements', 'createdBy'])->confirmed()
        ->whereDoesntHave('movements', function($q) {
            $q->confirmed();
        });

        $subsidiary = null;

        if($this->user->access_subsidiary_id != null) {
            $subsidiary = Subsidiary::find($this->user->access_subsidiary_id);
        } else if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        }

        if ($subsidiary) {
            // $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            $requests = $requests->where('subsidiary_id', $subsidiary->id);
        }

        $requests = $requests->orderBy('id','DESC')->get();    
        return view('admin.warehouse.requests.pending', compact('requests'));
    }

    public function finished(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();

        $now = Carbon::now();
        if ($request->has('start')) {
            $start = $request->start;
            $end = $request->end;
        } else {
            $start = $now->format('Y-m-d');
            $end = $now->format('Y-m-d');
        }

        if($request->has('subsidiary_id')) {
            $subsidiary_id = $request->subsidiary_id;
        } else if($this->user->access_subsidiary_id != null) {
            $subsidiary_id = $this->user->access_subsidiary_id;
        } else {
            $subsidiary_id = "";
        }

        $requests = WarehouseRequest::with(['subsidiary','movements', 'createdBy'])
        ->where('created_at','>=', $start." 00:00:00")
        ->where('created_at','<=', $end." 23:59:59");

        if($request->subsidiary_id != "") {
            $requests = $requests->where('subsidiary_id', $subsidiary_id);            
        }

        $requests = $requests->whereHas('movements', function($q){
            $q->where('is_confirmed', true);
        })->orderBy('id','DESC')->get();

        $user = $this->user;

        return view('admin.warehouse.requests.finished', compact('user', 'requests', 'subsidiaries', 'start', 'end', 'subsidiary_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $warehouseProducts = WarehouseProduct::with(['existences','product.subsidiaries'])
        ->active()->orderBy('code')->get();
        $uses = WarehouseProduct::getEnum('uses');
        // $subsidiary_code = $this->hasSubdomain(); 
        // $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        $user = Auth::user();
        return view('admin.warehouse.requests.create', compact('warehouseProducts', 'uses', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();

        if(!$employee = Employee::where('code',$data['code'])->where('password', $data['password'])->get()->first()) {
            $request->session()->flash('error', 'la clave o contraseña de empleado es incorrecta.');
            return redirect()->back();
        }

        $products = $data['warehouseProducts'];

        foreach ($products as $key => $product) {
            if($products[$key]['quantity'] == 0) {
                unset($products[$key]);
            }
        }
        if(count($products) == 0) {
            $request->session()->flash('error', 'Debe cambiar la cantidad de al menos un producto.');
            return redirect()->back();
        }
        
        $subsidiary_code = $this->hasSubdomain(); 
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();

        //dd($products);
        $folio = 1;
        if ($last = $subsidiary->requests()->orderBy('folio', 'DESC')->first()) {
            $folio = $last->folio + 1;
        }
        
        $warehouseRequest = WarehouseRequest::create([
            'subsidiary_id' => $subsidiary->id,
            'folio' => $folio,
            'date' => Carbon::now()->format('Y-m-d'),
            'created_by' => $user->id,
            'employee_id' => $employee->id
        ]);

        foreach ($products as $product) {
            $warehouseRequestProduct = WarehouseRequestProduct::create([
                'warehouse_request_id' => $warehouseRequest->id,
                'warehouse_product_id' => $product['id'],
                'quantity' => $product['quantity'],
                'amount' => $product['amount']
            ]);
        }

        $request->session()->flash('success', 'Solicitud dada de alta con éxito.');
        return redirect()->route('warehouse.requests.index');
    }

    public function store2(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        
        if(!$employee = Employee::where('code', $data['employee']['code'])->where('password', $data['employee']['password'])->get()->first()) {
            return response()->json([
                'message' => 'La clave o contraseña de empleado es incorrecta.',
            ], 404);
        }

        $products = $data['warehouse_products'];

        foreach ($products as $key => $product) {
            if($product['quantity'] <= 0) {
                unset($products[$key]);
            }
        }
        if(count($products) == 0) {
            return response()->json([
                'message' => 'Debe establecer la información de al menos un producto.',
            ], 404);
        }
        
        $subsidiary_code = $this->hasSubdomain(); 
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();

        $folio = 1;
        if ($last = $subsidiary->requests()->orderBy('folio', 'DESC')->first()) {
            $folio = $last->folio + 1;
        }
        
        $warehouseRequest = WarehouseRequest::create([
            'subsidiary_id' => $subsidiary->id,
            'folio' => $folio,
            'date' => Carbon::now()->format('Y-m-d'),
            'created_by' => $user->id,
            'employee_id' => $employee->id
        ]);

        foreach ($products as $product) {
            $warehouseRequestProduct = WarehouseRequestProduct::create([
                'warehouse_request_id' => $warehouseRequest->id,
                'warehouse_product_id' => $product['id'],
                'quantity' => $product['quantity'],
                'amount' => $product['amount'],
                'existence' => $product['existence'],
                'must_have' => $product['must_have'],
            ]);
        }

        $request->session()->flash('success', 'Solicitud dada de alta con éxito.');
        return response()->json([
                'message' => 'Solicitud dada de alta con éxito.',
            ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $warehouseRequest = WarehouseRequest::with(['products','products.product','createdBy','movements', 'movements.products'])->find($id);
        $uses = WarehouseProduct::getEnum('uses');
        $subdomain = $this->hasSubdomain();
        return view('admin.warehouse.requests.show', compact('warehouseRequest', 'uses', 'subdomain'));
    }

    public function confirm(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user();
        $warehouseRequest = WarehouseRequest::find($id);
        $warehouseRequest->is_confirmed = true;
        $warehouseRequest->confirmed_by = $user->id;
        $warehouseRequest->confirmed_at = Carbon::now()->format('Y-m-d H:i:s');;
        $warehouseRequest->save();
        $request->session()->flash('success', 'Pedido confirmado con éxito.');
        return redirect()->route('warehouse.requests.show', $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $warehouseRequest = WarehouseRequest::with(['products'])->find($id);
        $warehouseProducts = WarehouseProduct::with(['existences','product.subsidiaries'])
        ->active()->orderBy('code')->get();
        foreach ($warehouseProducts as $key => $warehouseProduct) {
            $warehouseProduct->initExistence();
        }

        $uses = WarehouseProduct::getEnum('uses');
        $user = Auth::user();
        return view('admin.warehouse.requests.edit', compact('warehouseRequest', 'warehouseProducts', 'uses', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user();
        $products = $data['warehouseProducts'];
        $productsOriginal = $products;

        foreach ($products as $key => $product) {
            if($products[$key]['quantity'] == 0) {
                unset($products[$key]);
            }
        }
        if(count($products) == 0) {
            $request->session()->flash('error', 'Debe cambiar la cantidad de al menos un producto.');
            return redirect()->back();
        }
        
        $subsidiary_code = $this->hasSubdomain(); 
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();

        $folio = 1;
        if ($last = $subsidiary->requests()->orderBy('folio', 'DESC')->first()) {
            $folio = $last->folio + 1;
        }
        
        $warehouseRequest = WarehouseRequest::with(['products'])->find($id);

        foreach ($productsOriginal as $product) {
            $productId = $product['id'];
            $filteredProduct = $warehouseRequest->products->filter(function($prod) use ($productId){
                    if($prod->warehouse_product_id == $productId) {
                        return $prod;
                    }
                });
            
            if(count($filteredProduct) > 0) {
                if($product['quantity'] > 0) {
                    $filteredProduct->first()->quantity = $product['quantity'];
                    $filteredProduct->first()->save();
                } else {
                    $filteredProduct->first()->delete();
                }
            } else {
                if($product['quantity'] > 0) {
                    $warehouseRequestProduct = WarehouseRequestProduct::create([
                    'warehouse_request_id' => $warehouseRequest->id,
                    'warehouse_product_id' => $product['id'],
                    'quantity' => $product['quantity'],
                    'amount' => $product['amount']
                    ]);
                }
            }
        }

        $request->session()->flash('success', 'Solicitud actualizada con éxito.');
        return redirect()->route('warehouse.requests.edit', $id);
    }

    public function update2(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $products = $data['warehouse_products'];
        $productsOriginal = $products;

        foreach ($products as $key => $product) {
            if($product['quantity'] == 0) {
                unset($products[$key]);
            }
        }
        if(count($products) == 0) {
            return response()->json([
                'message' => 'Debe establecer la información de al menos un producto.',
            ], 404);
        }
        
        $subsidiary_code = $this->hasSubdomain(); 
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();

        $folio = 1;
        if ($last = $subsidiary->requests()->orderBy('folio', 'DESC')->first()) {
            $folio = $last->folio + 1;
        }
        
        $warehouseRequest = WarehouseRequest::with(['products'])->find($data['warehouse_request']['id']);

        foreach ($productsOriginal as $product) {
            $productId = $product['id'];
            $filteredProduct = $warehouseRequest->products->filter(function($prod) use ($productId){
                    if($prod->warehouse_product_id == $productId) {
                        return $prod;
                    }
                });
            
            if(count($filteredProduct) > 0) {
                $productt = $filteredProduct->first();
                if($product['quantity'] > 0) {
                    $productt->quantity = $product['quantity'];
                    $productt->existence = $product['existence'];
                    $productt->must_have = $product['must_have'];
                    $productt->save();
                } else {
                    $productt->delete();
                }
            } else {
                if($product['quantity'] > 0) {
                    $warehouseRequestProduct = WarehouseRequestProduct::create([
                    'warehouse_request_id' => $warehouseRequest->id,
                    'warehouse_product_id' => $product['id'],
                    'quantity' => $product['quantity'],
                    'amount' => $product['amount'],
                    'existence' => $product['existence'],
                    'must_have' => $product['must_have'],
                    ]);
                }
            }
        }

        return response()->json([
                'message' => 'Solicitud actualizada con éxito.',
            ], 200);
    }

    public function printSubsidiary($id)
    {
        $warehouseRequest = WarehouseRequest::with(['subsidiary', 'products', 'products.product'])->find($id);
        $title = "Ticket de pedido";
        return view('admin.warehouse.requests.tickets.request_subsidiary', compact('warehouseRequest', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $warehouseRequest = WarehouseRequest::find($id);
        $warehouseRequest->products()->delete();
        $warehouseRequest->delete();
        $request->session()->flash('success', 'Solicitud eliminada con éxito.');
        return redirect()->route('warehouse.requests.index');
    }

    private function getSubsidiaries() {
        $subsidiaries = Subsidiary::active()->notLaundry()->where('id', '!=', 1);
        if($this->user->access_subsidiary_id != null) {
            $subsidiaries = $subsidiaries->where('id',$this->user->access_subsidiary_id);
        }
        return $subsidiaries->get();
    }
}
