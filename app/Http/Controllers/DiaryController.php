<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Diary;
use App\Eat;
use App\Service;
use App\Notifications\DiarySchedule;
use App\Subsidiary;
use App\Traits\SubDomainHelper;
use App\Turn;
use App\DiariesException;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;

class DiaryController extends Controller
{
    use SubDomainHelper;

    public function index(Request $request)
    {
        $data = $this->getDiariesData($request);
        return view('admin.diaries.index', $data);
    }

    public function show($id)
    {
        return response()->json([
            'data' => Diary::with('customer')->find($id),
        ]);
    }

    public function create()
    {
        $subsidiaries = $this->getSubsidiaries();

        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        } else {
            $subsidiary = $subsidiaries->first();
        }

        $eats = Eat::today()->orderBy('subsidiary_id')->get();
        $customers = Customer::notLaundry()->active()->get();
        $times = [];
        $barbers = $subsidiary->employees()
            ->where('job', 'Barbero')
            ->get();

        $services = Service::where('subsidiary_id', null)->orWhere('subsidiary_id', $subsidiary->id)->notLaundry()->get();
        return view('admin.diaries.create', compact('times', 'customers', 'barbers', 'subsidiary', 'subsidiaries', 'eats', 'services'));
    }

    public function createCustomer()
    {
        $subsidiaries = Subsidiary::active()->forCustomerDiaries()->get();
        $times = [];
        $services = Service::forCustomerDiaries()->notLaundry()->orderBy('name')->get();
        $current_date = Carbon::now();
        return view('admin.diaries.create_customer', compact('times', 'subsidiaries', 'services', 'current_date'));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $subsidiary = Subsidiary::find($data['subsidiary_id']);

        $cita_time = Carbon::parse("{$data['date']} {$data['time']}");
        $now = Carbon::now();

        // if ($cita_time->month == 12 && ($cita_time->day == 24 || $cita_time->day == 25 || $cita_time->day == 30 || $cita_time->day == 31)){
        //     $request->session()->flash('error', 'No puedes agendar citas el 24, 25, 30 y 31 de diciembre');
        //     return redirect()->back();
        // }

        // if ($cita_time->diffInHours($now) < 2) {
        //     $request->session()->flash('error', 'No puedes agendar con 3 horas de anticipación');
        //     return redirect()->back();
        // }

        if (!isset($data['service_id']) || empty($data['service_id'])) {
            $request->session()->flash('error', 'Debe seleccionar un servicio');
            return redirect()->back();
        }

        if($data['employee_id'] != null && $data['employee_id'] != 0 &&
        Diary::where('employee_id', $data['employee_id'])
        ->where('date', $data['date'])
        ->where('time', $data['time'])
        ->where('canceled', false)
        ->first()) {
            $request->session()->flash('error', 'Ya existe una cita con el barbero y hora establecidos, cambie de barbero u hora para agendar la cita');
            return redirect()->back();
        }

        $folio = $subsidiary->diaries()->count() + 1;
        $confirmation_code = "{$subsidiary->key}{$folio}{$cita_time->format('hi')}";
        $data['folio'] = $folio;
        $data['confirmation_code'] = $confirmation_code;
        if ($request->user()->isA('super-admin')) {
            $data['user_id'] = $request->user()->id;
        } else {
            $subsidiary_code = $this->hasSubdomain();
            $subsi = Subsidiary::whereKey($subsidiary_code)->first();
            if ($subsi) {
                if ($cash_register = $subsi->cashRegisters()->open()->today()->first()) {
                    $data['cashier_id'] = $cash_register->employee->id;
                } else {
                    $data['user_id'] = $request->user()->id;
                }
            } else {
                $data['user_id'] = $request->user()->id;
            }
        }
        if ($request->user()->isA('agenda')) {
            $data['has_discount'] = true;
        }

        if (isset($data['employee_id']) && empty($data['employee_id'])) {
            $data['employee_id'] = 0;
        }

        $diary = $subsidiary->diaries()->create($data);
        $customer = $diary->customer;
        $customer->phone = $data['phone'];
        $customer->save();

        try {
            if ($customer->validPhone() && $customer->id != 7) {
                $customer->notify(new DiarySchedule($diary));
            }
            $request->session()->flash('success', "Cita agendada con éxito, código de confirmación es: {$confirmation_code}");
        } catch (\Exception $e) {
            $request->session()->flash('success', "Cita agendada con éxito, código de confirmación es: {$confirmation_code}");
            $request->session()->flash('error', "NO se envió el mensaje: {$e->getMessage()}");
        }
        return redirect()->route('diary.create', $subsidiary->key);
    }

    public function storeCustomer(Request $request)
    {
        $data = $request->all();

        $v = Validator::make($data, [
            'phone' => 'required|numeric',
        ]);

        if ($v->fails())
        {
            foreach($v->errors()->all() as $error) {
                $request->session()->flash('error', $error);
                return redirect()->back()->withInput();
            }
        }

        if(strlen($data['phone']) != 10) {
            $request->session()->flash('error', 'El teléfono celular debe tener 10 caracteres');
            return redirect()->back()->withInput();
        }

        $subsidiary = Subsidiary::find($data['subsidiary_id']);
        $date = $data['date'];
        $time = $data['time'];
        $phone = $data['phone'];
        // $email = $data['email'];
        $cita_time = Carbon::parse("$date $time");
        $now = Carbon::now();

        if(!$subsidiary || !$subsidiary->show_for_customer_diaries) {
            $request->session()->flash('error', 'La sucursal seleccionada no es válida');
            return redirect()->back()->withInput();
        }

        if($cita_time < $now) {
            $request->session()->flash('error', 'La fecha y hora de la cita no puede ser anterior a la fecha actual');
            return redirect()->back()->withInput();
        }

        // if ($cita_time->diffInHours($now) < 3) {
        //     $request->session()->flash('error', 'No puedes agendar con 3 horas de anticipación');
        //     return redirect()->back();
        // }

        $week_times = $this->getWeekTimes();
        $allowed_times = array_column($week_times, 'time');

        if(!in_array($time, $allowed_times)) {
            $request->session()->flash('error', 'El horario seleccionado no está disponible, seleccione otro.');
            return redirect()->back()->withInput();
        }

        $diaries_per_time = $subsidiary->diaries->where('date',$date)->where('time',$time)->count();
        if($diaries_per_time >= $subsidiary->diaries_limit) {
            $request->session()->flash('error', 'El horario seleccionado no está disponible, seleccione otro.');
            return redirect()->back()->withInput();
        }

        $agended_diary = Diary::where('date',$date)->where('time',$time)
        ->where('phone', $phone)
        // ->where(function ($q) use ($phone, $email) {
        //     $q->where('phone', $phone)
        //     ->orWhere('email', $email);
        // })
        ->first();

        if($agended_diary) {
            $request->session()->flash('error', 'Usted ya cuenta con una cita en la hora y fecha establecidas');
            return redirect()->back()->withInput();
        }

        // if ($cita_time->month == 12 && ($cita_time->day == 24 || $cita_time->day == 25 || $cita_time->day == 30 || $cita_time->day == 31)){
        //     $request->session()->flash('error', 'No puedes agendar citas el 24, 25, 30 y 31 de diciembre');
        //     return redirect()->back()->withInput();
        // }

        if (!isset($data['service_id']) || empty($data['service_id'])) {
            $request->session()->flash('error', 'Debe seleccionar un servicio');
            return redirect()->back()->withInput();
        }

        // get suggested customer in case we find one coincidence
        $suggested_customer = Customer::where('phone',$phone)->first();
        $data['customer_id'] = $suggested_customer ? $suggested_customer->id : 7;
        $data['suggested_customer_id'] = $suggested_customer ? $suggested_customer->id : 7;

        $folio = $subsidiary->diaries()->count() + 1;
        $confirmation_code = "{$subsidiary->key}{$folio}{$cita_time->format('hi')}";
        $data['folio'] = $folio;
        $data['confirmation_code'] = $confirmation_code;

        $data['has_discount'] = true;
        $data['created_by_customer'] = true;

        if (isset($data['employee_id']) && empty($data['employee_id'])) {
            $data['employee_id'] = 0;
        }

        $diary = $subsidiary->diaries()->create($data);
        $diary = Diary::with('customer')->find($diary->id);
        // $customer = $diary->customer;
        // $customer->phone = $data['phone'];
        // $customer->save();

        try {
            $diary->notify(new DiarySchedule($diary));
            $request->session()->flash('success', "Cita agendada con éxito, código de confirmación es: {$confirmation_code}");
        } catch (\Exception $e) {
            $request->session()->flash('success', "Cita agendada con éxito, código de confirmación es: {$confirmation_code}");
            $request->session()->flash('error', "NO se envió el sms, porfavor guarde su código de confirmación");
        }
        return redirect()->back();
    }

    public function edit($id)
    {
        $subsidiaries = $this->getSubsidiaries();

        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        } else {
            $subsidiary = $subsidiaries->first();
        }

        $eats = Eat::today()->orderBy('subsidiary_id')->get();
        $customers = Customer::notLaundry()->active()->get();
        $times = [];
        $barbers = collect([]);
        $diary = Diary::find($id);
        $services = Service::where('subsidiary_id', null)->orWhere('subsidiary_id', $subsidiary->id)->notLaundry()->get();

        return view('admin.diaries.edit', compact('times', 'customers', 'barbers', 'subsidiary', 'subsidiaries', 'diary', 'eats', 'services'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $subsidiary = Subsidiary::find($data['subsidiary_id']);

        $cita_time = Carbon::parse("{$data['date']} {$data['time']}");
        $now = Carbon::now();
        // if ($cita_time->diffInHours($now) < 2) {
        //     $request->session()->flash('error', 'No puedes agendar con 3 horas de anticipación');
        //     return redirect()->back();
        // }

        if (isset($data['employee_id']) && empty($data['employee_id'])) {
            $data['employee_id'] = 0;
        }

        if (!isset($data['service_id']) || empty($data['service_id'])) {
            $request->session()->flash('error', 'Debe seleccionar un servicio');
            return redirect()->back();
        }

        $diary = Diary::find($id);
        $diary->update($data);
        $customer = $diary->customer;
        $customer->phone = $data['phone'];
        $customer->save();

        $request->session()->flash('success', "Cita actualizada con exito, codigo de confirmación es: {$diary->confirmation_code}");
        return back();
    }

    public function rangeTime($subsidiary, $date)
    {
        $times = [];

        $diary_exception = DiariesException::bySubsidiary($subsidiary->id)->active()->dayActive($date)->first();

        for ($i = 0; $i < $subsidiary->chairs_number; $i++) {
            $times[] = $this->getTimes($date, $diary_exception);
        }
        return $times;
    }

    public function dates($subsidiary)
    {
        $dates = $subsidiary->diaries()->with('customer')
            ->whereBetween('date', [request()->start, request()->end])
            ->get();
        return $dates->map(function ($diary) {
            return [
                'id' => $diary->id,
                'title' => "Cita de {$diary->customer->name}",
                'start' => "{$diary->date} {$diary->time}",
            ];
        });
    }

    public function getDiariesData($request)
    {
        $start = Carbon::now()->format('Y-m-d');
        if ($request->has('start')) {
            $start = $request->start;
        }
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            $diaries = $subsidiary->diaries()->with('customer')->date($start)->orderBy('time', 'asc')->get();
        } else {
            $diaries = Diary::with('customer')->date($start);
            $user = Auth::user();
            if($user->access_subsidiary_id != null){
                $diaries = $diaries->where('subsidiary_id', $user->access_subsidiary_id);
            }
            $diaries = $diaries->orderBy('subsidiary_id', 'asc')->orderBy('time', 'asc')->get();
        }
        return compact('diaries', 'start');
    }

    public function search(Request $request)
    {
        $data = $request->all();
        $subsidiary_id = $data['subsidiary_id'];
        $date = $data['date'];
        $times = collect([]);

        $subsidiary = Subsidiary::find($subsidiary_id);
        $diaries = $subsidiary->diaries()->where('date', $data['date'])->get();
        $times[] = $diaries->pluck('time')->map(function ($time) {
            $ttime = explode(':', $time);
            $time_es = $ttime[0];
            $at = 'am';
            if ($ttime[0] > 12) {
                $time_es = $ttime[0] - 12;
                $at = 'pm';
            }
            if ($ttime[1] < 10) {
                $time = "0{$time}";
            }
            return [
                'time' => "{$ttime[0]}:{$ttime[1]}:00",
                'time_es' => "{$time_es}:{$ttime[1]} {$at}",
                'active' => false,
                'disabled' => true,
            ];
        })->toArray();

        return response()->json([
            'times' => $this->array_merge_recursive_ex($this->rangeTime($subsidiary, $date), $times->toArray()),
        ]);
    }

    public function searchCustomer(Request $request)
    {
        $data = $request->all();
        $subsidiary_id = $data['subsidiary_id'];
        $date = $data['date'];

        $subsidiary = Subsidiary::find($subsidiary_id);

        if(!$subsidiary) {
            return response()->json([
                'message' => "Debe seleccionar una sucursal",
            ], 422);
        }

        if(Carbon::parse($date) < Carbon::now()->subDays(1)) {
            return response()->json([
                'message' => "La fecha debe ser posterior a la fecha actual",
            ], 422);
        }

        $week_times = $this->getWeekTimes($subsidiary_id);

        // $diaries = $subsidiary->diaries()->where('date', $data['date'])->groupBy('time')->get();
        $diaries = Diary::select('time', DB::raw('COUNT(*) as diaries'))->bySubsidiary($subsidiary->id)->where('date', $date)->groupBy('time')->get();

        $diaries_times = array();
        foreach($diaries as $diary) {
            $diaries_times[$diary->time] = $diary->diaries;
        }
        // return response()->json([
        //     'message' => json_encode($diaries_times),
        // ], 422);

        // $busy_dates = $diaries->pluck('time')->toArray();
        $times = array();
        foreach($week_times as $time) {
            $time_ = $time['time'];
            // if there is a diary in the desired time AND there are less diaries than subsidiary limit,
            // then we can show the time
            if(!isset($diaries_times[$time_]) || $diaries_times[$time_] < $subsidiary->diaries_limit) {
                $times[] = $time;
            }
        }
        $timess[] = $times;
        return response()->json([
            'times' => $timess,
        ]);
    }

    public function array_merge_recursive_ex(array $array1, array $array2)
    {
        $merged = $array1;

        foreach ($array2 as $key => &$value) {
            if (is_array($value) && isset($merged[$key]) && is_array($merged[$key])) {
                $merged[$key] = $this->array_merge_recursive_ex($merged[$key], $value);
            } else if (is_numeric($key)) {
                if (!in_array($value, $merged)) {
                    $merged[] = $value;
                }

            } else {
                $merged[$key] = $value;
            }

        }

        return $merged;
    }

    protected function getTimes($date, $diary_exception)
    {
        // if (Carbon::parse($date)->dayOfWeek == Carbon::SUNDAY) {
            // $times = $this->getSundaysTimes();
        // } else {
            $times = $this->getWeekTimes();
        // }
        $time_now = Carbon::now();
        // $time_now = Carbon::parse('15:00:00');
        $return_times = [];
        if ($date > $time_now->format('Y-m-d')) {
            foreach ($times as $time_arr) {
                if($diary_exception && $diary_exception->isTimeInRange($date, $time_arr['time'])) continue;
                $return_times[] = $time_arr;
            }
            return $return_times;
        }
        if ($date < $time_now->format('Y-m-d')) {
            return $return_times;
        }
        foreach ($times as $time_arr) {
            if ($time_arr['time'] < $time_now->format('H:i:s')) {
                continue;
            }
            $cita_time = Carbon::parse($time_arr['time']);
            if ($cita_time->diffInHours($time_now) < 2) {
                continue;
            }

            if($diary_exception && $diary_exception->isTimeInRange($date, $time_arr['time'])) {
                continue;
            }

            $return_times[] = $time_arr;
        }

        return $return_times;
    }

    public function getWeekTimes($subsidiary_id = null)
    {
        $array = array();

        if(!$subsidiary_id || $subsidiary_id != 6) {
            $array = array_merge($array, [
                [
                    'time' => "09:00:00",
                    'time_es' => "9:00 am",
                    'active' => false,
                    'disabled' => false,
                ],
                [
                    'time' => "09:40:00",
                    'time_es' => "9:40 am",
                    'active' => false,
                    'disabled' => false,
                ],
                [
                    'time' => "10:20:00",
                    'time_es' => "10:20 am",
                    'active' => false,
                    'disabled' => false,
                ],
            ]);
        }

        $array = array_merge($array, [
            [
                'time' => "11:00:00",
                'time_es' => "11:00 am",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "11:40:00",
                'time_es' => "11:40 am",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "12:20:00",
                'time_es' => "12:20 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "13:00:00",
                'time_es' => "1:00 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "13:40:00",
                'time_es' => "1:40 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "14:20:00",
                'time_es' => "2:20 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "15:00:00",
                'time_es' => "3:00 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "16:00:00",
                'time_es' => "4:00 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "16:40:00",
                'time_es' => "4:40 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "17:20:00",
                'time_es' => "5:20 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "18:00:00",
                'time_es' => "6:00 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "18:40:00",
                'time_es' => "6:40 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "19:20:00",
                'time_es' => "7:20 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "20:00:00",
                'time_es' => "8:00 pm",
                'active' => false,
                'disabled' => false,
            ],
        ]);

        if(!$subsidiary_id || $subsidiary_id != 6) {
            $array = array_merge($array, [
                [
                    'time' => "20:40:00",
                    'time_es' => "8:40 pm",
                    'active' => false,
                    'disabled' => false,
                ],
                [
                    'time' => "21:20:00",
                    'time_es' => "9:20 pm",
                    'active' => false,
                    'disabled' => false,
                ],
            ]);
        }

        if(!$subsidiary_id || !in_array($subsidiary_id, [4,6,9])) {
            $array = array_merge($array, [
                [
                    'time' => "10:00:00",
                    'time_es' => "10:00 pm",
                    'active' => false,
                    'disabled' => false,
                ]
            ]);
        }
        return $array;
    }

    public function getSundaysTimes()
    {
        return [
            [
                'time' => "11:30:00",
                'time_es' => "11:30 am",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "12:10:00",
                'time_es' => "12:10 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "12:50:00",
                'time_es' => "12:50 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "13:30:00",
                'time_es' => "1:30 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "14:10:00",
                'time_es' => "2:10 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "14:50:00",
                'time_es' => "2:50 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "15:30:00",
                'time_es' => "3:30 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "16:00:00",
                'time_es' => "4:00 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "16:40:00",
                'time_es' => "4:40 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "17:20:00",
                'time_es' => "5:20 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "18:00:00",
                'time_es' => "6:00 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "18:40:00",
                'time_es' => "6:40 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "19:20:00",
                'time_es' => "7:20 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "20:00:00",
                'time_es' => "8:00 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "20:40:00",
                'time_es' => "8:40 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "21:20:00",
                'time_es' => "9:20 pm",
                'active' => false,
                'disabled' => false,
            ],
            [
                'time' => "22:00:00",
                'time_es' => "10:00 pm",
                'active' => false,
                'disabled' => false,
            ],
            // [
            //     'time' => "16:10:00",
            //     'time_es' => "4:10 pm",
            //     'active' => false,
            //     'disabled' => false,
            // ],
            // [
            //     'time' => "16:50:00",
            //     'time_es' => "4:50 pm",
            //     'active' => false,
            //     'disabled' => false,
            // ],
            // [
            //     'time' => "17:30:00",
            //     'time_es' => "5:30 pm",
            //     'active' => false,
            //     'disabled' => false,
            // ],
            // [
            //     'time' => "18:10:00",
            //     'time_es' => "6:10 pm",
            //     'active' => false,
            //     'disabled' => false,
            // ],
            // [
            //     'time' => "18:50:00",
            //     'time_es' => "6:50 pm",
            //     'active' => false,
            //     'disabled' => false,
            // ],
            // [
            //     'time' => "19:30:00",
            //     'time_es' => "7:30 pm",
            //     'active' => false,
            //     'disabled' => false,
            // ],
            // [
            //     'time' => "20:10:00",
            //     'time_es' => "8:10 pm",
            //     'active' => false,
            //     'disabled' => false,
            // ],
            // [
            //     'time' => "20:50:00",
            //     'time_es' => "8:50 pm",
            //     'active' => false,
            //     'disabled' => false,
            // ],
            // [
            //     'time' => "21:30:00",
            //     'time_es' => "9:30 pm",
            //     'active' => false,
            //     'disabled' => false,
            // ],
            // [
            //     'time' => "22:10:00",
            //     'time_es' => "10:10 pm",
            //     'active' => false,
            //     'disabled' => false,
            // ],
            // [
            //     'time' => "22:50:00",
            //     'time_es' => "10:50 pm",
            //     'active' => false,
            //     'disabled' => false,
            // ],
        ];
    }

    public function searchForBarber(Request $request)
    {
        $data = $request->all();
        $subsidiary_id = $data['subsidiary_id'];
        $date = $data['date'];
        $time = $data['time'];

        $subsidiary = Subsidiary::find($subsidiary_id);
        $diaries = $subsidiary->diaries()->where('date', $data['date'])
            ->where('time', $data['time'])->notCancel()->get();
        $barbers = $this->getBarbers($subsidiary, $date, $diaries, $time);

        return response()->json([
            'barbers' => $barbers,
        ]);
    }

    protected function getBarbers($subsidiary, $date, $diaries, $time)
    {
        $turn = false;
        if ($time < '15:50:00') {
            $tid = 1;
            $turn = Turn::whereIn('identifier', [1, 3])->get()->pluck('id');
            $turnni = Turn::where('identifier', 1)->first();
        } else {
            $tid = 2;
            $turn = Turn::whereIn('identifier', [2, 3])->get()->pluck('id');
            $turnni = Turn::where('identifier', 2)->first();
        }

        $schedules = $subsidiary->schedules()->date($date)->whereIn('turn_id', $turn)->get();
        $barbers = [];

        foreach ($schedules as $schedule) {
            if (!$schedule->employee->hasJob('Barbero')) {
                continue;
            }
            $count = 0;
            foreach ($diaries as $diary) {
                if ($diary->employee && $diary->employee->id == $schedule->employee->id) {
                    $count++;
                }
            }
            if ($count > 0) {
                continue;
            }
            $barbers[] = [
                'id' => $schedule->employee->id,
                'text' => $schedule->employee->short_name,
            ];
        }
        $now = Carbon::now();
        if ($tid == 1) {
            $start = Carbon::parse("{$date} 00:00:00");
            $end = Carbon::parse("{$date} {$turnni->end}");
        } else {
            $start = Carbon::parse("{$date} {$turnni->start}");
            $end = Carbon::parse("{$date} 22:59:59");
        }
        if ($date == $now->format('Y-m-d') && $now->between($start, $end)) {
            $employee_ids = collect($barbers)->pluck('id');
            $barberos = \App\Attendance::select('employee_id', \DB::raw('COUNT(*) as count_attend'))
                ->where('subsidiary_id', $subsidiary->id)
                ->today()
                ->groupBy('employee_id')
            // ->whereNotIn('employee_id', $employee_ids)
                ->get()
                ->whereIn('count_attend', [1, 3])
                ->where('employee.job', 'Barbero')
                ->map(function ($key) {
                    return $key->employee;
                })->filter(function ($employee) use ($employee_ids) {
                    return in_array($employee->id, $employee_ids->toArray());
                });
            $barbers = [];
           foreach ($barberos as $barbero) {
               $barbers[] = [
                'id' => $barbero->id,
                'text' => $barbero->short_name,
               ];
           }
        }

        return $barbers;
    }

    public function destroy($id)
    {
        $diary = Diary::find($id);
        $diary->canceled = true;
        $diary->save();

        session()->flash('success', "Su cita ha sido cancelada.");
        return back();
    }

    public function next()
    {
        $subsidiary = Subsidiary::whereKey($this->hasSubdomain())->firstOrFail();
        $now = Carbon::now();
        $next = Carbon::now()->addHours(1);
        $diaries = $subsidiary->diaries()->with('customer')
            ->where('date', $now->format('Y-m-d'))
            ->read(false)
            ->whereBetween('time', [
                $now->format('H:i:s'), $next->format('H:i:s'),
            ])->get();
        return response()->json([
            'diaries' => $diaries,
        ]);
    }

    public function markAsRead($id)
    {
        $diary = Diary::find($id);
        $diary->read = true;
        $diary->save();

        return response()->json([
            'diary' => $diary,
        ]);
    }

    private function getSubsidiaries(){
        $subsidiaries = Subsidiary::where('is_active', true);
        $user = Auth::user();
        if($user->access_subsidiary_id != null){
            $subsidiaries = $subsidiaries->where('id',$user->access_subsidiary_id);
        }
        return $subsidiaries->get();
    }
}
