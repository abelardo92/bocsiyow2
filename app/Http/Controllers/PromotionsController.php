<?php

namespace App\Http\Controllers;

use App\Promotion;
use App\PromotionService;
use App\PromotionProduct;
use App\Subsidiary;
use App\Service;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PromotionsController extends Controller
{

    var $user = null;

    public function __construct() {
        $this->middleware(function ($request, $next){
            $this->user = Auth::user();
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($this->user->isA('subsidiary-admin')) {
            $active_promotions = Promotion::active()->bySubsidiary($this->user->access_subsidiary_id)->get();
            $inactive_promotions = Promotion::notActive()->bySubsidiary($this->user->access_subsidiary_id)->get();
        } else {
            $active_promotions = Promotion::active()->get();
            $inactive_promotions = Promotion::notActive()->get();
        }
        return view('admin.promotions.index', compact('active_promotions', 'inactive_promotions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subsidiaries = $this->getSubsidiaries();
        return view('admin.promotions.create', compact('subsidiaries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        // format times
        $start_time = Carbon::createFromFormat('h:i a', $request->start_time);
        $end_time = Carbon::createFromFormat('h:i a', $request->end_time);
        $data['start_time'] = $start_time->format('H:i:s');
        $data['end_time'] = $end_time = $end_time->format('H:i:s');

        if(empty($request->discount_percentaje)) {
            $data['discount_percentaje'] = null;
        }
        if(empty($request->subsidiary_id)) {
            $data['subsidiary_id'] = null;
        }

        if(!$this->validateForm($request)) {
            return redirect()->back();
        }

        $promotion = Promotion::create($data);
        $request->session()->flash('success', 'Promoción dada de alta con éxito.');
        return redirect()->route('promotions.edit', $promotion->id);
    }

    private function validateForm(Request $request) {

        if(!$request->name) {
            $request->session()->flash('error', 'Debe escribir un nombre de la promoción.');
            return false;
        }

        if($request->discount_percentaje > 100) {
            $request->session()->flash('error', 'El descuento no puede ser mayor a 100%.');
            return false;
        }

        if(!$request->monday && !$request->tuesday && !$request->wednesday && !$request->thursday && 
        !$request->friday && !$request->saturday && !$request->sunday && !$request->is_permanent) {
            $request->session()->flash('error', 'Debe seleccionar al menos un día de la semana.');
            return false;
        }

        if(!$request->start_date || !$request->end_date || !$request->start_time || !$request->end_time) {
            $request->session()->flash('error', 'Debe establecer las fechas y horas completas.');
        }

        if($request->start_date > $request->end_date) {
            $request->session()->flash('error', 'La fecha de inicio no debe ser mayor a la fecha final.');
            return false;
        }

        $start_time = Carbon::createFromFormat('h:i a', $request->start_time);
        $end_time = Carbon::createFromFormat('h:i a', $request->end_time);

        if($start_time > $end_time) {
            $request->session()->flash('error', 'La hora de inicio no debe ser mayor a la fecha final.');
            return false;
        }

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $promotion = Promotion::with('promotionservices.service','promotionProducts.product')->find($id);
        $subsidiaries = $this->getSubsidiaries();
        $services = Service::notLaundry()->get();
        $products = Product::notLaundry()->get();
        return view('admin.promotions.edit', compact('promotion', 'subsidiaries', 'services', 'products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        // format times
        $start_time = Carbon::createFromFormat('h:i a', $request->start_time);
        $end_time = Carbon::createFromFormat('h:i a', $request->end_time);
        $data['start_time'] = $start_time->format('H:i:s');
        $data['end_time'] = $end_time = $end_time->format('H:i:s');

        if(empty($request->discount_percentaje)) {
            $data['discount_percentaje'] = null;
        }
        if(empty($request->subsidiary_id)) {
            $data['subsidiary_id'] = null;
        }

        if(!$this->validateForm($request)) {
            return redirect()->back();
        }

        $promotion = Promotion::find($id);
        $promotion->update($data);
        $request->session()->flash('success', 'Promoción actualizada con éxito.');
        return redirect()->route('promotions.edit', $promotion->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $promotion = Promotion::find($id);
        $promotion->is_active = false;
        $promotion->save();

        $request->session()->flash('success', 'Promoción dada de baja con éxito');
        return redirect()->route('promotions.index');
    }

    public function active(Request $request, $id)
    {
        $promotion = Promotion::find($id);
        $promotion->update(['is_active' => true]);

        $request->session()->flash('success', 'Promoción dada de alta con éxito.');
        return back();
    }

    private function getSubsidiaries(){
        $subsidiaries = Subsidiary::where('is_active', true);
        $user = Auth::user();
        if($user->access_subsidiary_id != null){
            $subsidiaries = $subsidiaries->where('id',$user->access_subsidiary_id);
        }
        return $subsidiaries->get();
    }

    public function updatePromotionServices(Request $request)
    {
        $promotion = Promotion::with('promotionservices')->find($request->promotion_id);

        // delete all promotion services
        foreach($promotion->promotionservices as $promotion_service) {
            $promotion_service->delete();
        }

        // insert all new information
        foreach($request->promotion_services as $promotion_service) {
            PromotionService::create($promotion_service);
        }

        $message = 'Servicios actualizados con éxito.';
        return response()->json(compact('message'));
    }

    public function updatePromotionProducts(Request $request)
    {
        $promotion = Promotion::with('promotionProducts')->find($request->promotion_id);

        // delete all promotion products
        foreach($promotion->promotionProducts as $promotion_product) {
            $promotion_product->delete();
        }

        // insert all new information
        foreach($request->promotion_products as $promotion_product) {
            PromotionProduct::create($promotion_product);
        }

        $message = 'Productos actualizados con éxito.';
        return response()->json(compact('message'));
    }
}
