<?php

namespace App\Http\Controllers;

use App\CfdiUse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SaleFacturaRequestRequest;
use App\Mail\FacturaNotification;
use App\Sale;
use App\SaleFacturaRequest;
use App\State;
use App\Subsidiary;
use App\TaxRegime;
use Illuminate\Support\Facades\Auth;
use App\Traits\ImageableTrait;
use Illuminate\Support\Facades\Mail;

class SaleFacturaRequestController extends Controller
{
    use ImageableTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $factura_requests = SaleFacturaRequest::active()->orderBy('created_at', 'desc')->get();
        return view('admin.factura-requests.index', compact('factura_requests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $states = State::get();
        $cfdi_uses =  CfdiUse::get();
        $tax_regimes = TaxRegime::get();
        // dd($cfdi_uses);
        return view('admin.factura-requests.create', compact('states','cfdi_uses','tax_regimes'));
    }

    public function createCustomer($sale_id = null)
    {
        $states = State::get();
        $cfdi_uses =  CfdiUse::get();
        $tax_regimes = TaxRegime::get();
        $subsidiaries = Subsidiary::active()->forCustomerDiaries()->get();

        if($sale = Sale::with('subsidiary')->find($sale_id)) {
            //dd($sale);
            $sale_folio = "{$sale->subsidiary->key}-{$sale->folio}";
        } else {
            $sale_folio = "";
        }

        // dd($cfdi_uses);
        return view('admin.factura-requests.createCustomer', compact('sale_folio', 'states','cfdi_uses','tax_regimes', 'subsidiaries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaleFacturaRequestRequest $request)
    {
        $data = $request->all();

        $data['status'] = 'pendiente';

        list($subsidiary_key, $folio) = explode('-', $request->folio);
        $data['folio'] = $folio;

        $subsidiary = Subsidiary::where('key',$subsidiary_key)->active()->first();
        $sale = Sale::where('folio', $request->folio)->where('subsidiary_id', $subsidiary->id)->first();
        $data['sale_id'] = $sale->id;

        $sale_factura_request = SaleFacturaRequest::create($data);
        $this->CreateFile($sale_factura_request, $request, "sale_factura_requests/{$sale_factura_request->id}");

        Mail::to('facturasbocsiyow@outlook.es')->send(new FacturaNotification($sale->id));

        $request->session()->flash('success', 'Solicitud realizada con éxito, la factura sera enviada al correo electrónico proporcionado.');
        return redirect()->back();
    }

    public function storeCustomer(SaleFacturaRequestRequest $request)
    {
        $data = $request->all();

        $data['status'] = 'pendiente';

        list($subsidiary_key, $folio) = explode('-', $request->folio);
        $data['folio'] = $folio;

        $subsidiary = Subsidiary::where('key',$subsidiary_key)->active()->first();
        $sale = Sale::where('folio', $folio)->where('subsidiary_id', $subsidiary->id)->first();
        $data['sale_id'] = $sale->id;
        $data['subsidiary_id'] = $subsidiary->id;

        $sale_factura_request = SaleFacturaRequest::create($data);
        $this->CreateFile($sale_factura_request, $request, "sale_factura_requests/{$sale_factura_request->id}");

        Mail::to('facturasbocsiyow@outlook.es')->send(new FacturaNotification($sale->id));

        $request->session()->flash('success', 'Solicitud realizada con éxito, la factura sera enviada al correo electrónico proporcionado.');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $states = State::get();
        $cfdi_uses =  CfdiUse::get();
        $tax_regimes = TaxRegime::get();
        $factura_request = SaleFacturaRequest::find($id);
        // dd($cfdi_uses);
        return view('admin.factura-requests.edit', compact('factura_request', 'states','cfdi_uses','tax_regimes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SaleFacturaRequestRequest $request, $id)
    {
        $data = $request->all();

        list($subsidiary_key, $folio) = explode('-', $request->folio);
        $data['folio'] = $folio;

        $subsidiary = Subsidiary::where('key',$subsidiary_key)->active()->first();
        $sale = Sale::where('folio', $folio)->where('subsidiary_id', $subsidiary->id)->first();
        $data['sale_id'] = $sale->id;
        $data['subsidiary_id'] = $subsidiary->id;

        $factura_request = SaleFacturaRequest::find($id);
        $factura_request->update($data);

        $this->CreateFile($factura_request, $request, "sale_factura_requests/{$factura_request->id}");

        $request->session()->flash('success', 'Solicitud de factura actualizada con éxito');
        return redirect()->route('facturas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $factura_request = SaleFacturaRequest::find($id);
        $factura_request->is_active = false;
        $factura_request->save();

        $request->session()->flash('success', 'Solicitud de factura dada de baja con éxito');
        return redirect()->route('facturas.index');
    }
}
