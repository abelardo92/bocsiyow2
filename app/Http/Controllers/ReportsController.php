<?php

namespace App\Http\Controllers;

use App\Account;
use App\Attendance;
use App\CashRegister;
use App\CashRegisterMovement;
use App\Diary;
use App\Employee;
use App\Paysheet;
use App\Product;
use App\Customer;
use App\Sale;
use App\SaleProduct;
use App\SaleService;
use App\Service;
use App\LaundryService;
use App\Subsidiary;
use App\WarehouseRequest;
use App\WarehouseProduct;
use App\WarehouseHistory;
use App\WarehouseHistoryProduct;
use App\WarehouseMovement;
use App\WarehouseAdjustment;
use App\Surveys\Question;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use League\Csv\Writer;
use SplTempFileObject;
use App\Traits\SubDomainHelper;
use App\Turn;

class ReportsController extends Controller
{
    use SubDomainHelper;

    var $user = null;

    public function __construct() {
        set_time_limit(300);
        $this->middleware(function ($request, $next){
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function attendances(Request $request)
    {
        $now = Carbon::now();
        $today = $now->format('Y-m-d');
        if ($request->has('start')) {
            $start = $request->start;
        } else {
            $start = $now->format('Y-m-d');
        }

        $attendances = Attendance::date($start)
            ->orderBy('subsidiary_id', 'asc')
            ->orderBy('employee_id', 'asc')
            ->orderBy('time', 'asc')
            ->get();

        return view('admin.reports.attendances', compact('attendances', 'start', 'today'));
    }

    public function attendancesTime(Request $request, $id)
    {
        $attendance = Attendance::find($id);
        $attendance->time = $request->time;
        $attendance->save();
        $request->session()->flash('success', 'Hora actualizada con exito');
        return back();
    }

    public function destroyAttendances(Request $request, $id)
    {
        $attendance = Attendance::find($id);
        $attendance->delete();
        $request->session()->flash('success', 'Checada eliminada con exito');
        return back();
    }

    public function cashCutsByDate(Request $request) {
        $now = Carbon::now();
        if ($request->has('start')) {
            $start = $request->start;
        } else {
            $start = $now->format('Y-m-d');
        }

        $subsidiaries = $this->getSubsidiaries();
        return view('admin.reports.cashCutsByDate', compact('subsidiaries', 'start'));
    }

    public function cashierMovementsResume(Request $request) {
        $oneWeekBefore = Carbon::now()->subDays(7);
        if ($request->has('start')) {
            $start = $request->start;
            $end = $request->end;
        } else {
            $start = $oneWeekBefore->format('Y-m-d');
            $end = Carbon::now()->format('Y-m-d');
        }

        $subsidiaries = $this->getSubsidiaries();
        return view('admin.reports.cashierMovementsResume', compact('subsidiaries', 'start', 'end'));
    }

    public function issues(Request $request)
    {
        $now = Carbon::now();
        $barbers = Employee::all();
        if ($request->has('start')) {
            $barber = Employee::find($request->employee_id);
            $start = $request->start;
            $end = $request->end;
        } else {
            $barber = $barbers->first();
            $start = $now->format('Y-m-d');
            $end = $now->format('Y-m-d');
        }

        $issues = $barber->issues()->dates([$start, $end])->get();
        return view('admin.reports.issues', compact('issues', 'start', 'end', 'barber', 'barbers'));
    }

    public function schedules(Request $request)
    {
        $now = Carbon::now();
        $subsidiaries = $this->getSubsidiaries();
        $days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        if ($request->has('start')) {
            $start = Carbon::parse($request->start)->startOfWeek();
            $end = Carbon::parse($request->end)->endOfWeek();
        } else {
            $start = Carbon::now()->startOfWeek();
            $end = Carbon::now()->endOfWeek();
            $dates = [];
        }
        $copy = $start->copy();
        while ($copy->lte($end)) {
            $dates[] = $copy->copy();
            $copy->addDay();
        }
        return view('admin.reports.schedules', compact('subsidiaries', 'dates', 'start', 'end'));
    }

    public function attendanceCard(Request $request)
    {
        if ($request->has('employee_id')) {
            $start = $request->start;
            $end = $request->end;
            $employees = $this->getEmployeesWithAttendance($start, $end);
            $employee = $employees->where('id', $request->employee_id)->first();
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
            $employees = $this->getEmployeesWithAttendance($start, $end);
            $employee = $employees->first();
        }

        if($start > $end) {
            $request->session()->flash('error', 'La fecha inicial no puede ser posterior a la final');
            return redirect()->back();
        }

        return view('admin.reports.attendance_card', compact('employees', 'employee', 'start', 'end'));
    }

    public function notFrequentCustomers(Request $request)
    {
        $oneMonthBefore = Carbon::now()->subDays(30);
        if ($request->has('start')) {
            $start = $request->start;
            $end = $request->end;
        } else {
            $start = $oneMonthBefore->format('Y-m-d');
            $end = $oneMonthBefore->format('Y-m-d');
        }

        $endCarbon = Carbon::createFromFormat('Y-m-d', $end);
        //dd($endCarbon);

        $customers = Customer::active()->notLaundry()->withPhoneOrEmail()->
        whereIn('id', function($q) use ($start, $end) {
            $q->select('customer_id')->from('sales')->whereBetween('date', [$start, $end]);
        })->
        whereNotIn('id', function($q) use ($endCarbon) {
            $q->select('customer_id')->from('sales')->where('created_at', '>=', $endCarbon);
            //$q->select('customer_id')->from('sales')->where('date', '>=', $end);
        })->paginate(8);
        //->toSql();
        //dd($oneMonthBefore);
        return view('admin.reports.notFrequentCustomers', compact('customers', 'start', 'end'));
    }

    public function salesByCustomer(Request $request)
    {
        $customer = null;
        //$customer_id = 0;
        $customers = Customer::active()->get();
        if ($request->has('customer_id')) {
        //    $customer_id = $request->customer_id;
            $customer = Customer::find($request->customer_id);
        }
        return view('admin.reports.salesByCustomer', compact('customers', 'customer'));
    }

    public function customersByBarber(Request $request)
    {
        $employee_id = 0;
        $customers = null;
        $barbers = $this->getBarbers();
        if ($request->has('employee_id')) {
            $employee_id = $request->employee_id;

            $customers = Customer::select('customers.id', 'customers.name', 'customers.phone', 'customers.email', DB::raw('count(s1.id) as `sales_count`'))
            ->join('sales as s1',function($q) use ($employee_id) {
                $q->on('customers.id', '=', 's1.customer_id')->where('s1.employee_id', $employee_id)
                ->where('s1.paid', true)->where('s1.finish', true)->whereNull('s1.canceled_by');
            })
            ->active()->withPhoneOrEmail()->where('customers.id', '!=', 7)
            ->groupBy('customers.id')
            ->orderBy('sales_count', 'desc')
            ->paginate(8);
        }

        return view('admin.reports.customersByBarber', compact('customers', 'barbers', 'employee_id'));
    }

    public function subsidiary(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        if ($request->has('subsidiary_id')) {
            $subsidiary = Subsidiary::find($request->subsidiary_id);
            $start = $request->start;
            $end = $request->end;
        } else {
            $subsidiary = $subsidiaries->first();
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $sales = $subsidiary->sales()->select(DB::raw('id, date, SUM(subtotal) as sumtotal'))
        ->dates([$start, $end])->finished()->notCanceled()->groupBy('date')->get();
        return view('admin.reports.subsidiary', compact('subsidiaries', 'subsidiary', 'start', 'end', 'sales'));
    }

    public function subsidiaryAnalysis(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        if ($request->has('subsidiary_id')) {

            $subsidiary = $request->subsidiary_id != '0' ? Subsidiary::find($request->subsidiary_id) : null;
            $start = $request->start;
            $end = $request->end;
        } else {
            $subsidiary = null;
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $movements = WarehouseMovement::with(['products.product.product', 'request'])
        ->whereHas('request', function($q) use ($start, $end, $subsidiary) {
            if($subsidiary) {
                $q->where('created_at','>=', $start." 00:00:00")
                ->where('created_at','<=', $end." 23:59:59")
                ->where('subsidiary_id', $subsidiary->id);
            } else {
                $q->where('created_at','>=', $start." 00:00:00")
                ->where('created_at','<=', $end." 23:59:59");
            }
        })->where('is_confirmed', true);

        $movements = $movements->orderBy('id','DESC')->get();
        $movement_products = $movements->pluck('products')->collapse();
        $product_group = $movement_products->sortBy('product.code')->groupBy('product.id');

        $sale_products = SaleProduct::with(['product'])
        ->whereHas('sale', function($q) use ($start, $end, $subsidiary) {
            if($subsidiary) {
                $q->where('created_at','>=', $start." 00:00:00")
                ->where('created_at','<=', $end." 23:59:59")
                ->where('paid', true)->where('finish', true)
                ->where('canceled_by', null)
                ->where('subsidiary_id', $subsidiary->id);
            } else {
                $q->where('created_at','>=', $start." 00:00:00")
                ->where('created_at','<=', $end." 23:59:59")
                ->where('paid', true)->where('finish', true)
                ->where('canceled_by', null);
            }
        })->notCanceled()->get();

        $sale_products = $sale_products->groupBy('product_id');

        $sale_services = SaleService::with(['service'])
        ->whereHas('sale', function($q) use ($start, $end, $subsidiary) {
            if($subsidiary) {
                $q->whereBetween('date', [$start, $end])
                ->where('paid', true)->where('finish', true)
                ->where('canceled_by', null)
                ->where('subsidiary_id', $subsidiary->id);
            } else {
                $q->whereBetween('date', [$start, $end])
                ->where('paid', true)->where('finish', true)
                ->where('canceled_by', null);
            }
        })->notCanceled()->notWaiting()->get();

        $sale_services_groups = $sale_services->groupBy('service_id');

        $sale_services_promotion = SaleService::with(['service'])
        ->whereHas('sale', function($q) use ($start, $end, $subsidiary) {
            if($subsidiary) {
                $q->where('created_at','>=', $start." 00:00:00")
                ->where('created_at','<=', $end." 23:59:59")
                ->where('paid', true)->where('finish', true)
                ->where('canceled_by', null)
                ->where('subsidiary_id', $subsidiary->id)
                ->where(function ($query) {
                    $query->where('has_birthday', true)->orWhere('payment_gateway', 'Monedero')->orWhere('is_courtesy', true)->orWhere('is_promo_ut', true);
                });
            } else {
                $q->where('created_at','>=', $start." 00:00:00")
                ->where('created_at','<=', $end." 23:59:59")
                ->where('paid', true)->where('finish', true)
                ->where('canceled_by', null)
                ->where(function ($query) {
                    $query->where('has_birthday', true)->orWhere('payment_gateway', 'Monedero')->orWhere('is_courtesy', true)->orWhere('is_promo_ut', true);
                });
            }
        })->notCanceled()->notWaiting()->get();

        $sale_services_promotion_groups = $sale_services_promotion->groupBy('service_id');

        $employees = Employee::with([
            'cashRegisters' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'salesFinished' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'diariesCreated' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'salesFinished.products.product', 'salesFinished.services.service', 'cashRegisters.salesFinished.products.product', 'cashRegisters.salesFinished.services.service'
        ])->barbersOrCashiers()->active()->where('id', '!=', 46);

        if($subsidiary) {
            // $employees = $employees->where('subsidiary_id', $subsidiary->id);
            $subsidiary_id = $subsidiary->id;
            $employees = $employees->whereHas('sales', function($q) use ($subsidiary_id) {
                $q->where('subsidiary_id', $subsidiary_id);
            });
        }
        $employees = $employees->get();

        // dd($employees->first());

        return view('admin.reports.subsidiary_analysis', compact('subsidiaries', 'subsidiary', 'start', 'end', 'requests', 'product_group', 'sale_products', 'sale_services_groups', 'sale_services_promotion_groups', 'employees'));
    }

    public function soldProductsByEmployee(Request $request)
    {
        $employee = $this->user->employee()->get()->first();

        $date = Carbon::now();

        if ($request->has('date')) {
            $date = Carbon::parse($request->date);
        }
        $start = $date->copy()->startOfMonth();
        $end = $date->copy()->endOfMonth();

        $employee = Employee::with([
            'cashRegisters' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'salesFinished' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'salesFinished.products.product', 'salesFinished.services.service', 'cashRegisters.salesFinished.products.product', 'cashRegisters.salesFinished.services.service'
        ])->find($employee->id);

        $products_array = [];
        $product_totals_array = [];

        if($employee) {
            if($employee->job == 'Barbero') {
                foreach($employee->salesFinished as $sale) {
                    if($sale->services->where('id', '!=', 19)->count() > 0) {
                        foreach($sale->products as $product) {
                            if(!$product->product->include_in_reports) continue;
                            if(isset($product_totals_array[$product->product->id])) {
                                $product_totals_array[$product->product->id] += $product->qty;
                            } else {
                                $product_totals_array[$product->product->id] = $product->qty;
                                $products_array[$product->product->id] = $product->product;
                            }
                        }
                    }
                }
            }

            if($employee->job == 'Cajero') {
                foreach($employee->cashRegisters as $cashRegisters) {
                    foreach($cashRegisters->salesFinished as $sale) {
                        if($sale->services->where('service_id', '!=', 19)->count() == 0) {
                            foreach($sale->products as $product) {
                                if(!$product->product->include_in_reports) continue;
                                if(isset($product_totals_array[$product->product->id])) {
                                    $product_totals_array[$product->product->id] += $product->qty;
                                } else {
                                    $product_totals_array[$product->product->id] = $product->qty;
                                    $products_array[$product->product->id] = $product->product;
                                }
                            }
                        }
                    }
                }
            }
        }

        // dd($products_array);
        return view('admin.reports.products.sold_products_employee', compact('date', 'start', 'end', 'employee', 'products_array', 'product_totals_array'));
    }

    public function soldProducts(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        if ($request->has('subsidiary_id')) {

            $subsidiary = $request->subsidiary_id != '0' ? Subsidiary::find($request->subsidiary_id) : null;
            $start = $request->start;
            $end = $request->end;
        } else {
            $subsidiary = null;
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $sales = Sale::with('products.product')->finished()->notCanceled()->whereBetween('date', [$start, $end]);

        if($subsidiary != null) {
            $sales = $sales->where('subsidiary_id', $subsidiary->id);
        }

        $sales = $sales->get();
        $total_products_commission = 0;
        $total_products_without_commission = 0;
        foreach($sales as $sale) {
            foreach($sale->products as $product) {
                if($product->product->has_commission) {
                    $total_products_commission += $product->price;
                } else {
                    $total_products_without_commission += $product->price;
                }
            }
        }

        $total_products_commission_percentaje = $total_products_commission * 0.1;
        $total_products_without_commission_percentaje = $total_products_without_commission * 0.1;

        return view('admin.reports.products.sold_products', compact('subsidiaries', 'subsidiary', 'start', 'end',
        'total_products_commission', 'total_products_without_commission', 'total_products_commission_percentaje', 'total_products_without_commission_percentaje'));
    }

    public function subsidiaries(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        if ($request->has('start')) {
            $now = Carbon::now();
            $start = $request->start;
            $end = $request->end;
        } else {
            $subsidiary = $subsidiaries->first();
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        return view('admin.reports.subsidiaries', compact('subsidiaries', 'start', 'end'));
    }

    public function unsuppliedOrders(Request $request)
    {
        $subsidiaries = Subsidiary::active()->notLaundry()->get();
        $now = Carbon::now();
        if ($request->has('start')) {
            $start = $request->start;
            $end = $request->end;
        } else {
            $start = $now->format('Y-m-d');
            $end = $now->format('Y-m-d');
        }

        if($request->has('subsidiary_id')) {
            $subsidiary_id = $request->subsidiary_id;
        } else {
            $subsidiary_id = "";
        }

        $requests = WarehouseRequest::with(['subsidiary','products.product','movements.products', 'createdBy'])
        ->where('created_at','>=', $start." 00:00:00")
        ->where('created_at','<=', $end." 23:59:59");

        if($request->subsidiary_id != "") {
            $requests = $requests->where('subsidiary_id', $subsidiary_id);
        }

        $requests = $requests->whereHas('movements', function($q) {
            $q->where('is_confirmed', true);
        })->orderBy('id','DESC')->get();

        //identifica los pedidos que no fueron surtidos completamente y acumula en "filteresRequests"
        $original_requests = $requests;
        $filteredRequests = [];
        foreach ($requests as $index => $req) {
            $removeRequest = false;
            $movement = $req->movements->first();
            foreach ($req->products as $product) {
                if($movementProduct = $movement->products->where('warehouse_product_id', $product->warehouse_product_id)->first()) {
                    if($product->quantity > $movementProduct->quantity) {
                        $removeRequest = true;
                    }
                } else {
                    $removeRequest = true;
                }
            }
            if($removeRequest) {
                $filteredRequests[] = $requests->pull($index);
            }
        }

        return view('admin.reports.unsupplied_orders', compact('filteredRequests','requests', 'subsidiaries', 'start', 'end', 'subsidiary_id'));
    }

    public function barber(Request $request)
    {
        $barbers = $this->getBarbers();
        if ($request->has('employee_id')) {
            $barber = Employee::find($request->employee_id);
            $now = Carbon::now();
            $start = $request->start;
            $end = $request->end;
        } else {
            $barber = $barbers->first();
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $sales = $barber->sales()->select(DB::raw('id, subsidiary_id, date, SUM(total) as sumtotal, SUM(tip) as sumtip'));
        $user = Auth::user();
        if($user->access_subsidiary_id != null){
            $sales = $sales->where('subsidiary_id',$user->access_subsidiary_id);
        }
        $sales = $sales->whereBetween('date', [$start, $end])->finished()->notCanceled()->groupBy('date')->get();
        return view('admin.reports.barber', compact('barbers', 'barber', 'start', 'end', 'sales'));
    }

    public function salesCanceled(Request $request)
    {
        if ($request->has('start')) {
            $start = $request->start;
            $end = $request->end;
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $sales = Sale::with('subsidiary','canceledServices','canceledProducts')->byRange($start,$end)
        ->where(function ($query) {
            $query->canceled()
            ->orWhereHas('canceledServices', function($q) {})
            ->orWhereHas('canceledProducts', function($q) {});
        });
        if($this->user->access_subsidiary_id != null) {
            $sales = $sales->where('subsidiary_id', $this->user->access_subsidiary_id);
        }
        $sales = $sales->get();
        return view('admin.reports.canceledSales', compact('start', 'end', 'sales'));
    }

    public function turns(Request $request)
    {
        if ($request->has('start')) {
            $start = $request->start;
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
        }

        $cash_regiters = CashRegister::where('date', $start);
        $user = Auth::user();
        if($user->access_subsidiary_id != null){
            $cash_regiters = $cash_regiters->where('subsidiary_id',$user->access_subsidiary_id);
        }

        $cash_regiters = $cash_regiters->get()->sortBy(function ($cash_regiter, $key) {
            return $cash_regiter->subsidiary->name;
        });

        return view('admin.reports.turns', compact('cash_regiters', 'start', 'end'));
    }

    public function daily(Request $request)
    {
        if ($request->has('start')) {
            $start = $request->start;
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
        }

        $cash_regiters = CashRegister::with(['sales','subsidiary'])->where('date', $start);
        $user = Auth::user();
        if($user->access_subsidiary_id != null){
            $cash_regiters = $cash_regiters->where('subsidiary_id',$user->access_subsidiary_id);
        }

        $cash_regiters = $cash_regiters->get()->sortBy(function ($cash_regiter, $key) {
            return $cash_regiter->subsidiary->name;
        });
        return view('admin.reports.daily', compact('cash_regiters', 'start', 'end'));
    }

    public function accumulatedDaily(Request $request)
    {
        $now = Carbon::now();
        $start = $now->format('Y-m-d');
        $end = $now->format('Y-m-d');

        if ($request->has('start')) {
            $start = $request->start;
        }
        if ($request->has('end')) {
            $end = $request->end;
        }

        $subsidiaries = $this->getSubsidiaries();
        return view('admin.reports.accumulated_daily', compact('subsidiaries', 'start', 'end'));
    }

    public function dailyLaundryServices(Request $request)
    {
        if ($request->has('start')) {
            $start = $request->start;
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
        }

        $laundry_services = LaundryService::where('date', $start)->get();
        return view('admin.reports.daily_laundry_services', compact('laundry_services', 'start'));
    }

    public function servicesSubsidiaries(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();

        if ($request->has('start')) {
            $now = Carbon::now();
            $start = $request->start;
            $end = $request->end;
        } else {
            $subsidiary = $subsidiaries->first();
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        return view('admin.reports.services.subsidiaries', compact('subsidiaries', 'start', 'end'));
    }

    public function servicesEmployees(Request $request)
    {
        $barbers = $this->getBarbers();
        if ($request->has('start')) {
            $start = $request->start;
            $end = $request->end;
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        return view('admin.reports.services.employees', compact('barbers', 'start', 'end'));
    }

    public function servicesSubsidiary(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        if ($request->has('start')) {
            $subsidiary = Subsidiary::find($request->subsidiary_id);
            $start = $request->start;
            $end = $request->end;
        } else {
            $subsidiary = $subsidiaries->first();
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $sales = $subsidiary->sales()
            ->whereBetween('date', [$start, $end])
            ->finished()->notCanceled()->get();

        return view('admin.reports.services.subsidiary', compact('sales', 'subsidiary', 'subsidiaries', 'start', 'end'));
    }

    public function servicesPerTime(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        $turns = Turn::get();

        if ($request->has('start')) {
            $subsidiary = Subsidiary::find($request->subsidiary_id);
            $turn = Turn::find($request->turn_id);
            $start = $request->start;
            $end = $request->end;
        } else {
            $subsidiary = $subsidiaries->first();
            $turn = $turns->first();
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $filteredSales = $subsidiary->sales()->with('servicesWithoutWaiting')
        ->where('turn_id', $turn->id)
        ->whereBetween('date', [$start, $end])
        ->finished()->notCanceled()->get();

        $salesByDate = $filteredSales->groupBy(function($sale) {
            return date('Y-m-d',strtotime($sale->created_at));
        });
        foreach($salesByDate as $index => &$sales) {

            $salesByDate[$index] = $sales->groupBy(function($sale) {
                return date('H',strtotime($sale->created_at));
            });
        }
        $ranges = [];
        $rangeStart = intval(date('H',strtotime($turn->start)));
        $rangeEnd = intval(date('H',strtotime($turn->end)));

        for($c = $rangeStart; $c <= $rangeEnd; $c++) {
            $ranges[] = $c < 10 ? "0$c" : "$c";
        }

        return view('admin.reports.services.per-time', compact('ranges', 'filteredSales', 'salesByDate','turns', 'turn', 'subsidiary', 'subsidiaries', 'start', 'end'));
    }

    public function productsSubsidiary(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        if ($request->has('start')) {
            $subsidiary = Subsidiary::find($request->subsidiary_id);
            $start = $request->start;
            $end = $request->end;
        } else {
            $subsidiary = $subsidiaries->first();
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        return view('admin.reports.services.products', compact('subsidiary', 'subsidiaries', 'start', 'end'));
    }

    public function productsEmployees(Request $request)
    {

        if ($request->has('start')) {
            // $subsidiary = Subsidiary::find($request->subsidiary_id);
            $start = $request->start;
            $end = $request->end;
        } else {
            // $subsidiary = $subsidiaries->first();
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $employees = Employee::with([
        'cashRegisters' => function ($query) use ($start, $end) {
            $query->whereBetween('date', [$start, $end]);
        },
        'sales2' => function ($query) use ($start, $end) {
            $query->whereBetween('date', [$start, $end]);
        },
        'sales2.products.product', 'sales2.services.service', 'cashRegisters.salesFinished.products.product', 'cashRegisters.salesFinished.services.service'
        ])->barbersOrCashiers()->active()->where('id', '!=', 46)->get();

        return view('admin.reports.products_by_employee', compact('employees', 'start', 'end'));
    }

    public function dailyTurns(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        if ($request->has('start')) {
            $subsidiary = Subsidiary::find($request->subsidiary_id);
            $start = $request->start;
        } else {
            $now = Carbon::now();
            $subsidiary = $subsidiaries->first();
            $start = $now->format('Y-m-d');
        }

        $cash_regiters = $subsidiary->cashRegisters()->where('date', $start)->get();

        return view('admin.reports.daily_turns', compact('cash_regiters', 'start', 'subsidiaries', 'subsidiary'));
    }

    public function dailyCuts(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        if ($request->has('start')) {
            $start = $request->start;
        } else {
            $now = Carbon::now();
            $start = $now->format('Y-m-d');
        }

        $cash_registers = CashRegister::with('subsidiary','lastMovement.cashierMovement')->whereDate('created_at', $start);
        if($this->user->access_subsidiary_id != null) {
            $cash_registers = $cash_registers->where('subsidiary_id', $this->user->access_subsidiary_id);
        }
        $cash_registers = $cash_registers->groupBy('subsidiary_id')->get();
        //dd($cash_registers);
        return view('admin.reports.daily_cuts', compact('cash_registers', 'start'));
    }

    public function entries(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();

        if ($request->has('subsidiary_id')) {
            $subsidiary = Subsidiary::find($request->subsidiary_id);
            $folio = $request->folio;
            if ($folio) {
                $entries = $subsidiary->entries()->whereFolio($folio)->orderBy('created_at', 'desc')->get();
            } else {
                $entries = $subsidiary->entries()->orderBy('created_at', 'desc')->get();
            }
        } else {
            $subsidiary = $subsidiaries->first();
            $folio = null;
            $entries = $subsidiary->entries()->orderBy('created_at', 'desc')->get();
        }

        return view('admin.reports.inventories.entries', compact('entries', 'folio', 'subsidiaries', 'subsidiary'));
    }

    public function departures(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();

        if ($request->has('subsidiary_id')) {
            $subsidiary = Subsidiary::find($request->subsidiary_id);
            $folio = $request->folio;
            if ($folio) {
                $departures = $subsidiary->departures()->whereFolio($folio)->orderBy('created_at', 'desc')->get();
            } else {
                $departures = $subsidiary->departures()->orderBy('created_at', 'desc')->get();
            }
        } else {
            $subsidiary = $subsidiaries->first();
            $folio = null;
            $departures = $subsidiary->departures()->orderBy('created_at', 'desc')->get();
        }

        return view('admin.reports.inventories.departures', compact('departures', 'folio', 'subsidiaries', 'subsidiary'));
    }

    public function adjustments(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();

        if ($request->has('subsidiary_id')) {
            $subsidiary = Subsidiary::find($request->subsidiary_id);
            $folio = $request->folio;
            if ($folio) {
                $adjustments = $subsidiary->adjustments()->whereFolio($folio)->orderBy('created_at', 'desc')->get();
            } else {
                $adjustments = $subsidiary->adjustments()->orderBy('created_at', 'desc')->get();
            }

        } else {
            $subsidiary = $subsidiaries->first();
            $folio = null;
            $adjustments = $subsidiary->adjustments()->orderBy('created_at', 'desc')->get();
        }

        return view('admin.reports.inventories.adjustments', compact('adjustments', 'folio', 'subsidiaries', 'subsidiary'));
    }

    public function weeklyBarber(Request $request)
    {
        $barbers = $this->getBarbers();
        $user = Auth::user();

        if ($request->has('employee_id')) {
            $start = Carbon::parse($request->start);
            $end = Carbon::parse($request->end);
            $barber = Employee::with(['salesRepairedBy' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            }])->find($request->employee_id);
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek();
            $end = $now->endOfWeek();
            $barber = Employee::with(['salesRepairedBy' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            }])->whereJob('Barbero')->first();
        }

        if($user->isA('employee')) {
            $barber = Employee::with(['salesRepairedBy' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            }])->where('user_id', $user->id)->first();
        }


        // dd($barber);

        $copy = $start->copy();
        $sales = [];
        while ($copy->lte($end)) {
            $sales[] = $barber->weeklySales($copy->format('Y-m-d'))->select(DB::raw('id, subsidiary_id, date, SUM(subtotal) as sumtotal'))->groupBy('subsidiary_id')->get();
            $copy->addDay();
        }

        // dd($sales);

        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');

        return view('admin.reports.weekly-barber', compact('barbers', 'barber', 'start', 'end', 'sales'));
    }

    public function signatureByEmployee(Request $request) {
        $employees = Employee::all();
        if ($request->has('employee_id')) {
            $employee = Employee::find($request->employee_id);
        } else {
            $employee = $employees->first();
        }
        if($doc = $employee->hasDocument('id')){
            $path = $doc->path;
        } else {
            $path = "#";
        }

        return view('admin.reports.signatureByEmployee', compact('employees', 'employee', 'path'));
    }

    public function weeklyServicesBarber(Request $request)
    {
        $now = Carbon::now();
        if ($request->has('employee_id')) {
            $start = Carbon::parse($request->start);
            $end = Carbon::parse($request->end);
        } else {
            $start = $now->startOfWeek();
            $end = $now->endOfWeek();
        }

        $barbers = $this->getBarbers($start, $end);

        if ($request->has('employee_id')) {
            $barber = $barbers->where('id',$request->employee_id)->first();
        } else {
            $barber = $barbers->first();
        }

        $copy = $start->copy();
        $sales = [];
        // while ($copy->lte($end)) {
        //     $sales[] = $barber->weeklySales($copy->format('Y-m-d'))->select(DB::raw('id, subsidiary_id, date, SUM(subtotal) as sumtotal'))->groupBy('subsidiary_id')->get();
        //     $copy->addDay();
        // }

        $dates = [];
        while ($copy->lte($end)) {
            $date = $copy->format('Y-m-d');
            $sales[] = $barber->salesFinished->where('date', $date)->groupBy('subsidiary_id');
            $dates[] = $copy->format('Y-m-d');
            $copy->addDay();
        }

        $isMerida = $barber->subsidiary_id == 14;
        $guaranteed_salary = $isMerida ? $barber->salary : 200;

        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');
        return view('admin.reports.services.weekly-barbers', compact('isMerida', 'barbers', 'barber', 'start', 'end', 'sales', 'dates', 'guaranteed_salary'));
    }

    public function employeesExcellence(Request $request)
    {
        if ($request->has('start')) {
            $start = Carbon::parse($request->start)->format('Y-m-d');
            $end = Carbon::parse($request->end)->format('Y-m-d');
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $employees = Employee::with([
        'attendances' => function ($query) use ($start, $end) {
            $query->whereBetween('date', [$start, $end]);
        },
        'schedules' => function ($query) use ($start, $end) {
            $query->whereBetween('date', [$start, $end])->where('subsidiary_id', '!=', 8);
        },
        'issues' => function ($query) use ($start, $end) {
            $query->whereBetween('date', [$start, $end]);
        },
        'salesFinished' => function ($query) use ($start, $end) {
            $query->whereBetween('date', [$start, $end]);
        },
        'sales2' => function ($query) use ($start, $end) {
            $query->whereBetween('date', [$start, $end]);
        },
        'sales2.products.product', 'sales2.services.service','salesFinished.products.product', 'salesFinished.servicesWithoutWaiting','salesFinished.diary'
        ])->barbers()->active()->where('has_excellence', true);

        if($this->user->access_subsidiary_id != null) {
            $employees = $employees->where('subsidiary_id',$this->user->access_subsidiary_id);
        }

        $employees = $employees->get();
        $total_diaries = [];
        $total_services = [];
        $total_products = [];
        $total_schedules = [];
        $total_attendances = [];
        $total_punctuality = [];
        $punctuality_percentaje = [];
        $attendance_percentaje = [];
        $issue_percentaje = [];

        foreach($employees as $employee) {

            $total_diaries[$employee->id] = 0;
            $total_services[$employee->id] = 0;
            $total_products[$employee->id] = 0;

            $attendances = $employee->attendances->where('type', 'entrada');
            $schedules = $employee->schedules;

            $total_schedules[$employee->id] = $schedules->count();
            $total_attendances[$employee->id] = $attendances->count();
            $total_punctuality[$employee->id] = 0;

            foreach($schedules as $schedule) {

                if($employee->attendances == null || $employee->attendances->count() == 0) continue;

                $attendances_date = $employee->attendances->where('date', $schedule->date);
                $attendance = $attendances_date->first();

                if($attendance == null) continue;

                $attendanceStart = Carbon::parse($attendance->time);
                $scheduleStart = $schedule->employee->job == "Cajero" ? Carbon::parse($schedule->turn->start) : Carbon::parse($schedule->turn->barber_start);

                if($attendanceStart > $scheduleStart) continue;

                if($attendances_date->count() >= 3) {
                    $start_rest = $attendances_date->where('type', 'salida descanso')->first();
                    $end_rest = $attendances_date->where('type', 'entrada descanso')->first();

                    if($start_rest == null || $end_rest == null) continue;

                    $startt = Carbon::parse($start_rest->time);
                    $endd = Carbon::parse($end_rest->time);
                    $startt->second = 0;
                    $endd->second = 0;
                    if($startt->diffInMinutes($endd) <= $schedule->rest_minutes) {
                        $total_punctuality[$employee->id]++;
                    }

                } else {
                    $total_punctuality[$employee->id]++;
                }

            }

            $punctuality_percentaje[$employee->id] = $total_schedules[$employee->id] > 0 ? $total_punctuality[$employee->id] * 100 / $total_schedules[$employee->id] : 0;
            $attendance_percentaje[$employee->id] = $total_schedules[$employee->id] > 0 ? $total_attendances[$employee->id] * 100 / $total_schedules[$employee->id] : 0;
            $issue_percentaje[$employee->id] = $employee->issues && $employee->issues->count() > 0 ? 0 : 100;

            list($products, $commission, $products_qty) = $employee->getEmployeeProductCommission();
            $total_products[$employee->id] = $products_qty;
            $total_diaries[$employee->id] = $employee->getDiariesQty();

            // get total services.
            foreach ($employee->salesFinished as $sale) {
                if($sale->servicesWithoutWaiting) {
                    $total_services[$employee->id] += $sale->servicesWithoutWaiting->count();
                }
            }
        }

        // order all results to calculate rankings
        arsort($total_diaries);
        arsort($total_services);
        arsort($total_products);
        arsort($total_schedules);
        arsort($attendance_percentaje);
        arsort($total_punctuality);
        arsort($punctuality_percentaje);

        $total_diaries_points = [];
        $total_services_points = [];
        $total_products_points = [];
        $total_attendances_points = [];
        $total_punctuality_points = [];

        // many foreach to add equivalent points
        $cont = 1;
        foreach($total_diaries as $total_diary) {
            if(!isset($total_diaries_points[$total_diary])) {
                $total_diaries_points[$total_diary] = $cont;
                $cont++;
            }
        }
        $cont = 1;
        foreach($total_services as $total_service) {
            if(!isset($total_services_points[$total_service])) {
                $total_services_points[$total_service] = $cont;
                $cont++;
            }
        }
        $cont = 1;
        foreach($total_products as $total_product) {
            if(!isset($total_products_points[$total_product])) {
                $total_products_points[$total_product] = $cont;
                $cont++;
            }
        }

        $cont = 1;
        foreach($attendance_percentaje as $total_attendance) {
            if(!isset($total_attendances_points[$total_attendance])) {
                $total_attendances_points[$total_attendance] = $cont;
                $cont++;
            }
        }
        $cont = 1;
        foreach($punctuality_percentaje as $punctuality) {
            if(!isset($total_punctuality_points[$punctuality])){
                $total_punctuality_points[$punctuality] = $cont;
                $cont++;
            }
        }

        // set total points by employee
        foreach($employees as $employee) {
            $points = 0;
            $points += $total_diaries_points[$total_diaries[$employee->id]];
            $points += $total_services_points[$total_services[$employee->id]];
            $points += $total_products_points[$total_products[$employee->id]];
            $points += $total_attendances_points[$attendance_percentaje[$employee->id]];
            $points += $total_punctuality_points[$punctuality_percentaje[$employee->id]];
            if($issue_percentaje[$employee->id] == 100) {
                $points += 1;
            }

            $employee->points = $points;
        }

        return view('admin.reports.barber-excellence', compact('employees', 'start', 'end', 'total_diaries', 'total_services', 'total_products',
        'total_schedules', 'total_punctuality', 'total_attendances', 'punctuality_percentaje', 'attendance_percentaje', 'issue_percentaje',
    'total_diaries_points', 'total_services_points', 'total_products_points', 'total_schedules_points', 'total_attendances_points', 'total_punctuality_points'));
    }

    public function employeesWithoutExcellence(Request $request)
    {
        if ($request->has('start')) {
            $start = Carbon::parse($request->start)->format('Y-m-d');
            $end = Carbon::parse($request->end)->format('Y-m-d');
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $employees = Employee::with(['attendances' => function ($query) use ($start, $end) {
            $query->whereBetween('date', [$start, $end]);
        },
        'schedules' => function ($query) use ($start, $end) {
            $query->whereBetween('date', [$start, $end])->where('subsidiary_id', '!=', 8);
        },
        'issues' => function ($query) use ($start, $end) {
            $query->whereBetween('date', [$start, $end]);
        },
        'salesFinished' => function ($query) use ($start, $end) {
            $query->whereBetween('date', [$start, $end]);
        },
        'sales2' => function ($query) use ($start, $end) {
            $query->whereBetween('date', [$start, $end]);
        },
        'sales2.products.product', 'sales2.services.service',
        'salesFinished.products.product','salesFinished.servicesWithoutWaiting','salesFinished.diary'
        ])->barbers()->active()->where('has_excellence', false);

        if($this->user->access_subsidiary_id != null) {
            $employees = $employees->where('subsidiary_id',$this->user->access_subsidiary_id);
        }

        $employees = $employees->get();
        $total_diaries = [];
        $total_services = [];
        $total_products = [];
        $total_schedules = [];
        $total_attendances = [];
        $total_punctuality = [];
        $punctuality_percentaje = [];
        $attendance_percentaje = [];
        $issue_percentaje = [];

        foreach($employees as $employee) {

            $total_diaries[$employee->id] = 0;
            $total_services[$employee->id] = 0;
            $total_products[$employee->id] = 0;

            $attendances = $employee->attendances->where('type', 'entrada');
            $schedules = $employee->schedules;

            $total_schedules[$employee->id] = $schedules->count();
            $total_attendances[$employee->id] = $attendances->count();
            $total_punctuality[$employee->id] = 0;

            foreach($schedules as $schedule) {

                if($employee->attendances == null || $employee->attendances->count() == 0) continue;

                $attendances_date = $employee->attendances->where('date', $schedule->date);
                $attendance = $attendances_date->first();

                if($attendance == null) continue;

                $attendanceStart = Carbon::parse($attendance->time);
                $scheduleStart = $schedule->employee->job == "Cajero" ? Carbon::parse($schedule->turn->start) : Carbon::parse($schedule->turn->barber_start);

                if($attendanceStart > $scheduleStart) continue;

                if($attendances_date->count() >= 3) {
                    $start_rest = $attendances_date->where('type', 'salida descanso')->first();
                    $end_rest = $attendances_date->where('type', 'entrada descanso')->first();

                    if($start_rest == null || $end_rest == null) continue;

                    $startt = Carbon::parse($start_rest->time);
                    $endd = Carbon::parse($end_rest->time);
                    $startt->second = 0;
                    $endd->second = 0;
                    if($startt->diffInMinutes($endd) <= $schedule->rest_minutes) {
                        $total_punctuality[$employee->id]++;
                    }

                } else {
                    $total_punctuality[$employee->id]++;
                }
            }

            $punctuality_percentaje[$employee->id] = $total_schedules[$employee->id] > 0 ? $total_punctuality[$employee->id] * 100 / $total_schedules[$employee->id] : 0;
            $attendance_percentaje[$employee->id] = $total_schedules[$employee->id] > 0 ? $total_attendances[$employee->id] * 100 / $total_schedules[$employee->id] : 0;
            $issue_percentaje[$employee->id] = $employee->issues && $employee->issues->count() > 0 ? 0 : 100;

            list($products, $commission, $products_qty) = $employee->getEmployeeProductCommission();
            $total_products[$employee->id] = $products_qty;

            $total_diaries[$employee->id] = $employee->getDiariesQty();

            foreach ($employee->salesFinished as $sale) {
                if($sale->servicesWithoutWaiting->count() == 0) $total_services[$employee->id] += $sale->servicesWithoutWaiting->count();
            }
        }

        // order all results to calculate rankings
        arsort($total_diaries);
        arsort($total_services);
        arsort($total_products);
        arsort($total_schedules);
        arsort($attendance_percentaje);
        arsort($total_punctuality);
        arsort($punctuality_percentaje);

        $total_diaries_points = [];
        $total_services_points = [];
        $total_products_points = [];
        $total_attendances_points = [];
        $total_punctuality_points = [];

        // many foreach to add equivalent points
        $cont = 1;
        foreach($total_diaries as $total_diary) {
            if(!isset($total_diaries_points[$total_diary])) {
                $total_diaries_points[$total_diary] = $cont;
                $cont++;
            }
        }
        $cont = 1;
        foreach($total_services as $total_service) {
            if(!isset($total_services_points[$total_service])) {
                $total_services_points[$total_service] = $cont;
                $cont++;
            }
        }
        $cont = 1;
        foreach($total_products as $total_product) {
            if(!isset($total_products_points[$total_product])) {
                $total_products_points[$total_product] = $cont;
                $cont++;
            }
        }

        $cont = 1;
        foreach($attendance_percentaje as $total_attendance) {
            if(!isset($total_attendances_points[$total_attendance])) {
                $total_attendances_points[$total_attendance] = $cont;
                $cont++;
            }
        }
        $cont = 1;
        foreach($punctuality_percentaje as $punctuality) {
            if(!isset($total_punctuality_points[$punctuality])){
                $total_punctuality_points[$punctuality] = $cont;
                $cont++;
            }
        }

        // set total points by employee
        foreach($employees as $employee) {
            $points = 0;
            $points += $total_diaries_points[$total_diaries[$employee->id]];
            $points += $total_services_points[$total_services[$employee->id]];
            $points += $total_products_points[$total_products[$employee->id]];
            $points += $total_attendances_points[$attendance_percentaje[$employee->id]];
            $points += $total_punctuality_points[$punctuality_percentaje[$employee->id]];
            if($issue_percentaje[$employee->id] == 100) {
                $points += 1;
            }

            $employee->points = $points;
        }

        return view('admin.reports.barber-without-excellence', compact('employees', 'start', 'end', 'total_diaries', 'total_services', 'total_products',
        'total_schedules', 'total_punctuality', 'total_attendances', 'punctuality_percentaje', 'attendance_percentaje', 'issue_percentaje',
    'total_diaries_points', 'total_services_points', 'total_products_points', 'total_schedules_points', 'total_attendances_points', 'total_punctuality_points'));
    }

    public function promotionsByRange(Request $request)
    {
        if ($request->has('start')) {
            $start = Carbon::parse($request->start)->format('Y-m-d');
            $end = Carbon::parse($request->end)->format('Y-m-d');
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $sales = Sale::with(['subsidiary', 'customer', 'cashRegister.employee', 'authorized'])
        ->dates([$start, $end])->withPromotion();

        if($this->user->access_subsidiary_id != null){
            $sales = $sales->where('subsidiary_id',$this->user->access_subsidiary_id);
        }

        $sales = $sales->paginate(10);
        return view('admin.reports.services.with-promotion', compact('sales', 'start', 'end'));
    }

    public function attendancesControl(Request $request)
    {
        $employee = Employee::find($request->employee_id);
        $start = Carbon::parse($request->start);
        $end = Carbon::parse($request->end);
        return view('admin.reports.attendances.attendances-control', compact('employee', 'start', 'end'));
    }

    public function weeklyPaysheetBarber(Request $request)
    {
        $user = Auth::user();
        $barbers = $this->getBarbers();
        $now = Carbon::now();

        if ($request->has('employee_id')) {
            $start = Carbon::parse($request->start);
            $end = Carbon::parse($request->end);
        } else {
            $start = $now->startOfWeek();
            $end = $now->endOfWeek();
        }

        $barber = Employee::with([
            'salesRepairedBy' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'cashRegisters' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'salesFinished' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'sales2' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'schedules' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'attendances' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'percepciones' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'deducciones' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'issues' => function ($query) use ($start, $end) {
                $query->where('type','1')->whereBetween('date', [$start, $end]);
            },
            'sales2.products.product', 'sales2.services.service', 'cashRegisters.sales.products.product',
            'cashRegisters.sales.services.service', 'salesFinished.services.service'
        ])->active();

        if ($request->has('employee_id')) {
            $barber = $barber->find($request->employee_id);
        } else {
            $barber = $barber->whereJob('Barbero')->first();
        }

        $copy = $start->copy();
        $dates = [];
        while ($copy->lte($end)) {
            $dates[] = $copy->format('Y-m-d');
            $copy->addDay();
        }

        $apply_new_prices = $start >= Carbon::parse('2022-08-11');

        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');

        $paysheet = Paysheet::byDateRange($start,$end)->where('employee_id', $barber->id)->first();

        $employee_pending_sales = Sale::with('products','employee','payments')
        ->where('employee_id',$barber->id)->forEmployees()
        ->notFinished()->notCanceled()->get();

        $subsidiary_code = $this->hasSubdomain();
        $current_subsidiary = Subsidiary::whereKey($subsidiary_code)->get()->first();

        $isMerida = $barber->subsidiary_id == 14;

        return view('admin.reports.paysheet.weekly-barber', compact('isMerida', 'employee_pending_sales', 'current_subsidiary', 'barbers', 'barber', 'start', 'end', 'dates', 'user', 'paysheet', 'apply_new_prices'));
    }

    public function weeklyPaysheetBarberPrint($barber_id, $start, $end)
    {
        $user = Auth::user();
        $barbers = Employee::whereJob('Barbero')->active()->get();

        if ($barber_id) {
            $barber = Employee::with([
                'sales2' => function ($query) use ($start, $end) {
                    $query->whereBetween('date', [$start, $end]);
                },
                'attendances' => function ($query) use ($start, $end) {
                    $query->whereBetween('date', [$start, $end]);
                },
                'schedules' => function ($query) use ($start, $end) {
                    $query->whereBetween('date', [$start, $end]);
                },
            ])->find($barber_id);
            $start = Carbon::parse($start);
            $end = Carbon::parse($end);
        } else {
            $firstBarber = $barbers->first();
            $barber = Employee::with([
                'sales2' => function ($query) use ($start, $end) {
                    $query->whereBetween('date', [$start, $end]);
                },
                'attendances' => function ($query) use ($start, $end) {
                    $query->whereBetween('date', [$start, $end]);
                },
                'schedules' => function ($query) use ($start, $end) {
                    $query->whereBetween('date', [$start, $end]);
                },
            ])->find($firstBarber->id);
            $now = Carbon::now();
            $start = $now->startOfWeek();
            $end = $now->endOfWeek();
        }

        $copy = $start->copy();
        $dates = [];
        while ($copy->lte($end)) {
            $dates[] = $copy->format('Y-m-d');
            $copy->addDay();
        }
        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');

        return view('admin.reports.paysheet.weekly-barber-print', compact('barbers', 'barber', 'start', 'end', 'dates', 'user'));
    }

    public function saleStatus(Request $request)
    {
        if ($request->has('start')) {
            $start = $request->start;
        } else {
            $now = Carbon::now();
            $start = $now->format('Y-m-d');
        }

        $sales = Sale::where('date', $start);
        $user = Auth::user();
        if($user->access_subsidiary_id != null){
            $sales = $sales->where('subsidiary_id',$user->access_subsidiary_id);
        }
        $sales = $sales->finished()->orderBy('subsidiary_id')->get();
        return view('admin.reports.status', compact('sales', 'start'));
    }

    public function commissionStatus(Request $request)
    {
        if ($request->has('start')) {
            $start = Carbon::parse($request->start)->format('Y-m-d');;
            $end = Carbon::parse($request->end)->format('Y-m-d');;
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $employees = $this->getEmployeesWithSales($start, $end);

        if ($request->has('employee_id')) {
            $employee = $employees->where('id', $request->employee_id)->first();
        } else {
            $employee = $employees->first();
        }

        if($employee->job == 'Barbero') {
            $sales = $employee->salesFinished;
        } else {
            // get sales from cashier, only if the sale contains only products and applies commission
            $cash_register_ids = $employee->cashRegisters->pluck('id');
            $sales = Sale::whereIn('cash_register_id', $cash_register_ids)
            ->bocsiBarber()
            // ->whereDoesntHave('services')
            ->whereHas('products', function($q) {
                $q->whereHas('product', function($q2) {
                    $q2->where('include_in_reports', true);
                });
            })
            ->finished()->notCanceled()->get();
        }

        $user = Auth::user();
        // if($user->access_subsidiary_id != null){
        //     $sales = $sales->where('subsidiary_id',$user->access_subsidiary_id);
        // }
        // $sales = $sales->orderBy('subsidiary_id')->get();
        return view('admin.reports.commission_status', compact('sales', 'start', 'end', 'employees', 'employee'));
    }

    public function stocks()
    {
        $products = Product::all();
        $subsidiaries = Subsidiary::with(['products'])->active()->get();
        return view('admin.reports.inventories.stocks', compact('products', 'subsidiaries'));
    }

    public function warehouseStocks(Request $request)
    {
        if ($request->has('history_id')) {
            $history_id = $request->history_id;
            $products = WarehouseHistoryProduct::where('warehouse_history_id', $history_id)
            ->active()->orderBy('code')->get();
        } else {
            $history_id = null;
            $products = WarehouseProduct::active()->orderBy('code')->get();
        }
        $warehouse_histories = WarehouseHistory::active()->orderBy('date', 'desc')->get();
        return view('admin.reports.inventories.warehouse_stocks', compact('products','warehouse_histories','history_id'));
    }

    public function productsToOrder()
    {
        $products = WarehouseProduct::active()->orderBy('code')->get();
        return view('admin.reports.inventories.warehouse_products_to_order', compact('products'));
    }

    public function movementsByProduct(Request $request)
    {
        $products = WarehouseProduct::active()->orderBy('code', 'asc')->get();
        $product = $products->first();
        if ($request->has('product_id')) {
            $product = $products->where('id', $request->product_id)->first();
        }

        $product_id = $product->id;
        $movements = WarehouseMovement::with(['products'])->notCanceled()->confirmed()
            ->whereHas('products', function($q) use ($product_id) {
                $q->where('warehouse_product_id', $product_id);
            })->orderBy('id','desc')->limit(10)->get();

        $adjustments = WarehouseAdjustment::with(['product'])->where('warehouse_product_id', $product_id)->limit(10)->get();
        $types = WarehouseMovement::getEnum('types');

        return view('admin.reports.inventories.warehouse_movements_by_product', compact('movements', 'product', 'products', 'adjustments', 'types'));
    }

    public function weeklyPaysheetResume(Request $request)
    {
        $employees = Employee::active()
            ->orderByRaw('job = ? desc', ['Cajero'])
            ->orderByRaw('job = ? desc', ['Barbero'])
            ->orderByRaw('job = ? desc', ['Supervisor'])
            ->orderByRaw('job = ? desc', ['Auxiliar'])
            ->orderBy('short_name', 'asc')
            ->paginate(8);
        if ($request->has('start')) {
            $now = Carbon::now();
            $start = Carbon::parse($request->start);
            $end = Carbon::parse($request->end);
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek();
            $end = $now->endOfWeek();
        }

        $copy = $start->copy();
        $dates = [];
        while ($copy->lte($end)) {
            $dates[] = $copy->format('Y-m-d');
            $copy->addDay();
        }
        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');

        return view('admin.reports.paysheet.weekly-resume', compact('employees', 'start', 'end', 'dates'));
    }

    public function weeklyPaysheetResumePost(Request $request)
    {
        ini_set('max_input_vars', 1000000);
        $data = $request->all();
        foreach ($data['employee'] as $paysheet) {
            if (Paysheet::where('key', $data['start'] . '-' . $data['end'])->where('employee_id', $paysheet['id'])->first()) {
                continue;
            }

            $tt = Paysheet::create([
                'key' => $data['start'] . '-' . $data['end'],
                'start' => $data['start'],
                'end' => $data['end'],
                'employee_id' => $paysheet['id'] ?? 0,
                'total' => $paysheet['total'] ?? 0,
                'asistencia' => $paysheet['asistencia'] ?? 0,
                'asistencia_pay' => (($paysheet['asistencia'] ?? 0) > 0 ? true : false),
                'puntualidad' => $paysheet['puntualidad'] ?? 0,
                'puntualidad_pay' => (($paysheet['puntualidad'] ?? 0) > 0 ? true : false),
                'productividad' => $paysheet['productividad'] ?? 0,
                'productividad_pay' => (($paysheet['productividad'] ?? 0) > 0 ? true : false),
                'sunday' => $paysheet['sunday'] ?? 0,
                'sunday_pay' => (($paysheet['sunday'] ?? 0) > 0 ? true : false),
                'excellence' => $paysheet['excellence'] ?? 0,
                'excellence_pay' => (($paysheet['excellence'] ?? 0) > 0 ? true : false),
                'extra' => $paysheet['extra'] ?? 0,
                'extra_pay' => (($paysheet['extra'] ?? 0) > 0 ? true : false),
                'otros_ingresos' => $paysheet['otros_ingresos'] ?? 0,
                'repairs' => $paysheet['repairs'] ?? 0,
                'cardtips' => $paysheet['cardtips'] ?? 0,
                'products' => $paysheet['products'] ?? 0,
                'total_ingreso' => $paysheet['total_ingresos'] ?? 0,
                'prestamo' => $paysheet['prestamo'] ?? 0,
                'faltas' => $paysheet['total_faltas'] ?? 0,
                'faltas_pay' => ($paysheet['total_faltas'] > 0 ? true : false),
                'uniforme' => $paysheet['uniforme'] ?? 0,
                'infonavit' => $paysheet['monto_infonavit'] ?? 0,
                'otras' => $paysheet['otras'] ?? 0,
                'discount_repairs' => $paysheet['discount-repairs'] ?? 0,
                'total_deducciones' => $paysheet['desctotal'] ?? 0,
                'neto' => $paysheet['neto'] ?? 0,
                'depositado' => $paysheet['depositado'] ?? 0,
                'diferencia' => $paysheet['diferencia'] ?? 0,
            ]);
        }

        return redirect()->route('paysheets.index');
    }

    public function weeklyPaysheetEmployee(Request $request)
    {
        $employees = Employee::where('job', '<>', 'Barbero')->active()->get();
        if ($request->has('employee_id')) {
            $now = Carbon::now();
            $start = Carbon::parse($request->start);
            $end = Carbon::parse($request->end);
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek();
            $end = $now->endOfWeek();
        }

        $employee = Employee::with([
            'salesRepairedBy' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'cashRegisters' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'sales2' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'schedules' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'attendances' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'percepciones' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'deducciones' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'issues' => function ($query) use ($start, $end) {
                $query->where('type','1')->whereBetween('date', [$start, $end]);
            },
            'sales2.products.product', 'sales2.services.service',
            'cashRegisters.sales.products.product', 'cashRegisters.sales.services.service',
            'deposits'
        ])->active();

        if ($request->has('employee_id')) {
            $employee = $employee->find($request->employee_id);
        } else {
            $employee = $employee->where('job', '<>', 'Barbero')->first();
        }

        $copy = $start->copy();
        $dates = [];
        while ($copy->lte($end)) {
            $dates[] = $copy->format('Y-m-d');
            $copy->addDay();
        }
        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');

        $paysheet = Paysheet::byDateRange($start,$end)->where('employee_id', $employee->id)->first();
        $user = Auth::user();

        $employee_pending_sales = Sale::with('products','employee','payments')
        ->where('employee_id',$employee->id)->forEmployees()
        ->notFinished()->notCanceled()->get();

        $subsidiary_code = $this->hasSubdomain();
        $current_subsidiary = Subsidiary::whereKey($subsidiary_code)->get()->first();

        return view('admin.reports.paysheet.weekly-employee', compact('employee_pending_sales', 'current_subsidiary', 'employees', 'employee', 'start', 'end', 'dates', 'paysheet', 'user'));
    }

    public function servicesSubsidiariesResume(Request $request)
    {
        $services = Service::notLaundry()->get();

        if ($request->has('start')) {
            $start = Carbon::parse($request->start)->format('Y-m-d');;
            $end = Carbon::parse($request->end)->format('Y-m-d');;
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }
        $subsidiaries = Subsidiary::with(['sales' => function ($query) use ($start, $end) {
            $query->whereBetween('date', [$start, $end])
            ->where('paid', true)
            ->where('finish', true)
            ->whereNull('canceled_by');
        }])->active()->get();

        return view('admin.reports.services.subsidiaries-services', compact('services', 'subsidiaries', 'start', 'end'));
    }

    public function productsComisiones(Request $request)
    {
        if ($request->has('start')) {
            $start = Carbon::parse($request->start);
            $end = Carbon::parse($request->end);
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek();
            $end = $now->endOfWeek();
        }

        $employees = Employee::with([
            'cashRegisters' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'sales2' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'sales2.products.product', 'sales2.services.service', 'cashRegisters.sales.products.product', 'cashRegisters.sales.services.service'
        ])->barbersOrCashiers()->active()->where('id', '!=', 46);

        if($this->user->access_subsidiary_id != null) {
            $employees = $employees->where('subsidiary_id', $this->user->access_subsidiary_id);
        }

        $employees = $employees->get();

        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');

        return view('admin.reports.comisiones-products', compact('employees', 'start', 'end'));
    }

    public function salesTips(Request $request)
    {
        if ($request->has('start')) {
            $start = Carbon::parse($request->start);
            $end = Carbon::parse($request->end);
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek();
            $end = $now->endOfWeek();
        }

        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');

        $sales = Sale::whereBetween('date', [$start, $end]);
        $user = Auth::user();
        if($user->access_subsidiary_id != null){
            $sales = $sales->where('subsidiary_id',$user->access_subsidiary_id);
        }
        $sales = $sales->finished()->notCanceled()->orderBy('employee_id')->get()->groupBy('employee_id');
        return view('admin.reports.sales-tips', compact('sales', 'start', 'end'));
    }

    public function weeklyCommisionsResume(Request $request)
    {
        if ($request->has('start')) {
            $now = Carbon::now();
            $start = Carbon::parse($request->start);
            $end = Carbon::parse($request->end);
        } else {
            $now = Carbon::now();
            $start = $now->startOfWeek();
            $end = $now->endOfWeek();
        }

        $employees = Employee::with([
            'cashRegisters' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'salesFinished' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'attendances' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'schedules' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'salesFinished.products.product', 'salesFinished.services.service', 'cashRegisters.sales.products.product', 'cashRegisters.sales.services.service'
        ])->barbers()->active()->where('id', '!=', 46);

        if($this->user->access_subsidiary_id != null) {
            $employees = $employees->where('subsidiary_id', $this->user->access_subsidiary_id);
        }

        $employees = $employees->get();
        // dd($employees);
        $copy = $start->copy();
        $dates = [];
        while ($copy->lte($end)) {
            $dates[] = $copy->format('Y-m-d');
            $copy->addDay();
        }
        $start = $start->format('Y-m-d');
        $end = $end->format('Y-m-d');

        return view('admin.reports.weekly-comission', compact('employees', 'start', 'end', 'dates'));
    }

    public function weeklyCommisionsResumePost(Request $request)
    {
        $res = $request->all();
        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne(['Nombre', 'Comision', 'Puntualidad', 'Productos', 'propinas', 'Total']);
        foreach ($res['employee'] as $data) {
            $employee = Employee::find($data['id']);
            $csv->insertOne([
                $employee->name,
                number_format($data['commission'], 2, '.', ''),
                number_format($data['appoint'], 2, '.', ''),
                number_format($data['products'], 2, '.', ''),
                number_format($data['tip'], 2, '.', ''),
                number_format($data['bigtotal'], 2, '.', ''),
            ]);

        }
        $csv->output("Resumen-comisiones-ventas-{$request->start}-{$request->end}.csv");
        die;
    }

    public function diaries(Request $request)
    {
        $now = Carbon::now();
        if ($request->has('start')) {
            $start = $request->start;
            $end = $request->end;
        } else {
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $diaries_query = Diary::dates([$start, $end]);

        if($this->user->access_subsidiary_id != null){
            $diaries_query = $diaries_query->where('subsidiary_id',$this->user->access_subsidiary_id);
        }
        $diaries = $diaries_query->get();
        // dd($diaries_query->bySubsidiary()->isAttend()->toSql());
        // dd($diaries_query->callCenter()->isAttend()->toSql());
        $subsidiary_total_atend = $diaries->where('user_id', null)->where('attend', true)->count();
        $call_center_total_atend = $diaries->where('has_discount', true)->where('attend', true)->count();
        $subsidiary_total_active = $diaries->where('user_id', null)->where('attend', false)->count();
        $call_center_total_active = $diaries->where('has_discount', true)->where('attend', false)->count();
        $subsidiary_total_cancel = $diaries->where('user_id', null)->where('canceled', true)->count();
        $call_center_total_cancel = $diaries->where('has_discount', true)->where('canceled', true)->count();

        $subsidiary_total = $subsidiary_total_atend + $subsidiary_total_active;
        $call_center_total = $call_center_total_atend + $call_center_total_active;

        $data = compact('start', 'end', 'subsidiary_total', 'call_center_total', 'subsidiary_total_cancel', 'call_center_total_cancel', 'subsidiary_total_atend', 'call_center_total_atend', 'subsidiary_total_active', 'call_center_total_active');
        return view('admin.reports.diaries', $data);
    }

    public function promos(Request $request)
    {
        $now = Carbon::now();
        if ($request->has('start')) {
            $start = $request->start;
            $end = $request->end;
        } else {
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');
        }

        $subsidiaries = $this->getSubsidiaries();

        $sales = Sale::dates([$start, $end]);
        // if($this->user->access_subsidiary_id != null){
        //     $sales = $sales->where('subsidiary_id', $this->user->access_subsidiary_id);
        // }

        if ($request->has('subsidiary_id') && $request->subsidiary_id > 0) {
            $sales = $sales->where('subsidiary_id', $request->subsidiary_id);
            $subsidiary_id = $request->subsidiary_id;
        } else {
            $subsidiary_id = null;
        }

        $sales = $sales->withPromotion()->finished()->get();

        $sales_dates_finished = $sales->where('diary_id', '>', '0');

        $dates_total = $sales_dates_finished->count();
        $dates_total_atend = 0;
        foreach ($sales_dates_finished as $sale) {
            $services_max_price = $sale->services->max('price');
            $dates_total_atend += (is_null($services_max_price) ? 0 : $services_max_price) * .10;
        }

        $sales_birthday_finished = $sales->where('has_birthday', 1);
        $birthday_total = $sales_birthday_finished->count();
        $birthday_total_atend = 0;
        foreach ($sales_birthday_finished as $sale) {
            $services_max_price = $sale->services->max('price');
            $birthday_total_atend += is_null($services_max_price) ? 0 : $services_max_price;
        }

        $sales_courtesy_finished = $sales->where('is_courtesy', 1);

        $sales_monedero = $sales->where('payment_gateway', 'Monedero');

        $cortesy_total = $sales_courtesy_finished->count();
        $cortesy_total += $sales_monedero->count();

        $cortesy_total_atend = 0;
        foreach ($sales_courtesy_finished as $sale) {
            $services_max_price = $sale->services->max('price');
            $cortesy_total_atend += is_null($services_max_price) ? 0 : $services_max_price;
        }

        foreach ($sales_monedero as $sale) {
            $services_max_price = $sale->services->max('price');
            $cortesy_total_atend += is_null($services_max_price) ? 0 : $services_max_price;
        }

        $data = compact('start', 'end', 'dates_total', 'dates_total_atend', 'birthday_total', 'birthday_total_atend', 'cortesy_total', 'cortesy_total_atend', 'subsidiaries', 'subsidiary_id');
        return view('admin.reports.promos', $data);
    }

    public function calendar(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        $user = $this->user;
        return view('admin.reports.calendar', compact('subsidiaries','user'));
    }

    public function getCalendar(Request $request)
    {
        if ($request->subsidiary_id == 'all') {
            $sales_object = new Sale;
        }else {
            $sales_object = Subsidiary::find($request->subsidiary_id);
        }
        $sales = [];
        $start = Carbon::parse($request->start);
        $end = Carbon::parse($request->end);
        $copy = $start->copy();
        while ($copy->lte($end)) {
            if ($request->subsidiary_id == 'all') {
                $total = $sales_object->where('date', $copy->format('Y-m-d'))->finished()->notCanceled()->sum('subtotal');
            }else {
                $total = $sales_object->sales()->where('date', $copy->format('Y-m-d'))->finished()->notCanceled()->sum('subtotal');
            }
            $sales[] = [
                'start' => $copy->format('Y-m-d'),
                'title' => number_format($total, 2, '.', ',')
            ];
            $copy->addDay();
        }
        return response()->json($sales);
    }

    public function salesRepairs(Request $request)
    {
        $now = Carbon::now();
        $barbers = $this->getBarbers();
        if ($request->has('start')) {
            $barber = Employee::find($request->employee_id);
            $start = $request->start;
            $end = $request->end;
        }  else {
            $start = $now->format('Y-m-d');
            $end = $now->format('Y-m-d');
        }

        $barbers = Employee::with(['repairedSales' => function ($query) use ($start, $end) {
            $query->whereBetween('date', [$start, $end]);
        },
        'salesRepairedBy' => function ($query) use ($start, $end) {
            $query->whereBetween('date', [$start, $end]);
        }
        ])->barbers()->active();

        if($this->user->access_subsidiary_id != null) {
            $barbers = $barbers->where('subsidiary_id',$this->user->access_subsidiary_id);
        }

        $barbers = $barbers->get();

        if (!$request->has('start')) {
            $barber = $barbers->first();
        }

        return view('admin.reports.repairs', compact('barbers', 'barber', 'start', 'end'));
    }

    public function surveyBarber(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        $questions = Question::satisfactions()->active()->get();
        $question = $questions->first();
        $barbers = $this->getBarbers();
        $barber_id = $request->get('barber_id', 'all');
        if ($barber_id == 'all') {
            $barbersReport = $barbers;
        }else {
            $barbersReport = Employee::whereJob('Barbero')->active()->whereIn('id', [$barber_id])->get();
        }

        if ($request->has('question_id')) {
            $question = Question::find($request->get('question_id'));
        }

        if ($request->has('start')) {
            $start = Carbon::parse($request->start)->startOfDay()->format('Y-m-d  H:i:s');
            $end = Carbon::parse($request->end)->endOfDay()->format('Y-m-d  H:i:s');
        } else {
            $start = Carbon::now()->startOfWeek()->format('Y-m-d  H:i:s');
            $end = Carbon::now()->endOfWeek()->format('Y-m-d  H:i:s');
        }
        return view(
            'admin.reports.surveys.barber',
            compact('subsidiaries', 'questions', 'question', 'barbers', 'start', 'end', 'barber_id', 'barbersReport')
        );
    }

    public function surveySubsidiary(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        $questions = Question::satisfactions()->active()->get();
        $question = $questions->first();
        $subsidiary_id = $request->get('subsidiary_id', 'all');
        if ($subsidiary_id == 'all') {
            $subsidiariesReport = $subsidiaries;
        }else {
            $subsidiariesReport = Subsidiary::where('is_active', true)->whereIn('id', [$subsidiary_id])->get();
        }

        if ($request->has('question_id')) {
            $question = Question::find($request->get('question_id'));
        }

        if ($request->has('start')) {
            $start = Carbon::parse($request->start)->startOfDay()->format('Y-m-d H:i:s');
            $end = Carbon::parse($request->end)->endOfDay()->format('Y-m-d H:i:s');
        } else {
            $start = Carbon::now()->startOfWeek()->format('Y-m-d H:i:s');
            $end = Carbon::now()->endOfWeek()->format('Y-m-d H:i:s');
        }
        return view(
            'admin.reports.surveys.subsidiary',
            compact('subsidiaries', 'questions', 'question', 'start', 'end', 'subsidiary_id', 'subsidiariesReport')
        );
    }

    public function expenditureSubsidiary(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        $accounts = Account::all();
        $subsidiary_id = $request->get('subsidiary_id', 'all');
        if ($subsidiary_id == 'all') {
            $subsidiariesReport = $subsidiaries;
        }else {
            $subsidiariesReport = Subsidiary::whereIn('id', [$subsidiary_id])->get();
        }

        if ($request->has('start')) {
            $start = Carbon::parse($request->start)->startOfDay()->format('Y-m-d');
            $end = Carbon::parse($request->end)->endOfDay()->format('Y-m-d');
        } else {
            $start = Carbon::now()->startOfWeek()->format('Y-m-d');
            $end = Carbon::now()->endOfWeek()->format('Y-m-d');
        }
        return view(
            'admin.reports.expenditures.subsidiary',
            compact('subsidiaries', 'start', 'end', 'subsidiary_id', 'subsidiariesReport', 'accounts')
        );
    }

    public function expenditureMonthly(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();
        $accounts = Account::all();

        if ($request->has('start')) {
            $start = Carbon::parse($request->start)->startOfDay()->format('Y-m-d');
            $end = Carbon::parse($request->end)->endOfDay()->format('Y-m-d');
        } else {
            $start = Carbon::now()->startOfWeek()->format('Y-m-d');
            $end = Carbon::now()->endOfWeek()->format('Y-m-d');
        }
        return view(
            'admin.reports.expenditures.monthly',
            compact('subsidiaries', 'start', 'end', 'accounts')
        );
    }

    public function expenditureAccount(Request $request, $account_id)
    {
        $subsidiaries = $this->getSubsidiaries();
        $account = Account::find($account_id);
        $user = Auth::user();
        $expenditures = $account->expenditures()->dates([$request->start, $request->end]);
        if ($request->has('subsidiary_id')) {
            $expenditures = $expenditures->where('subsidiary_id', $request->subsidiary_id);
        }else {
            $expenditures->whereIn('subsidiary_id', $subsidiaries->pluck('id'));
        }
        $expenditures = $expenditures->orderBy('date', 'desc')->get();

        return view('admin.reports.expenditures.account', compact('expenditures', 'account', 'user')
        );
    }

    public function flow(Request $request)
    {
        $subsidiaries = $this->getSubsidiaries();

        if ($request->has('start')) {
            $start = Carbon::parse($request->start)->startOfDay()->format('Y-m-d');
            $end = Carbon::parse($request->end)->endOfDay()->format('Y-m-d');
        } else {
            $start = Carbon::now()->startOfWeek()->format('Y-m-d');
            $end = Carbon::now()->endOfWeek()->format('Y-m-d');
        }
        $laundry_services = LaundryService::whereBetween('date', [$start, $end])->get();
        return view('admin.reports.flow', compact('subsidiaries', 'laundry_services', 'start', 'end'));
    }

    private function getSubsidiaries() {
        $subsidiaries = Subsidiary::active();
        if($this->user->access_subsidiary_id != null) {
            $subsidiaries = $subsidiaries->where('id',$this->user->access_subsidiary_id);
        }
        return $subsidiaries->get();
    }

    public function getSubsidiary()
    {
        if ($subsidiary_code = $this->hasSubdomain()) {
            return Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        }
        return false;
    }

    private function getBarbers($start = null, $end = null) {

        if($start && $end) {
            $barbers = Employee::with([
                'attendances' => function ($query) use ($start, $end) {
                    $query->whereBetween('date', [$start, $end]);
                },
                'schedules' => function ($query) use ($start, $end) {
                    $query->whereBetween('date', [$start, $end]);
                },
                'salesFinished' => function ($query) use ($start, $end) {
                    $query->whereBetween('date', [$start, $end]);
                },'salesFinished.products', 'salesFinished.services', 'salesFinished.servicesWithoutWaiting'
            ])->active();
        } else {
            $barbers = Employee::active()->barbers();
        }

        if($this->user->access_subsidiary_id != null) {
            $barbers = $barbers->where('subsidiary_id',$this->user->access_subsidiary_id);
        }
        return $barbers->get();
    }

    private function getEmployees() {
        $barbers = Employee::active()->barbersOrCashiers();
        if($this->user->access_subsidiary_id != null) {
            $barbers = $barbers->where('subsidiary_id',$this->user->access_subsidiary_id);
        }
        return $barbers->get();
    }

    private function getEmployeesWithAttendance($start, $end) {

        $employees = Employee::with([
        'attendances' => function ($query) use ($start, $end) {
            $query->whereBetween('date', [$start, $end]);
        },
        'schedules' => function ($query) use ($start, $end) {
            $query->whereBetween('date', [$start, $end])->where('subsidiary_id', '!=', 8);
        }])->active()->get();
        return $employees;
    }

    private function getEmployeesWithSales($start, $end) {
        $employees = Employee::with([
            'cashRegisters' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },
            'salesFinished' => function ($query) use ($start, $end) {
                $query->whereBetween('date', [$start, $end]);
            },'salesFinished.products', 'salesFinished.services'
        ])->active()->barbersOrCashiers();
        if($this->user->access_subsidiary_id != null) {
            $employees = $employees->where('subsidiary_id',$this->user->access_subsidiary_id);
        }
        // dd($employees->get());
        return $employees->get();

    }

}
