<?php

namespace App\Http\Controllers;

use App\Configuration;
use App\Device;
use App\ExchangeRate;
use App\Job;
use App\Subsidiary;
use App\Traits\ImageableTrait;
use App\Turn;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ConfigurationController extends Controller
{
    use ImageableTrait;

    public function index()
    {
        $configurations = Configuration::all();
        return view('admin.configurations.index', compact('configurations'));
    }

    public function create()
    {
        $configuration = new Configuration();
        return view('admin.configurations.create', compact('configuration'));
    }

    public function store(Request $request)
    {
        $data = $request->all();     
        $configuration = Configuration::create($data);
        $this->CreateFile($configuration, $request, "configuration/{$configuration->id}");     
        $request->session()->flash('success', 'Configuración dada de alta con exito.');
        return redirect()->route('admin.configurations.edit', $configuration->id);
    }

    public function edit(Request $request, $id)
    {
        $configuration = Configuration::find($id);
        $jobs = Job::all();
        $turns = Turn::orderBy('identifier', 'asc')->get();
        $rates = ExchangeRate::orderBy('created_at', 'desc')->paginate(5);
        $devices = Device::paginate(5);
        return view('admin.configurations.edit', compact('configuration', 'jobs', 'rates', 'turns', 'devices'));
    }

    public function update(Request $request, $id)
    {
        $configuration = Configuration::find($id);
        $configuration->update($request->all());
        $this->CreateFile($configuration, $request, "configuration/{$configuration->id}");
        $request->session()->flash('success', 'Datos actualizados con éxito');
        return redirect()->route('configurations.index');
    }

    public function updateFiles(Request $request, $id)
    {
        $configuration = Configuration::find($id);
        $this->CreateFiles($configuration, $request, "configuration/{$configuration->id}/documents");
        $request->session()->flash('success', 'Archivo(s) subidos con éxito');
        return redirect()->back();
    }

    public function createRate()
    {
        return view('admin.configurations.new_rate');
    }

    public function storeRate(Request $request)
    {
        $rate = ExchangeRate::create($request->all());

        $request->session()->flash('success', 'Tipo de cambio creado con exito');
        return redirect()->route('configurations.index');
    }

    public function createJob()
    {
        return view('admin.configurations.new_job');
    }

    public function show() {
        return view('admin.configurations.new_job');
    }

    public function storeJob(Request $request)
    {
        $job = Job::create($request->all());

        $request->session()->flash('success', 'Puesto creado con exito');
        return redirect()->route('configurations.index');
    }

    public function createTurn()
    {
        return view('admin.configurations.new_turn');
    }

    public function storeTurn(Request $request)
    {
        $turn = Turn::create($request->all());

        $request->session()->flash('success', 'Turno creado con exito');
        return redirect()->route('configurations.index');
    }

    public function editTurn($id)
    {
        $turn = Turn::find($id);

        return view('admin.configurations.edit_turn', compact('turn'));
    }

    public function updateTurn(Request $request, $id)
    {
        $turn = Turn::find($id);
        $data = $request->all();
        $data['start'] = Carbon::parse($data['start'])->format('H:i:s');
        $data['end'] = Carbon::parse($data['end'])->format('H:i:s');
        $turn->update($data);

        $request->session()->flash('success', 'Turno actualizado con exito');
        return back();
    }

    public function createDevice()
    {
        $subsidiaries = Subsidiary::all();
        return view('admin.configurations.new_device', compact('subsidiaries'));
    }

    public function storeDevice(Request $request)
    {
        $device = Device::create($request->all());

        $request->session()->flash('success', 'Dispositivo "Digital Persona" dado de alta con exito');
        return redirect()->route('configurations.index');
    }

    public function editDevice($id)
    {
        $subsidiaries = Subsidiary::all();
        $device = Device::find($id);

        return view('admin.configurations.edit_device', compact('subsidiaries', 'device'));
    }

    public function updateDevice(Request $request, $id)
    {
        $device = Device::find($id);
        $device->update($request->all());

        $request->session()->flash('success', 'Dispositivo "Digital Persona" actualizado con exito');
        return back();
    }
}
