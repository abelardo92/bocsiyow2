<?php
namespace App\Http\Controllers;

use App\Product;
use App\Promotion;
use App\Sale;
use App\User;
use App\Subsidiary;
use App\Employee;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SalesProductsController extends Controller
{
    public function index($subsidiary_code, $sale_id)
    {
        return response()->json(Product::all());
    }

    public function store(Request $request, $subsidiary_code, $sale_id)
    {
        $data = $request->all();
        $sale = Sale::with(['services'])->find($sale_id);
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        $product = $sale->products()->where('product_id', $data['id'])->first();

        if ($product) {
            $product->increment('qty');
        } else {

            $currentProduct = Product::with(['subsidiaryPrices'])->find($data['id']);
            $productPrice = $currentProduct->currentPriceBySubsidiary($sale->subsidiary_id);

            // $productPrice = $data['sell_price'];
            $activePromotion = $this->activePromotion();

            if($activePromotion && $activePromotion->must_buy_a_service
                && $sale->services()->first()
                && !$activePromotion->promotionProductExceptions()->where('product_id',$data['id'])->get()->first()
                && ($subsidiary->id == $activePromotion->subsidiary_id || $activePromotion->subsidiary_id == null)){
                    //Si existe una promoción, requiere la compra de un servicio, existe un servicio, es una sucursal válida y no está excento de la promoción ...
                    $productPrice = ($productPrice * (100 - $activePromotion->products_discount_percentaje)) / 100;
            }

            $day = Carbon::now()->format('l');
            // if(!$sale->has_discount && ($day == 'Tuesday' || $subsidiary->id == 7) && $currentProduct->id == 22 && $sale->containsHairCut()) {
            //     $productPrice = $currentProduct->sell_price * (0.6); //40% off
            // }

            $product = $sale->products()->create([
                'product_id' => $data['id'],
                'qty' => 1,
                'price' => $productPrice,
                'original_price' => $data['sell_price']
            ]);
        }

        $sale->load(['employee', 'customer', 'services.service.subsidiaryPrices', 'products.product.subsidiaryPrices', 'diary']);
        $sale->updateTotals();
        $message = 'Producto agregado a la nota con exito.';
        $products = $subsidiary->products();
        return response()->json(compact('sale', 'message', 'products'));
    }

    public function destroy2(Request $request, $subsidiary_code, $sale_id, $sale_product_id)
    {
        $user = Auth::user();
        // PASH010597HA7
        if(isset($request->code) && isset($request->password)) {

            if(strpos($request->code, '@')) {
                if ($auth_user = User::where('email', $request->code)->first()) {
                    $user_id = $auth_user->id;
                }
            }
            else if (!$employee = Employee::with('user')->whereCode($request->code)->wherePassword($request->password)->first()) {
                return response()->json([
                    'message' => 'Verifica tus datos de acceso.',
                ], 404);
            } else {
                $user_id = $employee->user ? $employee->user->id : $user->id;
            }
        } else {
            $user_id = $user->id;
        }

        $data = $request->all();
        $canceled_description = $data['canceled_description'];
        $sale = Sale::find($sale_id);
        $product = $sale->products()->find($sale_product_id);

        $product->canceled_by = $user_id;
        $product->canceled_at = Carbon::now();
        $product->canceled_description = $canceled_description;
        $product->save();

        $sale = Sale::with(['employee', 'customer', 'services.service.subsidiaryPrices', 'products.product.subsidiaryPrices', 'packages.package', 'diary', 'payments'])->find($sale_id);
        $sale->updateTotals();
        $message = 'Haz eliminado el producto.';
        return response()->json(compact('sale', 'message'));
    }

    public function activePromotion()
    {
        return Promotion::where('is_active', true)
                ->whereDate('start_date','<=',date('Y-m-d'))
                ->whereDate('end_date','>=',date('Y-m-d'))
                ->where('start_time','<=',date('H:i:s'))
                ->where('end_time','>=',date('H:i:s'))
                ->where(strtolower(date('l')),true)->get()->first();
    }

    public function permanentPromotions()
    {
        return Promotion::where('is_active', true)
                ->where('is_permanent','>=',date('H:i:s'))
                ->where(strtolower(date('l')),true)->get();
    }
}
