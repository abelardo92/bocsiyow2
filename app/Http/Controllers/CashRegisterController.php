<?php
namespace App\Http\Controllers;

use App\CashRegister;
use App\CashRegisterCashierMovement;
use App\Employee;
use App\ExchangeRate;
use App\Subsidiary;
use App\Sale;
use App\SalePayment;
use App\MiniCutPetition;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DateTime;
use App\Traits\SubDomainHelper;

class CashRegisterController extends Controller 
{
    use SubDomainHelper;
    
    public function show($subsidiary_code)
    {
        $subsidiary_code = $this->hasSubdomain();
        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();

        if($subsidiary->id != 7) {
            $cash_register = $subsidiary->cashRegisters()->with(['movements', 'subsidiary'])->notDelayed()->open()->today()->first();           
        } else {
            $cash_register = CashRegister::with(['movements', 'subsidiary'])->open()->delayed()->orderBy('id', 'desc')->first();
        }

        if($cash_register == null){
            return response()->json([
                'message' => 'No hay cajas abiertas, abre tu caja.'
            ], 404);
        }

        return response()->json([
            'message' => 'Caja abierta',
            'cash_register' => $cash_register,
        ]);
    }

    public function store(Request $request, $subsidiary_code)
    {
        $data = $request->all();

        if ($request->total < 1) {
            return response()->json([
                'message' => '¿Seguro que estan correctos los datos?'
            ], 503);
        }

        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();

        if($subsidiary->id == 7 && $data['date'] == '') {
            return response()->json([
                'message' => 'Debe seleccionar una fecha para abrir la caja'
            ], 503);
        }

        if($subsidiary->id == 7 && $data['subsidiary_id'] == '') {
            return response()->json([
                'message' => 'Debe seleccionar una sucursal'
            ], 503);
        }

        $data['exchange_rate_id'] = ExchangeRate::latest()->first()->id;
        if (!$employee = Employee::whereCode($data['code'])->first()) {
            return response()->json([
                'message' => 'Verifica tus datos de acceso.'
            ], 503);
        }

        if (!$employee->hasJob('Cajero') && !$employee->hasJob('Supervisor')) {
            return response()->json([
                'message' => 'No tiene permisos para abrir y/o cerrar caja.'
            ], 503);
        }

        if ($subsidiary->id != 7 && !$employee->attendances()->where('subsidiary_id', $subsidiary->id)->today()->get()->count()) {
            return response()->json([
                'message' => 'Necesitas checar tu entrada primero.'
            ], 503);
        }

        if($subsidiary->id != 7) {
            $data['date'] = Carbon::now()->format('Y-m-d');
            $data['is_delayed'] = false;

            if($cash_register = CashRegister::open()->where('subsidiary_id', $subsidiary->id)->today()->first()) {
                return response()->json([
                    'message' => 'Ya existe una caja abierta en esta sucursal, refresca el navegador para ver los cambios.'
                ], 503);
            } 

        } else {
            $subsidiary = Subsidiary::find($data['subsidiary_id']);
            $data['is_delayed'] = true;
        }

        $data['time'] = Carbon::now()->format('H:i:s');
        $data['imprest'] = $subsidiary->imprest;
        $data['employee_id'] = $employee->id;
        $cash_register = $subsidiary->cashRegisters()->create($data);   

        $data['type'] = 'abrir';
        $data['user_id'] = $request->user()->id;
        $movement = $cash_register->movements()->create($data);

        $sales = $subsidiary->sales()->today()->active()->get();
        foreach ($sales as $sale) {
            $sale->cash_register_id = $cash_register->id;
            $sale->save();
        }

        $cash_register = CashRegister::with(['movements', 'subsidiary'])->find($cash_register->id);

        return response()->json([
            'message' => 'Has abierto la caja con éxito',
            'cash_register' => $cash_register,
            'ticket_url' => route('cash-register-movements.show', [$subsidiary_code, $movement->id])
        ]);
    }

    public function update(Request $request, $subsidiary_code, $id)
    {
        $data = $request->all();
        if ($data['cash_register_movement']['total'] < 1) {
            return response()->json([
                'message' => '¿Seguro que estan correctos los datos?'
            ], 503);
        }
        if (!$employee = Employee::whereCode($data['auth']['code'])->first()) {
            return response()->json([
                'message' => 'Verifica tus datos de acceso.'
            ], 503);
        }

        if (!$employee->hasJob('Cajero') && !$employee->hasJob('Supervisor')) {
            return response()->json([
                'message' => 'No tiene permisos para abrir y/o cerrar caja.'
            ], 503);
        }

        $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();

        $data['cash_register_movement']['employee_id'] = $employee->id;
        $cash_register = CashRegister::find($id);
        $cash_register->total = $cash_register->total - $data['cash_register_movement']['total'];
        $cash_register->save();
        $data['cash_register_movement']['type'] = 'mini-corte';
        
        $minicut = $subsidiary->miniCutPetitions()->latest()->first();
        if($minicut != null){
            $start_date = new DateTime($minicut->datetime);
            $end_date = new DateTime();
            $since_start = $start_date->diff($end_date);
            $minutes = $since_start->days * 24 * 60;
            $minutes += $since_start->h * 60;
            $minutes += $since_start->i;
            $data['cash_register_movement']['minutes_taken'] = $minutes;
        }
        $movement = $cash_register->movements()->create($data['cash_register_movement']);

        //Eliminar registros al hacer minicorte
        // $minicuts = $subsidiary->miniCutPetitions()->where('created_at', '<', Carbon::now()->subDays(30)->toDateTimeString());
        $minicuts = $subsidiary->miniCutPetitions();
        if($minicuts != null){
            $minicuts->delete();
        }

        if($subsidiary->id != 7) {
            $cash_register = $subsidiary->cashRegisters()->with('movements')
            ->notDelayed()->open()->today()->first();           
        } else {
            $cash_register = CashRegister::with(['movements', 'subsidiary'])->open()
            ->delayed()->orderBy('id', 'desc')->first();
        }

        return response()->json([
            'message' => 'Mini corte realizado con exito',
            'cash_register' => $subsidiary->cashRegisters()->with('movements')->open()->today()->first(),
            'ticket_url' => route('cash-register-movements.show', [$subsidiary_code, $movement->id])
        ]);
    }

    public function delete(Request $request, $subsidiary_code, $id)
    {
        $data = $request->all();
        $total = $data['cash_register_movement']['total'];
        $totalCashier = $data['cash_register_cashier_movement']['total'];
        if (($total != "0" && empty($total)) || ($totalCashier != "0" && empty($totalCashier))) {
            return response()->json([
                'message' => '¿Seguro que estan correctos los datos?'
            ], 503);
        }

        if (!$employee = Employee::where('code', $data['auth']['code'])->first()) {
            return response()->json([
                'message' => 'Verifica tus datos de acceso.'
            ], 503);
        }

        if (!$employee->hasJob('Cajero') && !$employee->hasJob('Supervisor')) {
            return response()->json([
                'message' => 'No tiene permisos para abrir y/o cerrar caja.'
            ], 503);
        }

        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            $cash_register_cashier_movement = CashRegisterCashierMovement::create([
                'fiftycents' => intval($data['cash_register_cashier_movement']['fiftycents']),
                'onepeso' => intval($data['cash_register_cashier_movement']['onepeso']),
                'twopesos' => intval($data['cash_register_cashier_movement']['twopesos']),
                'fivepesos' => intval($data['cash_register_cashier_movement']['fivepesos']),
                'tenpesos' => intval($data['cash_register_cashier_movement']['tenpesos']),
                'twentypesos' => intval($data['cash_register_cashier_movement']['twentypesos']),
                'fiftypesos' => intval($data['cash_register_cashier_movement']['fiftypesos']),
                'hundredpesos' => intval($data['cash_register_cashier_movement']['hundredpesos']),
                'twohundredpesos' => intval($data['cash_register_cashier_movement']['twohundredpesos']),
                'fivehundredpesos' => intval($data['cash_register_cashier_movement']['fivehundredpesos']),
                'onethousandpesos' => intval($data['cash_register_cashier_movement']['onethousandpesos']),
                'usd' => intval($data['cash_register_cashier_movement']['usd']),
                'exchange_rate' => $data['exchange_rate']
            ]);
            $data['cash_register_movement']['employee_id'] = $employee->id;
            $data['cash_register_movement']['cash_register_cashier_id'] = $cash_register_cashier_movement->id;
            $cash_register = CashRegister::find($id);
            $cash_register->total = $cash_register->total - $data['cash_register_movement']['total'];
            $cash_register->close = true;
            $cash_register->save();
            $data['cash_register_movement']['type'] = 'corte';
            $movement = $cash_register->movements()->create($data['cash_register_movement']);

            //Eliminar peticiones de mini-corte restantes al hacer corte final
            $minicuts = $subsidiary->miniCutPetitions();
            if($minicuts != null){
                $minicuts->delete();
            }

            return response()->json([
                'message' => 'Corte realizado con exito',
                'cash_register' => $cash_register,
                'ticket_url' => route('cash-register-movements.show', [$subsidiary_code, $movement->id])
            ]);
        } else {
            return response()->json([
                'message' => 'No se encontró una sucursal.'
            ], 503);
        }
    }

    public function currentAmount(Request $request) {
        $cash_register = CashRegister::with(['salesFinished', 'movements'])->find($request->id);

        $sales = Sale::finished()->notCanceled()->where('finished_cash_register_id', $cash_register->id)->get();
        $sale_ids = $sales->pluck('id');

        if($cash_register->subsidiary_id != 11) {
            // $cash_register_sales = $cash_register->salesFinished;
            // $sale_id = $cash_register_sales->pluck('id');
            $cash_total = SalePayment::whereIn('sale_id', $sale_ids)->efectivo()->mxn()->sum('total') - $sales->sum('money_change');
        } else {
            $cash_total = $cash_register->laundryServices()->notCanceled()->get()->sum('amount');
        }
        $minicuts_total = $cash_register->movements->where('type', 'mini-corte')->sum('total');

        // return $cash_register;
        return response()->json([
            'cash_total' => $cash_total,
            'minicuts_total' => $minicuts_total,
        ]);

    }
}