<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Traits\SubDomainHelper;
use App\User;
use App\Subsidiary;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required', 'password' => 'required',
        ]);
    }

    protected function authenticated(Request $request, $user)
    {
        $data = $request->all();
        if($this->hasSubdomain() && $user = User::where('email',$data['email'])->get()->first()){
            if($access_subsidiary = $user->accessSubsidiary()->get()->first()){
                $arr = explode(".", $_SERVER['HTTP_HOST']);
                $subsidiary_code = $arr[0];
                if($access_subsidiary->key != $subsidiary_code){
                    session()->flash('error', 'Solo puedes acceder a la sucursal "'.$access_subsidiary->name.'"');
                    Auth::logout();
                    return redirect()->to('login');
                }
            }
        }
    }

    private function hasSubdomain()
    {
        $bool = false;
        if (isset($_SERVER['HTTP_HOST'])) {
            $arr = explode(".", $_SERVER['HTTP_HOST']);
            $domain = array_shift($arr);
            $bool = $domain;
            if (in_array($domain, ['bocsiyow', 'bocsiyow2','bocsiyowbarbershop', 'bocsiyowbarbers', 'www', 'bocsi'])) {
                $bool = false;
            }
        }
        return $bool;
    }
}
