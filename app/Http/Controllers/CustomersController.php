<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Subsidiary;
use App\Service;
use App\CustomerCourtesyService;
use App\Traits\SubDomainHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Notifications\WelcomeMessage;
use App\Notifications\CourtesyCodeMessage;
use App\Mail\NewClientNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
// use Illuminate\Validation\Validator;
use Validator;

class CustomersController extends Controller
{
    use SubDomainHelper;
    var $user = null;

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $data = $request->all();
        $customers = Customer::active();
        $filter = '';
        if($request->has('filter')) {
            $filter = $data['filter'];
            $customers = $customers->where('name', 'LIKE', '%'.$filter.'%'); 
        }

        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();

            if(!$this->user->isA('super-admin')) {
                $customers = $customers->byBrand($subsidiary->related_brand);
            }

            if($subsidiary->is_laundry){
                $customers = $customers->laundry()->paginate(10);
                return view('admin.customers.index', compact('customers'));
            }
        }
        $customers = $customers->notLaundry()->paginate(10);
        return view('admin.customers.index', compact('customers', 'filter'));
    }

    public function byFilter(Request $request)
    {
        $data = $request->all();
        $customers = Customer::active();
        $filter = '';
        if($request->has('filter') && strlen($request->filter) >= 3) {

            $filter = $data['filter'];

            //$customers = $customers->where('name', 'LIKE', '%'.$filter.'%'); 
            $customers = $customers->where(function($query) use ($filter){
                return $query->where('name', 'LIKE', '%'.$filter.'%')
                ->orWhere('phone', 'LIKE', '%'.$filter.'%')
                ->orWhere('wallet_number', 'LIKE', '%'.$filter.'%');
            });


            if ($subsidiary_code = $this->hasSubdomain()) {
                $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
                if($subsidiary->is_laundry){
                    $customers = $customers->laundry();//->paginate(10);
                    return response()->json($customers->get());
                }
            }

            $customers = $customers->notLaundry();//->paginate(10);
            return response()->json($customers->get());
        }

        return response()->json([]);
    }

    public function excel() {

        Excel::create('customers', function($excel) {
            $excel->sheet('data', function($sheet) {
                
                $sheet->setOrientation('landscape');
                $customers = Customer::select('phone as telefono', 'name as nombre')
                ->whereRaw('phone <> ""')->active()
                //->limit(30)
                ->get();
                $sheet->fromArray($customers);

            });
        })->export('xls');
    }

    public function create(Request $request)
    {
        $redirect = false;
        if ($request->redirect) {
            $redirect = $request->redirect;
        }
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
        } else {
            $subsidiary = null;
        }
        $user = $this->user;
        $services = Service::notLaundry()->get();
        return view('admin.customers.create', compact('redirect','subsidiary','user','services'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $v = Validator::make($data, [
            'phone' => 'required|regex:/[0-9]/',
            'email' => 'email',
        ]);
    
        if ($v->fails())
        {
            foreach($v->errors()->all() as $error) {
                $request->session()->flash('error', $error);
                return redirect()->back();
            }
        }
        // if ($request->email && ) {
        //     $request->session()->flash('error', 'No puedes agendar con 3 horas de anticipación');
        //     return redirect()->back();
        // }


        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            if($subsidiary->is_laundry){
                $data['is_laundry'] = true;
            }
            if ($cash_register = $subsidiary->cashRegisters()->with('movements')->open()->today()->first()) {
                $data['created_by_class'] = 'Employee';
                $data['created_by'] = $cash_register->employee->id;
            } else {
                $data['created_by_class'] = 'User';
                $data['created_by'] = $request->user()->id;
            }

        } else {
            $data['created_by_class'] = 'User';
            $data['created_by'] = $request->user()->id;
        }

        if($data['birthday'] == "" || $data['birthday'] == "0000-00-00") $data['birthday'] = null; 

        $customer = Customer::create($data);
        $customer->findAndUpdateParentReferral();

        if(isset($data['service_id'])) {
            $this->saveCourtesyServices($data['service_id'], $customer->id);
        }

        try {
            if ($customer->validPhone()) {
                $customer->notify(new WelcomeMessage($customer));
            }
            Mail::to($customer->email)->send(new NewClientNotification($customer));
            $request->session()->flash('success', 'Cliente dado de alta con exito.');
        } catch (\Exception $e) {
            $request->session()->flash('success', 'Cliente dado de alta con exito. Mensaje no fue enviado');
        }

        // $request->session()->flash('success', 'Cliente dado de alta con exito.');

        if (isset($data['redirect'])) {
            return redirect()->to("{$data['redirect']}?customer_id={$customer->id}");
        }
        return redirect()->route('customers.edit', $customer->id);
    }

    public function edit($id)
    {
        $customer = Customer::with('courtesyServices')->find($id);
        $user = $this->user;
        $services = Service::notLaundry()->get();
        return view('admin.customers.edit', compact('customer','user','services'));
    }

    public function show($id)
    {
        return Customer::find($id);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $v = Validator::make($data, [
            'phone' => 'required|regex:/[0-9]/',
            'email' => 'email',
        ]);
    
        if ($v->fails())
        {
            foreach($v->errors()->all() as $error) {
                $request->session()->flash('error', $error);
                return redirect()->back();
            }
        }

        if($data['birthday'] == "" || $data['birthday'] == "0000-00-00") $data['birthday'] = null; 
        $customer = Customer::find($id);

        if(isset($data['service_id'])) {
            $this->saveCourtesyServices($data['service_id'], $id);
        }

        $customer->update($data);
        $customer->findAndUpdateParentReferral();

        $request->session()->flash('success', 'Cliente actualizado con exito.');
        return back();
    }

    function saveCourtesyServices($service_ids, $customer_id) {
        // Delete all related services
        CustomerCourtesyService::where('customer_id', $customer_id)->delete();

        // reinsert the new ones
        foreach($service_ids as $service_id_) {
            CustomerCourtesyService::create([
                'customer_id' => $customer_id,
                'service_id' => $service_id_
            ]);
        }
    }

    protected function createReferrals($customer)
    {
        foreach (range(1, 10) as $val) {
            $customer->referrals()->create([
                'wallet_number' => mt_rand(10000000, 99999999),
            ]);
        }
    }

    public function replaceWallet(Request $request, $id)
    {
        $data = $request->all();
        $customer = Customer::find($id);
        $customer->update($data);
        $customer->findAndUpdateParentReferral();

        $request->session()->flash('success', 'Monedero reemplazado con exito.');
        return response()->json([
            'success' => true,
        ]);
    }

    public function verifyBirthday($id)
    {
        $customer = Customer::find($id);
        $birthday = Carbon::parse($customer->birthday)->format('d-m');
        $now = Carbon::now()->format('d-m');
        if ($customer->birthday != null && $customer->birthday != "0000-00-00" && $now == $birthday) {
            $sales = $customer->sales()->today()->notCanceled()->get();

            return response()->json([
                'success' => true,
                'able_for_discount' => $sales->count() > 0 ? false : true,
                'customer' => $customer,
                'message' => $sales->count() > 0 ? "Ya aplicamos tu descuento en {$sales->first()->subsidiary->name}" : '',
            ]);
        }

        return response()->json([
            'success' => false,
        ]);
    }

    public function get($id) {
        $customer = Customer::find($id);
        $success = $customer ? true : false;
        return response()->json([
            'success' => $success,
            'customer' => $customer
        ]);
    }

    public function sendCourtesyCode($id) {
        $customer = Customer::find($id);

        $code = mt_rand(100000, 999999);
        $customer->courtesy_code = $code;
        $customer->courtesy_code_sent_at = Carbon::now()->format('Y-m-d H:i:s');
        $customer->save();

        try {
            if ($customer->validPhone()) {
                $customer->notify(new CourtesyCodeMessage($customer));
                $message = "El código ha sido enviado, escríbelo aquí";
            } else {
                $message = "El código no fue enviado, hubo un problema con el teléfono $customer->phone";
            }
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $message = "No se pudo enviar el código al $customer->phone, Error: {$e->getMessage()}";
        }
        return response()->json([
            'success' => $success,
            'message' => $message
        ]);
    }

    public function deactivate(Request $request, $customer_id)
    {
        $customer = Customer::find($customer_id);
        $customer->is_active = false;
        $customer->save();
        $request->session()->flash('success', 'Cliente eliminado con éxito.');
        return back();
    }
}
