<?php namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests;
use App\Schedule;
use App\Subsidiary;
use App\Diary;
use App\Turn;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmployeesSchedulesController extends Controller
{
    var $user = null;

    public function __construct() {
        $this->middleware(function ($request, $next){
            $this->user = Auth::user();
            return $next($request);
        });
    }

    function getSubsidiaries() {
        if($this->user->isA('subsidiary-admin')) {
            $subsidiaries = Subsidiary::active()->where('id', $this->user->access_subsidiary_id)->get();
        } else {
            $subsidiaries = Subsidiary::active()->get();
        }
        return $subsidiaries;
    }

    public function create($employee_id)
    {
        $subsidiaries = $this->getSubsidiaries();
        $turns = Turn::all();
        $employee = Employee::find($employee_id);
        return view('admin.employees.schedules.create', compact('employee_id', 'subsidiaries', 'turns', 'employee'));
    }

    public function store(Request $request, $employee_id)
    {
        $data = $request->all();
        $employee = Employee::find($employee_id);
        $dates = [];
        $start = new Carbon($data['start']);
        $end = new Carbon($data['end']);
        while ($start->lte($end)) {
            $date = $start->copy();
            if ($date->format('l') == $data['day']) {
                 $dates[] = $date;
            }
            $start->addDay();
        }
        $sched = [];

        if($data['has_rest'] == 0) {
            $data['rest_minutes'] = 0;
        }
        foreach ($dates  as $date) {
            $sched[] = $employee->schedules()->create([
                'day' => $data['day'],
                'turn_id' => $data['turn_id'],
                'subsidiary_id' => $data['subsidiary_id'],
                'start' => $data['start'],
                'end' => $data['end'],
                'extra_day' => $data['extra_day'],
                'has_rest' => $data['has_rest'],
                'rest_minutes' => $data['rest_minutes'],
                'date' => $date->format('Y-m-d')
            ]);
        }

        $request->session()->flash('success', 'Horarios dados de alta con exito.');
        if (count($sched) > 0) {
            return redirect()->route('employees.schedules.edit', [$employee_id, $sched[0]->id]);
        }
        return redirect()->route('employees.edit', [$employee_id]);
    }

    public function edit(Request $request, $employee_id, $id)
    {
        $schedule = Schedule::find($id);
        $subsidiaries = $this->getSubsidiaries();
        $turns = Turn::all();
        $employee = Employee::find($employee_id);
        return view('admin.employees.schedules.edit', compact('employee_id', 'schedule', 'subsidiaries', 'turns', 'employee'));
    }

    public function update(Request $request, $employee_id, $id)
    {
        $data = $request->all();
        
        $previousSchedule = Schedule::with('subsidiary')->where('employee_id', $employee_id)
        ->where('date', $request->date)->get()->first();
        if($previousSchedule != null) {
            $previousSubsidiary = $previousSchedule->subsidiary()->get()->first();
            if($previousSubsidiary->id =! $request->subsidiary_id) {
                $diary = Diary::where('employee_id', $employee_id)->where('date', $request->date)->get();
                if($diary != null) {
                    $request->session()->flash('error', 'No se puede cambiar el horario del empleado porque tiene citas agendadas.');
                    return redirect()->back();
                }
            }
        }
        $schedule = Schedule::find($id);
        $schedule->update($data);
        $request->session()->flash('success', 'Dia actualizado con éxito.');
        return redirect()->route('employees.schedules.edit', [$employee_id, $schedule->id]);
    }

    public function destroy(Request $request, $employee_id, $id)
    {
        $schedule = Schedule::find($id);
        $schedule->delete();
        
        $request->session()->flash('success', 'Dia eliminado con exito.');
        return redirect()->route('employees.edit', $employee_id);
    }

    public function print($id)
    {
        $employee = Employee::find($id);
        $now = Carbon::now();
        $start = $now->startOfWeek()->format('d-m-Y');
        $end = $now->endOfWeek()->format('d-m-Y');
        $schedules = $employee->schedules()->with(['subsidiary', 'turn'])
                    ->thisWeek()->get()
                    ->each(function ($schedule){
                        $schedule->day = trans("dates.{$schedule->day}");
                    });

        return view('admin.employees.schedules.print', compact('employee', 'schedules', 'start', 'end'));
    }

    public function unLock(Request $request, $id)
    {
        $data = $request->all();
        $schedule = Schedule::find($id);
        $schedule->can_check = true;
        $schedule->save();
        
        $request->session()->flash('success', 'Horario desbloqueado, ahora el empleado puede checar.');
        return redirect()->back();
    }
}
