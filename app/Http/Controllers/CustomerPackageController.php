<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Package;

class CustomerPackageController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        if ($subsidiary_code = $this->hasSubdomain()) {
            $subsidiary = Subsidiary::whereKey($subsidiary_code)->firstOrFail();
            if($subsidiary->is_laundry){
                $products = Product::laundry()->paginate(15);
                return view('admin.products.index', compact('products'));
            }
        }
        $products = Product::notLaundry()->paginate(15);
        return view('admin.products.index', compact('products'));
    }

    public function create()
    {
    	$customers = Customer::notLaundry()->active()->get();
    	$packages = Package::laundry()->get();
    	//dd("hola");
        return view('admin.customers.packages.create', compact('packages'));
    }

    public function store(Request $request)
    {
    	$data = $request->all();
    	dd($data);
    }

    public function destroy(Request $request, $customer_package_id)
    {
        $data = $request->all();
        $customer_package = SalePackage::find($customer_package_id);
        
        if($sale_package->products){
            $sale_package->products()->delete();
        }
        if($sale_package->services){
            $sale_package->services()->delete();
        }

        $sale_package->delete();
        $request->session()->flash('success', 'El paquete se ha eliminado exitósamente');
        return redirect()->back();
    }
}
