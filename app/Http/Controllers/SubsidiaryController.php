<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests;
use App\Subsidiary;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SubsidiaryController extends Controller
{
    public function auth(Request $request, $subsidiary_code)
    {
        if ($auth_user = User::where('email', $request->code)->first()) {
            if (Hash::check($request->password, $auth_user->password)) {
                return response()->json([
                    'success' => true,
                ]);
            }
            // $sale->authorized_by = $auth_user->id;
            // $sale->save();
        }        

        if (!$employee = Employee::whereCode($request->code)->first()) {
            return response()->json([
                'message' => 'Verifica tus datos de acceso.'
            ], 404);
        }

        $employeeUser = $employee->user;
        if($employee->user != null && $employeeUser->isA('cashier-admin')) {
            return response()->json([
                'success' => true,
            ]);
        }

        if ($employee->hasJobs(['Supervisor', 'Encargado', 'Cajero'])) {
            session()->put('employee_id', $employee->id);
            return response()->json([
                'success' => true,
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'No tienes permiso para acceder',
        ], 403);
    }

    public function authDelete(Request $request, $subsidiary_code)
    {
        $user = Auth::user();
        if($user->isA('super-admin', 'subsidiary-admin')) {

            if ($auth_user = User::where('email', $request->code)->first()) {
                if (Hash::check($request->password, $auth_user->password)) {
                    return response()->json([
                        'success' => true,
                    ]);
                }
            }

            return response()->json([
                'message' => 'Verifica tus datos de acceso.'
            ], 404);
        }

        if (!$employee = Employee::with('user')->whereCode($request->code)->first()) {
            return response()->json([
                'message' => 'Verifica tus datos de acceso.'
            ], 404);
        }

        $employeeUser = $employee->user;
        if($employee->user != null && $employeeUser->isA('cashier-admin')) {
            return response()->json([
                'success' => true,
            ]);
        }

        if (($request->type == 'product' && $employee->hasJobs(['Supervisor', 'Encargado', 'Cajero'])) || 
            ($request->type == 'service' && $employee->hasJobs(['Supervisor', 'Encargado']))) {
            session()->put('employee_id', $employee->id);
            return response()->json([
                'success' => true,
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'No tienes permiso para acceder',
        ], 403);
    }

    public function authAdmin(Request $request, $subsidiary_code)
    {
        if (!$user = User::where('email', $request->email)->first()) {
            return response()->json([
                'message' => 'Verifica tus datos de acceso.'
            ], 404);
        }

        return response()->json([
            'success' => true,
        ]);
    }
}
