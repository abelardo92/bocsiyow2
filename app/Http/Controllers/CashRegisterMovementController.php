<?php
namespace App\Http\Controllers;

use App\CashRegister;
use App\CashRegisterMovement;
use App\ExchangeRate;
use App\SaleProduct;
use App\Sale;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CashRegisterMovementController extends Controller
{
    public function show($subsidiary_code, $id)
    {
        $user = Auth::user();
        $movement = CashRegisterMovement::with(['cashierMovement','cashRegister.salesFinished.payments','cashRegister.subsidiary'])->find($id);
        $exchange_rate = ExchangeRate::latest()->first();
        $cashRegister = $movement->cashRegister;
        $type = $movement->getType();

        $sales2 = Sale::finished()->notCanceled()->where('finished_cash_register_id', $movement->cashRegister->id)->get();
        $sales = $sales2->groupBy('employee_id', 'employee2_id');
        
        $sale_ids = $sales2->pluck('id');
        $productos = SaleProduct::whereIn('sale_id', $sale_ids->toArray())->get()->groupBy('product_id');
        return view('admin.cash-register-movements.ticket', compact('movement', 'exchange_rate', 'type', 'cashRegister', 'sales', 'sales2', 'productos', 'sale_ids', 'user'));
    }

    function print($id) {
        return $this->show('01', $id);
    }

    public function resume($id)
    {
        $cashRegister = CashRegister::find($id);
        $exchange_rate = $cashRegister->exchangeRate;
        $sales = $cashRegister->sales()->notCanceled()->get()->groupBy('employee_id');

        $productos = SaleProduct::whereIn('sale_id', $cashRegister->sales()->notCanceled()->get()->pluck('id')->toArray())->get()->groupBy('product_id');
        return view('admin.cash-register-movements.resume', compact('exchange_rate', 'cashRegister', 'sales', 'productos'));
    }

    public function find(Request $request)
    {
        $cash_register = CashRegister::with(['movements'])->where('subsidiary_id', $request->subsidiary_id)
            ->where('date', $request->date)
            ->where('turn_id', $request->turn_id)
            ->first();

        if (!$cash_register) {
            return response()->json([
                'message' => 'No encontramos ningun corte con estos datos.',
            ], 404);
        }
        $movement = $cash_register->movements()->where('type', 'corte')->first();

        if(isset($request->type)) {
            if (!$movement && $request->type == "print" ) {
                return response()->json([
                    'message' => 'La caja aun no ha cerrado.',
                ], 404);
            }
            if ($movement && $request->type == "create" ) {
                return response()->json([
                    'message' => 'La caja ya tiene un corte y está cerrada.',
                ], 404);
            }
        }

        if($request->type == "print") {
            return response()->json([
                'movement_id' => $movement->id,
            ]);
        } else {
            return response()->json([
                'cash_register' => $cash_register
            ]);
        }
    }
}
