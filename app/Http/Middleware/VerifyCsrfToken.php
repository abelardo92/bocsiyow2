<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/employees/reg/register',
        '/employees/reg/process',
        '/employees/reg/getac',
        '/employees/reg/validate',
        '/employees/validate/process',
    ];
}
