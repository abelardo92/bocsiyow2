<?php

namespace App\Http\Middleware;

use Closure;
use App\Subsidiary;
use Illuminate\Support\Facades\Auth;
use App\Traits\SubDomainHelper;

class SetTimezone
{
    use SubDomainHelper;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $subsidiary_code = $this->hasSubdomain();
        if($subsidiary = Subsidiary::whereKey($subsidiary_code)->first()) {
            date_default_timezone_set($subsidiary->timezone);
        }
        return $next($request);
    }
}