<?php

namespace App\Http\Requests;

use App\CfdiUse;
use App\Sale;
use App\State;
use App\Subsidiary;
use App\TaxRegime;
use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'address' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'marital_status' => 'required',
            'gender' => 'required',
            'birth_date' => 'required|date',
            'subsidiary_id' => 'required',
            'nationality' => 'required',
            'state' => 'required',
            'city' => 'required',
            'rfc' => 'required|regex:/[a-zA-ZÑ&]{3,4}\d{6}[a-vA-V1-9][a-zA-Z1-9][0-9A]/u',
            // 'curp' => 'required',
            // 'nss' => 'required',
            'health_condition' => 'required',
            // 'suffer_some_illness' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'subsidiary_id' => 'Sucursal',
            'folio' => 'Folio',
            'folio_date' => 'Fecha del ticket',
            'subtotal' => 'Subtotal',
            'payment_type' => 'Forma de pago',
            'postal_code' => 'Codigo postal',
            'road_name' => 'Nombre de la vialidad',
            'external_number' => 'Numero exterior',
            'internal_number' => 'Numero interior',
            'suburb_name' => 'Colonia',
            'locality_name' => 'Localidad',
            'municipality_name' => 'Municipio',
            'state_id' => 'Entidad Federativa',
            'between_street_1' => 'Entre calle',
            'between_street_2' => 'Y calle',
            'rfc' => 'RFC',
            'full_name' => 'Nombre completo',
            'factura_email' => 'Correo electronico',
            'phone' => 'Telefono',
            'cfdi_use_id' => 'Uso del CFDI',
            'tax_regime_id' => 'Regimen fiscal',
            'image' => 'Constancia de situacion fiscal',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $failed = $validator->failed();
        });
    }
}
