<?php

namespace App\Http\Requests;

use App\CfdiUse;
use App\Sale;
use App\State;
use App\Subsidiary;
use App\TaxRegime;
use Illuminate\Foundation\Http\FormRequest;

class SaleFacturaRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'subsidiary_id' => 'required',
            'folio' => 'required|regex:/[0-9]{2}+-+[0-9]{1,8}/u',
            'folio_date' => 'required|date',
            'subtotal' => 'required|numeric',
            'payment_type' => 'required',
            'postal_code' => 'required|alpha_num',
            'road_name' => 'required|regex:/^[a-zA-Z0-9\s]+$/',
            'external_number' => 'required',
            'suburb_name' => 'required',
            'locality_name' => 'required',
            'municipality_name' => 'required',
            'state_id' => 'required',
            'between_street_1' => 'required',
            'between_street_2' => 'required',
            'rfc' => 'required|regex:/[a-zA-ZÑ&]{3,4}\d{6}[a-vA-V1-9][a-zA-Z1-9][0-9A]/u',
            'full_name' => 'required',
            'factura_email' => 'required|email',
            'phone' => 'required|numeric',
            'cfdi_use_id' => 'required',
            'tax_regime_id' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'subsidiary_id' => 'Sucursal',
            'folio' => 'Folio',
            'folio_date' => 'Fecha del ticket',
            'subtotal' => 'Subtotal',
            'payment_type' => 'Forma de pago',
            'postal_code' => 'Codigo postal',
            'road_name' => 'Nombre de la vialidad',
            'external_number' => 'Numero exterior',
            'internal_number' => 'Numero interior',
            'suburb_name' => 'Colonia',
            'locality_name' => 'Localidad',
            'municipality_name' => 'Municipio',
            'state_id' => 'Entidad Federativa',
            'between_street_1' => 'Entre calle',
            'between_street_2' => 'Y calle',
            'rfc' => 'RFC',
            'full_name' => 'Nombre completo',
            'factura_email' => 'Correo electronico',
            'phone' => 'Telefono',
            'cfdi_use_id' => 'Uso del CFDI',
            'tax_regime_id' => 'Regimen fiscal',
            'image' => 'Constancia de situacion fiscal',
        ];
    }

    public function messages()
    {
        return [
            'folio.regex' => 'Formato de folio incorrecto, Ej: 04-1234.',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            $failed = $validator->failed();

            if(!isset($failed['folio'])) {

                list($subsidiary_key, $folio) = explode('-', $this->folio);

                $folio_subsidiary = Subsidiary::where('key',$subsidiary_key)->active()->first();
                if(!$folio_subsidiary) {
                    $validator->errors()->add('folio', 'No encontramos ningun ticket con este folio.');
                    return;
                }

                $sale = Sale::where('folio',$folio)
                ->where('subsidiary_id', $folio_subsidiary->id)
                ->where('date', trim($this->folio_date))
                ->where('subtotal', $this->subtotal)->first();

                if(!$sale) {
                    $validator->errors()->add('folio', 'El folio, sucursal, fecha y subtotal no coinciden con ningun ticket.');
                    return;
                }
            }

            if(!isset($failed['state_id'])) {
                $state = State::find($this->state_id);
                if(!$state) {
                    $validator->errors()->add('state_id', 'El estado no es valido.');
                    return;
                }
            }

            if(!isset($failed['cfdi_use_id'])) {
                $cfdi_use_id = CfdiUse::find($this->cfdi_use_id);
                if(!$cfdi_use_id) {
                    $validator->errors()->add('cfdi_use_id', 'El Uso del CFDI no es válido.');
                    return;
                }
            }

            if(!isset($failed['tax_regime_id'])) {
                $tax_regime_id = TaxRegime::find($this->tax_regime_id);
                if(!$tax_regime_id) {
                    $validator->errors()->add('tax_regime_id', 'El Regimen fiscal no es válido.');
                    return;
                }
            }
        });
    }
}
