<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeRequest extends Model
{
    protected $fillable = [
        'name', 'address', 'email', 'phone', 'marital_status', 'gender',
        'birth_date', 'subsidiary_id', 'nationality', 'state', 'city',
        'rfc', 'curp', 'nss',
        'health_condition', 'suffer_some_illness',
        'father_name', 'father_alive', 'father_address', 'father_occupation',
        'mother_name', 'mother_alive', 'mother_address', 'mother_occupation',
        'spouse_name', 'spouse_alive', 'spouse_address', 'spouse_occupation',
        'status', 'is_active',
    ];

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }
}
