<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashRegisterCashierMovement extends Model
{
    protected $fillable = [
        'fiftycents', 'onepeso' ,'twopesos' ,'fivepesos' ,'tenpesos',
        'twentypesos' ,'fiftypesos' ,'hundredpesos' ,'twohundredpesos',
        'fivehundredpesos' ,'onethousandpesos' , 'usd', 'exchange_rate'
    ];

    public function total() 
    {
    	$total = 0;
        $total += $this->fiftycents * (0.50);
        $total += $this->onepeso * 1;
        $total += $this->twopesos * 2;
        $total += $this->fivepesos * 5;
        $total += $this->tenpesos * 10;
        $total += $this->twentypesos * 20;
        $total += $this->fiftypesos * 50;
        $total += $this->hundredpesos * 100;
        $total += $this->twohundredpesos * 200;
        $total += $this->fivehundredpesos * 500;
        $total += $this->onethousandpesos * 1000;
        $total += $this->usd * $this->exchange_rate;
        return $total;
    }
}
