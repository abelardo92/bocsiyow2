<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubsidiaryServicePrice extends Model
{
    protected $fillable = [
        'subsidiary_id', 'service_id', 'price', 'cost', 'include_in_diaries_discount'
    ];

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function scopeBySubsidiary($query, $subsidiary_id)
    {
        return $query->where('subsidiary_id', $subsidiary_id);
    }

    public function scopeByService($query, $service_id)
    {
        return $query->where('service_id', $service_id);
    }
}
