<?php

namespace App\Mail;

use App\Sale;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FacturaNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $sale_id;
    public $sale;
    public $sale_folio;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sale_id)
    {
        $this->sale_id = $sale_id;
        $this->sale = Sale::with('subsidiary')->find($this->sale_id);
        $this->sale_folio = "{$this->sale->subsidiary->key}-{$this->sale->folio}";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject("Factura para ticket {$this->sale_folio} ha sido solicitado")
            ->view('emails.factura');
    }
}
