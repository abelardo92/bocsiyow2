<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DiaryMail extends Mailable
{
    use Queueable, SerializesModels;

    public $diary;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($diary)
    {
        //
        $this->diary = $diary;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Has agendado una cita en BocsiYow")
                ->view('emails.diary.schedule');
    }
}
