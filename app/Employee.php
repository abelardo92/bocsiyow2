<?php

namespace App;

use App\SaleService;
use App\Surveys\Survey;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Employee extends Model
{
    protected $fillable = [
        'key', 'name', 'short_name', 'entry_date', 'rfc', 'curp', 'nss', 'address', 'marital_status', 'gender', 'salary', 'commission', 'imss_salary',
        'infonavit_number', 'email', 'phone', 'subsidiary_id', 'has_rest', 'rest_minutes', 'job', 'code', 'password', 'active', 'inactived_at',
        'commission_two', 'commission_number', 'account_number', 'monto_infonavit', 'no_banco', 'no_tarjeta', 'has_excellence', 'imss_date', 'bank_name',
        'employee_code', 'user_id', 'birthday', 'job2', 'related_brand', 'employee_request_id',
    ];

    const DOCUMENTS = [
        'id', 'curp', 'rfc', 'imss', 'rd', 'an', 'cap', 'cms',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class);
    }

    public function schedulesByRange($start,$end)
    {
        return $this->hasMany(Schedule::class)->whereBetween('date', [$start, $end]);
    }

    public function attendancesByRange($start,$end)
    {
        return $this->hasMany(Attendance::class)->whereBetween('date', [$start, $end]);
    }

    public function exchanges()
    {
        return $this->hasMany(ScheduleExchange::class);
    }

    public function currentExchanges()
    {
        /*return $this->exchanges()->orWhere('employee2_id',$this->id)->where('date','>=',date('Y-m-d'));*/
        $id = $this->id;
        $date = date('Y-m-d');
        //return $this->exchanges()->with(['exchanges'])
        return ScheduleExchange::with(['exchanges','employee','employee2'])
        ->where(function ($query) use ($id) {
            $query->where('employee_id',$id)
            ->orWhere('employee2_id',$id);
        })
        ->orWhereHas('exchanges', function ($q) use ($id) {
            $q->where('employee_id', $id)->orWhere('employee2_id', $id);
        })
        ->where('date','>=',$date);
    }

    public function disabilities()
    {
        return $this->hasMany(Disability::class);
    }

    public function sales()
    {
        return $this->hasMany(Sale::class);
    }

    public function salesByRange($start, $end)
    {
        return $this->hasMany(Sale::class)->where(function ($query) {
            $query->where('employee_id',$this->id)
            ->orWhere('employee2_id',$this->id);
        })->finished()->notCanceled();
    }

    public function cashRegisters()
    {
        return $this->hasMany(CashRegister::class);
    }

    public function repairedSales()
    {
        return $this->hasMany(Sale::class)->finished()->notCanceled()->repaireds();
    }

    public function sales2()
    {
        return $this->hasMany(Sale::class)->where(function ($query) {
            $query->where('employee_id',$this->id)
            ->orWhere('employee2_id',$this->id);
        })->forCustomers();
    }

    public function getDiariesQty() {
        // if sale has discount and there is the same employee in both diary and sale...
        $total_diaries = 0;
        foreach ($this->salesFinished as $sale) {
            if($sale->has_discount && $sale->employee_id == $sale->diary->employee_id) $total_diaries += 1;
        }
        return $total_diaries;
    }

    public function getEmployeeProductCommission() {
        $products = 0;
        $commission = 0;
        $products_qty = 0;
        $commission_percentage = $this->subsidiary->isMerida() ? 0.20 : 0.10;

        if($this->isBarber()) {
            foreach($this->sales2 as $sale) {
                if($sale->is_for_employee) continue;
                foreach($sale->products as $product) {
                    if($product->product->include_in_reports && $product->product->has_commission) {
                        $products_qty += $product->qty;
                        $products += $product->qty * $product->price;
                        $commission += ($product->qty * $product->price) * $commission_percentage;
                    }
                }
            }
        }

        if($this->isCashier()) {
            foreach($this->cashRegisters as $cashRegister) {
                foreach($cashRegister->sales as $sale) {

                    // * for cashiers, only add commissions if barber is "BOCSI"...
                    if(!$sale->isBocsiBarber() || $sale->is_for_employee) continue;

                    foreach($sale->products as $product) {
                        if($product->product->include_in_reports && $product->product->has_commission) {
                            $products_qty += $product->qty;
                            $products += $product->qty * $product->price;
                            $commission += ($product->qty * $product->price) * $commission_percentage;
                        }
                    }
                }
            }
        }

        return [$products, $commission, $products_qty];

    }

    public function salesFinished()
    {
        return $this->hasMany(Sale::class)->where(function ($query) {
            $query->where('employee_id',$this->id)
            ->orWhere('employee2_id',$this->id);
        })->finished()->notCanceled()->forCustomers();
    }

    public function salesCashier()
    {
        return $this->hasMany(Sale::class)->where(function ($query) {
            $query->where('employee_id',$this->id)
            ->orWhere('employee2_id',$this->id);
        });
    }

    public function lastSale()
    {
        return $this->sales()->where('date', date('Y-m-d'))->finished()->notCanceled()->get()->last();
        /*return $this->sales()->where('date', $date)->where(function ($query) {
        $query->where('status','paid')
        ->orWhere('status','finalizado');
        })->notCanceled();*/
    }

    public function salesByMovement($cash_register_movement)
    {
        return Sale::where('cash_register_id', $cash_register_movement)
        ->where(function ($query) {
        $query->where('employee_id',$this->id)
        ->orWhere('employee2_id',$this->id);
        })->finished()->notCanceled();
    }

    public function weeklySales($date)
    {
        return Sale::where('date', $date)->where(function ($query) {
        $query->where('employee_id',$this->id)
        ->orWhere('employee2_id',$this->id);
        })->finished()->notCanceled()->forCustomers();
    }

    public function weeklySalesBySubsidiary($date, $subsidiary_id)
    {
        return Sale::with('services')->where('date', $date)->where('subsidiary_id', $subsidiary_id)->where(function ($query) {
        $query->where('employee_id',$this->id)
        ->orWhere('employee2_id',$this->id);
        })->finished()->notCanceled();
    }

    public function salesByToday()
    {
        return Sale::with(['services'])->where(function ($query) {
        $query->where('employee_id',$this->id)
        ->orWhere('employee2_id',$this->id);
        })->today()->finished()->notCanceled();
    }

    public function weeklySalesRange($start,$end)
    {
        return Sale::with(['services.sale', 'products'])->whereBetween('date', [$start, $end])->where(function ($query) {
        $query->where('employee_id',$this->id)
        ->orWhere('employee2_id',$this->id);
        })->finished()->notCanceled();
    }

    public function repairs()
    {
        return $this->hasMany(Sale::class, 'repaired_by');
    }

    public function salesRepairedBy()
    {
        return $this->hasMany(Sale::class, 'repaired_by')->finished()->notCanceled()->repaireds();
    }

    public function attendances()
    {
        return $this->hasMany(Attendance::class);
    }

    public function todayAttendances()
    {
        return $this->hasMany(Attendance::class)->today();
    }

    public function issues()
    {
        return $this->hasMany(Issue::class);
    }

    public function manuals()
    {
        return $this->hasMany(Manual::class, 'cashier_id');
    }

    public function diaries()
    {
        return $this->hasMany(Diary::class);
    }

    public function diariesCreated()
    {
        return $this->hasMany(Diary::class, 'cashier_id')->notCancel();
    }

    public function percepciones()
    {
        return $this->hasMany(Percepcione::class);
    }

    public function deducciones()
    {
        return $this->hasMany(Deduccione::class);
    }

    public function surveys()
    {
        return $this->hasMany(Survey::class);
    }

    public function scopeNoInSubsidiary($query)
    {
        return $query->whereNull('subsidiary_id');
    }

    public function scopeActive($query)
    {
        return $query->whereActive(true);
    }

    public function scopeNotActive($query)
    {
        return $query->where('active', false);
    }

    public function scopeBarbers($query)
    {
        return $query->whereJob('Barbero');
    }

    public function scopeBarbersOrCashiers($query)
    {
        return $query->where(function($q) {
            return $q->where('job', 'Barbero')->orWhere('job', 'Cajero');
        });
        // return $query->whereJob('Barbero');
    }

    public function scopeByJob($query, $jobname)
    {
        return $query->where('job', $jobname)->orWhere('job2', $jobname);
    }

    public function scopeBySubsidiary($query, $subsidiary_id)
    {
        return $query->where('subsidiary_id', $subsidiary_id);
    }

    public function scopeByBrand($query, $related_brand) {
        return $query->whereHas('subsidiary', function($q) use ($related_brand) {
            $q->where('related_brand', $related_brand);
        });
    }

    public function scopeWorking($query) {
        return $query->whereDoesntHave('attendances', function($q) {
            $q->where('type','salida');
        });
    }

    public function hasJob($jobname)
    {
        return $this->job == $jobname || $this->job2 == $jobname;
    }

    public function hasJobs($jobs)
    {
        return in_array($this->job, $jobs) || in_array($this->job2, $jobs);
    }

    public function isBarber()
    {
        return $this->job == 'Barbero';
    }

    public function isCashier()
    {
        $this->job == 'Cajero';
    }

    public function isEating() {
        $entradaDescanso = $this->todayAttendances->where('type','entrada descanso')->first();
        $salidaDescanso = $this->todayAttendances->where('type','salida descanso')->first();
        return !$entradaDescanso && $salidaDescanso;
    }

    public function createAttendance($subsidiary_id, $type)
    {
        $now = Carbon::now();
        return $this->attendances()->create([
            'type' => $type,
            'subsidiary_id' => $subsidiary_id,
            'date' => $now->format('Y-m-d'),
            'time' => $now->format('H:i:s'),
        ]);
    }

    public function getFormattedEntryDate()
    {
        $months = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $day = date('d');
        $year = date('Y');
        $month_day = date('m');
        return "$day de {$months[$month_day-1]} de $year";
    }

    public function getSalaryByDateAndSubsidiary($date, $subsidiary_id, $has_punctuality)
    {
        $sales = $this->weeklySalesBySubsidiary($date, $subsidiary_id)->get();
        return $this->getSalary($sales, $has_punctuality);
    }

    public function getSalaryByDate($date, $has_punctuality)
    {
        $sales = $this->weeklySales($date)->get();
        return $this->getSalary($sales, $has_punctuality);
    }

    public function getCommissionByDate($sales) {
        $salary = 0;
        $sale_ids = $sales->pluck('id');
        $services = SaleService::with('sale')->whereIn('sale_id', $sale_ids)->get();
        foreach ($services as $service) {
            $salary += $service->getCommission();
        }
        return $salary;
    }

    public function getSalary($sales, $has_punctuality)
    {
        $sale_ids = $sales->pluck('id');
        $services = SaleService::with('sale')->whereIn('sale_id', $sale_ids)->notCanceled()->notWaiting()->get();

        $salary = 0;
        foreach ($services as $service) {
            $salary += $service->getCommission();
        }

        // if(!$has_punctuality) return $salary;

        // if($salary < $this->salary) return $this->salary;
        return $salary;
    }

    public function getSalaryBySales($sales)
    {
        $salary = 0;
        foreach($sales as $sale) {
            foreach ($sale->servicesWithoutWaiting as $service) {
                $salary += $service->getCommission();
            }
        }
        return $salary;
    }



    public function getServicesNumber($start, $end)
    {
        $sale_ids = $this->sales()->whereBetween('date', [$start, $end])
            ->finished()->notCanceled()->get()->pluck('id');
        return SaleService::whereIn('sale_id', $sale_ids)->notCanceled()->notWaiting()->get()->count();
    }

    public function getAttendedTimeAverage($start, $end)
    {
        $time = [];
        foreach ($this->sales()->whereBetween('date', [$start, $end])
            ->finished()->notCanceled()->get() as $sale) {
            $time[] = $sale->attended_at->diffInMinutes($sale->finished_at);
        }
        if (count($time) == 0) {
            return 0;
        }
        return number_format((array_sum($time) / count($time)), 2, '.', '');
    }

    public function getWaitTimeAverage($start, $end)
    {
        $time = [];
        foreach ($this->sales()->whereBetween('date', [$start, $end])
            ->finished()->notCanceled()->get() as $sale) {
            $time[] = $sale->created_at->diffInMinutes($sale->attended_at);
        }
        if (count($time) == 0) {
            return 0;
        }
        return number_format((array_sum($time) / count($time)), 2, '.', '');
    }

    public function getServicesReport()
    {
        $sales = $this->salesByToday()->get();
        $sale_ids = $sales->pluck('id');
        $kids_promotion_qty = 0;
        foreach ($sales as $sale) {
            $contains_kids = false;
            $contains_haircut = false;
            if($payments = $sale->services->where('service_id', 1)->where('canceled_by', null)->first()) {
                $contains_haircut = true;
            }
            if($payments = $sale->services->where('service_id', 16)->where('canceled_by', null)->first()) {
                $contains_kids = true;
            }
            if($contains_kids && $contains_haircut) {
                $kids_promotion_qty += 1;
            }
        }
        $services = SaleService::select(DB::raw('*, sum(qty) as services_count'))->notCanceled()->whereIn('sale_id', $sale_ids)->groupBy('service_id')->get();
        foreach ($services as &$service) {
            if($service->service_id == 1) {
                $service->services_count -= $kids_promotion_qty;
            }
        }
        return $services;
    }

    public function fingers()
    {
        return $this->hasMany(Finger::class, 'user_id');
    }

    public function deposits()
    {
        return $this->hasMany(Deposit::class);
    }

    public function getName()
    {
        return $this->short_name;
    }

    public function correctDocument($doc)
    {
        foreach ($this->getDoctos() as $document) {
            if ($doc == $document) return true;
        }
        return false;
    }

    public function getDoctos()
    {
        $doctos = [];
        foreach (static::DOCUMENTS as $document) {
            $doctos[] = strtolower(trim($this->short_name)) . "_{$document}";
        }

        return $doctos;
    }

    public function hasDocument($doc)
    {
        foreach ($this->images->sortByDesc('id') as $document) {
            $name_arr = explode('.', $document->name);

            if (strtolower(trim($this->short_name)) . "_{$doc}" == $name_arr[0]) {
                return $document;
            }
        }

        return false;
    }

    public function getProfileimage()
    {
        foreach ($this->images as $document) {
            $name_arr = explode('.', $document->name);
            if (!in_array($name_arr[0], static::DOCUMENTS) || $name_arr[0] == 'foto') {
                return $document;
            }
        }

        return false;
    }

}
