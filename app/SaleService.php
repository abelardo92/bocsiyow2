<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleService extends Model
{
    protected $fillable = [
        'qty', 'service_id', 'sale_id', 'price', 'has_promotion', 'original_price', 'cost', 'canceled_at', 
        'canceled_by', 'canceled_description', 'discount_percentaje', 'include_in_diaries_discount',
        'include_in_birthday_discount', 'has_courtesy'
    ];

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function sale()
    {
        return $this->belongsTo(Sale::class);
    }

    public function canceledBy()
    {
        return $this->belongsTo(User::class, 'canceled_by', 'id');
    }

    public function scopeNotCanceled($query)
    {
        return $query->whereNull('canceled_by');
    }

    public function scopeCanceled($query)
    {
        return $query->whereNotNull('canceled_by');
    }

    public function scopeNotWaiting($query)
    {
        return $query->where('service_id', '!=', 19);
    }

    public function getUpdatedQty() {
        $qty = $this->qty;
        $currentSale = $this->sale;

        if(strpos($this->name,'2x1') || strpos($this->name,'2X1')) {
            if($currentSale->employee_id == $currentSale->employee2_id) {
                $qty++;
            }  
        }

        if($qty > 0 && $currentSale->hasTwoBarbers() && $currentSale->containsHaircutAndKids()) {
            $qty = $qty - 1;
        }
        return $qty;
    }

    private function applyExtraCommission() {
        // * corte o barba
        return in_array($this->service_id, [1,2]);
    } 

    public function getCommission() 
    {
        $qty = $this->getUpdatedQty();
        return $this->cost * $qty;
    }

    public function getAdditionalBarberCommission()
    {
        if(!$this->applyExtraCommission()) return 0;
        $qty = $this->getUpdatedQty();
        return 10 * $qty;
    }
}
