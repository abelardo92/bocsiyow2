<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalePackageService extends Model
{
    protected $fillable = ['sale_package_id', 'service_id', 'quantity', 'price'];

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function salePackage()
    {
        return $this->belongsTo(SalePackage::class);
    }
}
