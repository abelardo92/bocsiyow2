<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CfdiUse extends Model
{
    protected $fillable = [
        'name', 'iso',
    ];
}
