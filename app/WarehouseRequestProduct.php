<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseRequestProduct extends Model
{
    protected $fillable = [
        'warehouse_request_id', 'warehouse_product_id', 'quantity', 'amount', 'existence', 'must_have' 
    ];

    public function movement()
    {
        return $this->belongsTo(WarehouseRequest::class);
    }

    public function product()
    {
        return $this->belongsTo(WarehouseProduct::class, 'warehouse_product_id', 'id');
    }
}
