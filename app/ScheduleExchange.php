<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleExchange extends Model
{
    protected $fillable = [
        'date', 'employee_id', 'employee2_id', 'employee_authorized', 'employee2_authorized', 'admin_authorized', 'opened_by'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id', 'id');
    }

    public function employee2()
    {
        return $this->belongsTo(Employee::class, 'employee2_id', 'id');
    }

    public function schedule1()
    {
        return Schedule::with(['turn', 'subsidiary', 'employee'])->where('date',$this->date)->where('employee_id',$this->employee_id)->get()->first();
    }

    public function schedule2()
    {
        return Schedule::with(['turn', 'subsidiary', 'employee'])->where('date',$this->date)->where('employee_id',$this->employee2_id)->get()->first();
    }

    public function openedBy()
    {
        return $this->belongsTo(User::class, 'opened_by', 'id');
    }

    public function getAllSchedules()
    {
        $schedules = collect([]);
        $schedules->push($this->schedule1());
        $schedules->push($this->schedule2());
        /*foreach ($this->exchanges as $exchange) {
            $exchange['schedule1'] = $exchange->schedule1();
            $exchange['schedule2'] = $exchange->schedule2();
            $schedules->push($exchange['schedule1']);
            $schedules->push($exchange['schedule2']);
        }*/
        return $schedules;
    }

    public function getSchedules1()
    {
        $schedules = collect([]);
        foreach ($this->exchanges as $exchange) {
            $schedules->push($exchange->schedule1());
        }
        return $schedules;
    }

    public function getSchedules2()
    {
        $schedules = collect([]);
        foreach ($this->exchanges as $exchange) {
            $schedules->push($exchange->schedule2());
        }
        return $schedules;
    }

    public function getChangedSchedules1($schedules1, $schedules2)
    {
        foreach ($schedules1 as $index => $schedule) {
            if($index == 0) {
                $schedule1 = $this->schedule1();
                $schedule2 = $this->schedule2();
                if($schedule1 != null && $schedule2 != null) {
                    if( $schedule->employee_id == $schedule1->employee_id) {        
                        $scheduleTemp = $schedule2; 
                        $scheduleTemp->employee_id = $schedule->employee_id;
                        $schedules1[$index] = $scheduleTemp;
                        $schedules1[$index]->load('employee');
                    } else if( $schedule->employee_id == $schedule2->employee_id) {
                        $scheduleTemp = $schedule1;
                        $scheduleTemp->employee_id = $schedule->employee_id;
                        $schedules1[$index] = $scheduleTemp;
                        $schedules1[$index]->load('employee');
                    }
                }
            } 
            else if($schedules1[$index - 1] != null && $schedules2[$index - 1] != null){
                if($schedule->employee_id == $schedules1[$index - 1]->employee_id){
                    $scheduleTemp = $schedules2[$index - 1];
                    $scheduleTemp->employee_id = $schedule->employee_id;
                    $schedules1[$index] = $scheduleTemp;
                    $schedules1[$index]->load('employee');
                } else if($schedule->employee_id == $schedules2[$index - 1]->employee_id){
                    $scheduleTemp = $schedules2[$index - 1];
                    $scheduleTemp->employee_id = $schedule->employee_id;
                    $schedules1[$index] = $scheduleTemp;
                    $schedules1[$index]->load('employee');
                }
            }
        }
        //dd($schedules1[0]->employee->name );
        return $schedules1;
    }

    public function getChangedSchedules2($schedules1, $schedules2)
    {
        /*
        foreach ($schedules2 as $index => $schedule) {
            if($schedule != null){
                if($index == 0 ){
                $schedule1 = $this->schedule1();
                $schedule2 = $this->schedule2();

                if($schedule->employee_id == $schedule1->employee_id){
                    $scheduleTemp = $schedule2;
                    $scheduleTemp->employee_id = $schedule->employee_id;
                    $schedules2[$index] = $scheduleTemp;
                    //dd($schedules2[$index]);
                    $schedules2[$index]->load('employee');
                } else if($schedule->employee_id == $schedule2->employee_id){
                    $scheduleTemp = $schedule1;
                    $scheduleTemp->employee_id = $schedule->employee_id;
                    $schedules2[$index] = $scheduleTemp;
                    $schedules2[$index]->load('employee');
                    //dd($schedules2[$index]);
                }
            } 
            else if($schedules1[$index - 1] != null && $schedules2[$index - 1] != null){
                if($schedule->employee_id == $schedules1[$index - 1]->employee_id){
                    $scheduleTemp = $schedules2[$index - 1]; 
                    $scheduleTemp->employee_id = $schedule->employee_id;
                    $schedules2[$index] = $scheduleTemp;
                    $schedules2[$index]->load('employee');
                } else if($schedule->employee_id == $schedules2[$index - 1]->employee_id){
                    $scheduleTemp = $schedules1[$index - 1];
                    $scheduleTemp->employee_id = $schedule->employee_id;
                    $schedules2[$index] = $scheduleTemp;
                    $schedules2[$index]->load('employee');
                }
            }
            }
        }
        */
        return $schedules2;
    }


    public function exchanges(){
        return $this->hasMany(ScheduleExchangeChild::class);
    }

    public function isAuthorizedBy($employee_id){
        if(!$this->employee2_authorized && $this->employee2_id = $employee_id)
            return false;

        foreach ($this->exchanges as $exchange) {
            if((!$exchange->employee_authorized && $exchange->employee_id = $employee_id) 
                || (!$exchange->employee2_authorized && $exchange->employee2_id = $employee_id))
                return false;
        }
        return true;
    }

    public function isAuthorizedByAll(){
        if(!$this->employee_authorized || !$this->employee2_authorized)
            return false;

        foreach ($this->exchanges as $exchange) {
            if(!$exchange->employee_authorized || !$exchange->employee2_authorized)
                return false;
        }
        return true;
    }
}
