<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $fillable = [
        'device_name', 'sn', 'vc', 'ac', 'vkey', 'subsidiary_id'
    ];

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function scopeSn($query, $sn)
    {
        return $query->where('sn', $sn);
    }

    public function scopeVc($query, $vc)
    {
        return $query->where('vc', $vc);
    }
}
