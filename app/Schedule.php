<?php

namespace App;

use App\Turn;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = [
        'employee_id', 'day', 'turn_id', 'subsidiary_id', 'start', 'end', 'date', 'has_rest', 'rest_minutes', 'extra_day','can_check'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function turn()
    {
        return $this->belongsTo(Turn::class);
    }

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function scopeThisWeek($query)
    {
        $now = Carbon::now();
        $first = $now->startOfWeek()->format('Y-m-d');
        $end = $now->endOfWeek()->format('Y-m-d');

        return $query->dates([$first, $end]);
    }

    public function scopefromThisWeek($query)
    {
        $now = Carbon::now();
        $first = $now->startOfWeek()->format('Y-m-d');

        return $query->where('date', '>=', $first);
    }

    public function scopeNextWeek($query)
    {
        $now = Carbon::now();
        $now = $now->endOfWeek()->addDays(1);
        $first = $now->startOfWeek()->format('Y-m-d');
        $end = $now->endOfWeek()->format('Y-m-d');

        return $query->dates([$first, $end]);
    }

    public function scopeDates($query, array $dates)
    {
        return $query->whereBetween('date', $dates);
    }

    public function scopeDate($query, $date)
    {
        return $query->where('date', $date);
    }

    public function scopeToday($query)
    {
        return $query->where('date', Carbon::now()->format('Y-m-d'));
    }

    public function scopeCurrentTurn($query, $subsidiary_id)
    {
        $now = Carbon::now()->format('H:i:s');
        $turn = false;
        if ($now <= '15:45:00') {
            $turn = Turn::whereIn('identifier', [1, 3])->get()->pluck('id');
        } else {
            $turn = Turn::whereIn('identifier', [2, 3])->get()->pluck('id');
        }
        return $query->whereIn('turn_id', $turn);
    }

    public function scopeExtraDay($query, $bool = true)
    {
        return $query->where('extra_day', $bool);
    }

    public function scopeTurnMixt($query)
    {
        $turn = Turn::where('identifier', 3)->get()->pluck('id');
        return $query->whereIn('turn_id', $turn);
    }

    public function canCheck()
    {
        if($this->employee != null && $this->turn != null) {

            $now = Carbon::now();
            if($this->employee->hasJob('Barbero')) {
                $start_time = $this->turn->barber_start;
            } else {
                $start_time = $this->turn->start;
            }

            if(time() <= strtotime($start_time)) {
                //dd("entra");
                return true;
            }
            //dd($this->turn->barber_start);
        }
        return false;
    }
}
