<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $fillable = [
        'subsidiary_id', 'employee_id', 'type', 'date', 'time'
    ];

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function scopeToday($query)
    {
        return $query->date(
            Carbon::now()->format('Y-m-d')
        );
    }

    public function scopeDate($query, $date)
    {
        return $query->where('date', $date);
    }

    public function getTime()
    {
        $time = explode(':', $this->time);
        $hour = $time[0] < 12 ? $time[0] : $time[0] - 12;
        $minute = $time[1];
        $a = $time[0] < 12 ? 'am' : 'pm';

        return "{$hour}:{$minute} {$a}";
    }
}
