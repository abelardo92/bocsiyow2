<?php

namespace App;

use App\Referral;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class CustomerCourtesyService extends Model
{
    protected $fillable = [
        'customer_id', 'service_id'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }
}