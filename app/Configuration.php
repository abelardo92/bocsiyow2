<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    protected $fillable = [
        'razon_social', 'rfc', 'address', 'contract', 'text_footer',
        'curp', 'created_in', 'regimen', 'registration_number'
    ];

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function subsidiaries()
    {
        return $this->hasMany(Subsidiary::class);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
}
