<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\WarehouseHistory;
use App\WarehouseProduct;
use App\WarehouseHistoryProduct;

class UpdateWarehouseHistory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'warehouse:history';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a copy of current warehouse inventory';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = date('Y-m-d');
        $warehouse_history = WarehouseHistory::create([
            'name' => "Warehouse history for $date",
            'date' => $date
        ]);

        $warehouse_products = WarehouseProduct::active()->orderBy('code')->get();
        $bar = $this->output->createProgressBar(count($warehouse_products));

        $bar->setMessage('Copying warehouse products...');

        foreach($warehouse_products as $warehouse_product) {
            $history_product = WarehouseHistoryProduct::create([
                'warehouse_history_id' => $warehouse_history->id,
                'warehouse_product_id' => $warehouse_product->id,
                'code' => $warehouse_product->code,
                'name' => $warehouse_product->name,
                'use' => $warehouse_product->use,
                'amount' => $warehouse_product->amount,
                'accounting' => $warehouse_product->accounting,
                'in_storage' => $warehouse_product->in_storage,
                'product_id' => $warehouse_product->product_id,
                'is_active' => $warehouse_product->is_active,
                'type' => $warehouse_product->type,
                'warehouse_desired_existence' => $warehouse_product->warehouse_desired_existence
            ]);
            $bar->advance();
        }

        $bar->setMessage('Warehouse history creation finished');
        $bar->finish();
    }
}
