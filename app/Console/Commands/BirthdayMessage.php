<?php

namespace App\Console\Commands;

use App\Customer;
use App\Notifications\DiaryBirtdayMessage;
use App\Mail\BirthdayNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class BirthdayMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'messages:birthday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send sms text to customer birthday';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::now()->format('Y-m-d');

        $customers = Customer::where('birthday', $today)->get();
        //$customers = Customer::where('id', 18162)->get();

        foreach($customers as $customer) {
            if($customer->email) {
                Mail::to($customer->email)->send(new BirthdayNotification($customer, $today));
            }
            Mail::to('abelardo-0@hotmail.com')->send(new BirthdayNotification($customer, $today));
            Mail::to('abelardinii@gmail.com')->send(new BirthdayNotification($customer, $today));
            Mail::to('arguellesyasociadossc@live.com.mx')->send(new BirthdayNotification($customer, $today));
        }

        foreach ($customers as $customer) {
            try {
                $customer->notify(new DiaryBirtdayMessage);
                $this->info("{$customer->name}: {$customer->phone}");
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
        }
    }
}
