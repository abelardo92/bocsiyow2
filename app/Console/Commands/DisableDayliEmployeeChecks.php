<?php

namespace App\Console\Commands;

use App\Subsidiary;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DisableDayliEmployeeChecks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'disable:checks {job=Barbero : El puesto del empleado}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Disable daily schedules for employee how came late to work.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $time = Carbon::now()->format('H:i:s');
        $bar = $this->output->createProgressBar(count($subsidiaries = Subsidiary::all()));
        $bar->setMessage('Task starts');
        // $job = $this->argument('job');
        foreach($subsidiaries as $subsidiary):
            $bar->setMessage('Task in progress...');
            foreach($subsidiary->schedules()->today()->currentTurn($subsidiary->id)->get() as $schedule):
                if($schedule->employee->attendances()->where('subsidiary_id', $subsidiary->id)->today()->get()->count() == 0):
                    $schedule->can_check = false;
                    $schedule->save();
                endif;
            endforeach;
            $bar->advance();
        endforeach;

        $bar->setMessage('Task is finished');
        $bar->finish();
    }
}
