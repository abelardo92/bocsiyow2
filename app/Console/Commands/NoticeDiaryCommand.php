<?php

namespace App\Console\Commands;

use App\Diary;
use App\Customer;
use App\Mail\NoticeDiaryMail;
use App\Notifications\DiaryScheduleRemember;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class NoticeDiaryCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:diaries';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification emails to customer they has schedule date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = Carbon::now()->format('Y-m-d');
        $time = Carbon::now()->addHour()->format('H:i:s');
        $diaries = Diary::where('date', $date)->where('time', $time)->get();
        foreach ($diaries as $diary) {
            Mail::to($diary->customer->email)
                ->send(new NoticeDiaryMail($diary));
            Mail::to('abelardo-0@hotmail.com')
                ->send(new NoticeDiaryMail($diary));

            if ($diary->customer->validPhone()) {
                $diary->customer->notify(new DiaryScheduleRemember($diary));
            }
        }
    }
}
