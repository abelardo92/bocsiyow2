<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class MiniCutPetition extends Model
{
    protected $fillable = [
        'subsidiary_id', 'datetime'
    ];

    public function subsidiary()
    {
        return $this->belongsTo(Subsidiary::class);
    }

    public function scopeToday($query)
    {
        $today = Carbon::now()->format('Y-m-d');
        return $query->whereDate('created_at', Carbon::today());
    }

}
