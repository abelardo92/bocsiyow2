<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});

Route::get('ticket/{sale_id}', [
    'as' => 'sale.print_customer',
    'uses' => 'SalesController@printCustomer',
]);

Route::post('ticket/send',[
    'as' => 'sale.send_ticket',
    'uses' => 'SalesController@sendTicket',
]);

Route::any('employees/reg/process', [
    'as' => 'employees.reg.process',
    'uses' => 'FingersController@registerProcess',
]);

Route::any('employees/reg/getac', [
    'as' => 'employees.reg.getac',
    'uses' => 'FingersController@getac',
]);

Route::any('employees/reg/register', [
    'as' => 'employees.reg.register',
    'uses' => 'FingersController@register',
]);

Route::any('employees/reg/validate', [
    'as' => 'employees.reg.validate',
    'uses' => 'FingersController@validat',
]);

Route::any('employees/validate/process', [
    'as' => 'employees.validate.process',
    'uses' => 'AttendanceController@storeDevice',
]);

Route::get('currentSales', [
        'as' => 'admin.sales.currentSales',
        'uses' => 'SalesController@currentSales',
    ]);
Route::get('complains/{id}/show', [
        'as' => 'complains.show',
        'uses' => 'ComplainsController@show',
    ]);

// Access to super-admin user only
Route::group([
    'middleware' => 'roles:super-admin',
], function () {
    Route::get('citas', [
        'as' => 'diaries.createCustomer',
        'uses' => 'DiaryController@createCustomer',
    ]);
    Route::post('citas', [
        'as' => 'diaries.storeCustomer',
        'uses' => 'DiaryController@storeCustomer',
    ]);
});

Route::get('solicitar-factura/{sale_id?}', [
    'as' => 'facturas.createCustomer',
    'uses' => 'SaleFacturaRequestController@createCustomer',
]);
Route::post('solicitar-factura', [
    'as' => 'facturas.storeCustomer',
    'uses' => 'SaleFacturaRequestController@storeCustomer',
]);

Route::get('solicitar-empleo', [
    'as' => 'employee-requests.createCustomer',
    'uses' => 'EmployeeRequestController@createCustomer',
]);
Route::post('solicitar-empleo', [
    'as' => 'employee-requests.storeCustomer',
    'uses' => 'EmployeeRequestController@storeCustomer',
]);

Route::get('search/timesCustomer', 'DiaryController@searchCustomer');

Route::post('sugerencias', [
    'as' => 'complains.store2',
    'uses' => 'ComplainsController@store2',
]);

Auth::routes();

if (isset($_SERVER['HTTP_HOST'])) {
    $url = $_SERVER['HTTP_HOST'];
    $array = explode(".", $_SERVER['HTTP_HOST']);
    $domain = substr ($url,strpos($url,".")+1);
} else {
    $domain = env('DOMAIN');
}

//$domain = env('DOMAIN');

Route::group([
    'domain' => "{subsidiary_code}." . $domain,
], function () {

    Route::get('/log', [
        'as' => 'home.sucursal.login',
        'uses' => 'HomeController@authSubsidiaryAsSuperUser',
    ]);
});

Route::group([
    'middleware' => ['auth','timezone'],
    'prefix' => 'home',
], function () {
    //$domain = env('DOMAIN');

    if (isset($_SERVER['HTTP_HOST'])) {
        $url = $_SERVER['HTTP_HOST'];
        $array = explode(".", $_SERVER['HTTP_HOST']);
        $domain = substr ($url,strpos($url,".")+1);
    } else {
        $domain = env('DOMAIN');
    }

    // Routes that doesn't need an specific role to have access
    Route::get('/', 'HomeController@index');
    Route::get('/diary/next', 'DiaryController@next');

    // Resources included
    Route::resource('diary', 'DiaryController');
    Route::resource('expenditures', 'ExpendituresController');
    Route::resource('accounts', 'AccountsController');
    Route::resource('exchanges', 'ScheduleExchangesController');
    Route::resource('complains', 'ComplainsController');
    Route::resource('facturas', 'SaleFacturaRequestController');
    Route::resource('employee-requests', 'EmployeeRequestController');

    // Access to super-admin user only
    Route::group([
        'middleware' => 'roles:super-admin',
    ], function () {
        Route::get('/testMessage', 'HomeController@testMessage');
        Route::get('/testNexmoMessage', 'HomeController@testNexmoMessage');
        Route::get('/infoo', 'HomeController@infoo');
    });

    Route::put('/diary/{id}/read', 'DiaryController@markAsRead');

    Route::get('activeSales', [
        'as' => 'admin.sales.activeSales',
        'uses' => 'SalesController@activeSales',
    ]);

    Route::get('/laundry-services/create2', [
        'as' => 'laundry-services.create2',
        'uses' => 'LaundryServicesController@create2',
    ]);

    Route::get('laundry-services/{id}/print', [
            'as' => 'laundry-services.print',
            'uses' => 'LaundryServicesController@print',
        ]);

    Route::get('laundry-services/{id}/cancel', [
            'as' => 'laundry-services.cancel',
            'uses' => 'LaundryServicesController@cancel',
        ]);

    Route::get('laundry-services/{folio}/search/{subsidiary_id}', [
            'as' => 'laundry-services.search',
            'uses' => 'LaundryServicesController@search',
        ]);

    Route::post('/laundry-services/store-previous', [
        'as' => 'laundry-services.store-previous',
        'uses' => 'LaundryServicesController@store2',
    ]);

    Route::get('warehouse/products/adjustMustHaves', [
        'as' => 'warehouse.products.adjust-must-haves',
        'uses' => 'WarehouseProductsController@adjustMustHaves',
    ]);

    Route::post('warehouse/products/storeMustHaves', [
        'as' => 'warehouse.products.storeMustHaves',
        'uses' => 'WarehouseProductsController@storeMustHaves',
    ]);

    Route::get('warehouse/movements', [
        'as' => 'warehouse.movements.index',
        'uses' => 'WarehouseMovementsController@index',
    ]);

    Route::get('warehouse/movements/create-missing-expenditures', [
        'as' => 'warehouse.movements.create-missing-expenditures',
        'uses' => 'WarehouseMovementsController@createMissingExpenditures',
    ]);

    Route::get('warehouse/movements/{id}/create', [
        'as' => 'warehouse.movements.create',
        'uses' => 'WarehouseMovementsController@create',
    ]);

    Route::post('warehouse/movements/store', [
        'as' => 'warehouse.movements.store',
        'uses' => 'WarehouseMovementsController@store',
    ]);

    Route::get('warehouse/movements/createEntry', [
        'as' => 'warehouse.movements.createEntry',
        'uses' => 'WarehouseMovementsController@createEntry',
    ]);

    Route::post('warehouse/movements/storeEntry', [
        'as' => 'warehouse.movements.storeEntry',
        'uses' => 'WarehouseMovementsController@storeEntry',
    ]);

    Route::post('warehouse/movements/storeEntry2', [
        'as' => 'warehouse.movements.storeEntry2',
        'uses' => 'WarehouseMovementsController@storeEntry2',
    ]);

    Route::get('warehouse/movements/{id}/editEntry', [
            'as' => 'warehouse.movements.editEntry',
            'uses' => 'WarehouseMovementsController@editEntry',
        ]);
        Route::post('warehouse/movements/{id}/updateEntry', [
            'as' => 'warehouse.movements.updateEntry',
            'uses' => 'WarehouseMovementsController@updateEntry',
        ]);

    Route::get('warehouse/movements/{id}/edit', [
        'as' => 'warehouse.movements.edit',
        'uses' => 'WarehouseMovementsController@edit',
    ]);

    Route::post('warehouse/movements/{id}/update', [
        'as' => 'warehouse.movements.update',
        'uses' => 'WarehouseMovementsController@update',
    ]);

    Route::get('warehouse/movements/{id}/confirm', [
        'as' => 'warehouse.movements.confirm',
        'uses' => 'WarehouseMovementsController@confirm',
    ]);

    Route::post('warehouse/movements/{id}/updateConfirm', [
        'as' => 'warehouse.movements.updateConfirm',
        'uses' => 'WarehouseMovementsController@updateConfirm',
    ]);

    Route::get('warehouse/movements/{id}/print', [
        'as' => 'warehouse.movements.print',
        'uses' => 'WarehouseMovementsController@print',
    ]);

    Route::get('warehouse/movements/{id}/printUnsupplied', [
        'as' => 'warehouse.movements.print-unsupplied',
        'uses' => 'WarehouseMovementsController@printUnsupplied',
    ]);

    Route::get('warehouse/movements/{id}/printSubsidiary', [
        'as' => 'warehouse.movements.printSubsidiary',
        'uses' => 'WarehouseMovementsController@printSubsidiary',
    ]);

    Route::get('warehouse/movements/{id}/show', [
        'as' => 'warehouse.movements.show',
        'uses' => 'WarehouseMovementsController@show',
    ]);

    Route::delete('delete/movements/{id}', [
        'as' => 'warehouse.movements.destroy',
        'uses' => 'WarehouseMovementsController@destroy',
    ]);

    Route::get('/warehouse/movements/{id}/destroyProduct', [
        'as' => 'warehouse.movements.destroyProduct',
        'uses' => 'WarehouseMovementsController@destroyProduct',
    ]);

    Route::get('warehouse/products', [
        'as' => 'warehouse.products.index',
        'uses' => 'WarehouseProductsController@index',
    ]);

    Route::get('warehouse/products/create', [
        'as' => 'warehouse.products.create',
        'uses' => 'WarehouseProductsController@create',
    ]);

    Route::post('warehouse/products/store', [
        'as' => 'warehouse.products.store',
        'uses' => 'WarehouseProductsController@store',
    ]);

    Route::get('warehouse/products/{id}/edit', [
        'as' => 'warehouse.products.edit',
        'uses' => 'WarehouseProductsController@edit',
    ]);

    Route::post('warehouse/products/{id}/update', [
        'as' => 'warehouse.products.update',
        'uses' => 'WarehouseProductsController@update',
    ]);

    Route::delete('delete/warehouse/products/{id}', [
        'as' => 'warehouse.product.destroy',
        'uses' => 'WarehouseProductsController@destroy',
    ]);

    Route::get('warehouse/requests', [
        'as' => 'warehouse.requests.index',
        'uses' => 'WarehouseRequestsController@index',
    ]);

    Route::get('warehouse/requests/pending', [
        'as' => 'warehouse.requests.pending',
        'uses' => 'WarehouseRequestsController@pending',
    ]);

    Route::get('warehouse/requests/finished', [
        'as' => 'warehouse.requests.finished',
        'uses' => 'WarehouseRequestsController@finished',
    ]);

    Route::get('warehouse/requests/create', [
        'as' => 'warehouse.requests.create',
        'uses' => 'WarehouseRequestsController@create',
    ]);

    Route::post('warehouse/requests/store', [
        'as' => 'warehouse.requests.store',
        'uses' => 'WarehouseRequestsController@store',
    ]);

    Route::post('warehouse/requests/store2', [
        'as' => 'warehouse.requests.store2',
        'uses' => 'WarehouseRequestsController@store2',
    ]);

    Route::get('warehouse/requests/{id}/edit', [
        'as' => 'warehouse.requests.edit',
        'uses' => 'WarehouseRequestsController@edit',
    ]);

    Route::get('warehouse/requests/{id}/show', [
        'as' => 'warehouse.requests.show',
        'uses' => 'WarehouseRequestsController@show',
    ]);

    Route::get('warehouse/requests/{id}/printSubsidiary', [
        'as' => 'warehouse.requests.printSubsidiary',
        'uses' => 'WarehouseRequestsController@printSubsidiary',
    ]);

    Route::post('warehouse/requests/{id}/update', [
        'as' => 'warehouse.requests.update',
        'uses' => 'WarehouseRequestsController@update',
    ]);

    Route::post('warehouse/requests/{id}/update2', [
        'as' => 'warehouse.requests.update2',
        'uses' => 'WarehouseRequestsController@update2',
    ]);

    Route::post('warehouse/requests/{id}/confirm', [
        'as' => 'warehouse.requests.confirm',
        'uses' => 'WarehouseRequestsController@confirm',
    ]);

    Route::delete('delete/requests/{id}', [
        'as' => 'warehouse.requests.destroy',
        'uses' => 'WarehouseRequestsController@destroy',
    ]);

    Route::resource('laundry-services', 'LaundryServicesController');

    Route::get('search/times', 'DiaryController@search');

    Route::get('search/barbers', 'DiaryController@searchForBarber');
    Route::get('/admin/sales', 'HomeController@sales');

    Route::get('/operations-manual', 'HomeController@operationsManual');
    Route::post('/operations-manual', 'HomeController@postOperationsManual');
    Route::delete('/operations-manual', 'HomeController@deleteOperationsManual');
    Route::get('/operations-manual/{id}', 'HomeController@editOperationsManual');
    Route::post('/operations-manual/{id}', 'HomeController@updateOperationsManual');

    Route::get('search/sales/{folio}', [
        'as' => 'search.sales',
        'uses' => 'HomeController@searchForSale',
    ]);

    Route::post('delete/sales/{id}', [
        'as' => 'delete.sales',
        'uses' => 'HomeController@destroySale',
    ]);

    Route::get('links/subsidiary/{id}', [
        'as' => 'links.subsidiary',
        'uses' => 'HomeController@getSubsidiaryUrl',
    ]);

    Route::put('customers/{id}/replace-wallet', 'CustomersController@replaceWallet');
    Route::get('customers/{id}/birthday', 'CustomersController@verifyBirthday');
    Route::get('customers/{id}/sendCourtesyCode', 'CustomersController@sendCourtesyCode');
    Route::get('customers/{id}/get', 'CustomersController@get');

    Route::post('customers/{id}/deactivate', [
        'as' => 'customers.deactivate',
        'uses' => 'CustomersController@deactivate',
    ]);

    Route::get('customers/byFilter', 'CustomersController@byFilter');

    Route::get('customers/excel', [
        'as' => 'customers.excel',
        'uses' => 'CustomersController@excel',
    ]);

    Route::resource('customers', 'CustomersController');

    Route::get('customer/packages/create', [
        'as' => 'customer.packages.create',
        'uses' => 'CustomerPackageController@create',
    ]);

    Route::post('customer/packages/store', [
        'as' => 'customer.packages.store',
        'uses' => 'CustomerPackageController@store',
    ]);

    Route::get('/packages/{id}/deleteProduct', [
        'as' => 'deleteProduct.packages',
        'uses' => 'PackagesController@destroyProduct',
    ]);

    Route::get('/packages/{id}/deleteService', [
        'as' => 'deleteService.packages',
        'uses' => 'PackagesController@destroyService',
    ]);

    Route::resource('packages', 'PackagesController');

    Route::resource('customers.referrals', 'CustomersReferralsController');

    Route::resource('deposits', 'DepositsController');

    Route::put('promotions/{id}/active', [
        'as' => 'promotions.active',
        'uses' => 'PromotionsController@active',
    ]);
    Route::post('promotions/updatePromotionServices', [
        'as' => 'promotions.update_promotion_services',
        'uses' => 'PromotionsController@updatePromotionServices',
    ]);
    Route::post('promotions/updatePromotionProducts', [
        'as' => 'promotions.update_promotion_products',
        'uses' => 'PromotionsController@updatePromotionProducts',
    ]);
    Route::resource('promotions', 'PromotionsController', ['except' => ['show']]);

    Route::get('inventories', [
        'as' => 'inventories.index',
        'uses' => 'InventoriesController@index',
    ]);

    Route::get('inventories/activos-insumos', [
        'as' => 'inventories.activos.insumos',
        'uses' => 'InventoriesActivesController@index',
    ]);

    Route::get('inventories/activos-insumos/create', [
        'as' => 'inventories.activos.insumos.create',
        'uses' => 'InventoriesActivesController@create',
    ]);

    Route::post('inventories/activos-insumos', [
        'as' => 'inventories.activos.insumos.post',
        'uses' => 'InventoriesActivesController@store',
    ]);

    Route::get('inventories/activos-insumos/{id}/edit', [
        'as' => 'inventories.activos.insumos.edit',
        'uses' => 'InventoriesActivesController@edit',
    ]);

    Route::post('inventories/activos-insumos/{id}', [
        'as' => 'inventories.activos.insumos.update',
        'uses' => 'InventoriesActivesController@update',
    ]);

    Route::get('inventories/activos-insumos/{kardex_type}/{folio}/{type}', [
        'as' => 'inventories.insumos.ticket',
        'uses' => 'InventoriesActivesController@show',
    ]);

    Route::resource('articles', 'ArticlesController');

    Route::get('inventories/warehouse/adjustments', [
        'as' => 'home.inventories.warehouse.adjustments.index',
        'uses' => 'InventoriesController@indexWarehouseAdjustments',
    ]);
    Route::get('inventories/warehouse/adjustments/create', [
        'as' => 'home.inventories.warehouse.adjustments.create',
        'uses' => 'InventoriesController@createWarehouseAdjustment',
    ]);
    Route::get('inventories/warehouse/adjustments/{folio}/print', [
        'as' => 'home.inventories.warehouse.adjustments.print',
        'uses' => 'InventoriesController@printWarehouseAdjustment',
    ]);
    Route::post('inventories/warehouse/adjustments/store', [
        'as' => 'home.inventories.warehouse.adjustments.store',
        'uses' => 'InventoriesController@storeWarehouseAdjustment',
    ]);

    Route::get('inventories/{type}/{folio}/{subsidiary_id}', [
        'as' => 'inventories.ticket',
        'uses' => 'InventoriesController@show',
    ]);

    Route::get('api/products/{key}', 'ProductsController@getByKey');
    Route::get('api/warehouse/products/{code}', 'WarehouseProductsController@getByCode');
    Route::get('api/articles/{key}', 'ArticlesController@getByKey');
    Route::resource('products', 'ProductsController');
    Route::resource('services', 'ServicesController');
    Route::post('services/updateSubsidiaryPrices', [
        'as' => 'services.update_subsidiary_prices',
        'uses' => 'ServicesController@updateSubsidiaryPrices',
    ]);
    Route::post('products/updateSubsidiaryPrices', [
        'as' => 'products.update_subsidiary_prices',
        'uses' => 'ProductsController@updateSubsidiaryPrices',
    ]);

    Route::resource('subsidiaries', 'SubsidiariesController');
    Route::resource('subsidiaries.turns', 'SubsidiariesTurnsController');
    Route::post('subsidiaries/{id}/users', [
        'as' => 'subsidiaries.users',
        'uses' => 'SubsidiariesController@asignUsers',
    ]);
    Route::post('subsidiaries/{id}/employees', [
        'as' => 'subsidiaries.employees',
        'uses' => 'SubsidiariesController@asignEmployees',
    ]);

    Route::post('employees/signin', [
        'as' => 'employees.verify.sign',
        'uses' => 'EmployeesController@show',
    ]);
    Route::put('employees/{id}/doctos', [
        'as' => 'employees.upload.documents',
        'uses' => 'EmployeesController@addDocuments',
    ]);
    Route::get('employees/{id}/contrato', [
        'as' => 'employees.contrato',
        'uses' => 'EmployeesController@contrato',
    ]);
    Route::put('employees/{id}/active', [
        'as' => 'employees.active',
        'uses' => 'EmployeesController@active',
    ]);

    Route::put('products/{id}/active', [
        'as' => 'products.active',
        'uses' => 'ProductsController@active',
    ]);

    Route::resource('alerts', 'AlertsController', ['except' => ['show']]);
    Route::get('alerts/today', [
        'as' => 'alerts.todayAlerts',
        'uses' => 'AlertsController@todayAlerts',
    ]);
    Route::put('alerts/{id}/active', [
        'as' => 'alerts.active',
        'uses' => 'AlertsController@active',
    ]);

    Route::resource('employees', 'EmployeesController', ['except' => ['show']]);

    Route::get('employees/{employee_request_id}/from-request', [
        'as' => 'employees.from-request',
        'uses' => 'EmployeesController@createFromRequest',
    ]);

    Route::get('employees/{id}/schedules/print', [
        'as' => 'employees.schedules.print',
        'uses' => 'EmployeesSchedulesController@print',
    ]);

    Route::resource('employees.schedules', 'EmployeesSchedulesController');
    Route::resource('employees.exchanges', 'ScheduleExchangesController');

    Route::post('exchanges/{id}/authorizee', [
        'as' => 'employees.exchanges.authorizee',
        'uses' => 'ScheduleExchangesController@authorizee',
    ]);
    Route::post('exchanges/{id}/unauthorize', [
        'as' => 'employees.exchanges.unauthorize',
        'uses' => 'ScheduleExchangesController@unauthorize',
    ]);
    Route::post('exchanges/{id}/authorizeadmin', [
        'as' => 'employees.exchanges.authorizeadmin',
        'uses' => 'ScheduleExchangesController@authorizeadmin',
    ]);
    Route::post('exchanges/{id}/unauthorizeadmin', [
        'as' => 'employees.exchanges.unauthorizeadmin',
        'uses' => 'ScheduleExchangesController@unauthorizeadmin',
    ]);

    Route::post('exchanges.unauthorize','ScheduleExchangesController@unauthorize');

    Route::post('employees/signin', [
        'as' => 'employees.verify.sign',
        'uses' => 'EmployeesController@show',
    ]);

    Route::resource('employees.disabilities', 'EmployeesDisabilitiesController');
    Route::resource('employees.issues', 'EmployeesIssuesController');

    Route::delete('/images/{id}', [
        'as' => 'destroy.images',
        'uses' => 'ImagesController@destroy',
    ]);

    Route::put('/schedules/{id}/unlock', [
        'as' => 'employees.schedules.unlock',
        'uses' => 'EmployeesSchedulesController@unLock',
    ]);

    Route::resource('users', 'UsersController');
    Route::resource('configurations', 'ConfigurationController');

    Route::get('configuration/{id}/edit', [
        'as' => 'admin.configurations.edit',
        'uses' => 'ConfigurationController@edit',
    ]);

    Route::get('configuration/create', [
        'as' => 'admin.configurations.create',
        'uses' => 'ConfigurationController@Create',
    ]);

    Route::get('paysheets/{key}/archive', 'PaysheetsController@archive');
    Route::get('paysheets/{key}/export/original', 'PaysheetsController@original');
    Route::get('paysheets/{key}/export/nomina', 'PaysheetsController@nomina');
    Route::get('paysheets/{key}/export/banco', 'PaysheetsController@banco');
    Route::put('paysheets/{id}/refresh', [
        'as' => 'paysheets.refresh',
        'uses' => 'PaysheetsController@refresh'
    ]);
    Route::post('paysheets/updateById', 'PaysheetsController@updateById');
    Route::post('paysheets/archiveById', 'PaysheetsController@archiveById');
    Route::resource('paysheets', 'PaysheetsController');

    Route::get('configuration/addRate', [
        'as' => 'configuration.rate.create',
        'uses' => 'ConfigurationController@createRate'
    ]);
    Route::post('configurations/rates', 'ConfigurationController@storeRate');
    Route::get('configurations/createJob', 'ConfigurationController@createJob');
    Route::post('configurations/jobs', 'ConfigurationController@storeJob');
    Route::get('configurations/turns', 'ConfigurationController@createTurn');
    Route::post('configurations/turns', 'ConfigurationController@storeTurn');
    Route::get('configurations/turns/{id}', 'ConfigurationController@editTurn');
    Route::put('configurations/turns/{id}', 'ConfigurationController@updateTurn');

    Route::get('configurations/devices', 'ConfigurationController@createDevice');
    Route::post('configurations/devices', 'ConfigurationController@storeDevice');
    Route::get('configurations/devices/{id}', 'ConfigurationController@editDevice');
    Route::put('configurations/devices/{id}', 'ConfigurationController@updateDevice');
    Route::put('configuration/{id}/edit', [
        'as' => 'configurations.updateFiles',
        'uses' => 'ConfigurationController@updateFiles',
    ]);
    // Reportes
    Route::get('reports/attendances', 'ReportsController@attendances');
    Route::post('reports/attendance_time/{id}', [
        'as' => 'time.edit',
        'uses' => 'ReportsController@attendancesTime',
    ]);
    Route::delete('reports/attendances/{id}', ['as' => 'delete_attendance', 'uses' => 'ReportsController@destroyAttendances']);
    Route::get('reports/issues', 'ReportsController@issues');
    Route::get('reports/schedules', 'ReportsController@schedules');
    Route::get('reports/attendance-card', 'ReportsController@attendanceCard');
    Route::get('reports/cashCutsByDate', 'ReportsController@cashCutsByDate');
    Route::get('reports/cashierMovementsResume', 'ReportsController@cashierMovementsResume');

    Route::get('reports/subsidiary', 'ReportsController@subsidiary');
    Route::get('reports/subsidiaries', 'ReportsController@subsidiaries')->name('report.subsidiaries');
    Route::get('reports/barber', 'ReportsController@barber');
    Route::get('reports/subsidiaries/turns', 'ReportsController@turns');
    Route::get('reports/flow', 'ReportsController@flow');
    Route::get('reports/daily', 'ReportsController@daily');
    Route::get('reports/daily-laundry-services', 'ReportsController@dailyLaundryServices');
    Route::get('reports/accumulated-daily', 'ReportsController@accumulatedDaily');
    Route::get('reports/daily-turns', 'ReportsController@dailyTurns');
    Route::get('reports/daily-cuts', 'ReportsController@dailyCuts');
    Route::get('reports/weekly/barbers', 'ReportsController@weeklyBarber');
    Route::get('reports/weekly/sold-products', 'ReportsController@soldProducts');
    Route::get('reports/sold-products', [
        'as' => 'reports.sales.sold-products-employee',
        'uses' => 'ReportsController@soldProductsByEmployee',
    ]);
    Route::get('reports/weekly/subsidiary-analysis', 'ReportsController@subsidiaryAnalysis');
    Route::get('reports/weekly/services/barbers', 'ReportsController@weeklyServicesBarber');
    Route::get('reports/weekly/barbers/excellence', 'ReportsController@employeesExcellence');
    Route::get('reports/weekly/barbers/without-excellence', 'ReportsController@employeesWithoutExcellence');
    Route::get('reports/weekly/attendances/barbers', 'ReportsController@attendancesControl');

    Route::get('reports/sales/commissions/products', 'ReportsController@productsComisiones');
    Route::get('reports/sales/tips', 'ReportsController@salesTips');

    Route::get('reports/sales/date', 'ReportsController@saleStatus');
    Route::get('reports/sales/commission/status', 'ReportsController@commissionStatus');
    Route::get('reports/sales/promotions', 'ReportsController@promotionsByRange');
    Route::get('reports/sales/notFrequentCustomers', 'ReportsController@notFrequentCustomers');
    Route::get('reports/signatureByEmployee', 'ReportsController@signatureByEmployee');
    Route::get('reports/customers/byBarber', 'ReportsController@customersByBarber');
    Route::get('reports/sales/byCustomer', [
        'as' => 'reports.sales.byCustomer',
        'uses' => 'ReportsController@salesByCustomer',
    ]);
    Route::get('reports/sales/canceled', [
        'as' => 'reports.sales.canceled',
        'uses' => 'ReportsController@salesCanceled',
    ]);

    Route::get('reports/weekly/paysheet/barber', 'ReportsController@weeklyPaysheetBarber');
    Route::get('reports/weekly/paysheet/barber/print/{barber_id}/{start}/{end}', [
        'as' => 'reports.weekly.services.barbers.print',
        'uses' => 'ReportsController@weeklyPaysheetBarberPrint',
    ]);
    Route::get('reports/weekly/paysheet/employee', 'ReportsController@weeklyPaysheetEmployee');
    Route::get('reports/weekly/paysheet/resume', 'ReportsController@weeklyPaysheetResume');
    Route::post('reports/weekly/paysheet/resume', 'ReportsController@weeklyPaysheetResumePost');

    Route::get('reports/weekly/sales/commision/resume', 'ReportsController@weeklyCommisionsResume');
    Route::post('reports/weekly/sales/commision/resume', 'ReportsController@weeklyCommisionsResumePost');

    Route::get('reports/services/subsidiaries', 'ReportsController@servicesSubsidiariesResume');
    Route::get('reports/services-subsidiaries', 'ReportsController@servicesSubsidiaries');
    Route::get('reports/services-employees', 'ReportsController@servicesEmployees');
    Route::get('reports/services-subsidiary', 'ReportsController@servicesSubsidiary');
    Route::get('reports/services-per-time', 'ReportsController@servicesPerTime');
    Route::get('reports/products-subsidiary', 'ReportsController@productsSubsidiary');
    Route::get('reports/products-employees', 'ReportsController@productsEmployees');

    Route::get('reports/inventories/entries', 'ReportsController@entries');
    Route::get('reports/inventories/departures', 'ReportsController@departures');
    Route::get('reports/inventories/adjustments', 'ReportsController@adjustments');
    Route::get('reports/inventories/stocks', 'ReportsController@stocks');
    Route::get('reports/inventories/warehouse/stocks', 'ReportsController@warehouseStocks');
    Route::get('reports/inventories/warehouse/productsToOrder', 'ReportsController@productsToOrder');
    Route::get('reports/inventories/warehouse/movementsByProduct', 'ReportsController@movementsByProduct');
    Route::get('reports/warehouse/requests/unsupplied_orders', 'ReportsController@unsuppliedOrders');

    Route::get('reports/diaries', 'ReportsController@diaries');
    Route::get('reports/promos', 'ReportsController@promos');
    Route::get('reports/calendar', 'ReportsController@calendar');
    Route::get('reports/calendars', 'ReportsController@getCalendar');

    Route::get('reports/sales/repairs', 'ReportsController@salesRepairs');

    // survey reports
    Route::get('reports/survey/subsidiary', 'ReportsController@surveySubsidiary');
    Route::get('reports/survey/barber', 'ReportsController@surveyBarber');

    // expenditure reports
    Route::get('reports/expenditures/subsidiary', 'ReportsController@expenditureSubsidiary');
    Route::get('reports/expenditures/monthly', 'ReportsController@expenditureMonthly')->name('monthly.expenditures');
    Route::get('reports/expenditures/accounts/{account_id}', 'ReportsController@expenditureAccount')->name('expenditures.accounts.report');

    Route::get('subsidiary/{key}/sales/{id}/print', [
        'as' => 'sales.print.admin',
        'uses' => 'SalesController@print',
    ]);

    Route::get('sales/{id}/printAdvance', [
        'as' => 'sales.printAdvance.admin',
        'uses' => 'SalesController@printAdvances',
    ]);

    Route::get('sales/createLaundry', [
        'as' => 'admin.sales.createLaundry',
        'uses' => 'SalesController@createLaundry',
    ]);

    Route::post('sales/storeLaundry', [
        'as' => 'sales.storeLaundry',
        'uses' => 'SalesController@storeLaundry',
    ]);

    Route::post('sales/storePaidAdvances', [
            'as' => 'sales.storePaidAdvances',
            'uses' => 'SalesController@storePaidAdvances',
        ]);

    //Route::get('sales/{id}/editPayments', 'SalesController@editPayments');
    Route::get('sales/{id}/editPayments', [
        'as' => 'admin.sales.editPayments',
        'uses' => 'SalesController@editPayments',
    ]);

    Route::put('sales/{id}/updatePayments', [
        'as' => 'sales.updatePayments',
        'uses' => 'SalesController@updatePayments',
    ]);

    Route::get('sales/{id}/editImage', [
        'as' => 'admin.sales.editImage',
        'uses' => 'SalesController@editImage',
    ]);

    Route::put('sales/{id}/updateImage', [
        'as' => 'sales.updateImage',
        'uses' => 'SalesController@updateImage',
    ]);

    // Catalogos
    Route::get('catalogs/employees', 'CatalogsController@employees');

    Route::post('find/cash-register-movements', [
        'as' => 'cash-register-movements.find.by.subsidiary',
        'uses' => 'CashRegisterMovementController@find',
    ]);

    Route::get('cash-register-movements/print/{id}', [
        'as' => 'cash-register-movements.print',
        'uses' => 'CashRegisterMovementController@print',
    ]);

    Route::get('cash-register-movements/resume/{id}', [
        'as' => 'cash-register-movements.resume',
        'uses' => 'CashRegisterMovementController@resume',
    ]);

    Route::resource('percepciones', 'PercepcioneController');
    Route::resource('deducciones', 'DeduccionesController');

    // Route::get('pendings/create', [
    //     'as' => 'pendings.create',
    //     'uses' => 'PendingsController@create',
    // ]);

    Route::get('pendings', [
        'as' => 'pendings.index',
        'uses' => 'PendingsController@index',
    ]);

    Route::get('pendings/create', [
        'as' => 'pendings.create',
        'uses' => 'PendingsController@create',
    ]);

    Route::post('pendings/store', [
        'as' => 'pendings.store',
        'uses' => 'PendingsController@store',
    ]);

    Route::get('pendings/{id}/edit', [
        'as' => 'pendings.edit',
        'uses' => 'PendingsController@edit',
    ]);

    Route::post('pendings/{id}/update', [
        'as' => 'pendings.update',
        'uses' => 'PendingsController@update',
    ]);

    Route::get('pendings/{id}/show', [
        'as' => 'pendings.show',
        'uses' => 'PendingsController@show',
    ]);

    Route::get('pendings/nonFinished', [
        'as' => 'pendings.nonFinished',
        'uses' => 'PendingsController@nonFinished',
    ]);

    Route::get('pendings/finished', [
        'as' => 'pendings.finished',
        'uses' => 'PendingsController@finished',
    ]);

    Route::get('pendings/admin/create', [
        'as' => 'pendings.admin.create',
        'uses' => 'PendingsController@createAdmin',
    ]);

    Route::get('pendings/admin/{id}/edit', [
        'as' => 'pendings.admin.edit',
        'uses' => 'PendingsController@editAdmin',
    ]);

    Route::get('pendings/admin/nonFinished', [
        'as' => 'pendings.admin.nonFinished',
        'uses' => 'PendingsController@nonFinishedAdmin',
    ]);

    Route::get('pendings/admin/finished', [
        'as' => 'pendings.admin.finished',
        'uses' => 'PendingsController@finishedAdmin',
    ]);

    Route::get('issues/create2', [
        'as' => 'issues.create2',
        'uses' => 'EmployeesIssuesController@create2',
    ]);

    Route::post('issues/store2', [
        'as' => 'issues.store2',
        'uses' => 'EmployeesIssuesController@store2',
    ]);

    Route::get('messages/create', [
        'as' => 'issues.messages.create',
        'uses' => 'EmployeesIssuesController@createMessage',
    ]);

    Route::get('messages/{id}/createAnswer', [
        'as' => 'issues.messages.createAnswer',
        'uses' => 'EmployeesIssuesController@createAnswerMessage',
    ]);

    Route::get('messages/{id}/show', [
        'as' => 'issues.messages.show',
        'uses' => 'EmployeesIssuesController@showMessage',
    ]);

    Route::post('messages/store', [
        'as' => 'issues.messages.store',
        'uses' => 'EmployeesIssuesController@storeMessage',
    ]);

    Route::get('messages/sent', [
        'as' => 'issues.messages.sent',
        'uses' => 'EmployeesIssuesController@sentMessages',
    ]);

    Route::get('messages/received', [
        'as' => 'issues.messages.received',
        'uses' => 'EmployeesIssuesController@receivedMessages',
    ]);

    Route::get('messages/employees/create', [
        'as' => 'issues.messages.employees.create',
        'uses' => 'EmployeesIssuesController@createEmployeeMessage',
    ]);

    Route::get('messages/employees/{id}/createAnswer', [
        'as' => 'issues.messages.employees.createAnswer',
        'uses' => 'EmployeesIssuesController@createAnswerEmployeeMessage',
    ]);

    Route::get('messages/employees/{id}/show', [
        'as' => 'issues.messages.employees.show',
        'uses' => 'EmployeesIssuesController@showEmployeeMessage',
    ]);

    Route::get('messages/employees/sent', [
        'as' => 'issues.messages.employees.sent',
        'uses' => 'EmployeesIssuesController@sentEmployeeMessages',
    ]);

    Route::get('messages/employees/received', [
        'as' => 'issues.messages.employees.received',
        'uses' => 'EmployeesIssuesController@receivedEmployeeMessages',
    ]);

    Route::get('sales/employees/create', [
        'as' => 'sales.employees.create',
        'uses' => 'SalesEmployeeController@create',
    ]);

    Route::get('sales/employees/list', [
        'as' => 'sales.employees',
        'uses' => 'SalesEmployeeController@index',
    ]);


    Route::get('sales/employees/updatePayments', [
        'as' => 'sales.employees',
        'uses' => 'SalesEmployeeController@index',
    ]);

    Route::post('sales/employees/store', [
        'as' => 'sales.employees.store',
        'uses' => 'SalesEmployeeController@store',
    ]);

    Route::get('sales/{id}/employees/editPayments', [
        'as' => 'sales.employees.editPayments',
        'uses' => 'SalesEmployeeController@editPayments',
    ]);

    Route::put('sales/{id}/employees/updatePayments', [
        'as' => 'sales.employees.updatePayments',
        'uses' => 'SalesEmployeeController@updatePayments',
    ]);

    Route::delete('delete/sales/employees/{id}', [
        'as' => 'sales.employees.destroy',
        'uses' => 'SalesEmployeeController@destroy',
    ]);

    Route::group([
        'domain' => "{subsidiary_code}." . $domain,
    ], function () {
        Route::resource('eats', 'EatsController');

        Route::get('/', [
            'as' => 'home.sucursal',
            'uses' => function ($subsidiary_code) {
                return redirect('/');
            },
        ]);

        Route::post('subsidiary/auth', [
            'as' => 'subsidiary.auth',
            'uses' => 'SubsidiaryController@auth',
        ]);

        Route::post('subsidiary/authDelete', [
            'as' => 'subsidiary.authDelete',
            'uses' => 'SubsidiaryController@authDelete',
        ]);

        Route::post('subsidiary/auth/admin', [
            'as' => 'subsidiary.auth.admin',
            'uses' => 'SubsidiaryController@authAdmin',
        ]);

        Route::post('attendance', [
            'as' => 'attendance.store',
            'uses' => 'AttendanceController@store',
        ]);

        Route::get('attendance/get-url/{code}', [
            'as' => 'attendance.get.url',
            'uses' => 'AttendanceController@getUrl',
        ]);

        Route::get('inventories/entries', [
            'as' => 'home.inventories.entries.create',
            'uses' => 'InventoriesController@createEntry',
        ]);
        Route::post('inventories/entries', [
            'as' => 'home.inventories.entries.store',
            'uses' => 'InventoriesController@storeEntry',
        ]);

        Route::get('inventories/departures', [
            'as' => 'home.inventories.departures.create',
            'uses' => 'InventoriesController@createDeparture',
        ]);
        Route::post('inventories/departures', [
            'as' => 'home.inventories.departures.store',
            'uses' => 'InventoriesController@storeDeparture',
        ]);

        Route::get('inventories/adjustments', [
            'as' => 'home.inventories.adjustments.create',
            'uses' => 'InventoriesController@createAdjustment',
        ]);
        Route::post('inventories/adjustments', [
            'as' => 'home.inventories.adjustments.store',
            'uses' => 'InventoriesController@storeAdjustment',
        ]);

        Route::get('inventories/stocks', [
            'as' => 'home.inventories.stocks.print',
            'uses' => 'InventoriesController@printExistence',
        ]);

        Route::get('cash-register', [
            'as' => 'cash-register.show',
            'uses' => 'CashRegisterController@show',
        ]);

        Route::post('cash-register', [
            'as' => 'cash-register.store',
            'uses' => 'CashRegisterController@store',
        ]);

        Route::post('cash-register/currentAmount', [
            'as' => 'cash-register.current_amount',
            'uses' => 'CashRegisterController@currentAmount',
        ]);

        Route::put('cash-register/{id}', [
            'as' => 'cash-register.update',
            'uses' => 'CashRegisterController@update',
        ]);

        Route::put('cash-register-delete/{id}', [
            'as' => 'cash-register.delete',
            'uses' => 'CashRegisterController@delete',
        ]);

        Route::get('cash-register-movements/{id}', [
            'as' => 'cash-register-movements.show',
            'uses' => 'CashRegisterMovementController@show',
        ]);

        Route::resource('sales', 'SalesController');
        // Route::resource('sales.employees', 'SalesEmployeeController');

        Route::get('sales/{id}/print', [
            'as' => 'sales.print',
            'uses' => 'SalesController@print',
        ]);

        Route::post('sales/addSaleContents', [
            'as' => 'sales.addSaleContents',
            'uses' => 'SalesController@addSaleContents',
        ]);

        Route::get('sales/{id}/barber', [
            'as' => 'sales.barber',
            'uses' => 'SalesController@barber',
        ]);
        Route::get('sales/{folio}/search', [
            'as' => 'sales.search',
            'uses' => 'SalesController@search',
        ]);

        Route::put('sales/{folio}/barber', [
            'as' => 'sales.barber',
            'uses' => 'SalesController@updateBarber',
        ]);

        Route::post('sales/{id}/survey', [
            'as' => 'sales.show.survey.form',
            'uses' => 'SalesController@showSurveyForm',
        ]);
        Route::post('products/{id}/search', [
            'as' => 'products.search',
            'uses' => 'ProductsController@search',
        ]);

        Route::post('sales/{id}/services/{service_id}/destroy', [
            'as' => 'sales.services.destroy2',
            'uses' => 'SalesServicesController@destroy2',
        ]);

        Route::post('sales/{id}/products/{product_id}/destroy', [
            'as' => 'sales.products.destroy2',
            'uses' => 'SalesProductsController@destroy2',
        ]);

        Route::resource('sales.services', 'SalesServicesController');
        Route::resource('sales.products', 'SalesProductsController');
        Route::resource('sales.packages', 'SalePackagesController');

        Route::get('attendances/{id}/print', [
            'as' => 'attendances.print',
            'uses' => 'AttendanceController@print',
        ]);

        Route::get('attendances-employee/{id}', [
            'as' => 'attendances.employee-number',
            'uses' => 'AttendanceController@getLink',
        ]);

        Route::get('issues-employee/{id}', [
            'as' => 'issues.employee-number',
            'uses' => 'EmployeesIssuesController@index',
        ]);
    });
});
