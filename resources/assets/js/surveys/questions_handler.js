window.QuestionsHandler = {
    init: function () {
        this.initEvents();
    },
    initEvents: function () {
        this.addInputTypeSelectEvent();
        this.addOptionButtonEvent();
        this.removeOptionButtonEvent();
    },
    addInputTypeSelectEvent: function () {
        $('.js-input-type-select').on('change', function (e) {
            var select = $(e.target);
            switch (select.val()) {
              case "checkbox":
              case "radio":
                $('.js-options').show();
                break;
              default:
                $('.js-options').hide();
            }
        }.bind(this));
    },
    addOptionButtonEvent: function () {
        $('.js-add-option').on('click', function (e) {
            $('.js-options-container').append(this.getHtmlOptionMarkup());
            this.removeOptionButtonEvent();
        }.bind(this));
    },
    getHtmlOptionMarkup: function () {
        var uuid = this.generateOptionId();
        return `
            <div class="form-group" id="option-${uuid}">
                <label for="${uuid}" class="col-md-4 text-right">
                    <button type="button" class="btn btn-link btn-xs js-remove-option" data-id="option-${uuid}">x</button>
                    Opción
                </label>
                <div class="col-md-8">
                    <input type="text" class="form-control" name="options[]" id="${uuid}" required>
                </div>
            </div>
        `;
    },
    generateOptionId: function () {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c=='x' ? r : (r&0x3|0x8)).toString(16);
        });
        return uuid;
    },
    removeOptionButtonEvent: function () {
        $('.js-remove-option').off().on('click', function (e) {
            var id = $(e.target).data('id');
            $(`#${id}`).remove();
        }.bind(this));
    },
};