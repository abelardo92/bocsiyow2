<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;">
 <head> 
  <meta charset="UTF-8"> 
  <meta content="width=device-width, initial-scale=1" name="viewport"> 
  <meta name="x-apple-disable-message-reformatting"> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
  <meta content="telephone=no" name="format-detection"> 
  <title>New Customer</title> 
  <!--[if (mso 16)]>
    <style type="text/css">
    a {text-decoration: none;}
    </style>
    <![endif]--> 
  <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> 
  <!--[if !mso]><!-- --> 
  <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <!--<![endif]--> 
  <style type="text/css">
/* @media only screen and (max-width:600px) {p, ul li, ol li, a { font-size:16px!important; line-height:150%!important } h1 { font-size:30px!important; text-align:center; line-height:120%!important } h2 { font-size:26px!important; text-align:center; line-height:120%!important } h3 { font-size:20px!important; text-align:center; line-height:120%!important } h1 a { font-size:30px!important } h2 a { font-size:26px!important } h3 a { font-size:20px!important } .es-menu td a { font-size:16px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:16px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class="gmail-fix"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:block!important } a.es-button { font-size:20px!important; display:block!important; border-width:10px 0px 10px 0px!important } .es-btn-fw { border-width:10px 0px!important; text-align:center!important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } .es-desk-menu-hidden { display:table-cell!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } }
#outlook a {
    padding:0;
}
.ExternalClass {
    width:100%;
}
.ExternalClass,
.ExternalClass p,
.ExternalClass span,
.ExternalClass font,
.ExternalClass td,
.ExternalClass div {
    line-height:100%;
}
.es-button {
    mso-style-priority:100!important;
    text-decoration:none!important;
}
a[x-apple-data-detectors] {
    color:inherit!important;
    text-decoration:none!important;
    font-size:inherit!important;
    font-family:inherit!important;
    font-weight:inherit!important;
    line-height:inherit!important;
}
.es-desk-hidden {
    display:none;
    float:left;
    overflow:hidden;
    width:0;
    max-height:0;
    line-height:0;
    mso-hide:all;
} */

img {
    display:block;
    border:0;
    outline:none;
    text-decoration:none;
    -ms-interpolation-mode:bicubic;
    text-align: center;
}

.center {
    display: block;
    margin-left: auto;
    margin-right: auto;
    text-align: center;
}

tr {
    
}

.subsidiary-container {
    display: flex;
    height: 40px;
    background-color: black;
    color: white;
}

.subsidiary-container td {
    width: 20%;
    text-align:center;
    margin-top:auto;
    margin-bottom:auto;
}

.social-media-container {
    display: flex;
    height: 80px;
    color: white;
    background-color: rgb(199, 214, 110);
}

.social-media-container td {
    width: 33%;
    text-align:center;
    margin-top:auto;
    margin-bottom:auto;
    color: darkblue;
}

.circle-icon {
    display: inline;
    border-radius: 100px;
    padding: 0.6em;
    background-color: darkblue;
    color: white;
}

.circle-icon i {
    width: 20px;
    height: 20px;
}

</style> 
 </head> 
 <body style="width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;"> 
    <table style="width: 100%; max-width: 700px;">
        <tr>
            <td style="width: 100%;">
                <img class="adapt-img center" src="{{ asset('images/logo_black.png') }}" alt="Gift " title="Gift " width="300">
            </td>
        </tr>
        <tr>
            <td style="width: 100%;">
                <h1 class="center">Bienvenido {{$customer->name}}!</h1>
                <hr>
            </td>
        </tr>
        <tr style="display:block;">
            <td style="width: 50%;">
                <h2 class="center"><strong>Registro exitoso!<strong><br><br>
                Ya puedes acceder a los beneficios de ser cliente frecuente</h2>
            </td>
            <td style="width: 50%;">
                <img class="adapt-img center" src="{{ asset('images/haircut.png') }}" alt="Gift " title="Gift " width="300">
            </td>
        </tr>
        <tr class="subsidiary-container">
            <td>Av. Tulum</td>
            <td>Av. Nichupte</td>
            <td>Santa Fe</td>
            <td>Av. Huayacan</td>
            <td>Andalucía</td>
        </tr>
        <tr class="social-media-container">
            <td>
                <a href="https://www.instagram.com/bocsiyow/" class="circle-icon"><i class="fa fa-instagram"></i></a>
                <a href="https://www.facebook.com/Bocsiyow.barberShop.cancun" class="circle-icon"><i class="fa fa-facebook"></i></a>
                <a href="#" class="circle-icon"><i class="fa fa-youtube-play"></i></a>
                @Bocsiyow
            </td>
            <td><a href="#" class="circle-icon"><i class="fa fa-phone"></i></a> 998-286-0491</td>
            <td><a href="#" class="circle-icon"><i class="fa fa-whatsapp"></i></a> 998-152-3571</td>
        </tr>
    </table>
 </body>
</html>