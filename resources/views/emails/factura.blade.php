<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Email</title>
</head>
<body>
    <p>La factura por el ticket {{ $sale_folio }} ha sido solicitado</p>
    <p>Para ver la información de la factura entre al siguiente enlace:</p>
    <p><a href="{{route('facturas.index')}}"> Click aquí para ver información </a></p>
</body>
</html>
