<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Email</title>
</head>
<body>
    <p>Estos son los clientes que cumplen años el {{$date}}</p>
    <table width="100%">
        <tr>
            <th>Nombre</th>
            <th>Telefono</th>
            <th>Correo</th>
        </tr>
        @foreach ($customers as $customer)
            <tr>
                <td>{{$customer->name}}</td>
                <td>{{$customer->phone}}</td>
                <td>{{$customer->email}}</td>
            </tr>
        @endforeach
    </table>
</body>
</html>