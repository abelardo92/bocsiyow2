@extends('layouts.survey')

@section('content')
    <form method="post" action="{{route('surveys.update', [$subsidiary->key, $survey->id])}}" class="js-form-respond" novalidate>
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        @foreach($questions as $index => $question)
            @include("surveys.fields.{$question->input_type}", [
                'question' => $question, 
                'focus' => $loop->first,
                'first' => $loop->first,
                'index' => ($index + 1),
            ])
        @endforeach

        <div class="panel panel-default js-buttons-content">
            <div class="panel-body text-center">
                <input type="button" class="js-next-button btn btn-primary btn-lg btn-block" value="Siguiente">
                <input type="submit" class="js-submit-button btn btn-primary btn-lg btn-block display-none" value="Enviar">
            </div>
        </div>
    </form>

    <div class="panel panel-default js-show-on-submit display-none">
        <div class="panel-body text-center">
            <h3 class="js-message">Enviando respuesta...</h3>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/respond_survey_handler.js') }}"></script>
    <script>
        $(function () {
            RespondSurveyHandler.total_questions = {{count($questions)}};
            RespondSurveyHandler.init();
        });
    </script>
@endsection
