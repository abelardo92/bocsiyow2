
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Horarios por sucursal
                <p class="pull-right">
                    {{ Carbon\Carbon::now()->formatLocalized('%A %d %B') }} -
                    @if(Carbon\Carbon::now()->format('H:i:s') <= '15:45:00')
                        Turno Matutino
                    @else
                        Turno Vespertino
                    @endif
                </p>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Sucursal</th>
                            <th>Puesto</th>
                            <th>Nombre</th>
                            <th>Asistencia</th>
                            <th>Hora entrada</th>
                            <th>Doble</th>
                            <th>¿Acceso?</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $curr_subsidiary = 0; ?>
                        @foreach($subsidiaries as $subsidiary)
                            <?php $schedules = $subsidiary->schedules()->with(['employee'])->today()->currentTurn($subsidiary->id)->get();?>
                            @if($schedules->count() > 0)
                                @foreach($schedules as $schedule)
                                    @if(!$schedule->employee->active)
                                        @continue
                                    @endif
                                    <tr
                                    @if($curr_subsidiary % 2 == 0)
                                        class="active"
                                    @endif
                                    >
                                    <?php
                                        $todayAttendances = $schedule->employee->attendances()->where('subsidiary_id', $subsidiary->id)->today()->get();
                                    ?>
                                        <td>{{$subsidiary->name}}</td>
                                        <td>{{$schedule->employee->job}}</td>
                                        <td>{{$schedule->employee->short_name}}</td>
                                        <td>
                                            @if($todayAttendances->count())
                                                <i class="fa fa-check"></i>
                                            @else
                                                <i class="fa fa-close"></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if($todayAttendances->count())
                                                {{$todayAttendances->first()->getTime()}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($schedule->turn->identifier == 3)
                                                <i class="fa fa-check"></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if($todayAttendances->count())
                                                <i class="fa fa-unlock"></i>
                                            @else
                                                @if($schedule->can_check)
                                                    <i class="fa fa-unlock"></i>
                                                @else
                                                    <form action="{{route('employees.schedules.unlock', $schedule->id)}}" method="post">
                                                        {{ csrf_field() }}
                                                        {{ method_field('PUT') }}
                                                        <button class="btn btn-link">
                                                            <i class="fa fa-lock"></i>
                                                        </button>
                                                    </form>
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{route('employees.edit', $schedule->employee->id)}}">Editar</a>
                                        </td>
                                   </tr>
                                @endforeach
                                <?php $curr_subsidiary++; ?>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>