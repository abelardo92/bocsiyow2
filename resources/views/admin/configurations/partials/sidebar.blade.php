<div class="panel panel-default panel-flush">
    <div class="panel-heading">Configuración</div>

    <div class="panel-body">
        <ul class="nav custom-stacked-pills" role="tablist">
            <!-- languages Link -->
            @if(Auth::user()->isA('super-admin', 'super-admin-restringido', 'accountant'))  
            <li role="presentation" class="active">
                <a href="#empresa" aria-controls="empresa" role="tab" data-toggle="tab" aria-expanded="true">
                    Datos empresa
                </a>
            </li>

            <li role="presentation">
                <a href="#archivos" aria-controls="archivos" role="tab" data-toggle="tab" aria-expanded="true">
                    Archivo(s)
                </a>
            </li>
            @endif

            @if(Auth::user()->isA('super-admin', 'super-admin-restringido'))
            <li role="presentation">
                <a href="#contrato" aria-controls="contrato" role="tab" data-toggle="tab" aria-expanded="true">
                    Contrato
                </a>
            </li>

            <li role="presentation">
                <a href="#puestos" aria-controls="puestos" role="tab" data-toggle="tab" aria-expanded="true">
                    Puestos
                </a>
            </li>

            <li role="presentation">
                <a href="#tipos" aria-controls="tipos" role="tab" data-toggle="tab" aria-expanded="true">
                    Tipos de cambio
                </a>
            </li>

            <li role="presentation">
                <a href="#huella" aria-controls="huella" role="tab" data-toggle="tab" aria-expanded="true">
                    Dispositivos de huella
                </a>
            </li>

            <li role="presentation">
                <a href="#turnos" aria-controls="turnos" role="tab" data-toggle="tab" aria-expanded="true">
                    Turnos
                </a>
            </li>
            @endif

        </ul>
    </div>
</div>
