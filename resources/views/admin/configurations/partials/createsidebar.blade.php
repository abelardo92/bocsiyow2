<div class="panel panel-default panel-flush">
    <div class="panel-heading">Configuración</div>

    <div class="panel-body">
        <ul class="nav custom-stacked-pills" role="tablist">
            <!-- languages Link -->
            <li role="presentation" class="active">
                <a href="#empresa" aria-controls="empresa" role="tab" data-toggle="tab" aria-expanded="true">
                    Datos empresa
                </a>
            </li>
            <li role="presentation">
                <a href="#contrato" aria-controls="contrato" role="tab" data-toggle="tab" aria-expanded="true">
                    Contrato
                </a>
            </li>
        </ul>
    </div>
</div>
