<div role="tabpanel" class="tab-pane" id="tipos">
    <div class="panel panel-default">
        <div class="panel-heading">
        Tipos de cambio - <code>* = Activo</code>
            <p class="pull-right">
                <a href="{{ route('configuration.rate.create') }}">Agregar nuevo</a>
            </p>
        </div>

        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Rate</th>
                        <th>Fecha</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rates as $rate)
                        <tr>
                            <td>
                                @if($loop->first)
                                    *
                                @endif
                                {{ $rate->rate }}
                            </td>
                            <td>{{ $rate->created_at }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $rates->links() }}
        </div>
    </div>
</div>
