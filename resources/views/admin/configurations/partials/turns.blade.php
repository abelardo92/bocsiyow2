<div role="tabpanel" class="tab-pane" id="turnos">
    <div class="panel panel-default">
        <div class="panel-heading">
            Turnos
            <p class="pull-right">
                <a href="{{ url('/home/configurations/turns') }}">Agregar nuevo</a>
            </p>
        </div>

        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Identificador</th>
                        <th>Nombre</th>
                        <th>Inicio</th>
                        <th>Fin</th>
                        <th>Descanso</th>
                        <th>¿Se paga?</th>
                        {{-- <th>Opciones</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach($turns as $turn)
                        <tr>
                            <td>{{ $turn->identifier }}</td>
                            <td>{{ $turn->name }}</td>
                            <td>{{ $turn->start }}</td>
                            <td>{{ $turn->end }}</td>
                            <td>{{ $turn->is_rest ? 'Si' : 'No' }}</td>
                            <td>{{ $turn->is_payed ? 'Si' : 'No' }}</td>
                            <td>
                                <a href="{{url('/home/configurations/turns/'.$turn->id)}}">
                                    Editar
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
