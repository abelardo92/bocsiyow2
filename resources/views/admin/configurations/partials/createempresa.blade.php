<div role="tabpanel" class="tab-pane active" id="empresa">
    <div class="panel panel-default">
        <div class="panel-body">
            <form action="{{route('configurations.store')}}" method="post" enctype="multipart/form-data">
                {{{ csrf_field() }}}
               
                @if($configuration->image)
                    <div class="text-center">
                        <img src="{{$configuration->image->path}}" alt="{{$configuration->image->name}}" class="img-thumbnail" width="200">
                    </div>
                @endif

                <div class="form-group">
                    <label for="image">Logo para los tickets</label>
                    <input type="file" name="image" id="image" class="form-control">
                </div>

                <div class="form-group">
                    <label for="razon_social">Razón Social</label>
                    <input type="text" name="razon_social" id="razon_social" class="form-control" value="{{old('razon_social', $configuration->razon_social)}}">
                </div>

                <div class="form-group">
                    <label for="rfc">RFC</label>
                    <input type="text" name="rfc" id="rfc" class="form-control" value="{{old('rfc', $configuration->rfc)}}">
                </div>

                <div class="form-group">
                    <label for="curp">CURP</label>
                    <input type="text" name="curp" id="curp" class="form-control" value="{{old('curp', $configuration->curp)}}">
                </div>

                <div class="form-group">
                    <label for="registration_number">Número de registro patronal</label>
                    <input type="text" name="registration_number" id="registration_number" class="form-control" value="{{old('registration_number', $configuration->registration_number)}}">
                </div>

                <div class="form-group">
                    <label for="regimen">Regimen</label>
                    <input type="text" name="regimen" id="regimen" class="form-control" value="{{old('regimen', $configuration->regimen)}}">
                </div>

                <div class="form-group">
                    <label for="address">Dirección</label>
                    <input type="text" name="address" id="address" class="form-control" value="{{old('address', $configuration->address)}}">
                </div>

                <div class="form-group">
                    <label for="created_in">Expedido en</label>
                    <input type="text" name="created_in" id="created_in" class="form-control" value="{{old('created_in', $configuration->created_in)}}">
                </div>

                <div class="form-group">
                    <label for="text_footer">texto pie de pagina de los tickets</label>
                    <textarea name="text_footer" id="text_footer" class="form-control" rows="5">{{old('text_footer', $configuration->text_footer)}}</textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-default" value="Guardar">
                </div>
            </form>
        </div>
    </div>
</div>
