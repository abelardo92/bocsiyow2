@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
            @if(Auth::user()->isA('super-admin'))
                <div class="panel panel-default">
                    <div class="panel-heading">Menu</div>
                    <div class="panel-body">
                        <ul class="list_unstyled">
                        <li>
                            <a href="{{route('admin.configurations.create')}}">Agregar configuración</a>
                        </li>
                    </ul>
                    </div>
                </div>
            @endif
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Configuraciones</div>

                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Razón social</th>
                                    <th>RFC</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($configurations as $configuration)
                                    <tr>
                                        <td>{{ $configuration->razon_social }}</td>
                                        <td>{{ $configuration->rfc }}</td>
                                        <td><a href="{{ route('admin.configurations.edit', $configuration->id) }}">Editar</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection