@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Dispositivos de huella
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/configurations/devices/'.$device->id)}}" method="POST" class="form-horizontal">
                        {{{ csrf_field() }}}
                        {{{ method_field('PUT') }}}

                        <div class="form-group">
                            <label for="device_name" class="col-sm-4 control-label">Nombre *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="device_name" name="device_name" required="required" value="{{ old('device_name', $device->device_name) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sn" class="col-sm-4 control-label">Numero de serie *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="sn" name="sn" required="required" value="{{ old('sn', $device->sn) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="vc" class="col-sm-4 control-label">Codigo de verificación *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="vc" name="vc" required="required" value="{{ old('vc', $device->vc) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ac" class="col-sm-4 control-label">Codigo de activación *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="ac" name="ac" required="required" value="{{ old('ac', $device->ac) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="vkey" class="col-sm-4 control-label">Clave de verificación *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="vkey" name="vkey" required="required" value="{{ old('vkey', $device->vkey) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="subsidiary_id" class="col-sm-4 control-label">Sucursal *</label>
                            <div class="col-sm-6">
                                <select class="form-control" id="subsidiary_id" name="subsidiary_id" required="required">
                                    <option value="">Seleccione uno por favor...</option>
                                    @foreach($subsidiaries as $subsidiary)
                                        <option @if($device->subsidiary_id == $subsidiary->id) selected="selected" @endif value="{{$subsidiary->id}}">{{$subsidiary->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-6">
                                <button type="submit" class="btn btn-default">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
