@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            @include('admin.configurations.partials.sidebar')
        </div>
        <div class="col-md-9">
            <div class="tab-content">
            @if(Auth::user()->isA('super-admin', 'super-admin-restringido', 'accountant'))
                @include('admin.configurations.partials.empresa')
                @include('admin.configurations.partials.archivos')
            @endif
            @if(Auth::user()->isA('super-admin', 'super-admin-restringido'))
                @include('admin.configurations.partials.contrato')
                @include('admin.configurations.partials.jobs')
                @include('admin.configurations.partials.exchange_money',['configuration' => $configuration ])
                @include('admin.configurations.partials.devices')
                @include('admin.configurations.partials.turns')
            @endif

            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    function insertAtCaret(text) {
        var txtarea = document.getElementById("contract");
        if (!txtarea) { return; }

        var scrollPos = txtarea.scrollTop;
        var strPos = 0;
        var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
            "ff" : (document.selection ? "ie" : false ) );
        if (br == "ie") {
            txtarea.focus();
            var range = document.selection.createRange();
            range.moveStart ('character', -txtarea.value.length);
            strPos = range.text.length;
        } else if (br == "ff") {
            strPos = txtarea.selectionStart;
        }

        var front = (txtarea.value).substring(0, strPos);
        var back = (txtarea.value).substring(strPos, txtarea.value.length);
        txtarea.value = front + text + back;
        strPos = strPos + text.length;
        if (br == "ie") {
            txtarea.focus();
            var ieRange = document.selection.createRange();
            ieRange.moveStart ('character', -txtarea.value.length);
            ieRange.moveStart ('character', strPos);
            ieRange.moveEnd ('character', 0);
            ieRange.select();
        } else if (br == "ff") {
            txtarea.selectionStart = strPos;
            txtarea.selectionEnd = strPos;
            txtarea.focus();
        }

        txtarea.scrollTop = scrollPos;
    }
</script>
@endsection