@extends('layouts.app_customers')

@section('css')
<style>
    body {
        font-family: "Segoe UI",Arial,sans-serif;
        padding-top: 0px;
    }
    .panel-body {
        height: 520px;
    }
    .search-button-container {
        display: flex;
    }

    .d-flex {
        display: flex !important;
    }
    .search-button-container button {
        margin-left: auto;
    }
    .time-button {
        width: 86px;
        margin-right: 10px;
        margin-top: 10px;
    }

    .mt-3, .my-3 {
        margin-top: 1rem !important;
    }

    .mb-3, .my-3 {
        margin-bottom: 1rem !important;
    }

    .mt-0, .my-0 {
        margin-top: 0 !important;
    }

    .ml-auto, .mx-auto {
        margin-left: auto !important;
    }
    .mr-auto, .mx-auto {
        margin-right: auto !important;
    }
    .out {
        width: 500px;
        height: 500px;
        overflow: hidden;
        background-image: url(/images/bocsiyow-02.png);
        background-size: cover;
        background-position: center;
    }

    .logo-container {
        flex-direction: column;
    }
    .logo-container img {
        max-width: 400px;
        transform: scale(1.5);
    }

    .logo-container h1 {
        margin-bottom: 3rem !important;
    }

    @media(max-width: 991px) {
        .panel-body {
            height: auto;
        }
    }

    [v-cloak] { visibility: hidden }

</style>
@endsection

@section('content')
<div class="container">
    <div class="row d-flex logo-container">
        <img src="/images/bocsiyow-02.png" class="mx-auto" alt="Bocsiyow" />
        {{-- <div class="out"></div> --}}
        <h1 class="mx-auto mt-0 mb-3">Agende una cita aqui!</h1>
    </div>
    <div class="row">
        <form action="{{route('diaries.storeCustomer')}}" method="POST">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Citas
                        <p class="pull-right help-block">Campos con * son obligatorios.</p>
                    </div>

                    <div class="panel-body">
                        {{{ csrf_field() }}}

                        <input type="hidden" name="time" v-model="time" value="0">

                        <div class="form-group">
                            <label for="customer_name" class="control-label">Nombre completo</label>
                            <input type="text" name="customer_name" id="customer_name" value="{{old('customer_name')}}" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="control-label">Telefono celular *</label>
                            <input type="text" class="form-control" name="phone" id="phone" value="{{old('phone')}}"/>
                        </div>
                        {{-- <div class="form-group">
                            <label for="email" class="control-label">Correo electrónico *</label>
                            <input type="text" class="form-control" name="email" id="email" value="{{old('email')}}"/>
                        </div> --}}

                        <div class="form-group">
                            <label for="subsidiary_id" class="control-label">
                                Sucursal *
                            </label>
                            <input type="hidden" id="original_subsidiary_id" value="{{intval(old('subsidiary_id'))}}" />
                            <select name="subsidiary_id" id="subsidiary_id" class="form-control" v-on:change="searchTimesCustomerForm" v-model="subsidiary_id">
                                <option value="">Seleccionar uno por favor...</option>
                                @foreach($subsidiaries as $subsidiary)
                                    <option value="{{$subsidiary->id}}">{{$subsidiary->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="date" class="control-label">Fecha *</label>
                            {{-- <input type="text" class="form-control datepicker" name="date" id="date" v-model="date"> --}}
                            <input type="hidden" id="original_date" value="{{ !empty(old('date')) ? old('date') : $current_date->format('Y-m-d') }}"/>
                            <input type="hidden" id="date" name="date" v-model="dateFormatter(date)"/>
                            <datepicker 
                                v-model="date"
                                :format="dateFormatter"
                                @input="searchTimesCustomerForm"
                                :bootstrap-styling="true"
                            ></datepicker>
                        </div>

                        <div class="form-group">
                            <label for="service_id" class="control-label">
                                Servicio *
                            </label>
                            <input type="hidden" id="old_service_id" value="{{intval(old('service_id'))}}" />
                            <select name="service_id" id="service_id" class="select2 form-control" required="required">
                                <option value="" {{ intval(old('service_id')) == 0 ? 'selected' : '' }}>Seleccionar uno por favor...</option>
                                @foreach($services as $service)
                                    <option value="{{$service->id}}"  {{ old('service_id') == $service->id ? 'selected' : '' }} >{{$service->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group search-button-container">
                            <button type="button" class="btn btn-primary" v-on:click="searchTimesCustomer">BUSCAR FECHAS</button>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Horas disponibles
                    </div>
                    <div class="panel-body">
                        <p class="alert alert-danger" v-cloak v-if="time_error">@{{time_error}}</p>
                        <div class="times-container" v-cloak v-for="timess in times">
                            <p>SELECCIONE UN HORARIO DISPONIBLE</p>
                            <p><b>IMPORTANTE:</b> Recibirá un código de confirmación por sms. En caso de cancelación o cambios comuniquese al 998-286-0491.</p>
                            {{-- <button class="btn btn-default time-button" :class="{active: time.active}" :disabled="time.disabled" v-for="time in timess" @click="setTimeActiveCustomers(time)">@{{time.time_es}}</button> --}}
                            <input 
                                type="button" 
                                class="btn btn-default time-button" 
                                :class="{active: time.active}" 
                                v-for="time in timess" 
                                :value="time.time_es"
                                v-on:click="setTimeActiveCustomers(time)"
                                />
                        </div>
                        <div class="row">
                            <div class="col-sm-12 d-flex mt-3">
                                <button type="submit" class="btn btn-primary ml-auto" v-cloak v-if="canCreateDiary">AGENDAR CITA</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            // $('#subsidiary_id').val($("#old_subsidiary_id").val());
            // $('#service_id').val($("#old_service_id").val());
            // $("#service_id").select2();
            // $('#customer_id').on('select2:select', function () {
            //     $.ajax({
            //         type: 'get',
            //         dataType: 'json',
            //         url: '/home/customers/'+$(this).val(),
            //         success: function (res) {
            //             $('#phone').val(res.phone)
            //         }
            //     });
            // });
            // $('#customer_id').on('change', function (evt) {
            //     if ($(this).val() == 7) {
            //         $('#comments-group').show();
            //     } else {
            //         $('#comments-group').hide();
            //     }
            // });
        });
    </script>
@endsection