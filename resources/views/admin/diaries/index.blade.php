@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Citas de hoy
                    <div class="pull-right">
                    @php
                        $user = Auth::user();
                    @endphp
                    @if($user->access_subsidiary_id == null)
                        <a href="{{route('diary.create')}}">Agregar cita</a>
                    @endif
                    </div>
                </div>

                <div class="panel-body">
                    <form action="{{route('diary.index')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="start">Fecha: </label>
                            <input type="text" class="form-control datepicker" name="start" value="{{$start}}">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Código de confirmación</th>
                                <th>Cliente</th>
                                <th>Servicio</th>
                                <th>Teléfono</th>
                                <th>Hora</th>
                                <th>Barbero</th>
                                <th>Fecha de creación</th>
                                <th>Agendado por</th>
                                <th>Atendido</th>
                                @if(!$subdomain)
                                    <th>Sucursal</th>
                                @endif
                                <th>Cancelado</th>
                                @if(Auth::user()->isA('super-admin', 'cashier-admin', 'subsidiary-admin') && $start == \Carbon\Carbon::now()->format('Y-m-d'))
                                    <th>Opciones</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($diaries as $diary)

                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $diary->confirmation_code }}</td>
                                    <td>
                                        {{ $diary->created_by_customer ? $diary->customer_name : $diary->customer->name }}
                                        @if($diary->comments)
                                            ({{$diary->comments}}) 
                                        @endif
                                    </td>
                                    <td>
                                        @if($diary->service)
                                            {{$diary->service->name}}
                                        @endif
                                    </td>
                                    <td>{{ $diary->customer->phone }}</td>
                                    <td>{{ $diary->time }}</td>
                                    <td>
                                        {{ $diary->employee ? $diary->employee->short_name : ''}}
                                    </td>
                                    <td>
                                        {{ $diary->created_at->format('h:i a d-m-Y')}}
                                    </td>
                                    @if($diary->user)
                                        <td>{{ $diary->user->name }}</td>
                                    @else
                                        <td>
                                            {{ $diary->cashier ? $diary->cashier->short_name : '' }}
                                            {{ $diary->created_by_customer ? 'Agendado por el cliente' : '' }}
                                        </td>
                                    @endif
                                    <td>{{ $diary->attend ? 'Si' : 'No' }}</td>
                                    @if(!$subdomain)
                                        <td>{{ $diary->subsidiary->name }}</td>
                                    @endif
                                    <td>
                                    @if($diary->canceled)
                                        Si - {{ $diary->updated_at->format('Y-m-d h:i a')}}
                                    @else
                                        No
                                    @endif
                                    </td>
                                    @if(Auth::user()->isA('super-admin', 'cashier-admin', 'subsidiary-admin') && $start >= \Carbon\Carbon::now()->format('Y-m-d'))
                                        <td>
                                            @if(Auth::user()->isA('super-admin', 'cashier-admin', 'subsidiary-admin'))
                                                @if(!$diary->canceled)
                                                    
                                                    <a href="{{route('diary.edit', $diary->id)}}" class="btn btn-info btn-xs">Editar</a>
                                                    <form action="{{route('diary.destroy', $diary->id)}}" method="post">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                        <input type="submit" class="btn btn-danger btn-xs" value="Cancelar">
                                                    </form>
                                                @endif
                                            @endif
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
