@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Menu</div>

                <div class="panel-body">
                    <ul class="list-unstyled">
                        <li><a href="{{route('articles.index', ["type" => 'insumo'])}}">Insumos</a></li>
                        <li><a href="{{route('articles.index', ["type" => 'activo'])}}">Activos</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Articulos
                    <p class="pull-right">
                        <a href="{{ route('articles.create') }}">Agregar</a>
                    </p>
                </div>

                <div class="panel-body">
                    <table class="table datatables">
                        <thead>
                            <tr>
                                <th>Key</th>
                                <th>Name</th>
                                <th>Precio compra</th>
                                <th>opcion</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($articles as $article)
                                <tr>
                                    <td>{{$article->key}}</td>
                                    <td>{{$article->name}}</td>
                                    <td>{{$article->buy_price}}</td>
                                    <td>
                                        <a href="{{ route('articles.edit', $article->id) }}">Editar</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection