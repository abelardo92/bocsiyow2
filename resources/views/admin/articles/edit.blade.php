@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Articulos
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <div class="">
                        <div class="">
                            <form action="{{route('articles.update', $article->id)}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                {{{ csrf_field() }}}
                                {{{ method_field('put') }}}
                                <div class="form-group">
                                    <label for="key" class="col-sm-4 control-label">Codigo *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="key" name="key" required="required" value="{{ old('key', $article->key) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">Nombre *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="name" name="name" required="required" value="{{ old('name', $article->name) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="buy_price" class="col-sm-4 control-label">Precio de compra *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="buy_price" name="buy_price" value="{{ old('buy_price', $article->buy_price) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="type" class="col-sm-4 control-label">Tipo *</label>
                                    <div class="col-sm-6">
                                        <select name="type" id="type" class="form-control" required>
                                            <option @if($article->type == 'activo') selected="selected" @endif value="activo">Activo</option>
                                            <option @if($article->type == 'insumo') selected="selected" @endif value="insumo">Insumo</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-6">
                                        <button type="submit" class="btn btn-default">Guardar</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
