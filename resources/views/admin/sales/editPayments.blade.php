@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Servicio {{$sale->id}}
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form id="submit" action="{{route('sales.updatePayments', [$sale->id])}}" method="POST" class="form-horizontal">
                        {{{ csrf_field() }}}
                        {{{ method_field('PUT') }}}
                        <div id="sale_payments_form">
                        <sale-payments-form
                        :sale="{{$sale->toJson()}}"
                        :barbers="{{$barbers->toJson()}}"
                        :products="{{$products->toJson()}}"
                        :services="{{$services->toJson()}}"
                        ></sale-payments-form>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('js/customer-filter.js') }}"></script>
@endsection
