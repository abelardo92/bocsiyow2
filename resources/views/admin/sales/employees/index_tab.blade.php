<table class="table table-striped datatables">
    <thead>
        <tr>
            <th>Empleado</th>
            <th>Productos</th>
            <th>Pagado</th>
            <th>Pendiente</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($sales as $sale)
            <tr>
                <td>{{ $sale->employee->name }}</td>
                <td>
                    @foreach ($sale->products as $product)
                    ({{$product->qty}}) {{ $product->product->name }} <br>
                    @endforeach
                </td>
                <td>{{ $sale->paidAmount() }}</td>
                <td>{{ $sale->total - $sale->paidAmount() }}</td>
                <td>
                    @if($sale->paid)
                    <a href="{{ url("/home/sales/{$sale->id}/employees/editPayments") }}">Ver Pagos</a> | <br>
                    @else
                    <a href="{{ url("/home/sales/{$sale->id}/employees/editPayments") }}">Editar Pagos</a> | <br>
                    @endif
                    <a href="{{ url("/home/sales/{$sale->id}/print") }}">Ver nota</a>
                    @if(Auth::user()->isA('super-admin'))
                        | <br>
                        <a href="#" data-toggle="modal" data-target="#modal-delete-{{$sale->id}}">
                            Cancelar
                        </a>
                        @include('admin.sales.employees.delete_modal', compact('sale'))
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>