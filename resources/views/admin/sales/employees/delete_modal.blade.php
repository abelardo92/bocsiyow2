<div class="modal fade" id="modal-delete-{{$sale->id}}" tabindex="-1" role="dialog"
    aria-labelledby="modal-delete-{{$sale->id}}Label">
    <form action="{{url("/home/delete/sales/employees/{$sale->folio}")}}" method="post">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modal-delete-{{$sale->id}}Label">Cancelar venta</h4>
                </div>
                <div class="modal-body">
                    <p>Desea cancelar esta venta?</p>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-danger" value="Cancelar">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </form>
</div>