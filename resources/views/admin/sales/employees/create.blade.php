@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Venta de producto(s) para empleado
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{route('sales.employees.store', $subsidiary->key)}}" method="POST" class="form-horizontal" name="sale-form" id="sale-form" enctype="multipart/form-data">
                        {{{ csrf_field() }}}

                        <div class="form-group">
                            <label for="employee_id" class="col-sm-4 control-label">Empleado</label>
                            <div class="col-sm-6">
                                <select name="employee_id" id="employee_id" required="required" class="select2 form-control">
                                    <option value="0">Seleccionar uno por favor...</option>
                                    @foreach($employees as $employee)
                                        <option value="{{$employee->id}}">{{$employee->short_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="employee_id" class="col-sm-4 control-label">Como realizara sus pagos? (Descripcion)</label>
                            <div class="col-sm-6">
                                <textarea class="form-control" id="employee_sale_comments" name="employee_sale_comments" required="required"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-6">
                                <button type="submit" class="btn btn-default" id="save">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Productos
                </div>

                <div class="panel-body">
                    <div class="row">
                    @php $cont = 0; @endphp
                        @foreach($products as $product)
                            @if($cont == 0)
                            <div class="row">
                            @endif
                                <div class="col-md-2">
                                    <label class="thumbnail">
                                        <img src="{{$product->image ? $product->image->path : 'http://placehold.it/200x200'}}" style="height:150px !important;" alt="{{$product->name}}">
                                        <div class="caption">
                                            <h5>
                                                <input type="checkbox" name="product_id[]" form="sale-form" value="{{$product->id}}">
                                                {{$product->name}}
                                            </h5>

                                            <input type="number" class="form-control" name="product_qty[{{$product->id}}]" form="sale-form" value="0">
                                        </div>
                                    </label>
                                </div>
                                @php $cont++; @endphp
                            @if($cont >= 6 || $loop->last)
                                </div>
                                @php $cont = 0; @endphp
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/customer-filter.js') }}"></script>
    <script>
        $(function() { 
            $("#sale-form").on('submit', function (e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': Laravel.csrfToken
                    }
                });

                $("#save").attr("disabled", true);

                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: '/home/sales/employees/store',
                    data: $("#sale-form").serialize(),
                    success: function({success, sale_id, message}) {
                        $("#save").removeAttr("disabled");
                        if(success) {
                            window.open(`/home/sales/${sale_id}/print`);
                            window.location.href = "/home/sales/employees/list";
                            return;
                        }
                        $('body').prepend(
                            `<div class="alert alert-danger alert-dismissible" style="right: 70px; z-index: 1031; position: absolute;">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">X</span></button>
                                <ul><li>${message}</li></ul>
                            </div>`
                        );
                    }
                });

            });
        });
    </script>
@endsection
