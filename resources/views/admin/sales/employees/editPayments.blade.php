@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Servicio {{$sale->id}}
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>
                <div class="panel-body">
                    <form id="submit" action="{{route('sales.employees.updatePayments', [$sale->id])}}" method="POST" class="form-horizontal">
                        {{{ csrf_field() }}}
                        {{{ method_field('PUT') }}}
                        <div id="sale_payments_form">
                        <sale-payments-employee-form
                        :sale="{{$sale->toJson()}}"
                        :products="{{$products->toJson()}}"
                        ></sale-payments-employee-form>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('js/customer-filter.js') }}"></script>
@endsection
