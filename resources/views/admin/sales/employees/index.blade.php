@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Ventas a empleados
                    @if(Auth::user()->isA('super-admin', 'horarios', 'subsidiary-admin', 'cashier-admin'))
                        <div class="pull-right">
                            <a href="{{route('employees.create')}}">Agregar venta a empleado</a>
                        </div>
                    @endif
                </div>

                <div class="panel-body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#actives" aria-controls="actives" role="tab" data-toggle="tab">Pendientes</a></li>
                        @if(!Auth::user()->isA('subsidiary-admin'))
                            <li role="presentation"><a href="#inactives" aria-controls="inactives" role="tab" data-toggle="tab">Pagados</a></li>
                        @endif
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="actives">
                            @include('admin.sales.employees.index_tab', ['sales' => $pending_sales])
                        </div>
                        <div role="tabpanel" class="tab-pane" id="inactives">
                            @include('admin.sales.employees.index_tab', ['sales' => $paid_sales])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
