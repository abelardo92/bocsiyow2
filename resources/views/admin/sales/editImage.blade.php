@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Servicio {{$sale->id}}
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{route('sales.updateImage', [$sale->id])}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                        {{{ csrf_field() }}}
                        {{{ method_field('PUT') }}}
                        
                        <div class="form-group">
                            <div class="col-sm-4">
                                <strong>Seleccione una imagen:</strong>       
                            </div>
                            <div class="col-sm-4">
                                <input class="form-control" type="file" id="image" name="image">
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-default">Guardar</button>
                            </div>
                        </div>
                        @if($sale->image != null) 
                        <div class="form-group">
                            <div class="col-sm-8">
                                <img src="{{$sale->image->path}}" alt="{{$sale->image->name}}" style="width: 80%;"/>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
