@extends('layouts.app')

@section('css')
    <style type="text/css">
        #bocsiyow-menu {
            display: none;
        }
        #time {
            font-size: 36px;
        }
        .table>tbody>tr>td, .table>thead>tr>th {
            font-size: 18px;
        }
        body {
            padding: 0px;
        }
        .container {
            width:95%;
        }
    </style>
@endsection

@section('content')
<active-sales></active-sales>
@endsection
