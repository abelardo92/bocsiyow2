@php
$is_customer_print = isset($customer_print) && $customer_print == true;
$extends = $is_customer_print ? 'layouts.tickets_customer' : 'layouts.tickets';
$text = $is_customer_print ? '' : 'Original';

// set to false for now just to avoid second ticket...
$print_second_ticket = false;
@endphp
@extends($extends)

@section('subsidiary')
    {{ $sale->subsidiary->name }}
@endsection

@section('fecha')
    {{ $sale->created_at->format('d-m-Y h:i a') }}
@endsection

@section('address')
    {{ $sale->subsidiary->address }}
@endsection

@section('content')
    @php
        if($sale->subsidiary->configuration_id != null) {
            $configuration = App\Configuration::find($sale->subsidiary->configuration_id);
        } else {
            $configuration = App\Configuration::first();
        }

        $imagepath = $configuration->image ? $configuration->image->path : '';
        // dd($imagepath);
    @endphp
    @if($sale->canceled_by)
        <div class="background">
            <p class="bg-text">CANCELADO</p>
        </div>
    @endif
    
    @if(isset($is_advance) && $is_advance)
        @include('admin.sales.tickets._sale_partial_advance', ['text' => $text])
    @else
        @include('admin.sales.tickets._sale_partial', ['text' => $text])
    @endif

    @if($print_second_ticket)
        @if($imagepath)
            <img src="{{ $imagepath }}" alt="" style="width: 100%;">
        @endif
        @if($sale->subsidiary->related_brand == 1)
        <p class="text-center">@yield('title', 'BocsiYow')</p>
        @endif
        <p>
        @if($sale->subsidiary->barber_can_sell)
            {{ $sale->employee->name }}<br>
            RFC: {{ $sale->employee->rfc }}<br>
            CURP: {{ $sale->employee->curp }}<br>
        @else
            {{ $configuration->razon_social }}<br>
            {{ $configuration->rfc }}<br>
            {{ $configuration->curp }}<br>
        @endif
        {{ $configuration->regimen }}<br>
        {{ $configuration->address }}<br>
        Lugar y fecha de expedición: <br>
        {{ $configuration->created_in }} - {{ $sale->created_at->format('d-m-Y h:i a')}}</p>
        <?php if($sale->subsidiary->key != "07"){ ?>
            <p class="text-center">
            <?php if($subsidiary->key != "08"){
                echo "Sucursal";
            }?> 
            "{{ $sale->subsidiary->name }}"</p>
            <p class="text-center">"{{ $sale->subsidiary->address }}"</p>
        <?php } ?>
        @if($sale->canceled_by)
            <div class="background">
                <p class="bg-text">CANCELADO</p>
            </div>
        @endif
        @if(isset($is_advance) && $is_advance)
            @include('admin.sales.tickets._sale_partial_advance', ['text' => 'Copia cliente'])
        @else
            @include('admin.sales.tickets._sale_partial', ['text' => 'Copia cliente'])
        @endif
    @endif

@endsection
