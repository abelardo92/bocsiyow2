<p><strong>Fecha:</strong> {{ $sale->date }}</p>
<p><strong>Hora:</strong> @if($sale->pay_at) {{ $sale->pay_at->format('h:i a') }} @endif</p>
@if($sale->customer)
<p>
    <strong>Cliente(s):</strong>
    @if($sale->subsidiary->is_laundry)
        <?php $wash_quantity = $sale->washQuantity(); ?>
        @if($sale->customer_subsidiary_id != null)
            {{ $sale->customerSubsidiary->name }}
        @else
            @if($sale->customer_id != null)
                {{ $sale->customer->name }}
            @else
                {{ $sale->comments }}
            @endif
        @endif
    @else
        {{ $sale->customer->name}}
        @if($sale->customer2 != "")
            , {{ $sale->customer2}}
        @endif
        @if($sale->customer->id == 7)
            ({{ $sale->comments}})
        @endif
    @endif
</p>
@else
<p>
    <strong>Cliente:</strong> {{$sale->employee->name}}
@endif
@if($text == "Original")
    <?php
        $saleproducts = $sale->allProducts;
        $saleservices = $sale->allServices;
    ?>
@else
    <?php
        $saleproducts = $sale->products;
        $saleservices = $sale->services;
    ?>
@endif
<table width="100%">
    <tr>
        <th>Cantidad</th>
        <th>Producto / Servicio</th>
        <th>Importe</th>
        @if($text == "Original" && $sale->canceled_by)
        <th></th>
        @endif
    </tr>
    <?php
        $hasPromotion = false;
        $subtotal = 0;
        $discount_courtesy = 0;
    ?>

    @foreach($saleservices as $sale_service)
        <tr>
            <td align="center">
                <span
                @if($sale_service->disabled)
                    style="text-decoration: line-through;"
                @endif>
                @if($sale->subsidiary->is_laundry && $sale_service->service_id == 23 && $wash_quantity <= 5)
                    1-5 kg
                @else
                    {{$sale_service->qty}}
                @endif
                </span>
            </td>
            <td align="center">
                <span
                @if($sale_service->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_service->service->name}}
                </span>
            </td>
            <td align="center">
                <span
                @if($sale_service->disabled)
                    style="text-decoration: line-through;"
                @endif>
                @if($sale_service->canceled_by)
                -
                @endif
                @if($sale_service->has_promotion)
                    {{$sale_service->original_price * $sale_service->qty}}
                @elseif($sale->is_courtesy)
                    @php
                        $original_price_qty = $sale_service->original_price * $sale_service->qty;
                        if($sale_service->has_courtesy) {
                            $discount_courtesy += $original_price_qty;
                        }
                    @endphp
                    {{$original_price_qty}}
                @elseif($sale_service->service_id == 23 && $wash_quantity <= 5)
                    {{$sale_service->price * 5}}
                @else
                    {{$sale_service->price * $sale_service->qty}}
                @endif
                </span>
            </td>
            @if($text == "Original")
                <td>
                    @if($sale_service->canceled_by)
                        Cancelado por:<br>{{ $sale_service->canceledBy->name }}
                    @endif
                </td>
            @endif
        </tr>
        @php
            $subtotal += $sale_service->original_price;
        @endphp
        @if(!$sale->has_birthday && $sale_service->has_promotion && $sale_service->original_price > $sale_service->price)
            <?php $hasPromotion = true;?>
        <tr>
            <td></td>
            <td align="center">
                Desc. promoción
            </td>
            <td align="center">
                - {{($sale_service->original_price - $sale_service->price) * $sale_service->qty}}
            </td>
        </tr>
        @endif
    @endforeach
    @foreach($saleproducts as $sale_product)
        <tr>
            <td align="center">
                <span
                @if($sale_product->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_product->qty}}
                </span>
            </td>
            <td align="center">
                <span
                @if($sale_product->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_product->product->name}}
                </span>
            </td>
            <td align="center">
                <span
                @if($sale_product->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_product->price * $sale_product->qty}}
                </span>
            </td>
            @if($text == "Original")
                <td>
                    @if($sale_product->canceled_by)
                        Cancelado por:<br>{{ $sale_product->canceledBy->name }}
                    @endif
                </td>
            @endif
        </tr>
        @if($sale_product->price < $sale_product->product->sell_price)
            <?php $hasPromotion = true;?>
        <tr>
            <td></td>
            <td align="center">
                {{ !$sale->is_for_employee ? "Desc. promoción" : "Descuento empleado {$sale_product->discount_percentaje}%" }}
            </td>
            <td align="center">
                - {{($sale_product->product->sell_price - $sale_product->price) * $sale_product->qty}}
            </td>
        </tr>
        @endif
        @php
            $subtotal += $sale_product->original_price;
        @endphp
    @endforeach
    @foreach($sale->packages as $sale_package)
        <tr>
            <td align="center">
                <span
                @if($sale_package->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_package->quantity}}
                </span>
            </td>
            <td align="center">
                <span
                @if($sale_package->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_package->package->name}}
                </span>
            </td>
            <td align="center">
                <span
                @if($sale_package->disabled)
                    style="text-decoration: line-through;"
                @endif>
                    {{$sale_package->package->price * $sale_package->quantity}}
                </span>
            </td>
        </tr>
        @php
            $subtotal += $sale_package->price;
        @endphp
    @endforeach
    <tr>
        <td colspan="2" align="right"><strong>Subtotal</strong></td>
        <td> $
        @if($sale->is_courtesy)
            {{$subtotal}}
        @else
            {{$sale->subtotal}}
        @endif
        </td>
    </tr>
    @if($sale->tip > 0)
    <tr>
        <td colspan="2" align="right"><strong>Propina</strong></td>
        <td>$ {{$sale->tip}}</td>
    </tr>
    @endif
    @if(!$hasPromotion && $sale->hasBirthday() && !$sale->kids_promotion)
        <tr>
            <td colspan="2" align="right"><strong>Descuento (cumpleaños)</strong></td>
            <td>$ {{number_format($sale->servicesMaxPrice(), 2, '.', '')}}</td>
        </tr>
    @endif

    @if($sale->promotion && $sale->promotion->discount_percentaje > 0)
        <tr>
            <td colspan="2" align="right"><strong>Descuento ({{$sale->discount_percentaje}}%)</strong></td>
            @php
                $services_promotion_price_total = $sale->services->where('discount_percentaje', $sale->discount_percentaje)->sum('price');
            @endphp
            <td>$ {{number_format(($services_promotion_price_total * $sale->discount_percentaje) / 100, 2, '.', '')}}</td>
        </tr>
    @endif

    @if((!$hasPromotion || ($sale->promotion && $sale->promotion->mix_with_diaries_discount)) &&
    !$sale->payments()->where('method','Monedero')->first() &&
    !$sale->is_promo_ut && !$sale->kids_promotion && !$sale->hasBirthday() &&
    !$sale->is_courtesy && $sale->frequent_customers_discount)
        <tr>
            <td colspan="2" align="right"><strong>Desc. Cliente Frecuente (10%)</strong></td>
            @php
                $services_promotion_price_total = $sale->services->where('include_in_diaries_discount', true)->sum('price');
            @endphp
            <td>$ {{number_format(($services_promotion_price_total * 10) / 100, 2, '.', '')}}</td>
        </tr>
    @endif

    @if((!$hasPromotion || ($sale->promotion && $sale->promotion->mix_with_diaries_discount)) && !$sale->payments()->where('method','Monedero')->first() && $sale->has_discount && !$sale->is_promo_ut && !$sale->kids_promotion)
        <tr>
            <td colspan="2" align="right"><strong>Desc. Cita ({{$sale->discount_percentaje}}%)</strong></td>
            @php
                $services_promotion_price_total = $sale->services->where('include_in_diaries_discount', true)->sum('price');
            @endphp
            <td>$ {{number_format(($services_promotion_price_total * $sale->discount_percentaje) / 100, 2, '.', '')}}</td>
        </tr>
    @endif
    @if($sale->is_promo_ut && !$sale->hasBirthday() && !$sale->payments()->where('method','Monedero')->first() && !$sale->kids_promotion)
        <tr>
            <td colspan="2" align="right"><strong>Descuento UT (20%)</strong></td>
            <td>$ {{number_format(($sale->servicesTotalPrice() * 20) / 100, 2, '.', '')}}</td>
        </tr>
    @endif
    @if(!$hasPromotion && $sale->kids_promotion > 0)
        <tr>
            <td colspan="2" align="right"><strong>Promocion dia del niño</strong></td>
            @if($sale->kids_promotion == 50)
                <?php $amount = (($sale->getKidsPromotionService()->price * 50) / 100) * $sale->getKidsPromotionService()->qty; ?>
            @else
                <?php $amount = (($sale->getKidsPromotionService()->price * 100) / 100) * $sale->getKidsPromotionService()->qty; ?>
            @endif
            <td>$ {{number_format($amount, 2, '.', '')}}</td>
        </tr>
    @endif

    @if($sale->is_courtesy)
    <tr>
        <td colspan="2" align="right"><strong>Cortesía</strong></td>
        <td>$
        @if($sale->is_courtesy)
            {{$discount_courtesy}}
        @else
            {{$sale->subtotal}}
        @endif
        </td>
    </tr>
    @endif

    <tr>
        <td colspan="2" align="right"><strong>A pagar</strong></td>
        <td>$
        @if($sale->is_courtesy)
            {{$subtotal + $sale->tip - $discount_courtesy}}
        @else
            {{$sale->total}}
        @endif
        </td>
    </tr>
    <?php $total = 0; ?>
    @foreach($sale->payments as $payment)
    <tr>
        <td colspan="2" align="right">
            <strong>{{$payment->method}}
            @if($payment->type == 1)
                (Anticipo)
            @endif
            </strong>
        </td>
        <td>$ {{$payment->total}} {{$payment->coin}}</td>
        <?php $total += $payment->total; ?>
    </tr>
    @endforeach

    <tr>
        <td colspan="2" align="right"><strong>Tipo de cambio</strong></td>
        <td>$ {{$sale->exchangeRate->rate}}</td>
    </tr>

    <tr>
        <td colspan="2" align="right"><strong>Cambio</strong></td>
        <td>$
        @if($sale->is_courtesy)
            {{$total - $sale->total}}
        @else
            {{$total - $sale->total}}
        @endif
        </td>
    </tr>
</table>
<?php
    $canceledServices = $saleservices->where('canceled_by', '!=', null);
    $canceledProducts = $saleproducts->where('canceled_by', '!=', null);
    // dd($canceledServices->first());
?>

@if($text == "Original" && ($sale->canceled_by || $canceledServices->count() > 0 || $canceledProducts->count() > 0 ))
    <p>
        <strong>Motivo de cancelacion:</strong>
        @if($sale->canceled_by)
            {{ $sale->canceled_description }}
        @else
            @if($canceledServices->count() > 0)
                {{ $canceledServices->first()->canceled_description }}
            @else
                @if($canceledProducts->count() > 0)
                    {{ $canceledProducts->first()->canceled_description }}
                @endif
            @endif
        @endif
    </p>
@endif

@if($sale->is_for_employee)
<p>
    <strong>Forma de pago:</strong><br>
    {{$sale->employee_sale_comments}}
</p>
@else
<p>
    <strong>Formas de pago:</strong>
    @foreach($sale->payments as $payment)
        {{$payment->method}} {{$payment->coin}} ,
    @endforeach
</p>
@endif


@if($sale->payments()->where('method', 'Tarjeta')->first() )
    <p><strong>Numero de tarjeta:</strong> **** **** **** {{ $sale->last_four }}</p>
@endif
<p><strong>Cajero:</strong> {{ $sale->cashRegister->employee->short_name }}</p>
@if($sale->employee)
    <p><strong>Atendido por:</strong> {{ $sale->employee->short_name }}
    @if($sale->employee2 != null)
        ,{{ $sale->employee2->short_name }}
    @endif
    </p>
@endif
@if($sale->customer != null)
    <p><strong>Usted lleva:</strong> {{ $sale->customer->visits }} visitas</p>
@endif
<h2><strong>Folio:</strong> {{$sale->subsidiary->key}} - {{ $sale->folio }}</h2>

@if($sale->is_for_employee)
<div class="d-flex">
    <div class="mx-auto">
        <p>--------------------------------------------------</p>
        <p class="text-center"><b>Firma del empleado</b></p>
    </div>
</div>
@endif
<p><strong>{{ $text }}</strong></p>
@if($sale->canceled_by)
    <p><strong>Cancelado</strong></p>
    <p><strong>Fecha de cancelacion:</strong> {{ $sale->canceled_at->format('d-m-Y h:i a') }}</p>
    <p><strong>Cancelado por:</strong> {{ $sale->canceler->name }}</p>
@endif

@if($sale->repair)
    <p><strong>Reparado</strong></p>
    <p><strong>Fecha de reparación:</strong> {{ $sale->repaired_at->format('d-m-Y h:i a') }}</p>
    @if($sale->repaired)
        <p><strong>Reparado por:</strong> {{ $sale->repaired->short_name }}</p>
    @endif
@endif

@if($sale->image != null && $user->isA('super-admin', 'cashier-admin'))
    <img src="{{$sale->image->path}}" alt="{{$sale->image->name}}" style="width: 80%;">
@endif

<p>{{ $configuration->text_footer }}</p>
@if($sale->subsidiary->related_brand == 1)
    <p>SUGERENCIAS EN www.bocsiyowbarbershop.com.mx/sugerencias</p>
@endif

<div class="d-flex">
    <div class="mx-auto">
        <p style="text-align: center;">Utilice el siguiente código para solicitar factura</p>
        {!!QrCode::size(200)->generate(route('facturas.createCustomer', $sale->id))!!}
    </div>
</div>

<hr>
