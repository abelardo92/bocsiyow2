@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Solicitudes de facturas</div>

                    <div class="panel-body">
                        <a href="/solicitar-factura">Agregar solicitud</a>
                        <table class="table datatables">
                            <thead>
                                <tr>
                                    <th>Fecha de solicitud</th>
                                    <th>Fecha de ticket</th>
                                    <th>Ticket</th>
                                    <th>Sucursal</th>
                                    <th>Estatus</th>
                                    <th>Constancia</th>
                                    @if(Auth::user()->isA('super-admin'))
                                    <th>Acciones</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($factura_requests as $factura_request)
                                    @php
                                        $sale = $factura_request->sale;
                                        $subsidiary = $sale->subsidiary;
                                    @endphp
                                    <tr>
                                        <td>{{ $factura_request->created_at }}</td>
                                        <td>{{ $factura_request->folio_date }}</td>
                                        <td>
                                            @if($sale)
                                            <a href="{{route('sales.print.admin', [$subsidiary->key, $sale->id])}}" target="_blank">
                                                {{ $subsidiary->key }}-{{ $sale->folio }}
                                            </a>
                                            @endif
                                        </td>
                                        <td>
                                            @if($subsidiary)
                                                {{ $subsidiary->name }}
                                            @endif
                                        </td>
                                        <td>
                                            @php
                                                $status = $factura_request->status;
                                                switch ($status) {
                                                    case 'pendiente':
                                                        $status_class = 'label label-warning';
                                                        break;
                                                    case 'en proceso':
                                                        $status_class = 'label label-success';
                                                        break;
                                                    case 'finalizado':
                                                        $status_class = 'label label-danger';
                                                        break;
                                                    default:
                                                        $status_class = 'label label-warning';
                                                        break;
                                                }
                                            @endphp
                                            <span class="{{ $status_class }}">{{ strtoupper($status) }}</span>
                                        </td>
                                        <td>
                                            @if($factura_request->image)
                                                <a href="{{ $factura_request->image->path }}">Ver archivo</a>
                                            @endif
                                        </td>
                                        @if(Auth::user()->isA('super-admin'))
                                        <td class="row" style="width: 80px !important;">
                                            <div class="col-sm-6 index-edit-button-container">
                                                <a href="{{ route('facturas.edit', $factura_request->id) }}" class="btn btn-link index-edit-button">
                                                    <i class="fa fa-pencil-square-o fa-2x"></i>
                                                </a>
                                            </div>

                                            @if(Auth::user()->isA('super-admin'))
                                            <div class="col-sm-6 index-delete-button-container">
                                                <form action="{{route('facturas.destroy', $factura_request->id)}}" method="post" class="">
                                                    {{{csrf_field()}}}
                                                    {{{method_field('DELETE')}}}
                                                    <button type="submit" class="btn btn-link index-delete-button">
                                                        <i class="fa fa-times fa-2x"></i>
                                                    </button>
                                                </form>
                                            </div>
                                            @endif
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
