@extends('layouts.app_customers')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">

        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Solicitud de factura
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <div class="">
                        <div class="">
                            <form action="{{route('facturas.storeCustomer')}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                {{{ csrf_field() }}}
                                <input type="hidden" class="form-control" id="date" name="date" value="<?php echo date('Y-m-d')?>">

                                <div class="col-12">
                                    <h4 class="my-4"><b>Informacion del ticket a facturar</b></h4>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label">Folio de compra *</label>
                                    <div class="col-sm-4">
                                        <input type="text" placeholder="(Ej: 04-1234)" class="form-control" id="folio" name="folio" value="{{ $sale_folio }}">
                                        @include('errors.field',['field' => 'folio'])
                                    </div>
                                    <label for="entry_date" class="col-sm-2 control-label">Fecha del ticket *</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control datepicker" id="folio_date" name="folio_date" value="{{ old('folio_date') }}">
                                        @include('errors.field',['field' => 'folio_date'])
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label">Subtotal *</label>
                                    <div class="col-sm-4">
                                        <input type="number" class="form-control" id="subtotal" name="subtotal" value="{{ old('subtotal') }}">
                                        @include('errors.field',['field' => 'subtotal'])
                                    </div>
                                    <label for="payment_type" class="col-sm-2 control-label">Forma de pago *</label>
                                    <div class="col-sm-4">
                                        <select name="payment_type" id="payment_type" class="select2 form-control">
                                            <option value="efectivo" {{ old('payment_type') == 'efectivo' ? 'selected' : '' }}>Efectivo</option>
                                            <option value="tarjeta" {{ old('payment_type') == 'tarjeta' ? 'selected' : '' }}>Tarjeta</option>
                                            <option value="transferencia" {{ old('payment_type') == 'transferencia' ? 'selected' : '' }}>Transferencia</option>
                                        </select>
                                        @include('errors.field',['field' => 'payment_type'])
                                    </div>
                                </div>

                                <div class="col-12">
                                    <h4 class="my-4"><b>Direccion fiscal</b></h4>
                                </div>

                                <div class="form-group">
                                    <label for="entry_date" class="col-sm-2 control-label">Codigo Postal *</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="postal_code" name="postal_code" value="{{ old('postal_code') }}">
                                        @include('errors.field',['field' => 'postal_code'])
                                    </div>

                                    <label for="name" class="col-sm-2 control-label">Nombre de la vialidad *</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="road_name" name="road_name" value="{{ old('road_name') }}">
                                        @include('errors.field',['field' => 'road_name'])
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="entry_date" class="col-sm-2 control-label">Numero exterior *</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="external_number" name="external_number" value="{{ old('external_number') }}">
                                        @include('errors.field',['field' => 'external_number'])
                                    </div>

                                    <label for="name" class="col-sm-2 control-label">Numero interior</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="internal_number" name="internal_number" value="{{ old('internal_number') }}">
                                        @include('errors.field',['field' => 'internal_number'])
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="entry_date" class="col-sm-2 control-label">Nombre de la colonia *</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="suburb_name" name="suburb_name" value="{{ old('suburb_name') }}">
                                        @include('errors.field',['field' => 'suburb_name'])
                                    </div>

                                    <label for="name" class="col-sm-2 control-label">Nombre de la localidad *</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="locality_name" name="locality_name" value="{{ old('locality_name') }}">
                                        @include('errors.field',['field' => 'locality_name'])
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="entry_date" class="col-sm-2 control-label">Nombre del municipio *</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="municipality_name" name="municipality_name" value="{{ old('municipality_name') }}">
                                        @include('errors.field',['field' => 'municipality_name'])
                                    </div>

                                    <label for="name" class="col-sm-2 control-label">Nombre de la entidad federativa *</label>
                                    <div class="col-sm-4">
                                        <select name="state_id" id="state_id" class="select2 form-control">
                                            @foreach($states as $state)
                                                <option value="{{$state->id}}" {{ old('state_id') == $state->id ? 'selected' : '' }}>{{$state->name}}</option>
                                            @endforeach
                                        </select>
                                        @include('errors.field',['field' => 'state_id'])
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="entry_date" class="col-sm-2 control-label">Entre calle *</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="between_street_1" name="between_street_1" value="{{ old('between_street_1') }}">
                                        @include('errors.field',['field' => 'between_street_1'])
                                    </div>

                                    <label for="name" class="col-sm-2 control-label">Y calle *</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="between_street_2" name="between_street_2" value="{{ old('between_street_2') }}">
                                        @include('errors.field',['field' => 'between_street_2'])
                                    </div>
                                </div>

                                <div class="col-12">
                                    <h4 class="my-4"><b>Informacion para facturacion</b></h4>
                                </div>

                                <div class="form-group">
                                    <label for="entry_date" class="col-sm-2 control-label">RFC *</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="rfc" name="rfc" value="{{ old('rfc') }}">
                                        @include('errors.field',['field' => 'rfc'])
                                    </div>

                                    <label for="name" class="col-sm-2 control-label">Nombre completo *</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="full_name" name="full_name" value="{{ old('full_name') }}">
                                        @include('errors.field',['field' => 'full_name'])
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="entry_date" class="col-sm-2 control-label">Correo electronico *</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="factura_email" name="factura_email" value="{{ old('factura_email') }}">
                                        @include('errors.field',['field' => 'factura_email'])
                                    </div>

                                    <label for="name" class="col-sm-2 control-label">Telefono *</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
                                        @include('errors.field',['field' => 'phone'])
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="entry_date" class="col-sm-2 control-label">Uso del CFDI *</label>
                                    <div class="col-sm-4">
                                        <select name="cfdi_use_id" id="cfdi_use_id" class="select2 form-control">
                                            @foreach($cfdi_uses as $cfdi_use)
                                                <option value="{{$cfdi_use->id}}" {{ old('cfdi_use_id') == $cfdi_use->id ? 'selected' : '' }}>{{$cfdi_use->name}}</option>
                                            @endforeach
                                        </select>
                                        @include('errors.field',['field' => 'cfdi_use_id'])
                                    </div>

                                    <label for="name" class="col-sm-2 control-label">Regimen fiscal *</label>
                                    <div class="col-sm-4">
                                        <select name="tax_regime_id" id="tax_regime_id" class="select2 form-control">
                                            @foreach($tax_regimes as $tax_regime)
                                                <option value="{{$tax_regime->id}}" {{ old('tax_regime_id') == $tax_regime->id ? 'selected' : '' }}>{{$tax_regime->name}}</option>
                                            @endforeach
                                        </select>
                                        @include('errors.field',['field' => 'tax_regime_id'])
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label">Constancia de situacion fiscal *</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" type="file" id="image" name="image">
                                        @include('errors.field',['field' => 'image'])
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12 d-flex">
                                        <button type="submit" class="btn btn-primary ml-auto">Realizar solicitud de factura</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
