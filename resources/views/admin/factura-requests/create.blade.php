@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Realizar solicitud de factura
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <div class="">
                        <div class="">
                            <form action="{{route('facturas.store')}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                {{{ csrf_field() }}}
                                <div class="form-group">
                                    <label for="key" class="col-sm-4 control-label">Fecha del incidente *</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control datepicker" id="date" name="date" required="required" value="{{ old('date') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">Nombre *</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="name" name="name" required="required" value="{{ old('name') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">Teléfono</label>
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">Motivo *</label>
                                    <div class="col-sm-6">
                                        <textarea class="form-control" rows="4" id="reason" name="reason">{{ old('reason') }}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">Folio</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="folio" name="folio" value="{{ old('folio') }}">
                                    </div>
                                </div>

                                @if(Auth::user()->isA('super-admin'))
                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">Seguimiento</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="tracing" name="tracing" value="{{ old('tracing') }}">
                                    </div>
                                </div>
                                @endif
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-6">
                                        <button type="submit" class="btn btn-default">Guardar</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
