@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Preguntas
                    <p class="pull-right">
                        <a href="{{ route('questions.create') }}" class="btn btn-primary btn-xs">Agregar nueva</a>
                    </p>
                </div>

                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Texto</th>
                                <th>Sucursal</th>
                                {{-- <th>Orden</th> --}}
                                <th>Requerido</th>
                                <th>Activo</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($questions as $question)
                                <tr>
                                    <td>{{ $question->title }}</td>
                                    <td>{{ $question->subsidiary ? $question->subsidiary->name : ''}}</td>
                                    {{-- <td>{{ $question->orden }}</td> --}}
                                    <td><span class="label label-success">{{ $question->required ? 'Si' : 'No' }}</span></td>
                                    <td>
                                        <span class="label label-info">{{ $question->active ? 'Si' : 'No' }}</span>
                                    </td>
                                    <td><a href="{{ route('questions.edit', $question->id) }}">Editar</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $questions->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
