<div class="panel panel-default">
    <div class="panel-heading">
        {{$question->title}}
        @if(!$question->required) <small>(Opcional)</small> @endif
    </div>
    <div class="panel-body">
        <p><strong>{{$question->pivot->answer}}</strong></p>
    </div>
</div>