@extends('layouts.tickets_internos')

@section('content')
    @php 
        $subsidiary = $cashRegister->subsidiary()->get()->first();
    @endphp
    <p><strong>Fecha y hora:</strong> {{ \Carbon\Carbon::now()->format('d-m-Y h:i a')}}</p>
    <p><strong>Cajero:</strong> {{ $cashRegister->employee->short_name }}</p>
    <p><strong>Sucursal:</strong> {{ $subsidiary->name }}</p>
    <p><strong>Turno:</strong> {{ $cashRegister->turn->name }}</p>
    <p><strong>Tipo:</strong> Resumen de caja</p>
    <br>

    <h2>Mini cortes</h2>
    <table width="100%" border="1">
        @foreach($cashRegister->movements()->whereType('mini-corte')->get() as $mini_cut)
            <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>$ {{ $mini_cut->total }}</td>
            </tr>
        @endforeach
    </table>

    <h2>Corte de caja</h2>
    <table width="100%" border="1">
        <tr>
            <td>EFECTIVO</td>
            @php
                if($subsidiary->id != 11){
                    $sale_id = $cashRegister->sales()->notCanceled()->get()->pluck('id');
                    $cash_total = App\SalePayment::whereIn('sale_id', $sale_id)->efectivo()->mxn()->sum('total') - $cashRegister->sales()->notCanceled()->sum('money_change');
                } else {
                    $cash_total = $cashRegister->laundryServices()->notCanceled()->get()->sum('amount');
                }
                
            @endphp
            <td>{{ $cash_total }}</td>
        </tr>
        <tr>
            <td>MONEDERO</td>
            @php
                $sale_id = $cashRegister->sales()->notCanceled()->get()->pluck('id');
            @endphp
            <td>{{ App\SalePayment::whereIn('sale_id', $sale_id)->monedero()->sum('total') }}</td>
        </tr>
        <tr>
            <td>TARJETAS</td>
            <td>{{ App\SalePayment::whereIn('sale_id', $sale_id)->card()->sum('total') }}</td>
        </tr>
        <tr>
            <td>DLS</td>
            @php
                $sale_id = $cashRegister->sales()->notCanceled()->get()->pluck('id');
            @endphp
            <td>{{ App\SalePayment::whereIn('sale_id', $sale_id)->efectivo()->usd()->sum('total') / $exchange_rate->rate }}</td>
        </tr>
        <tr>
            @php
                $sale_ids = $cashRegister->sales()->notCanceled()->courtesy()->get()->pluck('id');
                $services_total = App\SaleService::whereIn('sale_id', $sale_ids)->sum('original_price');
                $products_total = App\SaleProduct::whereIn('sale_id', $sale_ids)->sum('original_price');
            @endphp
            <td>CORTESÍAS</td>
            <td>{{ $services_total + $products_total }}</td>      
        </tr>
        <tr>
            <td>T.C</td>
            <td>{{ $exchange_rate->rate }}</td>
        </tr>
        <tr>
            <td>Total</td>
            <td>{{ App\SalePayment::whereIn('sale_id', $sale_id)->efectivo()->usd()->sum('total') }}</td>
        </tr>
        <tr>
            <th>Total</th>
            @php
                if($subsidiary->id != 11) {
                    $sale_id = $cashRegister->sales()->notCanceled()->get()->pluck('id');
                    $total = App\SalePayment::whereIn('sale_id', $sale_id)->sum('total') - $cashRegister->sales()->notCanceled()->sum('money_change');
                } else {
                    $total = $cash_total;
                }
            @endphp
            <td>{{ $total }}</td>
        </tr>
    </table>

    <h2>Importe de servicios efectuados</h2>
    <table width="100%" border="0">
        <tr>
            <th>Puesto</th>
            <th>Nombre</th>
            <th>Importe</th>
            <th>Propina</th>
        </tr>
        <?php $stt = 0;?>
        @foreach($sales as $group_sales)
            <?php
if (!$employee = $group_sales->first()->employee) {
    $employee = \App\Employee::find(46);
}
$ptot = 0;
$pcount = 0;

$producto_groups = App\SaleProduct::whereIn('sale_id', $group_sales->pluck('id'))->get()->groupBy('product_id');
foreach ($producto_groups as $producto_group) {
    $pcount = $producto_group->sum('qty');
    $ptot += $producto_group->first()->price * $pcount;
    $stt += $producto_group->first()->price * $pcount;
}
?>
            <tr>
                <td>{{ $employee->job }}</td>
                <td>{{ $employee->short_name }}</td>
                <td>
                    {{ App\SalePayment::whereIn('sale_id', $group_sales->pluck('id'))->sum('total') - $group_sales->sum('tip') - $ptot - $group_sales->sum('money_change')}}
                    ---
                    {{-- {{App\SaleService::whereIn('sale_id', $group_sales->pluck('id'))->get()->count()}}
                    - --}}
                    {{App\SaleService::whereIn('sale_id', $group_sales->pluck('id'))->sum('qty')}}
                </td>
                <td>{{ $group_sales->sum('tip') }}</td>
            </tr>
        @endforeach

        <tr>
            <td></td>
            <td></td>
            <td>{{ App\SalePayment::whereIn('sale_id', $cashRegister->sales()->notCanceled()->pluck('id'))->sum('total') - $cashRegister->sales()->notCanceled()->sum('tip') - $stt - $cashRegister->sales()->notCanceled()->sum('money_change') }}</td>
            <td>{{ $cashRegister->sales()->notCanceled()->sum('tip') }}</td>
        </tr>
    </table>

    <h2>Venta de productos</h2>
    <table width="100%" border="0">
        @foreach($productos as $sale_producto)
            <tr>
                <td>{{ $sale_producto->sum('qty') }}</td>
                <td>{{ $sale_producto->first()->product->name }}</td>
                <td>{{ $sale_producto->sum('qty')}} *  {{$sale_producto->first()->price }}</td>
                <td>{{ $sale_producto->sum('qty') * $sale_producto->first()->price }}</td>
            </tr>
        @endforeach

        <tr>
            <td colspan="3">Total venta de productos</td>
            <td>{{ $productos->sum(function ($sale_producto) {
                    return $sale_producto->sum('qty') * $sale_producto->first()->price;
                }) }}</td>
        </tr>
    </table>

    @if($cashRegister->sales()->repaireds()->count() > 0 )
        <h2>Servicios reparados</h2>
        <table width="100%" border="0">
            <tr>
                <th>Hecho por</th>
                <th>Reparado por</th>
                <th>Folio</th>
                <th>Fecha</th>
                <th>Total</th>
            </tr>
            @foreach($cashRegister->sales()->repaireds()->get() as $sale)
                <tr>
                    <td>{{ $sale->employee->short_name }}</td>
                    <td>
                        @if($sale->repaired)
                        {{ $sale->repaired->short_name }}
                        @endif
                    </td>
                    <td>{{ $sale->subsidiary->key }} - {{ $sale->folio }}</td>
                    <td>{{ $sale->date }}</td>
                    <td>{{ $sale->total }}</td>
                </tr>
            @endforeach

            <tr>
                <td colspan="2">Total en reparaciones</td>
                <td>{{ $cashRegister->sales()->repaireds()->sum('total') }}</td>
            </tr>
        </table>
    @endif

    <div class="sign text-area">
        <h4>Firma</h4>
    </div>
@endsection