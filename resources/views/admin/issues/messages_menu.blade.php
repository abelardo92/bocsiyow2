<div class="row">
    <div class="col-sm-2">
        <a href="{{ route('issues.messages.create') }}" class="btn btn-success">Enviar mensaje</a>
    </div>
    <div class="col-sm-1">
        <a href="{{ route('issues.messages.sent') }}" class="btn btn-primary">Enviados</a>
    </div>
    <div class="col-sm-1">
        <a href="{{ route('issues.messages.received') }}" class="btn btn-primary">Recibidos</a>
    </div>
</div>  