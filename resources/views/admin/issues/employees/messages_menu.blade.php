<div class="row">
    <div class="col-sm-2">
        <a href="{{ route('issues.messages.employees.create') }}" class="btn btn-success">Enviar mensaje</a>
    </div>
    <div class="col-sm-1">
        <a href="{{ route('issues.messages.employees.sent') }}" class="btn btn-primary">Enviados</a>
    </div>
    <div class="col-sm-1">
        <a href="{{ route('issues.messages.employees.received') }}" class="btn btn-primary">Recibidos</a>
    </div>
</div>