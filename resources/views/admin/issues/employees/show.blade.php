@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Información de mensaje:
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>
                <?php 
                    $relatedIssue = $issue; 
                    //dd($relatedIssue);
                ?>
                <div class="panel-body">
                  @while($relatedIssue != null)
                      <div class="form-group">
                          <div class="row">
                              <div class="col-sm-3">
                                  <strong>Fecha:</strong> {{ $relatedIssue->created_at }}        
                              </div>
                              <div class="col-sm-3">
                                  <strong>Enviado por:</strong> 
                                  @if($relatedIssue->originUser != null)
                                      {{ $relatedIssue->originUser->name }}     
                                  @endif  
                              </div>
                              <div class="col-sm-3">
                                  <strong>De:</strong> 
                                  @if($relatedIssue->originSubsidiary != null)
                                      {{ $relatedIssue->originSubsidiary->name }}   
                                  @endif     
                              </div>
                              <div class="col-sm-3">
                                  <strong>Para:</strong> 
                                  @if($relatedIssue->destinySubsidiary != null)
                                      {{ $relatedIssue->destinySubsidiary->name }}   
                                  @endif       
                              </div>
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="row">
                              <div class="col-sm-2">
                                  <strong>Asunto:</strong>       
                              </div>
                              <div class="col-sm-10">
                                  {{ $relatedIssue->subject }}       
                              </div>
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="row">
                              <div class="col-sm-2">
                                  <strong>Mensaje:</strong>       
                              </div>
                              <div class="col-sm-10">
                                  {{ $relatedIssue->action }}       
                              </div>
                          </div>
                      </div>
                      @if($relatedIssue->images != null && !$relatedIssue->images->isEmpty())
                      <div class="form-group">
                          <div class="row">
                              <div class="col-sm-2">
                                  <strong>Archivo(s):</strong>       
                              </div>
                              <div class="col-sm-10">
                                  @foreach($relatedIssue->images as $image)
                                      <a href="{{ $image->path }}" target="_blank">{{ $image->name }}</a>
                                  @endforeach      
                              </div>
                          </div>
                      </div>
                      @endif

                      <?php $relatedIssue = $relatedIssue->relatedIssue()->with('originSubsidiary', 'destinySubsidiary','originUser.employee', 'images')->get()->first(); ?>
                      ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                  @endwhile
                  <div class="form-group">
                      @if($issue->origin_user_id != $user->id)
                      <div class="col-sm-offset-8 col-sm-2">
                          <button class="btn btn-primary" type="button" onclick="window.history.back()">Regresar</button>
                      </div>
                      <div class="col-sm-1">
                          <a class="btn btn-primary" href="{{ route('issues.messages.employees.createAnswer', $issue->id) }}">Responder</a>
                      </div>
                      @else
                      <div class="col-sm-offset-10 col-sm-2">
                          <button class="btn btn-primary" type="button" onclick="window.history.back()">Regresar</button>
                      </div>
                      @endif
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
