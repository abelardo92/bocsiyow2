@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Mensajes recibidos</div>

                    <div class="panel-body">
                    @include('admin.issues.messages_menu')
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    @if($user->isA('super-admin'))
                                    <th>Sucursal</th>
                                    @endif
                                    <th>Enviado por</th>
                                    <th>Asunto</th>
                                    <th>Fecha / hora</th>
                                    <th>Archivo</th>
                                    @if($user->isA('super-admin'))
                                        <th>Visto por</th>
                                    @endif
                                    <th>Ver</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($issues as $issue)
                                    <tr>
                                        @if($user->isA('super-admin'))
                                            <td>
                                                @if($issue->originSubsidiary != null)
                                                    {{ $issue->originSubsidiary->name }}
                                                @endif
                                            </td>
                                        @endif
                                        <td>
                                            @if($issue->originUser->employee != null)
                                                {{ $issue->originUser->employee->short_name }}
                                            @else
                                                {{ $issue->originUser->name }}
                                            @endif
                                        </td>
                                        <td>{{ $issue->subject }}</td>
                                        <td>{{ $issue->created_at }}</td>
                                        <td>
                                            @if($issue->images != null)
                                                @foreach($issue->images as $image)
                                                <a href="{{ $image->path }}" target="_blank" title="{{ $image->name }}"><i class="fa fa-image fa-2x"></i></a>
                                                @endforeach
                                            @endif
                                        </td>
                                        @if($user->isA('super-admin'))
                                            <td>
                                                @if($issue->openedBy != null)
                                                    {{ $issue->openedBy->name }}
                                                @endif
                                            </td>
                                        @endif
                                        <td>
                                            <a class="btn btn-link" href="{{ route('issues.messages.show', $issue->id) }}">Ver</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $issues->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection