@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Comidas
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{route('eats.update', [$subsidiary->key, $eat->id])}}" method="POST" class="form-horizontal">
                        {{{ csrf_field() }}}
                        {{{ method_field('PUT') }}}

                        <div class="form-group">
                            <label for="employee_id" class="col-sm-4 control-label">Barbero</label>
                            <div class="col-sm-6">
                                <select type="text" class="form-control" id="employee_id" name="employee_id">
                                    @foreach($barbers as $barber)
                                        <option value="{{$barber->id}}" @if($eat->employee_id == $barber->id) selected="selected" @endif>{{$barber->short_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        {{-- <div class="form-group">
                            <label for="date" class="col-sm-4 control-label">Fecha</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control datepicker" id="date" name="date" value="{{ old('date') }}">
                            </div>
                        </div> --}}

                        <div class="form-group">
                            <label for="start" class="col-sm-4 control-label">Hora Inicio</label>
                            <div class="col-sm-6">
                              <input type="time" class="form-control" id="start" name="start" value="{{ old('start', $eat->start) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="end" class="col-sm-4 control-label">Hora final</label>
                            <div class="col-sm-6">
                              <input type="time" class="form-control" id="end" name="end" value="{{ old('end', $eat->end) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-6">
                                <button type="submit" class="btn btn-default">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
