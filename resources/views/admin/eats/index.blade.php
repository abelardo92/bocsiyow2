@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Comidas del dia
                    <div class="pull-right">
                        <a href="{{route('eats.create', $current_subsidiary->key)}}">Agregar comida</a>
                    </div>
                </div>

                <div class="panel-body">
                    {{-- <form action="{{route('eats.index', $current_subsidiary->key)}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="start">Fecha: </label>
                            <input type="text" class="form-control datepicker" name="start" value="{{$start}}">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form> --}}
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Barbero</th>
                                <th>Fecha</th>
                                <th>Inicio</th>
                                <th>Fin</th>
                                <th>Creado por</th>
                                <th>Creacion</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($eats as $eat)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $eat->employee->short_name }}</td>
                                    <td>{{ $eat->date }}</td>
                                    <td>{{ $eat->start }}</td>
                                    <td>{{ $eat->end }}</td>
                                    @if($eat->user)
                                        <td>{{ $eat->user->name }}</td>
                                    @elseif($eat->cashier)
                                        <td>{{ $eat->cashier->short_name }}</td>
                                    @else
                                        <td></td>
                                    @endif
                                    <td>{{ $eat->created_at->format('h:i a') }}</td>
                                    <td>
                                        <a href="{{route('eats.edit', [$current_subsidiary->key, $eat->id])}}" class="btn btn-info btn-xs">Editar</a>
                                        <form action="{{route('eats.destroy', [$current_subsidiary->key, $eat->id])}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <input type="submit" class="btn btn-danger btn-xs" value="Eliminar">
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
