@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Salidas
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <div class="">
                        <div class="">
                            <form action="{{route('inventories.activos.insumos.post')}}" method="POST" class="form "v-on:submit.prevent>
                                {{{ csrf_field() }}}

                                <input type="hidden" value="{{$type}}" name="type">
                                <input type="hidden" value="{{$kardex_type}}" name="kardex_type">

                                <div class="form-group">
                                    <label for="subsidiary_id">Sucursal</label>
                                    <select name="subsidiary_id" id="subsidiary_id" class="form-control" required="required">
                                        @foreach($subsidiaries as $subsidiary)
                                            <option value="{{$subsidiary->id}}">{{$subsidiary->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div>
                                    <label for="concepto">Concepto</label>
                                    <input name="concepto" id="concepto" class="form-control" required="required">
                                </div>

                                <entradas-articles></entradas-articles>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
