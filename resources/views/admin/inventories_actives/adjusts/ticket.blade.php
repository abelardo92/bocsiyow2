@extends('layouts.tickets')

@section('title')
    {{$title}}
@endsection

@section('subsidiary')
    {{$types->first()->subsidiary->name}}
@endsection

@section('content')
    <p><strong>Folio:</strong> {{$types->first()->folio}}</p>
    <p><strong>Fecha:</strong> {{$types->first()->created_at->format('d-m-Y') }}</p>
    <p><strong>Hora:</strong> {{$types->first()->created_at->format('H:i a') }}</p>
    {{-- <p><strong>Ajusto:</strong> {{$types->first()->user->name}}</p> --}}

    <table width="100%">
        <tr>
            <th>Codigo</th>
            <th colspan="2">Producto</th>
        </tr>
        <tr>
            <th>Existencia</th>
            <th>Ajuste</th>
            <th>Real</th>
        </tr>
        @foreach($types as $adjustment)
            <tr>
                <td align="center">{{$adjustment->article->key}}</td>
                <td align="center" colspan="2">{{$adjustment->article->name}}</td>
            </tr>
            <tr>
                <td align="center">{{$adjustment->current_existence}}</td>
                <td align="center">{{$adjustment->qty}}</td>
                <td align="center">{{$adjustment->real_existence}}</td>
            </tr>
        @endforeach
    </table>

    {{-- <div class="sign text-area">
        @if($types->first()->employee)
            <p>{{$types->first()->employee->name}}</p>
        @else
            <p>Firma</p>
        @endif
    </div> --}}
@endsection