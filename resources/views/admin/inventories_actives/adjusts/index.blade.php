<div class="panel panel-default">
    <div class="panel-heading">
        Ajustes
        <p class="pull-right">
            <a href="{{ route('inventories.activos.insumos.create', ["type" => $type, 'kardex_type' => 'ajuste']) }}">Nueva ajuste</a>
        </p>
    </div>

    <div class="panel-body">
        <table class="table datatables">
            <thead>
                <tr>
                    <th>Folio</th>
                    <th>Articulo</th>
                    <th>Existencia actual</th>
                    <th>Ajuste</th>
                    <th>Existencia real</th>
                    <th>Sucursal</th>
                    <th>Fecha</th>
                </tr>
            </thead>
            <tbody>
                @foreach($adjusts as $inventory)
                    <tr>
                        <td>{{$inventory->folio}}</td>
                        <td>{{$inventory->article->key}}</td>
                        <td>{{$inventory->current_existence}}</td>
                        <td>{{$inventory->qty}}</td>
                        <td>{{$inventory->real_existence}}</td>
                        <td>{{$inventory->subsidiary->name}}</td>
                        <td>{{$inventory->created_at->format('d-m-Y')}}</td>
                        <td>
                            <button type="button" onclick="window.open('/home/inventories/activos-insumos/ajuste/{{$inventory->folio}}/{{$type}}', '_blank')" class="btn btn-link">Imprimir nota</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>