@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Menu</div>

                <div class="panel-body">
                    <ul class="list-unstyled">
                        <li><a href="{{route('articles.index', ["type" => 'insumo'])}}">Insumos</a></li>
                        <li><a href="{{route('articles.index', ["type" => 'activo'])}}">Activos</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            @include('admin.inventories_actives.entries.index')
            @include('admin.inventories_actives.outs.index')
            @include('admin.inventories_actives.adjusts.index')
        </div>
    </div>
</div>
@endsection
