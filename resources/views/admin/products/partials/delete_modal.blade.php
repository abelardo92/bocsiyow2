<div class="modal fade" id="modal-delete-{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-delete-{{$product->id}}Label">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="modal-delete-{{$product->id}}Label">Baja del producto</h4>
        </div>
        <div class="modal-body">
          <form action="{{route('products.destroy', $product->id)}}" method="post">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <div class="form-group">
              <h4>Desea eliminar el producto "{{$product->name}}"?</h4>
            </div>
            <div class="form-group">
              <input type="submit" class="btn btn-danger" value="Dar de baja">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>