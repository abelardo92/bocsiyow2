@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Empleados
                    @if(Auth::user()->isA('super-admin', 'horarios', 'subsidiary-admin', 'cashier-admin', 'manager'))
                        <div class="pull-right">
                            <a href="{{route('employees.create')}}">Agregar empleado</a>
                        </div>
                    @endif
                </div>

                <div class="panel-body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#actives" aria-controls="actives" role="tab" data-toggle="tab">Activos</a></li>
                        @if(!Auth::user()->isA('subsidiary-admin'))
                            <li role="presentation"><a href="#inactives" aria-controls="inactives" role="tab" data-toggle="tab">Inactivos</a></li>
                        @endif
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="actives">
                            <table class="table table-striped datatables">
                                <thead>
                                    <tr>
                                        <th>Numero</th>
                                        <th>Nombre corto</th>
                                        <th>Nombre completo</th>
                                        @if(Auth::user()->isA('super-admin'))
                                            <th>Puesto</th>
                                            <th>Telefono</th>
                                            <th>Salario</th>
                                        @endif
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($employees as $employee)
                                        <tr>
                                            <td>{{ $employee->key }}</td>
                                            <td>{{ $employee->short_name }}</td>
                                            <td>{{ $employee->name }}</td>
                                            @if(Auth::user()->isA('super-admin'))
                                                <td>{{ $employee->job }}</td>
                                                <td>{{ $employee->phone }}</td>
                                                <td>{{ $employee->salary }}</td>
                                            @endif
                                            <td>
                                                <a href="{{ route('employees.edit', $employee->id) }}">Editar</a>
                                                @if(Auth::user()->isA('super-admin'))
                                                    |
                                                    <a href="#" data-toggle="modal" data-target="#modal-delete-{{$employee->id}}">
                                                        Dar de baja
                                                    </a>
                                                    @include('admin.employees.partials.delete_modal', compact('employee'))
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="inactives">
                            <table class="table table-striped datatables">
                                <thead>
                                    <tr>
                                        <th>Numero</th>
                                        <th>Nombre corto</th>
                                        <th>Nombre completo</th>
                                        @if(Auth::user()->isA('super-admin'))
                                            <th>Puesto</th>
                                            <th>Telefono</th>
                                            <th>Salario</th>
                                        @endif
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($inactive_employees as $employee)
                                        <tr>
                                            <td>{{ $employee->key }}</td>
                                            <td>{{ $employee->short_name }}</td>
                                            <td>{{ $employee->name }}</td>
                                            @if(Auth::user()->isA('super-admin'))
                                                <td>{{ $employee->job }}</td>
                                                <td>{{ $employee->phone }}</td>
                                                <td>{{ $employee->salary }}</td>
                                            @endif
                                            <td>
                                                <a href="{{ route('employees.edit', $employee->id) }}">Editar</a>
                                                @if(Auth::user()->isA('super-admin'))
                                                    |
                                                    <a href="#" onclick="document.getElementById('form-active-{{$employee->id}}').submit()">
                                                        Dar de alta
                                                    </a>
                                                    <form action="{{route('employees.active', $employee->id)}}" method="post" id="form-active-{{$employee->id}}">
                                                      {{ csrf_field() }}
                                                      {{ method_field('PUT') }}
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
