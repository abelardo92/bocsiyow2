@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Incapacidad del empleado: <strong>{{$employee->name}}</strong>

                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{ route('employees.disabilities.store', $employee_id) }}" method="post" autocomplete="off" enctype="multipart/form-data">
                      {{{ csrf_field() }}}

                    <div class="form-group">
                      <label for="image">Archivo de evidencia</label>
                      <input type="file" name="image" id="image" class="form-control">
                    </div>

                      <div class="form-group">
                        <label for="" class="control-label">Periodo de incapacidad *</label>
                        <div class="input-group input-daterange">
                            <input type="text" class="form-control" name="start_date" value="">
                            <span class="input-group-addon">al</span>
                            <input type="text" class="form-control" name="end_date" value="">
                        </div>
                      </div>

                      <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Asignar">
                         <a href="{{route('employees.edit', $employee_id)}}" class="btn btn-danger">Cancelar</a>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
