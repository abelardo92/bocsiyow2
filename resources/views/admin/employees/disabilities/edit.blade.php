@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Incapacidad del empleado: <strong>{{$employee->name}}</strong>
                        <p class="pull-right help-block">Campos con * son obligatorios.</p>
                    </div>

                    <div class="panel-body">
                        <form action="{{ route('employees.disabilities.update', [$employee_id, $disability->id ]) }}" method="post" autocomplete="off" enctype="multipart/form-data">
                          {{{ csrf_field() }}}
                          {{{ method_field('PUT') }}}

                          <div class="form-group">
                            <label for="image">Archivo de evidencia</label>
                            <input type="file" name="image" id="image" class="form-control">
                          </div>
                          <div class="form-group">
                            <label for="" class="control-label">Periodo de incapacidad *</label>
                            <div class="input-group input-daterange">
                              <input type="text" class="form-control" name="start_date" value="{{$disability->start_date}}">
                              <span class="input-group-addon">al</span>
                              <input type="text" class="form-control" name="end_date" value="{{$disability->end_date}}">
                            </div>
                          </div>

                          <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Asignar">
                            <a href="{{route('employees.edit', $employee_id)}}" class="btn btn-danger">Cancelar</a>
                            <a href="{{route('employees.disabilities.create', $employee_id)}}" class="btn btn-default">Crear otro</a>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
