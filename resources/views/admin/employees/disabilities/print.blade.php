@extends('layouts.tickets_internos')

@section('content')
    <p><strong>Nombre:</strong> {{ $employee->name }}</p>
    <p><strong>Fecha inicial:</strong> {{ $start}}</p>
    <p><strong>Fecha Final:</strong> {{ $end }}</p>

    <table width="100%">
        <tr>
            <th>Dia</th>
            <th>Turno</th>
            <th>Sucursal</th>
        </tr>
        @foreach($schedules as $schedule)
            @if(is_string($schedule))
                <tr>
                    <td colspan="3" align="center">
                        No hay horarios para el dia: {{ trans("dates.{$schedule}") }}
                    </td>
                </tr>
            @else
                <tr>
                    <td align="center">
                        {{ trans("dates.{$schedule->day}") }}
                    </td>
                    <td align="center">
                        {{ $schedule->turn->name }}
                    </td>
                    <td align="center">
                        {{ $schedule->subsidiary->name }}
                    </td>
                </tr>
            @endif
        @endforeach
    </table>

@endsection