<div class="panel panel-default">
    <div class="panel-heading">
        Horarios 
        @if(Auth::user()->isA('super-admin', 'horarios', 'cashier-admin', 'subsidiary-admin'))
            <a href="{{route('employees.schedules.create', $employee->id)}}">Crear nuevo horario</a>
        @endif
    </div>

    <div class="panel-body">
        <table class="table table-striped datatables">
            <thead>
                <tr>
                    <th>Periodo</th>
                    <th>Dia</th>
                    <th>Turno</th>
                    <th>Sucursal</th>
                    <th>Hora</th>
                    <th>Descanso</th>
                    @if(Auth::user()->isA('super-admin', 'horarios', 'supervisor', 'cashier-admin', 'subsidiary-admin'))
                    <th>Opciones</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @if(Auth::user()->isA('supervisor'))
                    <?php $schedules = $employee->schedules()->today()->get(); ?> 
                @else
                    <?php $schedules = $employee->schedules()->fromThisWeek()->orderBy('date', 'asc')->get(); ?>
                @endif
                @foreach($schedules as $schedule)
                    <tr>
                        <td>{{ $schedule->date }}</td>
                        <td>{{ trans("dates.{$schedule->day}") }}</td>
                        <td>{{ $schedule->turn->name }}</td>
                        <td>{{ $schedule->subsidiary->name }}</td>
                        <td>
                            {{ $schedule->turn->start }}
                            @if(Auth::user()->isA('super-admin', 'horarios')) - {{ $schedule->turn->end }} @endif
                        </td>
                        <td>{{ $schedule->turn->is_rest ? 'Si' : 'No' }}</td>
                        @if(Auth::user()->isA('super-admin', 'horarios', 'supervisor', 'cashier-admin', 'subsidiary-admin'))
                        <td>
                            <a href="{{route('employees.schedules.edit', [$employee->id, $schedule->id])}}" class="btn btn-link">Editar</a>
                            <form action="{{route('employees.schedules.destroy', [$employee->id, $schedule->id])}}" method="post">
                                {{{csrf_field()}}}
                                {{{method_field('DELETE')}}}
                                <input type="submit" class="btn btn-link" value="Eliminar">
                            </form>
                        </td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>

        <button class="btn btn-info btn-sm" type="button" onclick="window.history.back()">regresar</button>
    </div>
</div>
