<div class="panel panel-default">
    <div class="panel-heading">
        Intercambios de horarios
        <a href="{{route('employees.exchanges.create', $employee->id)}}">Crear nueva solicitud</a>
    </div>

    <div class="panel-body">
        <table class="table table-striped datatables">
            <thead>
                <tr>
                    <th>Fecha</th>
                    <th>Solicita</th>
                    <th>Intercambia con</th>
                    <th>Auth empleado</th>
                    <th>Auth Admin</th>
                    <th>F. Creación</th>
                    <th>Opciones</th>            
                </tr>
            </thead>
            <tbody>
                @foreach($employee->currentExchanges()->with(['exchanges.employee','exchanges.employee2'])->get() as $exchange)
                    <tr>
                        <td>{{ $exchange->date }}</td>
                        <td>{{ $exchange->employee->short_name }}</td>
                        <td>{{ $exchange->employee2->short_name }}</td>
                        <td>
                            @if($exchange->employee2_authorized)
                                {{ $exchange->employee2_authorized ? 'Si' : 'No' }}
                            @else
                                Pendiente
                            @endif
                        </td>
                        <td>
                            @if($exchange->admin_authorized)
                                {{ $exchange->admin_authorized ? 'Si' : 'No' }}
                            @else
                                Pendiente
                            @endif
                        </td>
                        <td>{{ $exchange->created_at }}</td>
                        <td>
                            @if($exchange->employee_id == $employee->id)
                                @if(!$exchange->admin_authorized && !$exchange->employee2_authorized)
                                <a href="{{route('employees.exchanges.edit', [$employee->id, $exchange->id])}}" class="btn btn-link">Editar</a>
                                <form action="{{route('employees.exchanges.destroy', [$employee->id, $exchange->id])}}" method="post">
                                    {{{csrf_field()}}}
                                    {{{method_field('DELETE')}}}
                                    <input type="submit" class="btn btn-link" value="Eliminar">
                                </form>
                                @endif
                            @else
                                @if(!$exchange->isAuthorizedBy($employee->id))  
                                    <form action="{{route('employees.exchanges.authorizee', $exchange->id)}}" method="post">
                                    {{{csrf_field()}}}
                                    <input type="hidden" name="employee_id" value="{{$employee->id}}"/>
                                    <input type="submit" class="btn btn-link" value="Autorizar">
                                    </form>
                                    <form action="{{route('employees.exchanges.unauthorize', $exchange->id)}}" method="post">
                                    {{{csrf_field()}}}
                                    <input type="hidden" name="employee_id" value="{{$employee->id}}"/>
                                    <input type="submit" class="btn btn-link" value="Rechazar">
                                    </form>     
                                @endif
                            @endif
                            @if(count($exchange->exchanges) > 0)
                                @php
                                    $schedules1 = $exchange->getSchedules1();
                                    $schedules2 = $exchange->getSchedules2();
                                    $changedSchedules1 = $exchange->getChangedSchedules1($schedules1, $schedules2);
                                    $changedSchedules2 = $exchange->getChangedSchedules2($changedSchedules1, $schedules2);
                                    $changedSchedules1[0]->load('employee');
                                @endphp
                                <show-exchange-children 
                                    :exchange="{{ $exchange->toJson() }}"
                                    :schedules="{{ $exchange->getAllSchedules()->toJson() }}"
                                    :schedules1="{{ $changedSchedules1->toJson() }}"
                                    :schedules2="{{ $changedSchedules2->toJson() }}"
                                    ></show-exchange-children>
                            @endif
                        </td>
                        
                    </tr>
                @endforeach
            </tbody>
        </table>

        <button class="btn btn-info btn-sm" type="button" onclick="window.history.back()">regresar</button>
    </div>
</div>
