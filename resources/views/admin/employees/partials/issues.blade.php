<div class="panel panel-default">
    <div class="panel-heading">
        Incidencias 
        @if(Auth::user()->isA('super-admin', 'cashier-admin', 'subsidiary-admin'))
            <a href="{{route('employees.issues.create', $employee->id)}}">Crear nueva incidencia</a>
        @endif
    </div>

    <div class="panel-body">
        <table class="table table-striped datatable-issues">
            <thead>
                <tr>
                    <th>Faltas incurridas</th>
                    <th>Observaciones</th>
                    <th>Fecha</th>
                    <th>Archivo(s)</th>
                    <th>Creación</th>
                    @if(Auth::user()->isA('super-admin', 'cashier'))
                        <th>Opciones</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach($employee->issues()->orderBy('date', 'desc')->get() as $issue)
                    <tr>
                        <td>{{ $issue->area }}</td>
                        <td>{{ $issue->action }}</td>
                        <td>{{ $issue->date }}</td>
                        <td>
                            @if($images = $issue->images()->get())
                                @foreach($images as $image)
                                    <a href="{{ $image->path }}" target="_blank">{{ $image->name }}</a>
                                @endforeach
                            @endif
                        </td>
                        <td>{{ $issue->created_at->format('Y-m-d') }}</td>
                        @if(Auth::user()->isA('super-admin', 'cashier_admin'))
                        <td>
                            <a href="{{route('employees.issues.edit', [$employee->id, $issue->id])}}" class="btn btn-link">Editar</a>
                            @if(Auth::user()->isA('super-admin'))
                                <form action="{{route('employees.issues.destroy', [$employee->id, $issue->id])}}" method="post">
                                    {{{csrf_field()}}}
                                    {{{method_field('DELETE')}}}
                                    <input type="submit" class="btn btn-link" value="Eliminar">
                                </form>
                            @endif
                        </td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
        @if(Auth::user()->isA('super-admin'))
            <button class="btn btn-info btn-sm" type="button" onclick="window.history.back()">regresar</button>
        @endif
    </div>
</div>
