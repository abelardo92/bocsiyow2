<div class="panel panel-default">
    <div class="panel-heading">
        Incidencias 
    </div>

    <div class="panel-body">
        @php
            $start_issue = request('start_issue', Carbon\Carbon::now()->subDays(14)->format('Y-m-d'));
            $end_issue = request('end_issue', Carbon\Carbon::now()->format('Y-m-d'));
        @endphp
        <form action="" method="post">
            {{ csrf_field() }}
            {{ method_field('GET') }}
            <div class="form-group">
                <label for="start_issue">Fecha de: </label>
                <div class="input-group input-daterange">
                    <input type="text" class="form-control" name="start_issue" value="{{$start_issue}}" disabled>
                    <span class="input-group-addon">al</span>
                    <input type="text" class="form-control" name="end_issue" value="{{$end_issue}}" disabled>
                </div>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
            </div>
        </form>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Faltas incurridas</th>
                    <th>Observaciones</th>
                    <th>Fecha</th>
                    <th>Archivo(s)</th>
                    <th>Creacion</th>
                </tr>
            </thead>
            <tbody>
                @foreach($employee->issues()->dates([$start_issue, $end_issue])->orderBy('date', 'desc')->get() as $issue)
                    <tr>
                        <td>{{ $issue->area }}</td>
                        <td>{{ $issue->action }}</td>
                        <td>{{ $issue->date }}</td>
                        <td>
                            @if($images = $issue->images()->get())
                                @foreach($images as $image)
                                    <a href="{{ $image->path }}" target="_blank">{{ $image->name }}</a>
                                @endforeach
                            @endif
                        </td>
                        <td>{{ $issue->created_at->format('Y-m-d') }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
