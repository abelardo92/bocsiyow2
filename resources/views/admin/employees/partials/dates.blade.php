<div class="panel panel-default">
    <div class="panel-heading">
        Citas 
    </div>

    <div class="panel-body">
        <table class="table table-striped datatables">
            <thead>
                <tr>
                    <th>Sucursal</th>
                    <th>Fecha</th>
                    <th>Hora</th>
                </tr>
            </thead>
            <tbody>
                @foreach($employee->diaries()->fromNow()->orderBy('date', 'asc')->get() as $diary)
                    <tr>
                        <td>{{ $diary->subsidiary->name }}</td>
                        <td>{{ $diary->date }}</td>
                        <td>{{ $diary->time }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @if(Auth::user()->isA('super-admin'))
            <button class="btn btn-info btn-sm" type="button" onclick="window.history.back()">regresar</button>
        @endif
    </div>
</div>
