<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>
        html, body {
            max-width: 100%;
            /*margin: 0 auto;*/
            padding: 0px;
            float: left;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            font-style: normal;
            line-height: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            color: #000;
        }
        table th {
            border-bottom: 1px solid black;
        }

        .text-center{
            text-align: center;
        }

        .sign {
            width: 50%;
            margin: 100px auto 0;
            display: block;
        }
        .sign p {
            border-top: 1px solid black;
            width: 100%;
            text-align: center;
        }

        @media print {
            @page { 
                size: auto;
                margin: 1.6cm; 
            }
            .page-break { 
                display: block; 
                page-break-after: avoid; 
                page-break-before: avoid; 
                page-break-inside: avoid;
            }
            table {float: none !important; }
            div { float: none !important; }
            .dont-print { display: none; } 
           /* div {
     page-break-after: always !important;
     page-break-inside: avoid !important;
        }*/
        }
    </style>

</head>
    <body>

        <div>
            <h3>Contrato</h3>
            <p style="text-align: justify;">
                {!! nl2br($contrato) !!}
            </p>
            <!--<p style='overflow:hidden;page-break-before:always;'></p>-->
        </div>

        <div class="page-break">
            <h3>Manual</h3>
            @foreach($manuals as $manual)
                <p style="text-align: justify;">
                    {!! nl2br($manual->text) !!}
                </p>
            @endforeach
            <!--<p style='overflow:hidden;page-break-before:always;'></p>-->
        </div>
        
        @if(count($reglamento) > 0)
            <div class="page-break">
                <h3>Reglamento</h3>
                @foreach($reglamento as $manual)
                    <p style="text-align: justify;">
                        {!! nl2br($manual->text) !!}
                    </p>
                @endforeach
               <!-- <p style='overflow:hidden;page-break-before:always;'></p>-->
            </div>
        @endif

        <script>
            (function () {
                window.print();
            })();
        </script>
    </body>
</html>