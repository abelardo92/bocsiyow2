@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Depositos
                    <div class="pull-right">
                        <a href="{{route('deposits.create')}}">Agregar deposito</a>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped ">
                        <thead>
                            <tr>
                                <th>Empleado</th>
                                <th>Nombre corto</th>
                                <th>Periodo</th>
                                <th>Cantidad</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $total = 0;
                            @endphp
                            @foreach($deposits->sortBy('employee.short_name') as $deposit)
                                <tr>
                                    <td>{{ $deposit->employee->name }}</td>
                                    <td>{{ $deposit->employee->short_name }}</td>
                                    <td>{{ $deposit->start }}-{{ $deposit->end }}</td>
                                    @php
                                        $total += $deposit->amount;
                                    @endphp
                                    <td>{{ $deposit->amount }}</td>
                                    <td><a href="{{route('deposits.edit', $deposit->id)}}" class="btn btn-info btn-xs">Editar</a></td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="3">Total</td>
                                <td>{{$total}}</td>
                            </tr>
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
