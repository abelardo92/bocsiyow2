@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Depositos
                </div>

                <div class="panel-body">
                    <form action="{{route('deposits.store')}}" method="post">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="">
                            </div>
                        </div>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Empleado</th>
                                    <th>Nombre corto</th>
                                    <th>Cantidad depositada</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($employees as $employee)
                                    <input type="hidden" class="form-control" name="id[{{$employee->id}}]" value="{{$employee->id}}">
                                    <tr>
                                        <td>{{$employee->name}}</td>
                                        <td>{{$employee->short_name}}</td>
                                        <td><input type="text" class="form-control" name="amount[{{$employee->id}}]" value=""></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Guardar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
