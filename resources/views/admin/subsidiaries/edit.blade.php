@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    @if($subsidiary->image)
                        <img src="{{$subsidiary->image->path}}" alt="{{$subsidiary->image->name}}" class="img-square img-responsive">
                    @endif
                </div>
            </div>

            {{-- <div class="panel panel-default">
                <div class="panel-body">
                    <ul class="list-unstyled">
                        @include('admin.subsidiaries.partials.users_modal')

                        @include('admin.subsidiaries.partials.employees_modal')
                    </ul>
                </div>
            </div> --}}
        </div>
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Sucursales
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <div class="">
                        <div class="">
                            <form action="{{route('subsidiaries.update', $subsidiary->id)}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                {{{ csrf_field() }}}
                                {{{ method_field('PUT') }}}
                                <div class="form-group">
                                    <label for="key" class="col-sm-4 control-label">Código *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="key" name="key" required="required" value="{{ old('key', $subsidiary->key) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">Nombre *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="name" name="name" required="required" value="{{ old('name', $subsidiary->name) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="link" class="col-sm-4 control-label">Link acceso *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="link" name="link" disabled="disabled" value="{{ route('home.sucursal', $subsidiary->key) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="razon_social" class="col-sm-4 control-label">Razón social *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="razon_social" name="razon_social" required="required" value="{{ old('razon_social', $subsidiary->razon_social) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="rfc" class="col-sm-4 control-label">RFC *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="rfc" name="rfc" required="required" value="{{ old('rfc', $subsidiary->rfc) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="image" class="col-sm-4 control-label">Logotipo</label>
                                    <div class="col-sm-6">
                                      <input type="file" class="form-control" id="image" name="image">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="address" class="col-sm-4 control-label">Dirección *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="address" name="address" required="required" value="{{ old('address', $subsidiary->address) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="imprest" class="col-sm-4 control-label">Fondo fijo *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="imprest" name="imprest" required="required" value="{{ old('imprest', $subsidiary->imprest) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="chairs_number" class="col-sm-4 control-label">Numero de sillas</label>
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control" id="chairs_number" name="chairs_number" required="required" value="{{ old('chairs_number', $subsidiary->chairs_number) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="show_for_customer_diaries" class="col-sm-4 control-label">Mostrar en citas (Clientes)?</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" id="show_for_customer_diaries" name="show_for_customer_diaries">
                                        <option @if($subsidiary->show_for_customer_diaries) selected="selected" @endif value="1">SI</option>
                                        <option @if(!$subsidiary->show_for_customer_diaries) selected="selected" @endif value="0">NO</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="diaries_limit" class="col-sm-4 control-label">Citas permitidas por hora</label>
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control" id="diaries_limit" name="diaries_limit" required="required" value="{{ old('diaries_limit', $subsidiary->diaries_limit) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="chairs_number" class="col-sm-4 control-label">Estatus</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" id="is_active" name="is_active">
                                        <option @if($subsidiary->is_active) selected="selected" @endif value="1">Activo</option>
                                        <option @if(!$subsidiary->is_active) selected="selected" @endif value="0">Inactivo</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="configuration_id" class="col-sm-4 control-label">Configuración</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" id="configuration_id" name="configuration_id" required="required">
                                            <option value="">Seleccione uno por favor...</option>
                                                @foreach($configurations as $configuration)
                                                    <option @if($configuration->id == $subsidiary->configuration_id) selected="selected" @endif value="{{$configuration->id}}">{{$configuration->razon_social}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="timezone" class="col-sm-4 control-label">Zona Horaria</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" id="timezone" name="timezone">
                                            <option value="America/Merida" @if($subsidiary->timezone == "America/Merida") selected @endif >MERIDA</option>
                                            <option value="America/Cancun" @if($subsidiary->timezone == "America/Cancun") selected @endif >CANCUN</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="residence_place" class="col-sm-4 control-label">Asociado a:</label>
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="related_brand" value="1" @if($subsidiary->related_brand == 1) checked @endif> Bocsiyow
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="related_brand" value="2" @if($subsidiary->related_brand == 2) checked @endif > Sevilla
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                @if(Auth::user()->isA('super-admin', 'manager'))
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-6">
                                        <button type="submit" class="btn btn-default">Guardar</button>
                                    </div>
                                </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            @include('admin.subsidiaries.partials.turns')

            @include('admin.subsidiaries.partials.employees')
        </div>
    </div>
</div>
@endsection
