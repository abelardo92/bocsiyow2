@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">

        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Sucursales
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <div class="">
                        <div class="">
                            <form action="{{route('subsidiaries.store')}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                {{{ csrf_field() }}}
                                <div class="form-group">
                                    <label for="key" class="col-sm-4 control-label">Codigo *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="key" name="key" required="required" value="{{ old('key') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">Nombre *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="name" name="name" required="required" value="{{ old('name') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="razon_social" class="col-sm-4 control-label">Razón social *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="razon_social" name="razon_social" required="required" value="{{ old('razon_social') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="rfc" class="col-sm-4 control-label">RFC *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="rfc" name="rfc" required="required" value="{{ old('rfc') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="image" class="col-sm-4 control-label">Logotipo</label>
                                    <div class="col-sm-6">
                                      <input type="file" class="form-control" id="image" name="image">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="address" class="col-sm-4 control-label">Dirección *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="address" name="address" required="required" value="{{ old('address') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="imprest" class="col-sm-4 control-label">Fondo fijo *</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="imprest" name="imprest" required="required" value="{{ old('imprest') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="chairs_number" class="col-sm-4 control-label">Numero de sillas</label>
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control" id="chairs_number" name="chairs_number" required="required" value="{{ old('chairs_number') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="show_for_customer_diaries" class="col-sm-4 control-label">Mostrar en citas (Clientes)?</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" id="show_for_customer_diaries" name="show_for_customer_diaries">
                                        <option value="1">SI</option>
                                        <option value="0">NO</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="diaries_limit" class="col-sm-4 control-label">Citas permitidas por hora</label>
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control" id="diaries_limit" name="diaries_limit" required="required" value="{{ old('chairs_number') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="configuration_id" class="col-sm-4 control-label">Configuración</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" id="configuration_id" name="configuration_id" required="required">
                                            <option value="">Seleccione uno por favor...</option>
                                            @foreach($configurations as $configuration)
                                                <option value="{{$configuration->id}}">{{$configuration->razon_social}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="timezone" class="col-sm-4 control-label">Zona Horaria</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" id="timezone" name="timezone">
                                            <option value="America/Merida">MERIDA</option>
                                            <option value="America/Cancun" selected>CANCUN</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="residence_place" class="col-sm-4 control-label">Asociado a:</label>
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="related_brand" value="1" checked> Bocsiyow
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="related_brand" value="2"> Sevilla
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-6">
                                        <button type="submit" class="btn btn-default">Guardar</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
