<li><a href="#" data-toggle="modal" data-target="#modal-users">Asignar usuarios</a></li>
<div class="modal fade" id="modal-users" tabindex="-1" role="dialog" aria-labelledby="modal-usersLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-usersLabel">Asignar usuarios</h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('subsidiaries.users', $subsidiary->id) }}" method="post">
          {{{ csrf_field() }}}
          @foreach($users as $user)
            <div class="checkbox">
              <label>
                <input type="checkbox" value="{{ $user->id }}" name="users[]">
                {{ $user->name }}
              </label>
            </div>
          @endforeach
          <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Asignar">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>