@if($subsidiary->employees->count())
    <div class="panel panel-default">
        <div class="panel-heading">Empleados</div>

        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Nombre corto</th>
                        <th>Puesto</th>
                        <th>Nombre completo</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($subsidiary->employees as $employee)
                        <tr>
                            <td>{{ $employee->short_name }}</td>
                            <td>{{ $employee->job }}</td>
                            <td>{{ $employee->name }}</td>
                            <td><a href="{{ route('employees.edit', $employee->id) }}">Editar</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endif