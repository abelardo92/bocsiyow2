<div class="panel panel-default">
    <div class="panel-heading">
        Turnos <a href="{{route('subsidiaries.turns.create', $subsidiary->id)}}">Crear nuevo turno</a>
    </div>

    <div class="panel-body">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Identificador</th>
                    <th>Nombre</th>
                    <th>Inicio</th>
                    <th>Fin</th>
                    <th>Descanso</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($subsidiary->turns()->orderBy('identifier', 'asc')->get() as $turn)
                    <tr>
                        <td>{{ $turn->identifier }}</td>
                        <td>{{ $turn->name }}</td>
                        <td>{{ $turn->start }}</td>
                        <td>{{ $turn->end }}</td>
                        <td>{{ $turn->is_rest ? 'Si' : 'No' }}</td>
                        <td><a href="{{route('subsidiaries.turns.edit', [$subsidiary->id, $turn->id])}}">Editar</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>