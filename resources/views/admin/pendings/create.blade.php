@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Escriba un mensaje
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{ route('pendings.store') }}" method="post" autocomplete="off" enctype="multipart/form-data">
                      {{{ csrf_field() }}}
                      <input type="hidden" class="form-control" id="is_admin" name="is_admin" value="{{$is_admin}}">
                      <div class="form-group">
                          <label for="subject" class="control-label">Asunto </label>
                          <input type="text" class="form-control" id="subject" name="subject" required="required">
                      </div>

                      <div class="form-group">
                          <label for="description_pending" class="control-label">Mensaje </label>
                          <textarea class="form-control" id="description_pending" rows="10" name="description_pending" ></textarea>
                      </div>

                      <div class="form-group">
                          <label for="image[]" class="control-label">Archivo *</label>
                          <input type="file" class="form-control" id="image[]" name="image[]" multiple>
                      </div>

                      <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Enviar">
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection