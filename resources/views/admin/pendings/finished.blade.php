@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Pendientes terminados</div>

                    <div class="panel-body">
                        @include('admin.pendings.messages_menu')
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Sucursal</th>
                                    <th>Creado por</th>
                                    <th>Asunto</th>
                                    <th>Estatus</th>
                                    <th>Terminado por</th>
                                    <th>Ver</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pendings as $pending)
                                    <tr>
                                        <td>{{ $pending->date }}</td>
                                        <td>
                                            @if($pending->subsidiary != null)
                                                {{ $pending->subsidiary->name }}
                                            @endif
                                        </td>
                                        <td>
                                            @if($pending->createdBy->employee != null)
                                                {{ $pending->createdBy->employee->short_name }}
                                            @else
                                                {{ $pending->createdBy->name }}
                                            @endif
                                        </td>
                                        <td>{{ $pending->subject }}</td>
                                        <td>{{ $status[$pending->status] }}</td>
                                        <td>
                                            @if($pending->finishedBy->employee != null)
                                                {{ $pending->finishedBy->employee->short_name }}
                                            @else
                                                {{ $pending->finishedBy->name }}
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-link" href="{{ route('pendings.show', $pending->id) }}">Ver</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $pendings->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection