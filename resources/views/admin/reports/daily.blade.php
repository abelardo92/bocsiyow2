@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de ventas diario
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/daily')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="start">Fecha: </label>
                            <input type="text" name="start" id="start" class="form-control datepicker" value="{{$start}}">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Sucursal</th>
                                <th>Turno</th>
                                <th>Efectivo</th>
                                <th>Tarjeta</th>
                                <th>Dolares (T.C)</th>
                                <th>Importe</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $cash_total = 0;?>
                            <?php $card_total = 0;?>
                            <?php $usd_total = 0;?>
                            <?php $total = 0;?>
                            @foreach($cash_regiters as $cash_regiter)
                                <tr>
                                    <td>{{ $cash_regiter->subsidiary->name }}</td>
                                    <td>{{ $cash_regiter->turn->name }}</td>
                                    <?php
                                        if($cash_regiter->subsidiary->id != 11) {
                                            
                                            $sales = $cash_regiter->sales()->whereDate('date', $start)
                                            ->finished()->notCanceled()->get();
                                            $sale_ids = $sales->pluck('id');
                                            $money_change = 0;
                                            
                                            if($cash_regiter->subsidiary->is_laundry) {
                                                $cash_tot = $cash_regiter->totalLaundry();
                                            } else {
                                                $cash_tot = App\SalePayment::whereIn('sale_id', $sale_ids)->efectivo()->mxn()->sum('total') - $sales->sum('money_change');
                                            }
                                        } else {
                                            $cash_tot = $cash_regiter->laundryServices()->notCanceled()->get()->sum('amount');
                                        } 
                                        $cash_total += $cash_tot;
                                    ?>
                                    <td>{{ number_format($cash_tot, 2, '.', ',') }}</td>
                                    @php
                                        $card_tot = 0;
                                        if($cash_regiter->subsidiary->id != 11){
                                            $card_tot = App\SalePayment::whereIn('sale_id', $sale_ids)->card()->sum('total');
                                            $card_total += $card_tot;
                                        }
                                    @endphp
                                    <td>{{ number_format($card_tot, 2, '.', ',') }}</td>
                                    @php
                                        if($cash_regiter->subsidiary->id != 11) {
                                            $usd_tot = App\SalePayment::whereIn('sale_id', $sale_ids)->efectivo()->usd()->sum('total') / $cash_regiter->exchangeRate->rate;
                                        $usd_total += $usd_tot;
                                        } else {
                                            $usd_tot = 0;
                                        }
                                    @endphp
                                    <td>$ {{ number_format($usd_tot, 2, '.', ',') }} ($ {{ $cash_regiter->exchangeRate->rate }})</td>
                                    <?php
$subtotal = $cash_tot + $card_tot + ($usd_tot * $cash_regiter->exchangeRate->rate);
$total += $subtotal;
?>
                                    <td>$ {{ number_format($subtotal, 2, '.', ',') }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <th colspan="2">Totales:</th>
                                <td>$ {{ number_format($cash_total, 2, '.', ',') }}</td>
                                <td>$ {{ number_format($card_total, 2, '.', ',') }}</td>
                                <td>$ {{ number_format($usd_total, 2, '.', ',') }}</td>
                                <td>$ {{ number_format($total, 2, '.', ',') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
