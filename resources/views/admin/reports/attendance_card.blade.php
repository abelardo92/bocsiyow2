@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Tarjeta de asistencia 
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/attendance-card')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="employee_id">Empleados: </label>
                            <select name="employee_id" id="employee_id" class="form-control select2">
                                @foreach($employees as $employ)
                                    <option 
                                        @if($employ->id == $employee->id) selected="selected" @endif
                                        value="{{$employ->id}}"
                                    >{{$employ->short_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group">
                                <input type="text" class="form-control datepicker" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control datepicker" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                            <a href="/home/reports/weekly/attendances/barbers?employee_id={{$employee->id}}&start={{$start}}&end={{$end}}" target="_blank" class="btn btn-primary">Control de asistencia</a>
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Sucursal</th>
                                <th>Hora entrada</th>
                                @if (Auth::user()->isA('super-admin', 'nomina'))
                                    <th>Salida a comer</th>
                                    <th>Regreso de comer</th>
                                    <th>Salida del trabajo</th>
                                    <th>Turno</th>
                                @endif

                                <th>Puntualidad</th>
                                <th>Asistencia</th>
                                <th>Extra</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($employee->schedules->sortBy('date') as $schedule)
                                <tr>
                                    <td>{{$schedule->date}}</td>
                                    <td>{{$schedule->subsidiary->name}}</td>
                                    <td>
                                        <?php 
                                            $attendances = $employee->attendances()->where('subsidiary_id', $schedule->subsidiary->id)->date($schedule->date)->get();
                                        ?>
                                        @if($attendances->count())
                                            @php
                                                $attendance = $attendances->first();
                                            @endphp
                                            {{$attendance->getTime()}}
                                        @endif
                                    </td>
                                    @if (Auth::user()->isA('super-admin', 'nomina'))
                                        <td>
                                            @if($attendances->count() > 2)
                                                {{$attendances[1]->getTime()}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($attendances->count() >= 3)
                                                {{$attendances[2]->getTime()}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($attendances->count() >= 4)
                                                {{$attendances[3]->getTime()}}
                                            @endif
                                            @if($attendances->count() == 2)
                                                {{$attendances[1]->getTime()}}
                                            @endif
                                        </td>
                                        <td>{{$schedule->turn->name}}</td>
                                    @endif
                                    <td>
                                        @if($schedule->subsidiary->id == 8) 
                                            <i class="fa fa-check"></i>
                                        @else
                                            @if($attendances->count())
                                                @php
                                                    $attendanceStart = \Carbon\Carbon::parse($attendance->time);
                                                    $scheduleStart = \Carbon\Carbon::parse($schedule->turn->start);
                                                    if($schedule->employee->job != "Cajero") {
                                                        $scheduleStart = \Carbon\Carbon::parse($schedule->turn->barber_start);
                                                    }
                                                @endphp
                                                @if ($attendanceStart <= $scheduleStart)
                                                    
                                                    @if($attendances->count() >= 3)
                                                        @php
                                                            $start = \Carbon\Carbon::parse($attendances[1]->time);
                                                            $end = \Carbon\Carbon::parse($attendances[2]->time);
                                                            $start->second = 0;
                                                            $end->second = 0;
                                                        @endphp
                                                        @if($start->diffInMinutes($end) <= $schedule->rest_minutes)
                                                            <i class="fa fa-check"></i>
                                                        @else
                                                            <i class="fa fa-close"></i>
                                                        @endif
                                                    @else
                                                        <i class="fa fa-check"></i>
                                                    @endif
                                                @else
                                                    <i class="fa fa-close"></i>
                                                @endif
                                            @else
                                                <i class="fa fa-close"></i>
                                            @endif
                                        @endif
                                    </td>
                                    <td>
                                        @if($schedule->subsidiary->id == 8) 
                                            <i class="fa fa-check"></i>
                                        @else
                                            @if($attendances->count())
                                                <i class="fa fa-check"></i>
                                            @else
                                                <i class="fa fa-close"></i>
                                            @endif
                                        @endif
                                    </td>
                                    <td></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
