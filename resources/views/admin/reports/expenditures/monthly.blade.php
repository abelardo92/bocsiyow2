@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de Gastos Mensual
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/expenditures/monthly')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}

                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Concepto</th>
                                @foreach($subsidiaries as $subsidiary)
                                    @php
                                        $totals[$subsidiary->key] = 0;
                                    @endphp
                                    <th>{{$subsidiary->name}}</th>
                                @endforeach
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $totalGlobal = 0;
                            @endphp
                            @foreach($accounts as $account)
                                @php
                                    if ($account->expenditures()->dates([$start, $end])->sum('total') == 0) {
                                        continue;
                                    }
                                    $totalSubsidiaries = 0;
                                @endphp
                                <tr>
                                    <td>
                                        <a href="{{route('expenditures.accounts.report', [
                                            'account_id' => $account->id,
                                            'start' => $start,
                                            'end' => $end,
                                        ])}}">
                                            {{ $account->name }}
                                        </a>
                                    </td>
                                    @foreach($subsidiaries as $subsidiary)
                                        @php
                                            $total = $account->expenditures()->dates([$start, $end])
                                                ->whereSubsidiaryId($subsidiary->id)->sum('total');
                                            $totalSubsidiaries += $total;
                                            $totals[$subsidiary->key] += $total;
                                        @endphp
                                        <td>
                                            <a href="{{route('expenditures.accounts.report', [
                                            'account_id' => $account->id,
                                            'subsidiary_id' => $subsidiary->id,
                                            'start' => $start,
                                                'end' => $end,
                                            ])}}">
                                                {{ number_format($total, 2, '.', ',')}}
                                            </a>
                                        </td>
                                    @endforeach
                                    @php
                                        $totalGlobal += $totalSubsidiaries;
                                    @endphp
                                    <td>{{ number_format($totalSubsidiaries, 2, '.', ',')}}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td>Total</td>
                                @foreach($subsidiaries as $subsidiary)
                                    <td>{{ number_format($totals[$subsidiary->key], 2, '.', ',')}}</td>
                                @endforeach
                                <td>{{ number_format($totalGlobal, 2, '.', ',')}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
