@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de Gastos por Sucursal
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/expenditures/subsidiary')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="subsidiary_id">Sucursal: </label>
                            <select name="subsidiary_id" id="subsidiary_id" class="form-control select2">
                                <option value="all" selected="selected">Todas o seleccionar sucursal</option>
                                @foreach($subsidiaries as $subsidiary)
                                    <option 
                                        @if($subsidiary_id == $subsidiary->id) selected="selected" @endif
                                        value="{{$subsidiary->id}}"
                                    >
                                        {{$subsidiary->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nombre de la cuenta</th>
                                <th>Importe</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $totalGlobal = 0;
                            @endphp
                            @foreach($accounts as $account)
                                @php
                                    $total = $account->expenditures()
                                        ->dates([$start, $end])
                                        ->whereIn('subsidiary_id', $subsidiariesReport->pluck('id'))
                                        ->sum('total');
                                        // dd($total->toSql());
                                    $totalGlobal += $total;
                                @endphp
                                <tr>
                                    <td>
                                        <a href="{{route('expenditures.accounts.report', [
                                            'account_id' => $account->id,
                                            'start' => $start,
                                            'end' => $end,
                                        ])}}">
                                            {{ $account->name }}
                                        </a>
                                    </td>
                                    <td>${{ number_format($total, 2, '.', ',')}}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td>Total</td>
                                <td>
                                    ${{ number_format($totalGlobal, 2, '.', ',')}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
