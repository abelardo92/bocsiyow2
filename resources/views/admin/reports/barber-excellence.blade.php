@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @if(Auth::user()->isA('super-admin', 'super-admin-restringido'))
                        <p class="pull-right">
                            <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                        </p>
                    @endif
                    Empleados con Excelencia
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/weekly/barbers/excellence')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Empleado</th>
                                <th>Puntualidad</th>
                                <th>Asistencia</th>
                                <th>Productividad</th>
                                <th># Servicios</th>
                                <th># Productos</th>
                                <th>Citas atendidas</th>
                                <th>Lugares</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $total = 0;
                                $total_comision = 0;
                                $total_discount_repaired = 0;
                                $total_repaired = 0;
                                $total_tips = 0;
                                $index = 0;
                            @endphp
                            @foreach($employees->sortBy('points')->groupBy('points') as $employees_points)
                                @php $index++; @endphp
                                @foreach($employees_points as $employee)
                                    <tr>
                                        <td>{{ $employee->name }}</td>
                                        <td>
                                            {{ number_format($punctuality_percentaje[$employee->id], 2, '.', '') }}%
                                            {{-- $total_punctuality_points[$punctuality_percentaje[$employee->id]] --}}
                                        </td>
                                        <td>
                                            {{ number_format($attendance_percentaje[$employee->id], 2, '.', '') }}%
                                            {{-- $total_attendances_points[$attendance_percentaje[$employee->id]] --}}
                                        </td>
                                        <td>
                                            {{ number_format($issue_percentaje[$employee->id], 2, '.', '') }}%
                                        </td>
                                    <td>{{ $total_services[$employee->id] }} {{--$total_services_points[$total_services[$employee->id]]--}}</td>
                                    <td>{{ $total_products[$employee->id] }} {{--$total_products_points[$total_products[$employee->id]]--}}</td>
                                    <td>{{ $total_diaries[$employee->id] }} {{--$total_diaries_points[$total_diaries[$employee->id]]--}}</td>
                                    
                                    <td>{{ $index }} {{-- $employee->points --}}</td>
                                    </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
