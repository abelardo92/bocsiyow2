@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de propinas
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/sales/tips')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Tarjeta de Credito</th>
                                <th>Efectivo</th>
                                <th>total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total_tarjeta = 0; ?>
                            <?php $total_efectivo = 0; ?>
                            <?php $total_neto = 0; ?>
                            @foreach($sales as $sales_group)
                                @if($sales_group->first()->employee)
                                    <?php $tarjeta = $sales_group->where('tip_in', 'tarjeta')->sum('tip'); ?>
                                    <?php $efectivo = $sales_group->where('tip_in', 'efectivo')->sum('tip'); ?>
                                    <?php $neto = $tarjeta + $efectivo; ?>

                                    <?php $total_tarjeta += $tarjeta; ?>
                                    <?php $total_efectivo += $efectivo; ?>
                                    <?php $total_neto += $neto; ?>

                                    <tr>
                                        <td>{{$sales_group->first()->employee->name}}</td>
                                        <td>{{number_format($tarjeta, 2, '.', ',')}}</td>
                                        <td>{{number_format($efectivo, 2, '.', ',')}}</td>
                                        <td>{{number_format($neto, 2, '.', ',')}}</td>
                                    </tr>
                                @endif
                            @endforeach
                            <tr>
                                <td>Totales: </td>
                                <td>{{number_format($total_tarjeta, 2, '.', ',')}}</td>
                                <td>{{number_format($total_efectivo, 2, '.', ',')}}</td>
                                <td>{{number_format($total_neto, 2, '.', ',')}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
