@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de encuestas por sucursal
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/survey/subsidiary')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="subsidiary_id">Sucursal: </label>
                            <select name="subsidiary_id" id="subsidiary_id" class="form-control select2">
                                <option value="all" selected="selected">Todas o seleccionar sucursal</option>
                                @foreach($subsidiaries as $subsidiary)
                                    <option 
                                        @if($subsidiary_id == $subsidiary->id) selected="selected" @endif
                                        value="{{$subsidiary->id}}"
                                    >
                                        {{$subsidiary->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="question_id">Seleccionar pregunta: </label>
                            <select name="question_id" id="question_id" class="form-control select2">
                                @foreach($questions as $questi)
                                    <option 
                                        @if($question->id == $questi->id) selected="selected" @endif
                                        value="{{$questi->id}}"
                                    >
                                        {{$questi->title}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Concepto</th>
                                @foreach([ 'excelente', 'bueno', 'regular', 'malo', 'muy-malo'] as $option)
                                    <th>{{ucwords(str_replace('-', ' ', $option))}}</th>
                                @endforeach
                                <th>N/R</th>
                                <th>Total De Tickets</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $totalSurveysGlobal = 0;
                                $totalAnswer1 = 0;
                                $totalAnswer2 = 0;
                                $totalAnswer3 = 0;
                                $totalAnswer4 = 0;
                                $totalAnswer5 = 0;
                                $totalAnswer6 = 0;
                            @endphp
                            @foreach($subsidiariesReport as $subsidiary)
                                @php
                                    $totalSurveys = $subsidiary->surveys()->dates([$start, $end])->whereHas('questions')->count();
                                    $totalSurveysGlobal += $totalSurveys;
                                    $notCompleted = $subsidiary->surveys()->dates([$start, $end])->whereHas('questions')->completed(false)->count();
                                    $totalAnswer6 += $notCompleted;
                                @endphp
                                <tr>
                                    <td>{{ $subsidiary->name }}</td>
                                    @foreach([ 'excelente', 'bueno', 'regular', 'malo', 'muy-malo'] as $option)
                                        @php
                                            $answer = ucwords(str_replace('-', ' ', $option));
                                            $currentAnswerCount =  $subsidiary->surveys()->dates([$start, $end])->whereHas('questions', function ($query) use ($answer, $question) {
                                                $query->where('answer', $answer)->where('question_id', $question->id);
                                            })->completed()->count();
                                            if($option == 'excelente'){
                                                $totalAnswer1 += $currentAnswerCount;
                                            }
                                            if($option == 'bueno'){
                                                $totalAnswer2 += $currentAnswerCount;
                                            }
                                            if($option == 'regular'){
                                                $totalAnswer3 += $currentAnswerCount;
                                            }
                                            if($option == 'malo'){
                                                $totalAnswer4 += $currentAnswerCount;
                                            }
                                            if($option == 'muy-malo'){
                                                $totalAnswer5 += $currentAnswerCount;
                                            }
                                        @endphp
                                        <td>
                                            {{ $currentAnswerCount}} 
                                            @if($totalSurveys == 0)
                                                (%0)
                                            @else
                                                (%{{ number_format($currentAnswerCount / $totalSurveys * 100, 0, '.', '')}})
                                            @endif
                                        </td>
                                    @endforeach
                                    <td>
                                        {{ $notCompleted}} 
                                        @if($totalSurveys == 0)
                                            (%0)
                                        @else
                                            (%{{ number_format($notCompleted / $totalSurveys * 100, 0, '.', '')}})
                                        @endif
                                    </td>
                                    <td>{{ $totalSurveys }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td>Total</td>
                                <td>
                                    {{$totalAnswer1}}
                                    <br>
                                    @if($totalSurveysGlobal == 0)
                                        (%0)
                                    @else
                                        (%{{ number_format($totalAnswer1 / $totalSurveysGlobal * 100, 0, '.', '')}})
                                    @endif
                                </td>
                                <td>
                                    {{$totalAnswer2}}
                                    <br>
                                    @if($totalSurveysGlobal == 0)
                                        (%0)
                                    @else
                                        (%{{ number_format($totalAnswer2 / $totalSurveysGlobal * 100, 0, '.', '')}})
                                    @endif
                                </td>
                                <td>
                                    {{$totalAnswer3}}
                                    <br>
                                    @if($totalSurveysGlobal == 0)
                                        (%0)
                                    @else
                                        (%{{ number_format($totalAnswer3 / $totalSurveysGlobal * 100, 0, '.', '')}})
                                    @endif
                                </td>
                                <td>
                                    {{$totalAnswer4}}
                                    <br>
                                    @if($totalSurveysGlobal == 0)
                                        (%0)
                                    @else
                                        (%{{ number_format($totalAnswer4 / $totalSurveysGlobal * 100, 0, '.', '')}})
                                    @endif
                                </td>
                                <td>
                                    {{$totalAnswer5}}
                                    <br>
                                    @if($totalSurveysGlobal == 0)
                                        (%0)
                                    @else
                                        (%{{ number_format($totalAnswer5 / $totalSurveysGlobal * 100, 0, '.', '')}})
                                    @endif
                                </td>
                                <td>
                                    {{$totalAnswer6}}
                                    <br>
                                    @if($totalSurveysGlobal == 0)
                                        (%0)
                                    @else
                                        (%{{ number_format($totalAnswer6 / $totalSurveysGlobal * 100, 0, '.', '')}})
                                    @endif
                                </td>
                                <td>
                                    {{$totalSurveysGlobal}}
                                    <br>
                                    @if($totalSurveysGlobal == 0)
                                        (%0)
                                    @else
                                        (%{{ number_format($totalSurveysGlobal / $totalSurveysGlobal * 100, 0, '.', '')}})
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
