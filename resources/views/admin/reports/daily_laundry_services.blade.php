@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de servicios de lavandería por día
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/daily-laundry-services')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="start">Fecha: </label>
                            <input type="text" name="start" id="start" class="form-control datepicker" value="{{$start}}">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Sucursal</th>
                                <th>Monto</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $cash_total = 0;?>
                            @foreach($laundry_services as $index => $laundry_service)
                                <tr>
                                    <td>{{ $index + 1 }}</td>
                                    <td>{{ $laundry_service->customerSubsidiary->name }}</td>
                                    <td>$ {{ $laundry_service->amount }}</td>
                                    <td>
                                        <a href="{{ route('laundry-services.edit', $laundry_service->id) }}">Editar</a>
                                        @if(Auth::user()->isA('super-admin'))
                                            |
                                            <a href="#" onclick="document.getElementById('form-delete-{{$laundry_service->id}}').submit()">
                                                Eliminar
                                            </a>
                                            <form action="{{route('laundry-services.destroy', $laundry_service->id)}}" method="post" id="form-delete-{{$laundry_service->id}}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                            </form>
                                        @endif
                                    </td>
                                    @php
                                        $cash_total += $laundry_service->amount;
                                    @endphp
                                </tr>
                            @endforeach
                            <tr>
                                <th colspan="2">Total:</th>
                                <td>$ {{ number_format($cash_total, 2, '.', ',') }}</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
