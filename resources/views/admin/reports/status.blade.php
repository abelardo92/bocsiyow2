@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Status por fecha
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/sales/date')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="start">Fecha: </label>
                            <input type="text" name="start" id="start" class="form-control datepicker" value="{{$start}}">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>

                    <div class="hide-in-print">
                        <table class="table table-striped datatables">
                            <thead>
                                <tr>
                                    <th>Sucursal</th>
                                    <th>Folio</th>
                                    <th>Status</th>
                                    <th>Importe</th>
                                    <th>H. entrada</th>
                                    <th>H. atención</th>
                                    <th>H. finalización</th>
                                    <th>Barbero</th>
                                    <th>Cliente</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sales as $sale)
                                    <tr>
                                        <td>{{ $sale->subsidiary->name }}</td>
                                        <td>
                                            <a href="{{route('sales.print.admin', [$sale->subsidiary->key, $sale->id])}}" target="_blank">
                                                {{ $sale->subsidiary->key }} - {{ $sale->folio }}
                                            </a>
                                        </td>
                                        @if($sale->canceler)
                                            <td>Cancelado</td>
                                        @endif
                                        @if(!$sale->canceler)
                                            <td>Cobrado</td>
                                        @endif

                                        <td>{{ $sale->subtotal }}</td>

                                        <td>{{ $sale->created_at->format('h:i a') }}</td>
                                        <td>{{ $sale->attended_at->format('h:i a') }}</td>
                                        <td>{{ $sale->finished_at->format('h:i a') }}</td>

                                        @if($sale->employee)
                                            <td>{{ $sale->employee->short_name }}</td>
                                        @endif
                                        @if(!$sale->employee)
                                            <td>{{ App\Employee::find(46)->short_name }}</td>
                                        @endif
                                        <td>{{ $sale->customer->name }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
