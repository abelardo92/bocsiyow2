@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Entradas de inventario
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>
                <div class="panel-body">
                    <form action="{{url('/home/reports/inventories/warehouse/stocks')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="status">Estatus: </label>
                            <select name="history_id" id="history_id" class="form-control">
                                <option value="">Periodo actual</option>
                                @foreach ($warehouse_histories as $warehouse_history)
                                    <option value="{{$warehouse_history->id}}" @if($warehouse_history->id == $history_id) selected @endif>{{$warehouse_history->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Producto</th>
                                <th>En almacen</th>
                                <th>Precio venta</th>
                                <th>Neto</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total_neto = 0; ?>
                            @foreach($products as $product)
                                <tr>    
                                    <td>{{$product->code}}</td>                      
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->in_storage}}</td>              
                                    <td>$ {{$product->amount}}</td>
                                    <td>$ {{$product->amount * $product->in_storage}}</td>
                                    <?php $total_neto += $product->amount * $product->in_storage; ?>
                                </tr>
                            @endforeach
                                <tr>
                                    <td colspan="3"></td>
                                    <td>Total</td>
                                    <td>$ {{$total_neto}}</td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
