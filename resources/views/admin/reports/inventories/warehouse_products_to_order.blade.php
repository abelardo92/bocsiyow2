@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Entradas de inventario
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i>
                        </button>
                    </p>
                </div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Artículo</th>
                                <th>Existencias</th>
                                <th>Máximo</th>
                                <th>Comprar</th>
                                <th>Importe</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total_neto = 0; ?>
                            @foreach($products as $product)
                            <?php 
                                $products_to_buy = $product->warehouse_desired_existence - $product->in_storage;
                                if($products_to_buy < 0){
                                    $products_to_buy = 0;
                                }
                                $total = $product->amount * $products_to_buy;
                            ?>
                                <tr>    
                                    <td>{{$product->code}}</td>                      
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->in_storage}}</td>
                                    <td>{{$product->warehouse_desired_existence}}</td>
                                    <td>{{$products_to_buy}}</td>           
                                    <td>$ {{$product->amount}}</td>
                                    <td>$ {{$total}}</td>
                                    <?php $total_neto += $total; ?>
                                </tr>
                            @endforeach
                                <tr>
                                    <td colspan="5"></td>
                                    <td>Total</td>
                                    <td>$ {{$total_neto}}</td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
