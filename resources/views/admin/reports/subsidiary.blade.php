@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de ventas por sucursal
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/subsidiary')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="subsidiary_id">Sucursal: </label>
                            <select name="subsidiary_id" id="subsidiary_id" class="form-control select2">
                                @foreach($subsidiaries as $subsi)
                                    <option 
                                        @if($subsidiary->id == $subsi->id) selected="selected" @endif
                                        value="{{$subsi->id}}"
                                    >{{$subsi->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Importe</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $total = 0;
                                $subsi_cash_registers = $subsi->cashRegisters()->with(['sales','subsidiary'])->get(); 
                            ?>
                            @foreach($sales as $sale)
                                <tr>
                                    <td>{{ $sale->date }}</td>
                                    <?php
                                        $subtotal = 0;
                                        if($subsi->is_laundry) {
                                            $cash_registers = $subsi_cash_registers->where('date', $sale->date);
                                            foreach ($cash_registers as $cr) {
                                                $subtotal += $cr->totalLaundry();
                                            }
                                        } else {
                                            $subtotal += $sale->sumtotal;
                                        }
                                        $total += $subtotal;
                                    ?>
                                    <td>$ {{ number_format($subtotal, 2, '.', ',') }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <th>Total:</th>
                                <td>$ {{ number_format($total, 2, '.', ',') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
