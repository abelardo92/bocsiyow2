@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de Citas
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/diaries')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Citas</th>
                                <th>Citas agendadas</th>
                                <th>Citas atendidas</th>
                                <th>Citas no atendidas</th>
                                <th>Citas canceladas</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>En sucursal</th>
                                <td>{{$subsidiary_total}}</td>
                                <td>{{$subsidiary_total_atend}}</td>
                                <td>{{$subsidiary_total_active}}</td>
                                <td>{{$subsidiary_total_cancel}}</td>
                            </tr>
                            <tr>
                                <th>En call center</th>
                                <td>{{$call_center_total}}</td>
                                <td>{{$call_center_total_atend}}</td>
                                <td>{{$call_center_total_active}}</td>
                                <td>{{$call_center_total_cancel}}</td>
                            </tr>
                            <tr>
                                <th>Total</th>
                                <td>{{$subsidiary_total + $call_center_total}}</td>
                                <td>{{$subsidiary_total_atend + $call_center_total_atend}}</td>
                                <td>{{$subsidiary_total_active + $call_center_total_active}}</td>
                                <td>{{$subsidiary_total_cancel + $call_center_total_cancel}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
