@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de tickets y servicios cancelados
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/sales/canceled')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Ticket</th>
                                <th>Sucursal</th>
                                <th>Canceló</th>
                                <th>Fecha</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($sales as $sale)
                            <tr>
                                <td>
                                    <a href="{{route('sales.print.admin', [$sale->subsidiary->key, $sale->id])}}" target="_blank">
                                        {{ $sale->subsidiary->key }} - {{ $sale->folio }}
                                    </a>
                                </td>
                                <td>{{ $sale->subsidiary->name }}</td>
                                <td>
                                    @if($sale->canceler) 
                                        {{ $sale->canceler->name }}
                                    @else
                                        @if($sale->canceledServices && $canceledService = $sale->canceledServices->first())
                                            @if($canceledService->canceledBy)
                                                {{ $canceledService->canceledBy->name }}
                                            @endif
                                        @endif
                                        @if($sale->canceledProducts && $canceledProduct = $sale->canceledProducts->first())
                                            @if($canceledProduct->canceledBy)
                                                {{ $canceledProduct->canceledBy->name }}
                                            @endif
                                        @endif
                                    @endif
                                </td>
                                <td>{{ $sale->created_at }}</td>
                            </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
