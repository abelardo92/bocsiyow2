@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Firma por empleado
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/signatureByEmployee')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-1">
                                    <label>Empleado: </label>
                                </div>
                                <div class="col-md-3">
                                    <select class="select2 form-control" id="employee_id" name="employee_id">
                                    @foreach($employees as $emp)
                                        <option @if($employee->id == $emp->id) selected="selected" @endif value="{{$emp->id}}">{{$emp->name}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-default hide-in-print" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>    
                            </div>
                        </div>
                    </form>
                    @if(strpos($path, '.pdf'))
                        <embed src="{{$path}}" width="100%" height="1000px" type='application/pdf'>
                    @else
                        <img src="{{$path}}" alt="{{$employee->name}}" class="img-square img-responsive">
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
