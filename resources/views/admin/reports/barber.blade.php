@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de ventas por barbero
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/barber')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="employee_id">Barbero: </label>
                            <select name="employee_id" id="employee_id" class="form-control select2">
                                @foreach($barbers as $employee)
                                    <option
                                        @if($employee->id == $barber->id) selected="selected" @endif
                                        value="{{$employee->id}}"
                                    >{{$employee->short_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Sucursal</th>
                                <th>Importe</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0;?>
                            @foreach($sales as $sale)
<?php
$ptot = 0;
$pcount = 0;
$stt = 0;

$producto_groups = App\SaleProduct::whereIn('sale_id', $barber->sales()->where('date', $sale->date)->finished()->notCanceled()->pluck('id'))->get()->groupBy('product_id');
foreach ($producto_groups as $producto_group) {
    $pcount = $producto_group->sum('qty');
    $ptot += $producto_group->first()->price * $pcount;
}
?>
                                <tr>
                                    <td>{{ $sale->date }}</td>
                                    <td>{{ $sale->subsidiary->name }}</td>
                                    <?php
// if ($sale->date == '2017-01-11') {
//     dd($sale->sumtotal);
// }
$subtotal = $sale->sumtotal - $ptot;
$subtotal -= $sale->sumtip;
// $subtotal = $sale->sumtotal;
$total += $subtotal;
?>
                                    <td>$ {{ number_format($subtotal, 2, '.', ',') }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <th colspan="2">Total:</th>
                                <td>$ {{ number_format($total, 2, '.', ',') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
