@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de comisiones por productos
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/sales/commissions/products')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Importe vendido</th>
                                <th>Comisión</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $products_total = 0; ?>
                            <?php $commission_total = 0; ?>
                            @foreach($employees as $employee)
                                @php
                                    list($products, $commission) =  $employee->getEmployeeProductCommission();
                                    $products_total += $products;
                                    $commission_total += $commission;
                                @endphp
                                <tr>
                                    <td>{{$employee->name}}</td>
                                    <td>{{number_format($products, 2, '.', ',')}}</td>
                                    <td>{{number_format($commission, 2, '.', ',')}}</td>
                                </tr>
                            @endforeach

   
                            <tr>
                                <td>Totales: </td>
                                <td>{{number_format($products_total, 2, '.', ',')}}</td>
                                <td>{{number_format($commission_total, 2, '.', ',')}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
