@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de Promociones
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/promos')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="subsidiary_id">Sucursal: </label>
                            <select name="subsidiary_id" id="subsidiary_id" class="form-control">
                                <option value="">Todas las sucursales</option>
                                @foreach($subsidiaries as $subsi)
                                    <option 
                                        @if($subsidiary_id == $subsi->id) selected="selected" @endif
                                        value="{{$subsi->id}}"
                                    >{{$subsi->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Concepto</th>
                                <th>No. servicios</th>
                                <th>Importe descontado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Por agendar cita</th>
                                <td>{{$dates_total}}</td>
                                <td>$ {{number_format($dates_total_atend, 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <th>Por cumpleaños</th>
                                <td>{{$birthday_total}}</td>
                                <td>$ {{number_format($birthday_total_atend, 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <th>No. de visitas / Referencias / Cortesias</th>
                                <td>{{$cortesy_total}}</td>
                                <td>$ {{number_format($cortesy_total_atend, 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <th>Total</th>
                                <td>{{$dates_total + $birthday_total + $cortesy_total}}</td>
                                <td>$ {{number_format($dates_total_atend + $birthday_total_atend + $cortesy_total_atend, 2, '.', ',')}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
