@extends('layouts.app')

@section('content')
<div class="container">
    @foreach($subsidiaries as $subsidiary)
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                    Horarios de {{ $subsidiary->name }}
                        <p class="pull-right">
                            <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                        </p>
                    </div>

                    <div class="panel-body">
                        <form action="{{url('/home/reports/schedules')}}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('GET') }}
                            
                            <div class="form-group">
                                <label for="start">Fecha de: </label>
                                <div class="input-group input-daterange">
                                    <input type="text" class="form-control" name="start" value="{{$start->format('Y-m-d')}}">
                                    <span class="input-group-addon">al</span>
                                    <input type="text" class="form-control" name="end" value="{{$end->format('Y-m-d')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                            </div>
                        </form>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>LUNES</th>
                                    <th>MARTES</th>
                                    <th>MIERCOLES</th>
                                    <th>JUEVES</th>
                                    <th>VIERNES</th>
                                    <th>SABADO</th>
                                    <th>DOMINGO</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                @foreach($dates as $date)
                                    <td>
                                        <table class="table">
                                            <tr>
                                                <td>
                                                    {{ $date->format('d-m-Y') }}
                                                </td>
                                            </tr>
                                        @foreach($subsidiary
                                                ->schedules()
                                                ->where('date', $date->format('Y-m-d'))
                                                ->get()->sortBy('turn_id')
                                                ->groupBy('turn_id')
                                                as $schedule_group)
                                                <tr>
                                                    <td>
                                                        {{ $schedule_group->first()->turn->name }}
                                                    </td>
                                                </tr>
                                                @foreach($schedule_group->sortByDesc(function ($schedule, $key){
                                                        return $schedule->employee->job;
                                                    }) as $schedule)
                                                    <tr>
                                                        <td>
                                                            <a href="{{route('employees.schedules.edit', [$schedule->employee->id, $schedule->id])}}" class="hide-in-print">{{ $schedule->employee->short_name }}</a>
                                                            <span class="show-in-print">{{ $schedule->employee->short_name }}</span>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        </table>
                                    </td>
                                @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection
