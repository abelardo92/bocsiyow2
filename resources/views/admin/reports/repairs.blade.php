@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de reparaciones
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/sales/repairs')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Empleado</th>
                                {{-- <th>Total de ventas</th> --}}
                                <th>Reparaciones</th>
                                <th>Descuento por raparaciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0;?>
                            @foreach($barbers as $barber)
                                <tr>
                                    <td>{{$barber->short_name}}</td>
                                    {{-- <td>{{number_format($sales, 2, '.', '')}}</td> --}}
                                    <?php
                                    $repairs = 0;
                                    $sale_ids = $barber->repairedSales->pluck('id');
                                    $services = App\SaleService::whereIn('sale_id', $sale_ids)
                                        ->get()->groupBy('service_id');
                                    ?>
                                    @foreach($services as $service_group)
                                        @php
                                            $repairs += ($service_group->first()->service->sell_price * $service_group->sum('qty'));
                                        @endphp
                                    @endforeach
                                    <td>$ {{number_format($repairs, 2, '.', '')}}</td>
                                    <?php
                                    $discount_repairs = 0;
                                    $sale_ids = $barber->salesRepairedBy->pluck('id');
                                    $services = App\SaleService::whereIn('sale_id', $sale_ids)
                                        ->get()->groupBy('service_id');
                                    ?>
                                    @foreach($services as $service_group)
                                        @php

                                            $discount_repairs += ($service_group->first()->service->sell_price * $service_group->sum('qty'));
                                        @endphp
                                    @endforeach
                                    <td>$ {{number_format($discount_repairs, 2, '.', '')}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
