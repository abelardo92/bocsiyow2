@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Productos vendidos por mes
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/sold-products-employee')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-left">
                                    <a href="{{ route('reports.sales.sold-products-employee', ['date' => $start->copy()->subDay()->format('Y-m-d')]) }}"
                                        class="pull-left">{{$start->copy()->subDay()->format('M Y')}}</a>
                                </div>
                                <div class="col-md-4">
                                    <p class="text-center">Mes actual: <strong>{{$date->copy()->format('M Y')}}</strong></p>
                                </div>
                                <div class="col-md-4">
                                    <a href="{{ route('reports.sales.sold-products-employee', ['date' => $end->copy()->addDay()->format('Y-m-d')]) }}"
                                        class="pull-right">{{$end->copy()->addDay()->format('M Y')}}</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Producto</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($product_totals_array as $id => $qty)
                            <tr>
                                <td>{{ $products_array[$id]->key }}</td>
                                <td>{{ $products_array[$id]->name }}</td>
                                <td>{{ $qty }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection