@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Importe de productos vendidos
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/weekly/sold-products')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="subsidiary_id">Sucursal: </label>
                            <select name="subsidiary_id" id="subsidiary_id" class="form-control select2">
                                <option value="0">TODOS</option>
                                @foreach($subsidiaries as $subsi)
                                    <option 
                                        @if($subsidiary != null && $subsidiary->id == $subsi->id) selected="selected" @endif
                                        value="{{$subsi->id}}"
                                    >{{$subsi->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Importe</th>
                                <th>Comisión</th>
                                <th>Neto</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Productos con comisión:</th>
                                <td>{{ $total_products_commission }}</td>
                                <td>{{ $total_products_commission_percentaje }}</td>
                                <td>{{ $total_products_commission - $total_products_commission_percentaje }}</td>
                            </tr>
                            <tr>
                                <th>Productos sin comisión:</th>
                                <td>{{ $total_products_without_commission }}</td>
                                <td> - </td>
                                <td>{{ $total_products_without_commission }}</td>
                            </tr>
                            <tr>
                                <th>Total:</th>
                                <td>{{ $total_products_commission + $total_products_without_commission }}</td>
                                <td>{{ $total_products_commission_percentaje }}</td>
                                <td>{{ $total_products_commission + $total_products_without_commission - $total_products_commission_percentaje }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection