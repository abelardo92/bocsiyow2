<div class="modal fade" id="modal-sunday-details" tabindex="-1" role="dialog"
    aria-labelledby="modal-sunday-detailsLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal-sunday-detailsLabel">Detalles de calculo dominical.</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped">
                    <tr>
                        <th>Fecha (Domingo)</th>
                        <th>Comision original</th>
                        <th>Productividad</th>
                        <th>Comision + productividad</th>
                        <th>20% Domingo</th>
                        <th>Subtotal</th>
                    </tr>
                    @php
                        $employee_sales = $employee->salesFinished->groupBy('date');
                        $commission_total = 0;
                        $barber_commission_total = 0;
                        $commission_subtotal_total = 0;
                        $sunday_commission_total = 0;
                        $general_total = 0;
                    @endphp
                    @foreach ($employee_sales as $date => $sales)

                        @php
                        // * if it's NOT sunday
                        if(date('w', strtotime($date)) != 0) continue;

                        $schedule = $employee->schedules && $employee->schedules->count() > 0 ? $employee->schedules->where('date', $date)->first() : null;
                        if(!$schedule) continue;

                        $commission = 0;
                        $barber_commission = 0;
                        foreach ($sales as $sale) {
                            foreach ($sale->services as $sale_service) {
                                $commission += $sale_service->getCommission();
                                $barber_commission += $sale_service->getAdditionalBarberCommission();
                            }
                        }
                        $commission_subtotal = $commission + $barber_commission;
                        $sunday_commission = $commission_subtotal * (0.2);
                        $date_total = $commission_subtotal + $sunday_commission;
                        
                        $commission_total += $commission;
                        $barber_commission_total += $barber_commission;
                        $commission_subtotal_total += $commission_subtotal;
                        $sunday_commission_total += $sunday_commission;
                        $general_total += $date_total;
                        
                        @endphp
                        <tr>
                            <td>{{ $date }}</td>
                            <td>{{ $commission }}</td>
                            <td>{{ $barber_commission }}</td>
                            <td>{{ $commission_subtotal }}</td>
                            <td>{{ $sunday_commission }}</td>
                            <td>{{ $date_total }}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <th>Totals:</th>
                        <th>{{ $commission_total }}</th>
                        <th>{{ $barber_commission_total }}</th>
                        <th>{{ $commission_subtotal_total }}</th>
                        <th>{{ $sunday_commission_total }}</th>
                        <th>{{ $general_total }}</th>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>