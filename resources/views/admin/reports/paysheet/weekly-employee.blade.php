@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de nomina semanal por empleado
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/weekly/paysheet/employee')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="employee_id">Empleado: <code>{{$employee->job}}</code></label>
                            <select name="employee_id" id="employee_id" class="form-control select2">
                                @foreach($employees as $employe)
                                    <option 
                                        @if($employe->id == $employee->id) selected="selected" @endif
                                        value="{{$employe->id}}"
                                    >{{$employe->short_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="alert alert-danger" id="paysheet-error" hidden></div>
                            <div class="alert alert-success" id="paysheet-success" hidden></div>
                        </div>

                        <input type="hidden" id="original_start" value="{{$start}}">
                        <input type="hidden" id="original_end" value="{{$end}}">
                        <input type="hidden" id="original_employee_id" value="{{$employee->id}}">
                        <input type="hidden" id="paysheet_id" value="{{ $paysheet ? $paysheet->id : 0 }}">

                        <div class="form-group">
                            <input id="search" type="submit" class="btn btn-default hide-in-print" value="Buscar">
                            @if($user->isA('super-admin'))
                                <input type="button" class="btn btn-success hide-in-print" onClick="updatePaysheet()" value="Guardar">
                            @endif
                        </div>
                    </form>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Día garantizado</th>
                            </tr>
                        </thead>

                        <tbody>
                             @php
                                $total = 0;
                                $upper_total = 0;
                                $salary_total = 0;
                                $faltas = [];
                                $no_asistencias = count($dates);
                                $no_puntualidad = count($dates);
                            @endphp
                            @foreach($dates as $date)
                                @php
                                    $falta = true;
                                    $atiempo = false;
                                    $schedule = $employee->schedules && $employee->schedules->count() > 0 ? $employee->schedules->where('date', $date)->first() : null;
                                    $descanso = $schedule ? $schedule->turn->is_rest : false;
                                    $rest_name = $descanso ? $schedule->turn->name : '';
                                    $is_payed = $descanso ? $schedule->turn->is_payed : false;
                                    $attendances = $employee->attendances->where('date', $date);
                                    $attendances_count = !empty($attendances) ? $attendances->count() : 0;
                                    $has_punctuality = false;
                                @endphp
                                @if(in_array($employee->job, ['Supervisor', 'Encargado']))
                                    <?php $attendance = ['2']; ?>
                                @else
                                    <?php $attendance = $employee->attendances->where('date', $date); ?>
                                @endif
                                @if(count($attendance))
                                    <?php $falta = false; ?>
                                    <?php $no_asistencias -= 1; ?>
                                    <?php $salary_total += $employee->salary; ?>
                                    @if(in_array($employee->job, ['Supervisor', 'Encargado']))
                                        <?php $atiempo = true; ?>
                                        <?php $no_puntualidad -= 1; ?>
                                    @elseif($schedule)
                                        @if (startltqend($attendance->first()->time, $schedule->turn->start))
                                            <?php $atiempo = true;?>
                                            <?php $no_puntualidad -= 1;?>
                                            @php
                                                $attendances = $employee->attendances->where('date', $date);
                                            @endphp
                                            @if($attendances_count >= 3)
                                                @php
                                                    $attendance_values = $attendances->values(); 
                                                    $start1 = \Carbon\Carbon::parse($attendance_values->get(1)->time);
                                                    $end1 = \Carbon\Carbon::parse($attendance_values->get(2)->time);
                                                @endphp
                                                @if($start1->diffInMinutes($end1) <= $schedule->rest_minutes)
                                                    <?php $no_puntualidad -= 1; ?>
                                                    <?php $has_punctuality = true; ?>
                                                @endif
                                            @else
                                                <?php $no_puntualidad -= 1; ?>
                                                <?php $has_punctuality = true; ?>
                                            @endif
                                        @endif
                                    @endif
                                @endif
                                @if($descanso)
                                    <?php 
                                    $falta = true;
                                    $no_asistencias -= 1; 
                                    $atiempo = true; 
                                    $no_puntualidad -= 1;
                                    ?>
                                @endif
                                <tr @if($falta) class="danger" @endif>
                                    <td @if($falta && !$descanso) colspan="4" @endif>
                                        {{$date}}
                                        @if($descanso)
                                            <small>{{$rest_name}}</small>
                                        @else
                                            @if($falta)
                                                <?php $faltas[] = $date; ?>
                                                <small>Falta</small>
                                            @endif
                                        @endif
                                    </td>
                                    @if($descanso)
                                        <td>
                                            @if($is_payed)
                                            <?php 
                                                $salary_total += $employee->salary;
                                                $upper_total += $employee->salary; 
                                            ?>
                                            $ {{$employee->salary}}
                                            @else
                                                $0.00
                                            @endif
                                        </td>
                                        {{-- <td>
                                            $0.00
                                        </td>
                                        <td>
                                            $0.00
                                        </td> --}}
                                    @endif
                                    @if(!$falta)
                                        <?php $upper_total += $employee->salary; ?>
                                        <td>
                                            $ {{ number_format($employee->salary, 2, '.', ',') }}
                                        </td>
                                    @endif

                                </tr>
                            @endforeach
                            @if(in_array($employee->job, ['Supervisor', 'Encargado']))
                                <?php $upper_total = count($dates) * $employee->salary; ?>
                            @endif
                            <tr>
                                <td>Total:</td>
                                <td>$ {{ number_format($upper_total, 2, '.', ',') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            {{-- ! SHOW IF ADMON ONLY --}}
            @if(!empty($current_subsidiary) && $current_subsidiary->id == 7)
            <div class="panel panel-default">
                <div class="panel-heading">
                    Ventas a empleado pendientes por pagar
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Producto(s)</th>
                                <th>Pagado</th>
                                <th>Pendiente</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                @foreach ($employee_pending_sales as $sale)
                                <td>
                                    @foreach ($sale->products as $product)
                                    ({{$product->qty}}) {{ $product->product->name }} <br>
                                    @endforeach
                                </td>
                                <td>{{ $sale->paidAmount() }}</td>
                                <td>{{ $sale->total - $sale->paidAmount() }}</td>
                                @endforeach
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table">
                        <tbody>
                            <?php 
                                $big_total = $upper_total; 
                                list($products_total, $comisionProductos, $products_qty) = $employee->getEmployeeProductCommission();
                            ?>
                            <tr>
                                <td colspan="4">Productos vendidos:</td>
                                <td>{{ $products_qty }}</td>
                            </tr>
                            {{-- <tr>
                                <td colspan="4">Citas atendidas:</td>
                                <td>{{ $employee->getDiariesQty() }}</td>
                            </tr> --}}
                            <tr>
                                <td colspan="4">Total salarios:</td>
                                <td>$ <span id="salarios-text">{{ number_format($paysheet ? $paysheet->total : $upper_total, 2, '.', ',') }}</span></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <a 
                                        href="/home/reports/attendance-card?employee_id={{$employee->id}}&start={{$start}}&end={{$end}}"
                                        target="_blank" 
                                    >
                                        Bono por asistencia:
                                    </a>
                                </td>
                                <?php $ba_money = ($upper_total * 10) / 100; ?>
                                <?php $ba = 0; ?>
                                <?php $pay_ba = false; ?>
                                @if($no_asistencias == 0)
                                    <?php $pay_ba = true; ?>
                                    <?php $ba = $ba_money; ?>
                                    <?php $big_total += $paysheet ? ($paysheet->asistencia) : $ba; ?> 
                                @endif
                                <?php $asistencia = $paysheet ? ($paysheet->asistencia > $ba ? $paysheet->asistencia : $ba) : $ba; ?>
                                <td style="align:left;" class="d-flex"> 
                                    @if($user->isA('super-admin'))
                                        @php $check_assistance = ($paysheet && $paysheet->asistencia_pay == 1) || (!$paysheet && $pay_ba);  @endphp
                                        <input type="checkbox" class="check-attendance" id="check-attendance" @if($check_assistance) checked @endif> 
                                    @endif
                                </td>
                                <td>
                                    <input type="hidden" id="attendance" name="attendance" value="{{ $ba_money }}">
                                    $ <span id="attendance-text">{{ number_format($ba, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <?php $bp_money = ($upper_total * 10) / 100; ?>
                            <?php $bp = 0; ?>
                            <?php $pay_bp = false; ?>
                            @if($no_puntualidad == 0)
                                <?php $pay_bp = true; ?>
                                <?php $bp = $bp_money; ?>
                                <?php $big_total += $paysheet ? ($paysheet->puntualidad) : $bp; ?> 
                            @endif
                            <?php $puntualidad = $paysheet ? ($paysheet->puntualidad > $bp ? $paysheet->puntualidad : $bp) : $bp; ?>
                            <tr>
                                <td colspan="3">
                                    <a 
                                        href="/home/reports/attendance-card?employee_id={{$employee->id}}&start={{$start}}&end={{$end}}"
                                        target="_blank" 
                                    >
                                        Bono por puntualidad:
                                    </a>
                                </td>
                                <td style="align:left;"> 
                                    @if($user->isA('super-admin'))
                                        @php $check_punctuality = ($paysheet && $paysheet->puntualidad_pay == 1) || (!$paysheet && $has_punctuality); @endphp
                                        <input type="checkbox" class="check-punctuality" id="check-punctuality" @if($check_punctuality) checked @endif> 
                                    @endif
                                </td>
                                <td>
                                    <input type="hidden" id="punctuality" name="punctuality" value="{{ $bp_money }}">
                                    $ <span id="punctuality-text">{{ number_format($puntualidad, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <?php $porcen_productividad = 20; ?>
                            @if($employee->hasJob('Cajero'))
                                <?php $porcen_productividad = 25; ?>
                            @endif
                            <?php $bpr = ($upper_total * $porcen_productividad) / 100; ?>
                            <?php $big_total += $bpr; ?>
                            <?php $productividad = $paysheet ? ($paysheet->productividad > $bpr ? $paysheet->productividad : $bpr) : $bpr; ?>
                            <tr>
                                <td colspan="3">
                                    <a 
                                        href="/home/reports/issues?employee_id={{$employee->id}}&start={{$start}}&end={{$end}}"
                                        target="_blank" 
                                    >
                                        Bono por productividad:
                                    </a>
                                </td>
                                <td style="align:left;"> 
                                    @if($user->isA('super-admin'))
                                        @php $check_productivity = ($paysheet && $paysheet->productividad_pay == 1) || (!$paysheet && $employee->issues->count() == 0); @endphp
                                        <input type="checkbox" class="check-productivity" id="check-productivity" @if($check_productivity) checked @endif>  
                                    @endif
                                </td>
                                <td>
                                    <input type="hidden" id="productivity" name="productivity" value="{{ $bpr }}">
                                    $ <span id="productivity-text">{{ number_format($productividad, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">Dia extra:</td>
                                @php
                                    $extra_day = 0;
                                    $eam = 200;
                                    $extra_count = $employee->schedules()
                                                ->extraDay()->dates([$start, $end])->count();
                                    $extra_count += $employee->schedules()
                                                ->TurnMixt()->dates([$start, $end])->count();
                                    $extra_day = (($extra_count - 1) <= 0 ? 0 : ($extra_count - 1)) * $eam;
                                    $big_total += $paysheet ? ($paysheet->extra > $extra_day ? $paysheet->extra : $extra_day) : $extra_day;
                                @endphp
                                <td style="align:left;"> 
                                    @if($user->isA('super-admin'))
                                    <input type="hidden" id="extraday" name="extraday" value="{{ $extra_day }}">
                                    <input type="checkbox" class="check-extraday" id="check-extraday" @if($paysheet && $paysheet->extra_pay == 1) checked @endif> 
                                    @endif
                                </td>
                                <td>
                                    $ <span id="extraday-text">{{ number_format($extra_day, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <?php
                            $percepciones = $employee->percepciones->sum('amount');
                            $big_total += $percepciones;
                            ?>
                            <tr>
                                <td colspan="4">Comision por productos:</td>
                                <?php
                                    $big_total += $comisionProductos;
                                ?>

                                <td>
                                    $ <span id="commission-text">{{ number_format($comisionProductos, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">Percepciones:</td>
                                <td>$ <span id="perceptions-text">{{ number_format($percepciones, 2, '.', ',') }}</span></td>
                            </tr>
                            <tr>
                                <td colspan="4">Gran total:</td>
                                <td>$ <span id="total-text">{{ number_format($paysheet ? ($paysheet->total_ingresos > $big_total ? $paysheet->total_ingresos : $big_total) : $big_total, 2, '.', ',') }}</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            @include('admin.reports.paysheet.percepcione', [
                'employee' => $employee,
                'objects' => 'percepciones',
                'dates' => [$start, $end]
            ])

            <div class="panel panel-default">
                <div class="panel-heading">Descuentos</div>

                <div class="panel-body">
                    <table class="table">
                        <tbody>
                            <?php $desc_total = 0; ?>
                            <?php $prestamo = $paysheet ? $paysheet->prestamo : 0; ?>
                            <tr>
                                <td colspan="3">Prestamo:</td>
                                <td>
                                    $ <span id="prestamo-text"> {{ number_format($prestamo, 2, '.', ',') }} </span>
                                </td>
                            </tr>
                            <?php
                            // $total_faltas = count($faltas) * 200;
                            $total_faltas = 0;
                            $desc_total += $total_faltas;
                            $deducciones = $employee->deducciones->sum('amount');
                            // $deducciones = $paysheet ? ($paysheet->otras > $deducciones ? $paysheet->otras : $deducciones) : $deducciones;
                            $desc_total += $deducciones;
                            ?>
                            <tr>
                                <td colspan="3">Faltas:</td>
                                <td>
                                    <input type="hidden" id="faults" name="faults" value="{{ $total_faltas }}">
                                    $ <span id="faults-text">{{ number_format($total_faltas, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">Uniforme:</td>
                                <td>
                                    <?php $uniforme = $paysheet ? $paysheet->uniforme : 0; ?>
                                    $ <span id="uniform-text">{{ number_format($uniforme, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">Infonavit:</td>
                                @php
                                $monto_infonavit = 0;
                                if ($employee->monto_infonavit) {
                                    $monto_infonavit = (float) $employee->monto_infonavit;
                                }
                                $desc_total += $monto_infonavit;
                                $infonavit = $paysheet ? $paysheet->infonavit : $monto_infonavit; 
                                @endphp
                                <td>
                                    $ <span id="infonavit-text">{{ number_format($infonavit, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">Deducciones:</td>
                                <td>$ <span id="deductions-text">{{ number_format($deducciones, 2, '.', ',') }}</span></td>
                            </tr>
                            <tr>
                                <td colspan="3">Total:</td>
                                <td>$ <span id="total-deducciones-text">{{ number_format($paysheet ? ($paysheet->total_deducciones > $desc_total ? $paysheet->total_deducciones : $desc_total) : $desc_total, 2, '.', ',') }}</span></td>
                            </tr>
                        </tbody>
                        
                    </table>
                </div>
            </div>

            @include('admin.reports.paysheet.percepcione', [
                'employee' => $employee,
                'objects' => 'deducciones',
                'dates' => [$start, $end]
            ])

            <?php
            $neto = $big_total - $desc_total;
            $depositado = $employee->deposits->where('start', $start)->where('end', $end)->sum('amount');
            $diferencia = $neto - $depositado;
            ?>

            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td colspan="3">Neto a pagar:</td>
                                <td>$ <span id="neto-text">{{ number_format($paysheet ? ($paysheet->neto > $neto ? $paysheet->neto : $neto) : $neto, 2, '.', ',') }}</span></td>
                            </tr>
                            <tr>
                                <td colspan="3">Depositado:</td>
                                <td>
                                    <input type="hidden" name="depositado" id="depositado" value="{{ $depositado }}" />
                                    $ {{ number_format($depositado, 2, '.', ',') }}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">Diferencia:</td>
                                <td>$ <span id="diferencia-text">{{ number_format($paysheet ? ($paysheet->diferencia > $diferencia ? $paysheet->diferencia : $diferencia) : $diferencia, 2, '.', ',') }}</span></td>
                            </tr>
                        </tbody>
                        
                    </table>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td colspan="5">
                                Observaciones:<br>
                                <textarea id="note" style="width: 100%; height: 100px;">{{$paysheet ? $paysheet->note : ''}}</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">Numero de cuenta:</td>
                            <td><span id="neto-text">{{ $employee->account_number }}</span></td>
                        </tr>
                        <tr>
                            <td colspan="3">Nombre del banco:</td>
                            <td><span id="neto-text">{{ $employee->bank_name }}</span></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="/js/nomina.js?v={{strtotime('now')}}"></script>
@endsection
