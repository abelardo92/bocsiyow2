@extends('layouts.app')

@section('content')
<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Resumen de nomina
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/weekly/paysheet/resume')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-1">
                                    <label>Fecha de: </label>
                                </div>
                                <div class="col-md-8">
                                    <div class="input-group input-daterange">
                                        <input type="text" class="form-control" name="start" value="{{$start}}">
                                        <span class="input-group-addon">al</span>
                                        <input type="text" class="form-control" name="end" value="{{$end}}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="">
                        <form action="{{url('/home/reports/weekly/paysheet/resume')}} " method="post" onkeypress="return event.keyCode != 13;">
                            {{ csrf_field() }}
                            <input type="hidden" name="start" value="{{$start}}">
                            <input type="hidden" name="end" value="{{$end}}">
                            <div class="pull-right">
                                <input type="submit" class="btn btn-primary" value="Guardar">
                            </div>
                            <!--
                            <div class="clearfix"></div>
                            <fieldset class="table-responsiv" style="margin-top: 69px;">
                                
                                <table class="table table-striped table-scroll" style="font-size: 0.8em; position: fixed; margin-top: -69px; background: white;">
                                    <thead>
                                        <tr>
                                            <th>
                                                Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </th>
                                            <th>Total</th>
                                            <th colspan="2">Asistencia</th>
                                            <th colspan="2">Puntualidad</th>
                                            <th colspan="2">Productividad</th>
                                            <th colspan="2">Excelencia</th>
                                            <th colspan="2">Dia&nbsp;extra</th>
                                            <th>Otros ingresos</th>
                                            <th>Reparaciones</th>
                                            <th>Propinas</th>
                                            <th>Comision productos</th>
                                            <th>Total ingresos</th>
                                            <th>Prestamo</th>
                                            <th colspan="2">Faltas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                            <th>Uniforme</th>
                                            <th>Infonavit</th>
                                            <th>Otras</th>
                                            <th>Reparaciones</th>
                                            <th>Total deducciones</th>
                                            <th>Neto a pagar</th>
                                            <th>Depositado</th>
                                            <th>Diferencia</th>
                                            <th>Nombre</th>
                                        </tr>
                                    </thead>
                                </table>
                                -->
                                <table class="table table-striped" style="font-size: 0.8em; ">
                                    <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Total</th>
                                            <th colspan="2">Asistencia</th>
                                            <th colspan="2">Puntualidad</th>
                                            <th colspan="2">Productividad</th>
                                            <th colspan="2">Excelencia</th>
                                            <th colspan="2">Dia extra</th>
                                            <th>Otros ingresos</th>
                                            <th>Reparaciones</th>
                                            <th>Propinas</th>
                                            <th>Comision productos</th>
                                            <th>Total ingresos</th>
                                            <th>Prestamo</th>
                                            <th colspan="2">Faltas</th>
                                            <th>Uniforme</th>
                                            <th>Infonavit</th>
                                            <th>Otras</th>
                                            <th>Reparaciones</th>
                                            <th>Total deducciones</th>
                                            <th>Neto a pagar</th>
                                            <th>Depositado</th>
                                            <th>Diferencia</th>
                                            <th>Nombre</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $tabindex1 = 1;
                                            $tabindex2 = 2;
                                            $tabindex3 = 3;
                                            $tabindex4 = 4;
                                            $tabindex5 = 5;
                                        @endphp
                                        @foreach($employees as $employee)
                                            <?php $total_salario = 0;?>
                                            <?php $total_ingresos = 0;?>
                                            <?php $total_faltas = 0;?>
                                            <?php $total_deducciones = 0;?>
                                            
                                            @php
                                                $total = 0;
                                                $upper_total = 0;
                                                $salary_total = 0;
                                                $repairs = 0;
                                                $total_repairs = 0;
                                                $discount_repairs = 0;
                                                $total_discount_repairs = 0;
                                                $faltas = [];
                                                $no_asistencias = count($dates);
                                                $no_puntualidad = count($dates);
                                            @endphp
                                            @foreach($dates as $date)
                                                @php
                                                    $falta = true;
                                                    $atiempo = false;
                                                    $schedule = $employee->schedules()->with(['turn'])->where('date', $date)->first();
                                                    $descanso = $schedule ? $schedule->turn->is_rest : false;
                                                    $is_payed = $descanso ? $schedule->turn->is_payed : false;
                                                @endphp

                                                <?php
                                                $employee_repaired_sale_ids = $employee->sales()->where('date', $date)->finished()->notCanceled()->repaireds()->get()->pluck('id');
                                                $services_repaired = App\SaleService::whereIn('sale_id', $employee_repaired_sale_ids)->get()->groupBy('service_id');
                                                $attendances = $employee->attendances()->date($date)->get();
                                                ?>

                                                @if(in_array($employee->job, ['Supervisor', 'Encargado']))
                                                    <?php $attendance = ['2'];?>
                                                @else
                                                    <?php //$attendance = $employee->attendances()->date($date)->get();?>
                                                @endif
                                                @if(count($attendances))
                                                    <?php $falta = false;?>
                                                    <?php $no_asistencias -= 1;?>
                                                    <?php $salary_total += $employee->salary;?>
                                                    @if(in_array($employee->job, ['Supervisor', 'Encargado']))
                                                        <?php $atiempo = true;?>
                                                        <?php $no_puntualidad -= 1;?>
                                                    @elseif($schedule)
                                                        @if (startltqend($attendances->first()->time, $schedule->turn->start))
                                                            <?php $atiempo = true;?>
                                                            <?php $no_puntualidad -= 1;?>
                                                             @php
                                                                /*
                                                                $attendances = $employee->attendances()->date($date)->get();
                                                                */
                                                            @endphp
                                                            @if($attendances->count() >= 3)
                                                                @php
                                                                    $start1 = \Carbon\Carbon::parse($attendances[1]->time);
                                                                    $end1 = \Carbon\Carbon::parse($attendances[2]->time);
                                                                @endphp
                                                                @if($start1->diffInMinutes($end1) <= $schedule->rest_minutes)
                                                                    <?php $no_puntualidad -= 1;?>
                                                                @endif
                                                            @else
                                                                <?php $no_puntualidad -= 1;?>
                                                            @endif
                                                        @endif
                                                    @endif
                                                @endif
                                                @if($descanso)
                                                    <?php $falta = true;?>
                                                    <?php $no_asistencias -= 1;?>
                                                    <?php $atiempo = true;?>
                                                    <?php $no_puntualidad -= 1;?>
                                                @endif
                                                @if($descanso)
                                                    @if($employee->job != 'Barbero')
                                                        @if($is_payed)
                                                            <?php $salary_total += $employee->salary;?>
                                                            <?php $upper_total += $employee->salary;?>
                                                        @endif
                                                    @endif
                                                @else
                                                    @if($falta)
                                                        <?php $faltas[] = $date;?>
                                                    @endif
                                                @endif

                                                @if(!$falta)
                                                    <?php
                                                    $sale_ids = $employee->sales()->where('date', $date)
                                                        ->finished()->notCanceled()->get()->pluck('id');
                                                    $services = App\SaleService::whereIn('sale_id', $sale_ids)->get()->groupBy('service_id');
                                                    $subtotal = 0;
                                                    ?>
                                                    @foreach($services as $service_group)
                                                        @php
                                                            $subtotal += ($service_group->first()->service->cost * $service_group->count());
                                                        @endphp
                                                    @endforeach
                                                    @php
                                                    $total += $subtotal;
                                                    @endphp
                                                    @if($employee->salary > $subtotal)
                                                        <?php $upper = $employee->salary;?>
                                                    @else
                                                        <?php $upper = $subtotal;?>
                                                    @endif
                                                    <?php $upper_total += $upper;?>
                                                @endif

                                                <?php
                                                /*
                                                $sale_ids = $employee->sales()->where('date', $date)
                                                    ->finished()->notCanceled()->repaireds()->get()->pluck('id');
                                                $services = App\SaleService::whereIn('sale_id', $sale_ids)
                                                    ->get()->groupBy('service_id');
                                                    */
                                                ?>
                                                @foreach($services_repaired as $service_group)
                                                    @php

                                                        $repairs += ($service_group->first()->price * $service_group->sum('qty'));
                                                    @endphp
                                                @endforeach

                                                <?php
                                                $sale_ids = $employee->repairs()->where('date', $date)
                                                    ->finished()->notCanceled()->get()->pluck('id');
                                                $services = App\SaleService::whereIn('sale_id', $sale_ids)
                                                    ->get()->groupBy('service_id');
                                                ?>
                                                @foreach($services as $service_group)
                                                    @php

                                                        $discount_repairs += ($service_group->first()->price * $service_group->sum('qty'));
                                                    @endphp
                                                @endforeach

                                                @php
                                                        $total_repairs += $repairs;
                                                        $total_discount_repairs += $discount_repairs;
                                                @endphp

                                            @endforeach

                                            @if(in_array($employee->job, ['Supervisor', 'Encargado']))
                                                <?php $upper_total = count($dates) * $employee->salary;?>
                                            @endif
                                            {{-- @if($employee->id == 19)
                                               {{dd($upper_total)}}
                                            @endif --}}
                                            
                                            <?php $big_total = $upper_total;?>
                                            <tr>
                                                <td>
                                                {{-- {{$employee->job}} --}}
                                                    @if($employee->hasJob('Barbero'))
                                                        <a href="{{url('/home/reports/weekly/paysheet/barber?employee_id='.$employee->id.'&start='.$start.'&end='.$end)}}" target="_blank">{{$employee->short_name}}</a>
                                                    @else
                                                        <a href="{{url('/home/reports/weekly/paysheet/employee?employee_id='.$employee->id.'&start='.$start.'&end='.$end)}}" target="_blank">{{$employee->short_name}}</a>
                                                    @endif
                                                    @if(\App\Paysheet::where('key', "{$start}-{$end}")->where('employee_id', $employee->id)->first())
                                                        Nomina generada
                                                    @endif
                                                    <input type="hidden" name="employee[{{$employee->id}}][id]" value="{{$employee->id}}">
                                                </td>
                                                <input type="hidden" name="employee[{{$employee->id}}][total]" value="{{ number_format($upper_total, 2, '.', '') }}" id="total-{{$employee->id}}-hidden">
                                                <td> $ {{ number_format($upper_total, 2, '.', '') }}</td>
                                                @if($no_asistencias == 0)
                                                    <?php $ba = ($upper_total * 10) / 100;?>
                                                    <?php $big_total += $ba;?>
                                                @else
                                                    <?php $ba = 0?>
                                                @endif
                                                <td id="asistencia-{{$employee->id}}-text">$ {{ number_format($ba, 2, '.', '') }}</td>
                                                <td><input type="checkbox" name="attendance[{{$employee->id}}]" @if($no_asistencias == 0) checked="checked" @endif data-employee-id="{{$employee->id}}" class="check-attendance"></td>
                                                <input type="hidden" name="employee[{{$employee->id}}][asistencia]" value="{{ number_format($ba, 2, '.', '') }}" id="asistencia-{{$employee->id}}-hidden">
                                                @if($no_puntualidad == 0)
                                                    <?php $bp = ($upper_total * 10) / 100;?>
                                                    <?php $big_total += $bp;?>
                                                @else
                                                    <?php $bp = 0?>
                                                @endif
                                                <td id="puntualidad-{{$employee->id}}-text">$ {{ number_format($bp, 2, '.', '') }}</td>
                                                <td><input type="checkbox" name="appoint[{{$employee->id}}]" @if($no_puntualidad == 0) checked="checked" @endif data-employee-id="{{$employee->id}}"  class="check-puntualidad"></td>
                                                <input type="hidden" name="employee[{{$employee->id}}][puntualidad]" value="{{ number_format($bp, 2, '.', '') }}" id="puntualidad-{{$employee->id}}-hidden">
                                                <?php
                                                $porcen_productividad = 0.20;
                                                if($employee->hasJob('Cajero')) {
                                                    $porcen_productividad = 0.25;
                                                }
                                                $productividad = $upper_total * $porcen_productividad;
                                                if ($employee->sales()->whereBetween('date', [$start, $end])
                                                    ->finished()->notCanceled()->repaireds()->get()->count() > 0) {
                                                    $productividad = 0;
                                                }
                                                if ($employee->issues()->whereBetween('date', [$start, $end])->get()->count() >= 3) {
                                                    $productividad = 0;
                                                }
                                                $big_total += $productividad;
                                                ?>
                                                <td id="productividad-{{$employee->id}}-text"> $ {{ number_format($productividad, 2, '.', '') }}</td>
                                                <td><input type="checkbox" name="productivity[{{$employee->id}}]" checked="checked" data-employee-id="{{$employee->id}}"  class="check-productividad"></td>
                                                <input type="hidden" name="employee[{{$employee->id}}][productividad]" value="{{ number_format($productividad, 2, '.', '') }}" id="productividad-{{$employee->id}}-hidden">
                                                <input type="hidden" value="{{ number_format($porcen_productividad, 2, '.', '') }}" id="porcen-productividad-{{$employee->id}}-hidden">
                                                <?php $excellence = 0; ?>
                                                @if($employee->has_excellence)
                                                    <?php $excellence = ($upper_total * 40) / 100; ?>
                                                @endif
                                                <?php $big_total += $excellence; ?>
                                                <td id="excellence-{{$employee->id}}-text"> $ {{ number_format($excellence, 2, '.', '') }}</td>
                                                <td>
                                                    <input type="checkbox" name="excellence[{{$employee->id}}]" @if($employee->has_excellence) checked="checked" @endif class="check-excellence" data-val="{{ number_format($excellence, 2, '.', '') }}" data-employee-id="{{$employee->id}}">
                                                </td>
                                                <input type="hidden" name="employee[{{$employee->id}}][excellence]" value="{{ number_format($excellence, 2, '.', '') }}" data-val="{{ number_format($excellence, 2, '.', '') }}" id="excellence-{{$employee->id}}-hidden">

                                                @php
                                                    $extra_day = 0;
                                                    $eam = $employee->hasJob('Barbero') ? 100 : 200;
                                                    $extra_count = $employee->schedules()
                                                                ->extraDay()->dates([$start, $end])->count();
                                                    $extra_count += $employee->schedules()
                                                                ->TurnMixt()->dates([$start, $end])->count();
                                                    $extra_day = (($extra_count - 1) <= 0 ? 0 : ($extra_count - 1)) * $eam;
                                                    
                                                @endphp
                                                <td id="extra-{{$employee->id}}-text"> $ {{ number_format($extra_day, 2, '.', '') }}</td>
                                                <td><input type="checkbox" name="extra[{{$employee->id}}]" @if($extra_day > 0) checked="checked" @endif  class="check-extra" data-employee-id="{{$employee->id}}"></td>
                                                <input type="hidden" name="employee[{{$employee->id}}][extra]" value="{{ number_format($extra_day, 2, '.', '') }}" data-val="{{ number_format($extra_day, 2, '.', '') }}" id="extra-{{$employee->id}}-hidden" data-employee-id="{{$employee->id}}">
                                                <?php
                                                    //$big_total += $extra_day;
                                                    //$total_ingresos = $big_total;
                                                    $otros_ingresos = $employee->percepciones()->dates([$start, $end])->sum('amount');
                                                    $total_ingresos += $otros_ingresos;
                                                    ?>
                                                <td>
                                                    <input type="text" name="employee[{{$employee->id}}][otros_ingresos]" value="{{ number_format($otros_ingresos, 2, '.', '') }}" id="otros_ingresos-{{$employee->id}}-hidden" data-employee-id="{{$employee->id}}" class="otros_ingresos" style="width: 50px;" tabindex="{{$tabindex1}}">
                                                </td>

                                                <td id="repairs-{{$employee->id}}-text"> $ {{ number_format($repairs, 2, '.', '') }}</td>
                                                <?php
                                                // $repairs = 0;
                                                $total_ingresos += $repairs;
                                                ?>
                                                <input type="hidden" name="employee[{{$employee->id}}][repairs]" value="{{ number_format($repairs, 2, '.', '') }}" id="repairs-{{$employee->id}}-hidden">

                                                <?php 
                                                $cardtips = $employee->sales()->dates([$start, $end])->where('tip_in', 'tarjeta')->sum('tip'); 
                                                $total_ingresos += $cardtips;
                                                ?>
                                                <td id="cardtips-{{$employee->id}}-text"> $ {{ number_format($cardtips, 2, '.', '') }}</td>
                                                <input type="hidden" name="employee[{{$employee->id}}][cardtips]" value="{{ number_format($cardtips, 2, '.', '') }}" id="cardtips-{{$employee->id}}-hidden">

                                                <?php
                                                $productos = 0;
                                                $prods = App\SaleProduct::whereIn('sale_id', $employee->sales()->dates([$start, $end])
                                                    ->finished()->notCanceled()->pluck('id'))->get();

                                                foreach ($prods->filter(function ($item) {
                                                    return $item->product->has_commission;
                                                })->groupBy('product_id') as $group) {
                                                    foreach ($group as $sale_prod) {
                                                        $productos += $group->count() * $sale_prod->price;
                                                    }
                                                }
                                                ?>
                                                <?php 
                                                $products = $productos * 0.10;
                                                $total_ingresos += $products;
                                                ?>

                                                <td id="products-{{$employee->id}}-text"> $ {{ number_format($products, 2, '.', '') }}</td>
                                                <input type="hidden" name="employee[{{$employee->id}}][products]" value="{{ number_format($products, 2, '.', '') }}" id="products-{{$employee->id}}-hidden">

                                                <td id="total_ingresos-{{$employee->id}}-text"> $ {{ number_format($total_ingresos, 2, '.', '') }}</td>
                                                <input type="hidden" name="employee[{{$employee->id}}][total_ingresos]" value="{{ number_format($total_ingresos, 2, '.', '') }}" id="total_ingresos-{{$employee->id}}-hidden">

                                                <?php $desc_total = 0;?>
                                                <td><input type="text" name="employee[{{$employee->id}}][prestamo]" style="width: 50px;" class="prestamo" data-employee-id="{{$employee->id}}" id="prestamo-{{$employee->id}}-hidden" tabindex="{{$tabindex2}}"></td>
                                                <?php
                                                    $total_faltas = count($faltas) * 200;
                                                    $desc_total += $total_faltas;
                                                    ?>
                                                <td  id="total_faltas-{{$employee->id}}-text"> $ {{ number_format($total_faltas, 2, '.', '') }}</td>
                                                <td><input type="checkbox" name="total_faltas[{{$employee->id}}]" @if(count($faltas) > 0) checked="checked" @endif data-employee-id="{{$employee->id}}" class="check-faltas"></td>
                                                <input type="hidden" name="employee[{{$employee->id}}][total_faltas]" value="{{$total_faltas}}" data-val="{{ number_format($total_faltas, 2, '.', '') }}" id="total_faltas-{{$employee->id}}-hidden">
                                                <td><input type="text" name="employee[{{$employee->id}}][uniforme]" style="width: 50px;" class="uniforme" data-employee-id="{{$employee->id}}" id="uniforme-{{$employee->id}}-hidden" tabindex="{{$tabindex3}}"></td>
                                                <td> $ {{ number_format($employee->monto_infonavit, 2, '.', '') }}</td>
                                                <?php
                                            $desc_total += $employee->monto_infonavit;
                                            ?>
                                                <input type="hidden" name="employee[{{$employee->id}}][monto_infonavit]" value="{{$employee->monto_infonavit}}" id="monto_infonavit-{{$employee->id}}-hidden">
                                                <?php
                                                    $otros_deducciones = $employee->deducciones()->dates([$start, $end])->sum('amount');
                                                    $desc_total += $otros_deducciones;
                                                ?>
                                                <td><input type="text" name="employee[{{$employee->id}}][otras]" style="width: 50px;" class="otras" data-employee-id="{{$employee->id}}" id="otras-{{$employee->id}}-hidden" tabindex="{{$tabindex4}}" value="{{ number_format($otros_deducciones, 2, '.', '') }}"></td>
                                                    <?php
                                                    // $discount_repairs = 0;
                                                    $desc_total += $discount_repairs;
                                                    ?>

                                                <td id="discount-repairs-{{$employee->id}}-text"> $ {{ number_format($discount_repairs, 2, '.', '') }}</td>
                                                <input type="hidden" name="employee[{{$employee->id}}][discount-repairs]" value="{{ number_format($discount_repairs, 2, '.', '') }}" id="discount-repairs-{{$employee->id}}-hidden">

                                                <td id="desctotal-{{$employee->id}}-text"> $ {{ number_format($desc_total, 2, '.', '') }}</td>
                                                <input type="hidden" name="employee[{{$employee->id}}][desctotal]" value="{{$desc_total}}" id="desctotal-{{$employee->id}}-hidden">

                                                @php 
                                                $neto = ($total_ingresos - $desc_total);
                                                $depositado = $employee->deposits()->where('start', $start)->where('end', $end)->sum('amount'); 
                                                @endphp
                                                <td id="neto-{{$employee->id}}-text"> $ {{ number_format($neto, 2, '.', '') }}</td>
                                                <input type="hidden" name="employee[{{$employee->id}}][neto]" value="{{number_format($neto, 2, '.', '') }}" id="neto-{{$employee->id}}-hidden">
                                                <td><input type="text" name="employee[{{$employee->id}}][depositado]"  style="width: 50px;" class="depositado" data-employee-id="{{$employee->id}}" id="depositado-{{$employee->id}}-hidden" tabindex="{{$tabindex5}}" value="{{number_format($depositado, 2, '.', '')}}"></td>
                                                <input type="hidden" name="employee[{{$employee->id}}][diferencia]" id="diferencia-{{$employee->id}}-hidden" value="{{number_format($neto - $depositado, 2, '.', '') }}">
                                                <td id="diferencia-{{$employee->id}}-text"> $ {{number_format($neto - $depositado, 2, '.', '') }}
                                                </td>
                                                <td>
                                                    @if($employee->hasJob('Barbero'))
                                                        <a href="{{url('/home/reports/weekly/paysheet/barber?employee_id='.$employee->id.'&start='.$start.'&end='.$end)}}" target="_blank">{{$employee->short_name}}</a>
                                                    @else
                                                        <a href="{{url('/home/reports/weekly/paysheet/employee?employee_id='.$employee->id.'&start='.$start.'&end='.$end)}}" target="_blank">{{$employee->short_name}}</a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @php
                                                $tabindex1 += 5;
                                                $tabindex2 += 5;
                                                $tabindex3 += 5;
                                                $tabindex4 += 5;
                                                $tabindex5 += 5;
                                            @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                                {{$employees->appends(compact('start', 'end'))->links()}}
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
