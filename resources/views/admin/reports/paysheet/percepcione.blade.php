<div class="panel panel-default">
    <div class="panel-heading">
        {{ ucfirst($objects) }}
        @include('admin.reports.paysheet.modal_object', [
            'employee' => $employee,
            'objects' => $objects,
        ])
    </div>
    <div class="panel-body">
        <table class="table">
            <thead>
                <tr>
                    <th>Concepto</th>
                    <th>Importe</th>
                    <th></th>
                </tr>
            </thead>

            <tbody>
                @foreach($employee->{$objects} as $object)
                    <tr>
                        <td>{{ $object->text }}</td>
                        <td>$ {{ number_format($object->amount, 2, '.', ',') }}</td>
                        <td>
                            <form action="{{route($objects.".destroy", $object->id)}}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <input type="submit" class="btn btn-danger btn-xs" value="Eliminar">
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            
        </table>
    </div>
</div>