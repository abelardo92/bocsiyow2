@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de nomina semanal por barbero
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>
                <div class="panel-body">
                    <form action="{{url('/home/reports/weekly/paysheet/barber')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="employee_id">Barbero: </label>
                            <select name="employee_id" id="employee_id" class="form-control select2">
                                @foreach($barbers as $employee)
                                    <option
                                        @if($employee->id == $barber->id) selected="selected" @endif
                                        value="{{$employee->id}}"
                                    >{{$employee->short_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" id="start" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" id="end" name="end" value="{{$end}}">
                            </div>
                        </div>

                        <input type="hidden" id="original_start" value="{{$start}}">
                        <input type="hidden" id="original_end" value="{{$end}}">
                        <input type="hidden" id="original_employee_id" value="{{$barber->id}}">
                        <input type="hidden" id="paysheet_id" value="{{ $paysheet ? $paysheet->id : 0 }}">

                        <div class="form-group">
                            <input id="search" type="submit" class="btn btn-default hide-in-print" value="Buscar">
                            <a href="/home/reports/weekly/services/barbers?employee_id={{$barber->id}}&start={{$start}}&end={{$end}}" class="btn btn-primary">Reporte de servicios</a>
                            @if($isMerida)
                                <a href="{{ route('reports.weekly.services.barbers.print', [$barber->id, $start, $end]) }}" class="btn btn-default">Imprimir ticket</a>
                            @endif
                            @if($user->isA('super-admin'))
                                <input type="button" class="btn btn-success hide-in-print" onClick="updatePaysheet()" value="Guardar">
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="alert alert-danger" id="paysheet-error" hidden></div>
                            <div class="alert alert-success" id="paysheet-success" hidden></div>
                        </div>
                    </form>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Servicio</th>
                                <th>Sucursal</th>
                                <th>Cantidad</th>
                                <th>Importe</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $sale_ids = $barber->salesFinished->pluck('id');
                                $services = App\SaleService::with('sale')->whereIn('sale_id', $sale_ids)->notCanceled()->notWaiting()->get()->groupBy('service_id');

                                $sunday_total = 0;
                                $sunday_qty = 0;
                                $total = 0;
                            @endphp
                            @foreach($services as $service)
                                @php
                                    $nichupte_subtotal = $tulum_huayacan_subtotal = $others_subtotal = 0;
                                    $nichupte_qty = $tulum_huayacan_qty = $others_qty = 0;

                                    foreach ($service as $serv) {

                                        $commission = $serv->getCommission();
                                        $current_qty = $serv->getUpdatedQty();

                                        if($serv->sale->isSunday()) {
                                            $sunday_qty += $current_qty;
                                            $sunday_total += $commission;
                                            // continue;
                                        }

                                        $subsidiary_id = $serv->sale->subsidiary_id;
                                        $subtotal = $commission;

                                        // * Nichupte
                                        if($subsidiary_id == 4) {
                                            $nichupte_subtotal += $subtotal;
                                            $nichupte_qty += $current_qty;
                                        }
                                        // * TULUM Y HUAYACAN
                                        else if(in_array($subsidiary_id,[4,9])) {
                                            $tulum_huayacan_subtotal += $subtotal;
                                            $tulum_huayacan_qty += $current_qty;
                                        }
                                        else {
                                            $others_subtotal += $subtotal;
                                            $others_qty += $current_qty;
                                        }
                                        $total += $subtotal;
                                    }
                                @endphp
                                @if($nichupte_subtotal > 0)
                                <tr>
                                    <td>{{ $service->first()->service->name }}</td>
                                    <td>NICHUPTE</td>
                                    <td>{{ $nichupte_qty }}</td>
                                    <td>$ {{ $nichupte_subtotal }}</td>
                                </tr>
                                @endif
                                @if($tulum_huayacan_subtotal > 0)
                                <tr>
                                    <td>{{ $service->first()->service->name }}</td>
                                    <td>TULUM Y HUAYACAN</td>
                                    <td>{{ $tulum_huayacan_qty }}</td>
                                    <td>$ {{ $tulum_huayacan_subtotal }}</td>
                                </tr>
                                @endif
                                @if($others_subtotal > 0)
                                <tr>
                                    <td>{{ $service->first()->service->name }}</td>
                                    <td>OTRAS</td>
                                    <td>{{ $others_qty }}</td>
                                    <td>$ {{ $others_subtotal }}</td>
                                </tr>
                                @endif
                            @endforeach
                            {{-- <tr>
                                <td>IMPORTE DOMINICAL</td>
                                <td></td>
                                <td></td>
                                <td>$ {{ $sunday_total }}</td>
                            </tr> --}}
                            <tr>
                                <td>TOTAL</td>
                                <td></td>
                                <td></td>
                                <td>$ {{ $total }}</td>
                            </tr>
                        </tbody>
                    </table>

                    @php
                        // $salary_total = $sum;
                        $salary_total = $total;
                        // $total = 0;
                        // $salary_total = 0;
                        // $garantizado_total = 0;
                        // $faltas = [];
                        $no_asistencia = count($dates);
                        $no_puntualidad = count($dates);
                        $weekly_sales_range = $barber->weeklySalesRange($start,$end)->get();
                        $tips = $weekly_sales_range->sum('tip');

                        foreach($dates as $date) {

                        //     $falta = true;
                        //     $atiempo = false;
                            $schedule = $barber->schedules && $barber->schedules->count() > 0 ? $barber->schedules->where('date', $date)->first() : null;
                            $descanso = $schedule ? $schedule->turn->is_rest: false;
                        //     $rest_name = $descanso ? $schedule->turn->name : '';
                        //     $is_payed = $descanso ? $schedule->turn->is_payed : false;
                            $attendances = $barber->attendances->where('date', $date);
                            $attendances_count = !empty($attendances) ? $attendances->count() : 0;
                            $has_punctuality = false;
                            if($attendances_count) {
                                $falta = false;
                                $no_asistencia -= 1;

                                if($schedule) {

                                    $attendanceStart = \Carbon\Carbon::parse($attendances->first()->time);
                                    $scheduleStart = \Carbon\Carbon::parse($schedule->turn->start);
                                    if($schedule->employee->job != "Cajero") {
                                        $scheduleStart = \Carbon\Carbon::parse($schedule->turn->barber_start);
                                    }
                                    if($attendanceStart <= $scheduleStart) {

                                        if($attendances_count >= 3) {
                                            $attendance_values = $attendances->values();
                                            $start = \Carbon\Carbon::parse($attendance_values->get(1)->time);
                                            $end = \Carbon\Carbon::parse($attendance_values->get(2)->time);
                                            $start->second = 0;
                                            $end->second = 0;
                                            if($start->diffInMinutes($end) <= $schedule->rest_minutes) {
                                                $no_puntualidad -= 1;
                                                $has_punctuality = true;
                                            }
                                        } else {
                                            $no_puntualidad -= 1;
                                            $has_punctuality = true;
                                        }
                                    }
                                }
                            }
                            if($descanso) {
                                $falta = true;
                                $no_asistencia -= 1;
                                $atiempo = true;
                                $no_puntualidad -= 1;
                            }
                        }
                    @endphp
                </div>
            </div>
            {{-- ! SHOW IF ADMON ONLY --}}
            @if(!empty($current_subsidiary) && $current_subsidiary->id == 7)
            <div class="panel panel-default">
                <div class="panel-heading">
                    Ventas a empleado pendientes por pagar
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Producto(s)</th>
                                <th>Pagado</th>
                                <th>Pendiente</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($employee_pending_sales as $sale)
                            <tr>
                                <td>
                                    @foreach ($sale->products as $product)
                                    ({{$product->qty}}) {{ $product->product->name }} <br>
                                    @endforeach
                                </td>
                                <td>{{ $sale->paidAmount() }}</td>
                                <td>{{ $sale->total - $sale->paidAmount() }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @endif
            {{-- <form action="{{url('/home/reports/weekly/paysheet/barberPost')}}" method="post"> --}}

            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table">
                        <tbody>
                            <?php
                                $big_total = $salary_total;
                                $total_salarios = $paysheet ? $paysheet->total : $salary_total;
                                list($products_total, $comisionProductos, $products_qty) = $barber->getEmployeeProductCommission();
                            ?>
                            <tr>
                                <td colspan="3">Productos vendidos:</td>
                                <td>{{ $products_qty }}</td>
                            </tr>
                            <tr>
                                <td colspan="3">Citas atendidas:</td>
                                <td>{{ $barber->getDiariesQty() }}</td>
                            </tr>
                            <tr>
                                <td colspan="3">Total salarios:</td>
                                <td>$ <span id="salarios-text">{{ number_format($total_salarios, 2, '.', ',') }}</span></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <a
                                        href="/home/reports/attendance-card?employee_id={{$barber->id}}&start={{$start}}&end={{$end}}"
                                        target="_blank">
                                        Bono por asistencia:
                                    </a>
                                </td>
                                <?php $ba_money = ($total_salarios * ($apply_new_prices ? 15 : 10)) / 100; ?>
                                <?php $ba = 0; ?>
                                <?php $pay_ba = false; ?>
                                @if($no_asistencia == 0)
                                    <?php $pay_ba = true; ?>
                                    <?php $ba = $ba_money; ?>
                                    <?php $big_total += $ba; ?>
                                @endif
                                <?php $asistencia = $paysheet ? ($paysheet->asistencia > $ba ? $paysheet->asistencia : $ba) : $ba; ?>
                                <td style="align:left;" class="d-flex">
                                    @if($user->isA('super-admin'))
                                        @php $check_assistance = ($paysheet && $paysheet->asistencia_pay == 1) || (!$paysheet && $pay_ba);  @endphp
                                        <input type="checkbox" class="check-attendance" id="check-attendance" @if($check_assistance) checked @endif>
                                    @endif
                                </td>
                                <td>
                                    <input type="hidden" id="attendance" name="attendance" value="{{ $ba_money }}">
                                    $ <span id="attendance-text">{{ number_format($ba, 2, '.', ',') }}</span>
                                </td>
                            </tr>

                            <?php $bp_money = ($total_salarios * 10) / 100; ?>
                            <?php $bp = 0; ?>
                            <?php $pay_bp = false; ?>
                            @if($no_puntualidad == 0)
                                <?php $pay_bp = true; ?>
                                <?php $bp = $bp_money; ?>
                                <?php $big_total += $bp; ?>
                            @endif
                            <?php $puntualidad = $paysheet ? ($paysheet->puntualidad > $bp ? $paysheet->puntualidad : $bp) : $bp; ?>
                            <tr>
                                <td colspan="2">
                                    <a
                                        href="/home/reports/attendance-card?employee_id={{$barber->id}}&start={{$start}}&end={{$end}}"
                                        target="_blank">
                                        Bono por puntualidad:
                                    </a>
                                </td>
                                <td style="align:left;">
                                    @if($user->isA('super-admin'))
                                        @php $check_punctuality = ($paysheet && $paysheet->puntualidad_pay == 1) || (!$paysheet && $has_punctuality); @endphp
                                        <input type="checkbox" class="check-punctuality" id="check-punctuality" @if($check_punctuality) checked @endif>
                                    @endif
                                </td>
                                <td>
                                    <input type="hidden" id="punctuality" name="punctuality" value="{{ $bp_money }}">
                                    $ <span id="punctuality-text">{{ number_format($puntualidad, 2, '.', ',') }}</span>
                                </td>
                            </tr>

                            <?php
                                $regular_productivity = ($total_salarios * ($apply_new_prices ? 15 : 20)) / 100;
                                $barber_productivity = 0;
                                foreach ($services as $service) {
                                    foreach ($service as $serv) {
                                        $barber_productivity += $serv->getAdditionalBarberCommission();
                                    }
                                }
                                $bpr = $employee->isBarber() ? $barber_productivity : $regular_productivity;
                                $big_total += $bpr;
                                $productividad = $paysheet ? ($paysheet->productividad > $bpr ? $paysheet->productividad : $bpr) : $bpr;
                            ?>
                            <tr>
                                <td colspan="2">
                                    <a
                                        href="/home/reports/issues?employee_id={{$barber->id}}&start={{$start}}&end={{$end}}"
                                        target="_blank" >
                                        Bono por productividad:
                                    </a>
                                </td>
                                <td style="align:left;">
                                    @if($user->isA('super-admin'))
                                    @php $check_productivity = ($paysheet && $paysheet->productividad_pay == 1) || (!$paysheet && $barber->issues->count() == 0); @endphp
                                    <input type="checkbox" class="check-productivity" id="check-productivity" @if($check_productivity) checked @endif>
                                    @endif
                                </td>
                                <td>
                                    <input type="hidden" id="productivity" name="productivity" value="{{ $bpr }}">
                                    $ <span id="productivity-text">{{ number_format($productividad, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            @if($user->isA('super-admin'))
                            {{-- <tr>
                                <td colspan="2">
                                    <a href="#"
                                        data-toggle="modal"
                                        data-target="#modal-sunday-details">
                                        Calculo dominical:
                                    </a>
                                    @include('admin.reports.paysheet.modal_sunday_bonus', [
                                        'employee' => $barber,
                                    ])
                                </td>
                                <td style="align:left;">
                                    @php
                                        $check_sunday_bonus = ($paysheet && $paysheet->sunday_pay == 1) || (!$paysheet && $barber->issues->count() == 0);
                                        $sunday_bonus = $sunday_total * (0.2);
                                        $big_total += $sunday_bonus;
                                    @endphp
                                    <input type="checkbox" class="check-sunday" id="check-sunday" @if($check_sunday_bonus) checked @endif>
                                </td>
                                <td>
                                    <input type="hidden" id="sunday" name="sunday" value="{{ $sunday_bonus }}">
                                    $ <span id="sunday-text">{{ number_format( $sunday_bonus, 2, '.', ',') }}</span>
                                </td>
                            </tr> --}}
                            @endif

                            <?php $bpe_money = ($salary_total * 40) / 100; ?>
                            <?php $bpe = 0; ?>
                            <?php $pay_bpe = false; ?>
                            @if($barber->has_excellence)
                                <?php $big_total += $bpe; ?>
                                <?php $pay_bpe = true; ?>
                                <?php $bpe = $bpe_money; ?>
                            @endif
                            <?php $excelencia = $paysheet ? $paysheet->excellence : $bpe; ?>
                            <?php $big_total += $bpe; ?>
                            <tr>
                                <td colspan="2">Bono por excelencia:</td>
                                <td style="align:left;">
                                    @if($user->isA('super-admin'))
                                    <input type="checkbox" class="check-excellence" id="check-excellence" @if($paysheet && $paysheet->excellence_pay == 1) checked @endif>
                                    @endif
                                </td>
                                <td>
                                    <input type="hidden" id="excellence" name="excellence" value="{{ $bpe_money }}">
                                    $ <span id="excellence-text">{{ number_format($excelencia, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">Dia extra:</td>
                                @php
                                    $extra_day = 0;
                                    $eam = 100;
                                    $extra_count = $barber->schedules()->extraDay()->dates([$start, $end])->count();
                                    $extra_count += $barber->schedules()->TurnMixt()->dates([$start, $end])->count();
                                    $extra_day = (($extra_count - 1) <= 0 ? 0 : ($extra_count - 1)) * $eam;
                                    $big_total += $extra_day;
                                @endphp
                                <td style="align:left;">
                                    @if($user->isA('super-admin'))
                                    <input type="hidden" id="extraday" name="extraday" value="{{ $extra_day }}">
                                    <input type="checkbox" class="check-extraday" id="check-extraday" @if($paysheet && $paysheet->extra_pay == 1) checked @endif>
                                    @endif
                                </td>
                                <td>
                                    $ <span id="extraday-text">{{ number_format($extra_day, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">Propinas:</td>
                                <td>
                                    $ <span id="tips-text">{{ number_format($tips, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">Comision por productos:</td>
                                <td>
                                    $ <span id="commission-text">{{ number_format($comisionProductos, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <?php
                            $percepciones = $barber->percepciones->sum('amount');
                            //$big_total += $paysheet ? ($paysheet->otros_ingresos > $big_total ? $paysheet->otros_ingresos : $percepciones) : $percepciones;
                            $big_total += $percepciones;
                            ?>
                            <tr>
                                <td colspan="3">Percepciones:</td>
                                <td>$ <span id="perceptions-text">{{ number_format($percepciones, 2, '.', ',') }}</span></td>
                            </tr>
                            <tr>
                                <td colspan="3">Gran total:</td>
                                <?php $granTotal = $total_salarios + $asistencia + $puntualidad + $productividad + $excelencia + $extra_day + $tips + $comisionProductos + $percepciones; ?>
                                <td>$ <span id="total-text">{{ number_format($granTotal, 2, '.', ',') }}</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            @include('admin.reports.paysheet.percepcione', [
                'employee' => $barber,
                'objects' => 'percepciones',
                'dates' => [$start, $end]
            ])

            <div class="panel panel-default">
                <div class="panel-heading">Descuentos</div>

                <div class="panel-body">
                    <table class="table">
                        <tbody>
                            <?php $desc_total = 0; ?>
                            <?php $prestamo = $paysheet ? $paysheet->prestamo : 0; ?>
                            <tr>
                                <td colspan="3">Prestamo:</td>
                                <td>
                                    {{-- @if($paysheet && $paysheet->status != 'archived' && $user->isA('super-admin'))
                                        <input type="number" name="prestamo" id="prestamo" value="{{ $prestamo }}" min="0" class="form-control">
                                    @else --}}
                                        $ <span id="prestamo-text"> {{ number_format($prestamo, 2, '.', ',') }} </span>
                                    {{-- @endif --}}
                                </td>
                            </tr>
                            <?php
                            // $total_faltas = count($faltas) * 200;
                            $total_faltas = 0;
                            $desc_total += $total_faltas;
                            $deducciones = $barber->deducciones->sum('amount');
                            $desc_total += $deducciones;
                            $faltas = $paysheet ? $paysheet->faltas : $total_faltas;
                            //$faltas = $total_faltas;
                            ?>
                            <tr>
                                <td colspan="2">Faltas:</td>
                                <td style="align:left;">
                                    {{-- @if($paysheet && $paysheet->status != 'archived' && $user->isA('super-admin'))
                                    <input type="checkbox" class="check-faults" id="check-faults" @if($paysheet->extra_pay == 1) checked @endif>
                                    @endif --}}
                                </td>
                                <td>
                                    <input type="hidden" id="faults" name="faults" value="{{ $total_faltas }}">
                                    $ <span id="faults-text">{{ number_format($total_faltas, 2, '.', ',') }}</span>
                                </td>
                            </tr>
                            <?php $uniforme = $paysheet ? $paysheet->uniforme : 0; ?>
                            <tr>
                                <td colspan="3">Uniforme:</td>
                                <td>
                                    {{-- @if($paysheet && $paysheet->status != 'archived' && $user->isA('super-admin'))
                                        <input type="number" name="uniform" id="uniform" value="{{ $uniforme }}" min="0" class="form-control">
                                    @else --}}
                                        $ <span id="uniform-text">{{ number_format($uniforme, 2, '.', ',') }}</span>
                                    {{-- @endif --}}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">Infonavit:</td>
                                @php
                                $monto_infonavit = 0;
                                if ($barber->monto_infonavit) {
                                    $monto_infonavit = (float) $barber->monto_infonavit;
                                }
                                $desc_total += $monto_infonavit;
                                $infonavit = $paysheet ? $paysheet->infonavit : $monto_infonavit;
                                $tipcomision = $tips * ($apply_new_prices ? 0.05 : 0.03);

                                @endphp
                                <td>
                                    {{-- @if($paysheet && $paysheet->status != 'archived' && $user->isA('super-admin'))
                                        <input type="number" name="infonavit" id="infonavit" value="{{ $infonavit }}" min="0" class="form-control">
                                    @else --}}
                                        $ <span id="infonavit-text">{{ number_format($infonavit, 2, '.', ',') }}</span>
                                    {{-- @endif --}}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">Comisión bancaria de propinas (5%):</td>
                                <td>$ <span id="commissiontip-text">{{ number_format($tipcomision, 2, '.', ',') }}</span></td>
                            </tr>
                            <tr>
                                <td colspan="3">Deducciones:</td>
                                <td>$ <span id="deductions-text">{{ number_format($deducciones, 2, '.', ',') }}</span></td>
                            </tr>
                            <?php
                                // $total_descuento = $prestamo + $faltas + $uniforme + $infonavit + $deducciones + $tipcomision;
                                $total_descuento = $prestamo + $uniforme + $infonavit + $deducciones + $tipcomision;
                            ?>
                            <tr>
                                <td colspan="3">Total:</td>
                                <td>$ <span id="total-deducciones-text">{{ number_format($total_descuento, 2, '.', ',') }}</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            @include('admin.reports.paysheet.percepcione', [
                'employee' => $barber,
                'objects' => 'deducciones',
                'dates' => [$start, $end]
            ])
            <?php
            $neto = $granTotal - $total_descuento;
            $depositado = $barber->deposits()->where('start', $start)->where('end', $end)->sum('amount');
            $diferencia = $neto - $depositado;
            ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td colspan="3">Neto a pagar:</td>
                                <td>$ <span id="neto-text">{{ number_format($neto, 2, '.', ',') }}</span></td>
                            </tr>
                            <tr>
                                <td colspan="3">Depositado:</td>
                                <td>
                                    <input type="hidden" name="depositado" id="depositado" value="{{ $depositado }}" />
                                    {{-- @if($paysheet && $paysheet->status != 'archived' && $user->isA('super-admin'))
                                        <input type="number" name="depositado" id="depositado" value="{{ $depositado }}" min="0" class="form-control">
                                    @else --}}
                                        $ {{ number_format($depositado, 2, '.', ',') }}
                                    {{-- @endif --}}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">Diferencia:</td>
                                <td>$ <span id="diferencia-text">{{ number_format($diferencia, 2, '.', ',') }}</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td colspan="5">
                                Observaciones:<br>
                                <textarea id="note" style="width: 100%; height: 100px;">{{$paysheet ? $paysheet->note : ''}}</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">Numero de cuenta:</td>
                            <td><span id="neto-text">{{ $barber->account_number }}</span></td>
                        </tr>
                        <tr>
                            <td colspan="3">Nombre del banco:</td>
                            <td><span id="neto-text">{{ $barber->bank_name }}</span></td>
                        </tr>
                    </table>
                </div>
            </div>

            {{-- </form> --}}
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="/js/nomina-resumen.js?v={{strtotime('now')}}"></script>
@endsection
