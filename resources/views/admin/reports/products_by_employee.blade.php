@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de productos vendidos por empleado
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/products-employees')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Empleado</th>
                                <th>Productos vendidos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0; ?>
                            @foreach($employees as $employee)
                            <?php $products_total = 0; ?>
                                <tr>
                                    <td>{{ $employee->name }}{{--$employee->job--}}</td>
                                    @php 
                                    list($products, $commission, $products_qty) = $employee->getEmployeeProductCommission(); 
                                    @endphp

                                    <td>
                                        <a href="#" data-toggle="modal" data-target="#products{{$employee->id}}">{{ $products_qty }}</a>
                                    </td>
                                </tr>
                            <div class="modal fade" id="products{{$employee->id}}" tabindex="-1" role="dialog" aria-labelledby="products{{$employee->id}}Label" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <h5 class="modal-title" id="exampleModalLabel">Productos vendidos por {{ $employee->name }}</h5>
                                                </div>
                                                <div class="col-md-2">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-body">
                                            @if($products_total > 0)
                                                <?php $products_array = []; ?>
                                                @if($employee->job == 'Barbero')
                                                    @foreach($employee->sales2->where('paid', true)->where('finish', true)->where('canceled_by', null) as $sale)
                                                        @if($sale->services->count() > 0)
                                                            @foreach($sale->products as $product)
                                                                @if($product->product->include_in_reports)
                                                                    @if(isset($products_array[$product->product->name]))
                                                                        <?php $products_array[$product->product->name] += $product->qty; ?>
                                                                    @else
                                                                        <?php $products_array[$product->product->name] = $product->qty; ?>
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                @endif

                                                @if($employee->job == 'Cajero')
                                                    @foreach($employee->cashRegisters as $cashRegisters)
                                                        @foreach($cashRegisters->salesFinished as $sale)
                                                            @if($sale->services->where('service_id', '!=', 19)->count() == 0)
                                                                @foreach($sale->products as $product)
                                                                    @if($product->product->include_in_reports)
                                                                        @if(isset($products_array[$product->product->name]))
                                                                            <?php $products_array[$product->product->name] += $product->qty; ?>
                                                                        @else
                                                                            <?php $products_array[$product->product->name] = $product->qty; ?>
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    @endforeach
                                                @endif

                                                @foreach($products_array as $key => $value)
                                                    {{$key}} ({{$value}})<br>
                                                @endforeach
                                            @else
                                                No hay productos vendidos
                                            @endif
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                                            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $total += $products_total; ?>
                            @endforeach

                            <tr>
                                <th>Total:</th>
                                <td>{{ $total }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection