@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de ventas por sucursales por turno
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/subsidiaries/turns')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="start">Fecha: </label>
                            <input type="text" name="start" id="start" class="form-control datepicker" value="{{$start}}">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Sucursal</th>
                                <th>Turno</th>
                                <th>Importe</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0; ?>
                            @foreach($cash_regiters as $cash_register)
                                <tr>
                                    <td>{{ $cash_register->subsidiary->name }}</td>
                                    <td>{{ $cash_register->turn->name }}</td>
                                    <?php
                                        if($cash_register->subsidiary->is_laundry) {
                                            $subtotal = $cash_register->totalLaundry();
                                        } else {
                                            $subtotal = $cash_register->sales()->finished()->notCanceled()->sum('subtotal');
                                        }
                                        $total += $subtotal;
                                    ?>
                                    <td>$ {{ number_format($subtotal, 2, '.', ',') }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <th colspan="2">Total:</th>
                                <td>$ {{ number_format($total, 2, '.', ',') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
