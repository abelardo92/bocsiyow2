@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Sucursales
                </div>

                <div class="panel-body">
                    <div class="form-group">
                            <select name="subsidiary_id" id="subsidiary_id" class="form-control" @if($user->access_subsidiary_id != null) disable @endif>
                            @if(!$user->access_subsidiary_id)
                                <option value="all">Todas</option>
                            @endif
                                @foreach($subsidiaries as $subsidiary)
                                    <option value="{{$subsidiary->id}}" @if($user->access_subsidiary_id == $subsidiary->id) selected @endif>
                                        {{$subsidiary->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de ventas global calendario
                </div>

                <div class="panel-body" id="calendar">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>

    $('#subsidiary_id').change(function(){
        //alert("hola");
        $('#calendar').fullCalendar({
            lang: 'es',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            events: {
                url: '/home/reports/calendars',
                type: 'GET',
                textColor: '#ffffff',
                data: function() { // a function that returns an object
                    return {
                        subsidiary_id: $('#subsidiary_id').val()
                    };
                }
            }
        });
    });

    </script>
@endsection
