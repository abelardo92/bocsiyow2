@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Análisis de sucursales
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/weekly/subsidiary-analysis')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="subsidiary_id">Sucursal: </label>
                            <select name="subsidiary_id" id="subsidiary_id" class="form-control select2">
                                <option value="0">TODOS</option>
                                @foreach($subsidiaries as $subsi)
                                    <option 
                                        @if($subsidiary != null && $subsidiary->id == $subsi->id) selected="selected" @endif
                                        value="{{$subsi->id}}"
                                    >{{$subsi->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>

                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#products" aria-controls="products" role="tab" data-toggle="tab">Productos surtidos</a></li>
                        <li role="presentation"><a href="#services" aria-controls="services" role="tab" data-toggle="tab">Servicios realizados</a></li>
                        <li role="presentation"><a href="#promotions" aria-controls="promotions" role="tab" data-toggle="tab">Promociones</a></li>
                        <li role="presentation"><a href="#totals" aria-controls="totals" role="tab" data-toggle="tab">Totales por empleado</a></li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="products">
                            {{-- <p><b>Productos surtidos</b></p> --}}
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Producto</th>
                                        <th>Total surtido</th>
                                        <th>Total vendido</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @foreach($product_group as $product_list)
                                    @php 
                                        $current_product = $product_list->first()->product; 
                                    @endphp
                                    <tr>
                                        <td>{{ $current_product->code }}</td>
                                        <td>{{ $current_product->name }}</td>
                                        <td>{{ $product_list->sum('quantity') }}</td>
                                        <td>
                                            @if(isset($current_product->product))
                                                @if(isset($sale_products[$current_product->product->id]))
                                                    {{ $sale_products[$current_product->product->id]->sum('qty') }}
                                                @else
                                                    0
                                                @endif
                                            @else
                                                ----
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="services">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Servicios realizados</th>
                                        <th>Unidades</th>
                                        <th>Pesos</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                     $total_quantities = 0;
                                     $total_prices = 0;   
                                    @endphp
                                    @foreach($sale_services_groups as $sale_services_group)
                                        @php 
                                            $current_service = $sale_services_group->first()->service; 
                                            $qty = $sale_services_group->sum('qty');
                                            
                                            $total = 0;
                                            foreach ($sale_services_group as $sale_service) {
                                                $total += $sale_service->qty * $sale_service->price;
                                            }
                                            
                                            $total_quantities += $qty;
                                            $total_prices += $total;
                                        @endphp
                                        <tr>
                                            <td>{{ $current_service->name }}</td>
                                            <td>{{ $qty }}</td>
                                            <td>$ {{ $total }}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>Totales</td>
                                        <td>{{ $total_quantities }}</td>
                                        <td>$ {{ $total_prices }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="promotions">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Promociones</th>
                                        <th>Cumpleaños</th>
                                        <th>Monedero</th>
                                        <th>Cortesía</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $total_birthday = 0;
                                        $total_wallet = 0;
                                        $total_courtesy = 0;
                                    @endphp
                                    @foreach($sale_services_promotion_groups as $sale_services_group)
                                        @php 
                                            $current_service = $sale_services_group->first()->service; 

                                            $birthday = 0;
                                            $wallet = 0;
                                            $courtesy = 0;

                                            foreach ($sale_services_group as $sale_service) {
                                                $sale = $sale_service->sale;
                                                if($sale->has_birthday) {
                                                    $birthday++;
                                                }
                                                if($sale->is_courtesy) {
                                                    $courtesy++;
                                                }
                                                if(!$sale->has_birthday && !$sale->is_courtesy && !$sale->is_promo_ut) {
                                                    $wallet++;
                                                }
                                            }

                                            $total_birthday += $birthday;
                                            $total_wallet += $wallet;
                                            $total_courtesy += $courtesy;

                                        @endphp
                                        <tr>
                                            <td>{{ $current_service->name }}</td>
                                            <td>{{ $birthday }}</td>
                                            <td>{{ $wallet }}</td>
                                            <td>{{ $courtesy }}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>Totales</td>
                                        <td>{{ $total_birthday }}</td>
                                        <td>{{ $total_wallet }}</td>
                                        <td>{{ $total_courtesy }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="totals">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Empleado</th>
                                        <th>No. Servicios</th>
                                        <th>Citas agendadas</th>
                                        <th>Articulos vendidos</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $services_count_total = 0;
                                        $sold_products_total = 0;
                                        $diaries_total = 0;
                                    @endphp
                                    @foreach ($employees as $employee)
                                    @php 
                                        $salesFinished = $employee->salesFinished;
                                        if($subsidiary) {
                                            $salesFinished = $salesFinished->where('subsidiary_id', $subsidiary->id);
                                        }
                                        $services = $salesFinished->pluck('services')->collapse();
                                        $services_total = $services->count() > 0 ? $services->where('canceled_by', null)->where('service_id', '!=', 19)->sum('qty') : 0;

                                        $sold_products = 0;
                                        $diaries = 0;

                                        if($employee->job == 'Barbero') {
                                            foreach($employee->salesFinished as $sale) {
                                                if($sale->services->where('canceled_by', null)->where('service_id', '!=', 19)->count() > 0) {
                                                    if($sale->has_discount) {
                                                        $diaries += 1;
                                                    }
                                                    foreach($sale->products->where('canceled_by', null) as $product) {
                                                        if($product->product->include_in_reports) {
                                                            $sold_products += $product->qty;
                                                            // if(isset($product_totals_array[$product->product->id])) {
                                                            //     $product_totals_array[$product->product->id] += $product->qty;
                                                            // } else {
                                                            //     $product_totals_array[$product->product->id] = $product->qty;
                                                            //     $products_array[$product->product->id] = $product->product;
                                                            // }
                                                        }
                                                    }
                                                }
                                            }
                                        } 
                                        if($employee->job == 'Cajero') {
                                            $sales = $employee->cashRegisters->pluck('salesFinished')->collapse();
                                            foreach($sales as $sale) {
                                                if($sale->services->count() == 0) {
                                                    if($sale->has_discount) {
                                                        $diaries += 1;
                                                    }
                                                    foreach($sale->products->where('canceled_by', null) as $product) {
                                                        if($product->product->include_in_reports) {
                                                            $sold_products += $product->qty;
                                                            // if(isset($product_totals_array[$product->product->id])) {
                                                            //     $product_totals_array[$product->product->id] += $product->qty;
                                                            // } else {
                                                            //     $product_totals_array[$product->product->id] = $product->qty;
                                                            //     $products_array[$product->product->id] = $product->product;
                                                            // }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        $sold_products_total += $sold_products;
                                        $services_count_total += $services_total;
                                        $diaries_total += $diaries;

                                    @endphp
                                    <tr>
                                        <td>{{$employee->name}}</td>
                                        <td>{{$services_total}}</td>
                                        <td>{{$diaries}}</td>
                                        <td>{{$sold_products}}</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <th>TOTALS:</th>
                                        <th>{{$services_count_total}}</th>
                                        <th>{{$diaries_total}}</th>
                                        <th>{{$sold_products_total}}</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection