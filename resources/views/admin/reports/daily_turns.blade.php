@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Diario por turno
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/daily-turns')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}

                        <div class="form-group">
                            <label for="subsidiary_id">Sucursal: </label>
                            <select name="subsidiary_id" id="subsidiary_id" class="form-control select2">
                                @foreach($subsidiaries as $subsi)
                                    <option 
                                        @if($subsidiary->id == $subsi->id) selected="selected" @endif
                                        value="{{$subsi->id}}"
                                    >{{$subsi->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="start">Fecha: </label>
                            <input type="text" name="start" id="start" class="form-control datepicker" value="{{$start}}">
                        </div>
                        
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        
        @foreach($cash_regiters as $cash_register)
            <?php $total = 0; ?>
            <div class="col-md-6 col-xs-6 col-sm-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                       <strong>Turno</strong>: {{$cash_register->turn->name}}
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Folio</th>
                                    <th>Importe</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $sales = $cash_register->sales; 
                                    if($cash_register->subsidiary->is_laundry) {
                                        $sales_additional = $cash_register->additionalSales();
                                        $sales = $sales->merge($sales_additional);
                                    }
                                ?>
                                @foreach($sales as $sale)
                                    <?php 
                                        if (!$sale->canceled_by) {    
                                            $subtotal = $sale->totalByCashRegister($cash_register->id);
                                        }
                                    ?>
                                    <tr>
                                        <td>
                                            {{$sale->subsidiary->key}} -
                                            {{$sale->folio}}
                                            @if($sale->canceled_by)
                                                <span class="label label-danger">Cancelado</span>
                                            @endif
                                        </td>
                                        <td>$ {{number_format($subtotal, 2, '.', ',')}}</th>
                                    </tr>
                                    <?php $total += $subtotal; ?>
                                @endforeach
                                <tr>
                                    <th>Total:</th>
                                    <td>$ {{number_format($total, 2, '.', ',')}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
