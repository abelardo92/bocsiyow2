@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Lista de ventas por cliente
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/customers/byBarber')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-1">
                                    <label>Barbero: </label>
                                </div>
                                <div class="col-md-3">
                                    <select class="form-control" id="employee_id" name="employee_id">
                                    @foreach($barbers as $barber)
                                        <option @if($employee_id == $barber->id) selected="selected" @endif value="{{$barber->id}}">{{$barber->name}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-default hide-in-print" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>    
                            </div>
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Cliente</th>
                                <th>Teléfono</th>
                                <th>Correo</th>
                                <th>Servicios efectuados</th>
                                <th>Servicios agendados</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if($customers != null)
                            @foreach($customers as $customer)
                                <?php 
                                    $sales_count = $customer->sales_count;
                                    $sales_diaries_count = $customer->sales()
                                    ->where('employee_id', $employee_id)->where('has_discount', true)
                                    ->where('paid', true)->where('finish', true)->where('canceled_by', null)
                                    ->count();
                                ?>
                                <tr>
                                    <td>{{ $customer->name }}</td>
                                    <td>{{ $customer->phone }}</td>
                                    <td>{{ $customer->email }}</td>
                                    <td>{{$sales_count}}</td>
                                    <td>{{$sales_diaries_count}}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                    @if($employee_id != null)
                        {{ $customers->appends(['employee_id' => $employee_id])->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
