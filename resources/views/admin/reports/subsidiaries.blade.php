@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de ventas por sucursales
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/subsidiaries')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Sucursal</th>
                                <th>Importe</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0; ?>
                            @foreach($subsidiaries as $subsidiary)
                                <tr>
                                    <td>{{ $subsidiary->name }}</td>
                                    <td>
                                    <?php
                                        $subtotal = 0;
                                        if($subsidiary->id != 11) {
                                            $sales = $subsidiary->sales()->with('payments','products','services','packages')->dates([$start, $end])->finished()->notCanceled()->get();
                                            
                                            if($subsidiary->is_laundry) {
                                                $subsi_cash_registers = $subsidiary->cashRegisters()->with(['sales','subsidiary'])->byRange($start,$end)->get();
                                                foreach ($subsi_cash_registers as $cr) {
                                                    $subtotal += $cr->totalLaundry();
                                                }
                                            } else {
                                                $subtotal = $sales->sum('subtotal');
                                            }
                                        } else {
                                            $subtotal += $subsidiary->laundryServices()->whereBetween('date', [$start, $end])->notCanceled()->sum('amount');
                                        }   
                                        $total += $subtotal;   
                                    ?>
                                    $ {{ number_format($subtotal, 2, '.', ',') }}
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <th>Total:</th>
                                <td>$ {{ number_format($total, 2, '.', ',') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
