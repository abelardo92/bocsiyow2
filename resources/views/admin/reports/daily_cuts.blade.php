@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Cierres de caja
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/daily-cuts')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}

                        <div class="form-group">
                            <label for="start">Fecha: </label>
                            <input type="text" name="start" id="start" class="form-control datepicker" value="{{$start}}">
                        </div>
                        
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        
        @foreach($cash_registers as $cash_register)
            <?php 
                $total = 0; 
                $lastMovement = $cash_register->lastMovement;
            ?>
            @if($lastMovement && $lastMovement->cashierMovement)
            <?php $cashierMovement = $lastMovement->cashierMovement; ?>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                       <strong>Sucursal</strong>: {{$cash_register->subsidiary->name}} ({{$lastMovement->type}})
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Denominación</th>
                                    <th>Cantidad</th>
                                    <th>Importe</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>.50</td>
                                    <td>{{$cashierMovement->fiftycents}}</td>
                                    <td>$ {{number_format($cashierMovement->fiftycents * (0.5), 2, '.', ',')}}</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>{{$cashierMovement->onepeso}}</td>
                                    <td>$ {{number_format($cashierMovement->onepeso * (1), 2, '.', ',')}}</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>{{$cashierMovement->twopesos}}</td>
                                    <td>$ {{number_format($cashierMovement->twopesos * (2), 2, '.', ',')}}</td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>{{$cashierMovement->fivepesos}}</td>
                                    <td>$ {{number_format($cashierMovement->fivepesos * (5), 2, '.', ',')}}</td>
                                </tr>
                                <tr>
                                    <td>10</td>
                                    <td>{{$cashierMovement->tenpesos}}</td>
                                    <td>$ {{number_format($cashierMovement->tenpesos * (10), 2, '.', ',')}}</td>
                                </tr>
                                <tr>
                                    <td>20</td>
                                    <td>{{$cashierMovement->twentypesos}}</td>
                                    <td>$ {{number_format($cashierMovement->twentypesos * (20), 2, '.', ',')}}</td>
                                </tr>
                                <tr>
                                    <td>50</td>
                                    <td>{{$cashierMovement->fiftypesos}}</td>
                                    <td>$ {{number_format($cashierMovement->fiftypesos * (50), 2, '.', ',')}}</td>
                                </tr>
                                <tr>
                                    <td>100</td>
                                    <td>{{$cashierMovement->hundredpesos}}</td>
                                    <td>$ {{number_format($cashierMovement->hundredpesos * (100), 2, '.', ',')}}</td>
                                </tr>
                                <tr>
                                    <td>200</td>
                                    <td>{{$cashierMovement->twohundredpesos}}</td>
                                    <td>$ {{number_format($cashierMovement->twohundredpesos * (200), 2, '.', ',')}}</td>
                                </tr>
                                <tr>
                                    <td>500</td>
                                    <td>{{$cashierMovement->fivehundredpesos}}</td>
                                    <td>$ {{number_format($cashierMovement->fivehundredpesos * (500), 2, '.', ',')}}</td>
                                </tr>
                                <tr>
                                    <td>1000</td>
                                    <td>{{$cashierMovement->onethousandpesos}}</td>
                                    <td>$ {{number_format($cashierMovement->onethousandpesos * (1000), 2, '.', ',')}}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <th>Total:</th>
                                    <th>$ {{number_format($cashierMovement->total(), 2, '.', ',')}}</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endif
        @endforeach
    </div>
</div>
@endsection
