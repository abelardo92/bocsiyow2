@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de Tiempos de entrada de servicios
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/services-per-time')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="subsidiary_id">Sucursal: </label>
                                    <select name="subsidiary_id" id="subsidiary_id" class="form-control select2">
                                        @foreach($subsidiaries as $subsi)
                                            <option
                                                @if($subsidiary->id == $subsi->id) selected="selected" @endif
                                                value="{{$subsi->id}}"
                                            >{{$subsi->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="turn_id">Turno: </label>
                                    <select name="turn_id" id="turn_id" class="form-control select2">
                                        @foreach($turns as $t)
                                            <option
                                                @if($turn->id == $t->id) selected="selected" @endif
                                                value="{{$t->id}}"
                                            >{{$t->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>

                    <div class="panel-body">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#per-time" aria-controls="per-time" role="tab" data-toggle="tab">Ventas por  horas</a>
                            </li>
                            <li role="presentation">
                                <a href="#sales" aria-controls="sales" role="tab" data-toggle="tab">Detalle de ventas</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="per-time">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            @foreach ($ranges as $range)
                                                <th>{{$range}}:00 - {{$range}}:59</th>
                                            @endforeach
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $totals_by_range = [];
                                        @endphp
                                        @foreach($salesByDate as $date => $salesPerHour)
                                            @php
                                                $total_by_date = 0;
                                            @endphp
                                            <tr>
                                                <td>{{ $date }}</td>
                                                @foreach ($ranges as $range)
                                                    @php
                                                        $qty = isset($salesPerHour[$range]) ? $salesPerHour[$range]->count() : 0;
                                                        $total_by_date += $qty;
                                                        if(!isset($totals_by_range[$range])) {
                                                            $totals_by_range[$range] = $qty;
                                                        } else {
                                                            $totals_by_range[$range] += $qty;
                                                        }
                                                    @endphp
                                                    <td>{{ $qty }}</td>
                                                @endforeach
                                                <td>
                                                    {{ $total_by_date }}
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <th>Promedios:</th>
                                            @foreach ($ranges as $range)
                                                @php
                                                    $current_total = isset($totals_by_range[$range]) ? $totals_by_range[$range] / $salesByDate->count() : 0;
                                                @endphp
                                                <th>{{ number_format($current_total, 2, '.', ',') }}</th>
                                            @endforeach
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="sales">
                                <table class="table table-striped datatables">
                                    <thead>
                                        <tr>
                                            <th>Folio</th>
                                            <th>Hora registro</th>
                                            <th>Hora atencion</th>
                                            <th>Hora finalizacion</th>
                                            <th>No. Servicios</th>
                                            <th>Importe cobrado</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($filteredSales as $sale)
                                            <tr>
                                                <td>{{ $sale->folio }}</td>
                                                <td>{{ $sale->created_at }}</td>
                                                <td>{{ $sale->attended_at }}</td>
                                                <td>{{ $sale->finished_at }}</td>
                                                <td>{{ $sale->servicesWithoutWaiting->sum('qty') }}</td>
                                                <td>{{ $sale->total }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
