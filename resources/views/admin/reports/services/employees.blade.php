@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de promedio de servicios por empleados
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/services-employees')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Empleado</th>
                                <th>Servicios efectuados</th>
                                <th>Promedio tiempo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0; ?>
                            <?php $attended_time = 0; ?>
                            @foreach($barbers as $employee)
                                <tr>
                                    <td>{{ $employee->short_name }}</td>
                                    <td>
                                        <?php
                                            $total += $employee->getServicesNumber($start, $end);
                                        ?>
                                        {{ $employee->getServicesNumber($start, $end) }}
                                    </td>
                                    <td>
                                        <?php
                                            $attended_time += $employee->getAttendedTimeAverage($start, $end);
                                        ?>
                                        {{ $employee->getAttendedTimeAverage($start, $end) }} min
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <th>Total:</th>
                                <td>{{ $total }}</td>
                                <td>{{ number_format($attended_time, 2, '.', ',') }} min</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
