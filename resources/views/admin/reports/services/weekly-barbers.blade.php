@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reporte de ventas semanal por barbero
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/reports/weekly/services/barbers')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="employee_id">Barbero: </label>
                            <select name="employee_id" id="employee_id" class="form-control select2">
                                @foreach($barbers as $employee)
                                    <option
                                        @if($employee->id == $barber->id) selected="selected" @endif
                                        value="{{$employee->id}}"
                                    >{{$employee->short_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="start">Fecha de: </label>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Sucursal</th>
                                <th>Costo</th>
                                <th>Entrada</th>
                                <th>Puntualidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0;?>
                            @foreach($sales as $group_sale)
                                @foreach($group_sale as $sales_list)
                                @php
                                    $sale = $sales_list->first();
                                    $date = $sale->date;
                                @endphp
                                <tr>
                                    <td>{{ $date }}</td>
                                    <td>{{ $sale->subsidiary->name }}</td>
                                    <?php
                                        $subtotal = $barber->getSalaryBySales($sales_list);
                                        $total += $subtotal;
                                    ?>
                                    <td>$ {{ number_format($subtotal, 2, '.', ',') }}</td>
                                    <td>
                                        @php
                                            $attendance = $barber->attendances()->where('subsidiary_id', $sale->subsidiary->id)->date($date)->get();
                                        @endphp
                                        @if($attendance->count())
                                            {{$attendance->first()->getTime()}}
                                        @endif
                                    </td>

                                    <td>
                                    <?php $schedules = $barber->schedules()->get(); ?>
                                        @if($attendance->count())
                                            @if ($schedules->count() > 0 && startltqend($attendance->first()->time, $schedules->where('date', $date)->first()->turn->start))
                                                <i class="fa fa-check"></i>
                                            @else
                                                <i class="fa fa-close"></i>
                                            @endif
                                        @else
                                            @if (!$schedules->count())
                                                <i class="fa fa-check"></i>
                                            @else
                                                <i class="fa fa-close"></i>
                                            @endif
                                        @endif
                                    </td>

                                </tr>
                                @endforeach
                            @endforeach
                            <tr>
                                <th colspan="2">Total:</th>
                                <td colspan="3">$ {{ number_format($total, 2, '.', ',') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Día garantizado</th>
                                <th>Pago de servicios</th>
                                <th>Pago a aplicar</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php
                                $total = 0;
                                $salary_total = 0;
                                $garantizado_total = 0;
                                $faltas = [];
                                $no_asistencia = count($dates);
                                $no_puntualidad = count($dates);
                                $weekly_sales_range = $barber->weeklySalesRange($start,$end)->get();
                                $tips = $weekly_sales_range->sum('tip');
                            @endphp
                            @foreach($dates as $date)
                                @php
                                    $falta = true;
                                    $atiempo = false;
                                    $schedule = $barber->schedules && $barber->schedules->count() > 0 ? $barber->schedules->where('date', $date)->first() : null;
                                    $descanso = $schedule ? $schedule->turn->is_rest: false;
                                    $rest_name = $descanso ? $schedule->turn->name : '';
                                    $is_payed = $descanso ? $schedule->turn->is_payed : false;
                                    $attendances = $barber->attendances->where('date', $date);
                                    $attendances_count = !empty($attendances) ? $attendances->count() : 0;
                                    $has_punctuality = false;
                                @endphp
                                @if($attendances_count)
                                    <?php
                                        $falta = false;
                                        $no_asistencia -= 1;
                                    ?>

                                    @if($schedule)
                                        @php
                                            $attendanceStart = \Carbon\Carbon::parse($attendances->first()->time);
                                            $scheduleStart = \Carbon\Carbon::parse($schedule->turn->start);
                                            if($schedule->employee->job != "Cajero") {
                                                $scheduleStart = \Carbon\Carbon::parse($schedule->turn->barber_start);
                                            }
                                        @endphp
                                        @if ($attendanceStart <= $scheduleStart)

                                            @if($attendances_count >= 3)
                                                @php
                                                    $attendance_values = $attendances->values();
                                                    $start = \Carbon\Carbon::parse($attendance_values->get(1)->time);
                                                    $end = \Carbon\Carbon::parse($attendance_values->get(2)->time);
                                                    $start->second = 0;
                                                    $end->second = 0;
                                                @endphp
                                                @if($start->diffInMinutes($end) <= $schedule->rest_minutes)
                                                    <?php $no_puntualidad -= 1; ?>
                                                    <?php $has_punctuality = true; ?>
                                                @endif
                                            @else
                                                <?php $no_puntualidad -= 1; ?>
                                                <?php $has_punctuality = true; ?>
                                            @endif
                                        @endif
                                    @endif
                                @endif
                                @if($descanso)
                                    <?php $falta = true; ?>
                                    <?php $no_asistencia -= 1; ?>
                                    <?php $atiempo = true; ?>
                                    <?php $no_puntualidad -= 1; ?>
                                @endif

                                <tr @if($falta) class="danger" @endif>
                                    <td @if($falta && !$descanso) colspan="4" @endif>
                                        {{$date}}
                                        @if($descanso)
                                            <small>{{$rest_name}}</small>
                                        @else
                                            @if($falta)
                                                <?php $faltas[] = $date; ?>
                                                <small>Falta</small>
                                            @endif
                                        @endif
                                    </td>
                                    @if($descanso)
                                        <td>
                                            $ 0.00
                                        </td>
                                        <td>
                                            $0.00
                                        </td>
                                        <td>
                                            $0.00
                                        </td>
                                    @endif
                                    @if(!$falta)
                                        <?php
                                            $garantizado_total += $guaranteed_salary;
                                        ?>
                                        <td>
                                            $ {{ number_format($guaranteed_salary, 2, '.', ',') }}
                                        </td>
                                        <?php

                                        $sale_services = $weekly_sales_range->where('date', $date)->pluck('services')->flatten();
                                        $subtotal = 0;
                                        foreach($sale_services as $sale_service) {
                                            $subtotal += $sale_service->getCommission();
                                        }

                                        $total += $subtotal;
                                        $salary_by_date = $subtotal > $guaranteed_salary ? $subtotal : $guaranteed_salary;
                                        $salary_total += $salary_by_date;
                                        ?>

                                        <td>$ {{ number_format($subtotal, 2, '.', ',') }}</td>
                                        <td>
                                            $ {{ number_format($salary_by_date, 2, '.', ',') }}
                                        </td>
                                    @endif

                                </tr>
                            @endforeach

                            <tr>
                                <td>Total:</td>
                                <td>$ {{ number_format($garantizado_total, 2, '.', ',') }}</td>
                                <td>$ {{ number_format($total, 2, '.', ',') }}</td>
                                <td>$ {{ number_format($salary_total, 2, '.', ',') }}</td>
                            </tr>
                        </tbody>
                    </table>

                    @php $saless = $barber->salesFinished; @endphp


                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-striped">
                        <tbody>
                            <?php $tot = 0;?>
                            @foreach($sales as $group_sale)
                                @foreach($group_sale as $sales_list)
                                @php
                                    if(empty($sales_list)) continue;
                                    $sale = $sales_list->first();
                                    if(empty($sale)) continue;
                                    $date = $sale->date;
                                @endphp
                                <tr>
                                    <th>{{ $date }}</th>
                                    <th colspan="5">{{ $sale->subsidiary->name }}</th>
                                </tr>
                                <?php
                                    $sale_ids = $sales_list->pluck('id');
                                    $services = App\SaleService::with('sale')->whereIn('sale_id', $sale_ids)->notCanceled()->notWaiting()->get()->groupBy('service_id');
                                    $service_total = 0;

                                    $kids_promotion_qty = 0;
                                    foreach ($saless as $sale) {
                                        if($sale->hasTwoBarbers() && $sale->containsHaircutAndKids()) {
                                            $kids_promotion_qty += 1;
                                        }
                                    }
                                ?>
                                @foreach($services as $service)
                                    @php
                                        $service_total = 0;
                                    @endphp
                                    @foreach($service->groupBy('cost') as $service_group)
                                        @php
                                            $cost = $service_group->first()->cost;
                                        @endphp
                                        @if($service_group->first()->service_id == 1)
                                            <?php $qty = $service_group->sum('qty') - $kids_promotion_qty;?>
                                        @else
                                            <?php $qty = $service_group->sum('qty'); ?>
                                        @endif
                                        @if($qty > 0)
                                        @php
                                            $service_total += ($cost * $qty);
                                        @endphp
                                        <tr>
                                            <td>{{ $qty }}</td>
                                            <td>{{ $service_group->first()->service->name }}</td>
                                            <td>$ {{ $cost }}</td>
                                            <td>$ {{ $cost * $qty }}</td>
                                            <td>
                                                @if($loop->last)
                                                    $ {{$service_total}}
                                                    <?php $tot += $service_total;?>
                                                @endif
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                @endforeach
                                @endforeach
                            @endforeach
                            <tr>
                                <th colspan="3">Total:</th>
                                <td>$ {{ number_format($tot, 2, '.', ',') }}</td>
                                <td>$ {{ number_format($tot, 2, '.', ',') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Reparaciones = {{\App\Sale::where('repaired_by', $barber->id)->whereBetween('date', [$start, $end])
                                            ->finished()->notCanceled()->repaireds()->count()}}</div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Sucursal</th>
                                <th>Fecha</th>
                                <th>Reparacion de</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $sum = 0;?>
                                @foreach(\App\Sale::where('repaired_by', $barber->id)->whereBetween('date', [$start, $end])
                                            ->finished()->notCanceled()->repaireds()->get() as $sale)
                                    <tr>
                                        <td>{{ $sale->subsidiary->name }}</td>
                                        <td>{{ $sale->getOriginal('date') }}</td>
                                        <td>{{ $sale->employee->short_name }}</td>
                                        <?php $sum += $sale->services->first()->service->cost; ?>
                                        <td>{{ $sale->services->first()->service->cost }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <th colspan="3">Total</th>
                                    <td>$ {{$sum}}</td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Reparaciones en contra = {{$barber->sales()->whereBetween('date', [$start, $end])->finished()->notCanceled()->repaireds()->count()}}</div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Sucursal</th>
                                <th>Fecha</th>
                                <th>Reparacion por</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $sum = 0;?>
                                @foreach($barber->sales()->whereBetween('date', [$start, $end])
                                            ->finished()->notCanceled()->repaireds()->get() as $sale)
                                    <tr>
                                        <td>{{ $sale->subsidiary->name }}</td>
                                        <td>{{ $sale->getOriginal('date') }}</td>
                                        <td>
                                        @if($sale->repaired != null)
                                        {{ $sale->repaired->short_name }}
                                        @endif
                                        </td>
                                        <?php $sum += $sale->services->first()->service->cost; ?>
                                        <td>{{ $sale->services->first()->service->cost }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <th colspan="3">Total</th>
                                    <td>$ {{$sum}}</td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
