
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<link href="/css/app.css" rel="stylesheet">-->
    <style>
        html, body {
            max-width: 100%;
            /*margin: 0 auto;*/
            padding: 0px;
            float: left;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            font-style: normal;
            line-height: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            color: #000;
        }
        table th {
            border-bottom: 1px solid black;
        }

        th td {
            width: 20%;
        }

        .text-center{
            text-align: center;
        }

        .sign {
            width: 50%;
            margin: 100px auto 0;
            display: block;
        }
        .sign p {
            border-top: 1px solid black;
            width: 100%;
            text-align: center;
        }

        @media print {
            @page { 
                size: auto;
                margin: 1.6cm; 
            }
            .page-break { 
                display: block; 
                page-break-after: avoid; 
                page-break-before: avoid; 
                page-break-inside: avoid;
            }
            table {float: none !important; }
            div { float: none !important; }
            .dont-print { display: none; } 
        }
    </style>

</head>
    <body>
    @php
        $configuration = App\Configuration::find($employee->subsidiary->configuration_id); 
        $schedules = $employee->schedules()->dates([$start, $end])->orderBy('date', 'asc')->get();
        $worked_days = 0;
        foreach($schedules as $schedule){
            if($schedule->subsidiary->id != 7 && $schedule->turn_id != 4 && $schedule->turn_id != 7){
                $worked_days++;    
            }
        }

    @endphp
        <center>
            <b>{{$configuration->razon_social}}</b>
            <p>
                {{$configuration->address}}<br/>
                {{$configuration->rfc}}<br/>
                IMSS<br/>
                <b>CONTROL DE ASISTENCIA</b>
                <br/><br/>
                PERIODO DE {{$start->format('d/m/Y')}} A {{$end->format('d/m/Y')}} 
            </p>
            
        </center>
            <table>
                <tbody>
                    <tr>
                        <td width="25%"><b>No. TRABAJADOR</b></td>
                        <td width="25%">{{ $employee->employee_code }}</td>
                        <td width="25%"></td>
                        <td width="25%"></td>
                    </tr>
                    <tr>
                        <td><b>NOMBRE</b></td>
                        <td colspan="3">{{ $employee->name }}</td>
                    </tr>
                    <tr>
                        <td><b>RFC</b></td>
                        <td colspan="3">{{ $employee->rfc }}</td>
                    </tr>
                    <tr>
                        <td><b>PUESTO</b></td>
                        <td colspan="3">{{ $employee->job }}</td>
                    </tr>
                    <tr>
                        <td><b>FECHA INGRESO</b></td>
                        <td>{{ Carbon\Carbon::parse($employee->entry_date)->format('d/m/Y') }}</td>
                        <td><b>NO. IMSS</b></td>
                        <td>{{ $employee->imss }}</td>
                    </tr>
                    <tr>
                        <td><b>DIAS LABORADOS</b></td>
                        <td>{{ $worked_days }}</td>
                        <td><b>CURP</b></td>
                        <td>{{ $employee->curp }}</td>
                    </tr>
                </tbody>
            </table>
            <br/>
                <table>
                    <thead>
                        <tr>
                            <th width="15%">Fecha</th>
                            <th width="15%">Día</th>
                            <th width="20%">Asistencia</th>
                            <th width="30%">Duración de la jornada</th>
                            <th width="20%">Extras trabajadas</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($schedules as $schedule)
                            @php
                                $currentDate = Carbon\Carbon::parse($schedule->date);
                                $isRest = false;
                                if($schedule->subsidiary->id == 7 || $schedule->turn_id == 4 || $schedule->turn_id == 7){
                                    $isRest = true;
                                }
                                //dd($schedule); 
                            @endphp
                            <tr>
                                <td>{{$currentDate->format('d/m/Y')}}</td>
                                <td>
                                    @php
                                        switch ($currentDate->dayOfWeek) {
                                        case 0:
                                            echo "DOMINGO";
                                        break;
                                        case 1:
                                            echo "LUNES";
                                        break;
                                        case 2:
                                            echo "MARTES";
                                        break;
                                        case 3:
                                            echo "MIERCOLES";
                                        break;
                                        case 4:
                                            echo "JUEVES";
                                        break;
                                        case 5:
                                            echo "VIERNES";
                                        break;
                                        case 6:
                                            echo "SABADO";
                                        break;
                                        }
                                    @endphp
                                </td>
                                <td>
                                    @if($isRest) 
                                        DESCANSO
                                    @else
                                        @if($schedule->employee->attendances()->where('subsidiary_id', $schedule->subsidiary->id)->date($schedule->date)->get()->count())
                                            SI
                                        @else
                                            NO
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    @if(!$isRest)
                                        COMPLETA 8 HORAS
                                    @endif
                                </td>
                                <td>
                                    @if(!$isRest)
                                        NO
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <p>
                    ACEPTO QUE EL PRESENTE CONTROL DE ASISTENCIAS ES CORRECTO<br/>
                    REITERO QUE NO TRABAJE TIEMPO EXTRAORDINARIO DESPUES DE MI JORNADA<br/>
                    COMPLETA DE OCHO HORAS POR DIA
                </p>
                <br/><br/><br/><br/>
                <center>
                _________________________________________ <br/>
                {{$employee->name}}<br/>
                FIRMA DEL TRABAJADOR
                </center>

    <script>
                (function () {
                    window.print();
                })();
            </script>
    </body>
</html>
