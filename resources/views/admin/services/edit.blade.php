@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    @if($service->image)
                        <img src="{{$service->image->path}}" alt="{{$service->image->name}}" class="img-square img-responsive">
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Servicios
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#general_information" aria-controls="general_information" role="tab" data-toggle="tab">Información general</a></li>
                        @if(Auth::user()->isA('super-admin'))
                            <li role="presentation"><a href="#custom_prices" aria-controls="custom_prices" role="tab" data-toggle="tab">Precios/Costos por sucursal</a></li>
                        @endif
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="general_information">
                            <form action="{{route('services.update', $service->id)}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                {{{ csrf_field() }}}
                                {{{ method_field('PUT') }}}
                                <div class="form-group">
                                    <label for="key" class="col-sm-4 control-label">Codigo *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="key" name="key" required="required" value="{{ old('key', $service->key) }}">
                                    </div>
                                </div>
            
                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">Nombre *</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="name" name="name" required="required" value="{{ old('name', $service->name) }}">
                                    </div>
                                </div>
            
                                <div class="form-group">
                                    <label for="image" class="col-sm-4 control-label">Foto</label>
                                    <div class="col-sm-6">
                                        <input type="file" class="form-control" id="image" name="image">
                                    </div>
                                </div>
            
                                <div class="form-group">
                                    <label for="sell_price" class="col-sm-4 control-label">Precio de venta *</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="sell_price" name="sell_price" required="required" value="{{ old('sell_price', $service->sell_price) }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="commission" class="col-sm-4 control-label">Comisión *</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="commission" name="commission" required="required" value="{{ old('commission', $service->commission) }}">
                                    </div>
                                </div>
            
                                <div class="form-group">
                                    <label for="cost" class="col-sm-4 control-label">Costo *</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="cost" name="cost" required="required" value="{{ old('cost', $service->cost) }}">
                                    </div>
                                </div>

                                @if(\Carbon\Carbon::now()->format('m') == '04')
                                    <div class="form-group">
                                        <label for="kids_promotion" class="col-sm-4 control-label">Promocion del dia del niño</label>
                                        <div class="col-sm-6">
                                          <select class="form-control" id="kids_promotion" name="kids_promotion" required="required">
                                            <option @if($service->kids_promotion) selected="selected" @endif value="1">Si</option>
                                            <option @if(!$service->kids_promotion) selected="selected" @endif value="0">No</option>
                                          </select>
                                        </div>
                                    </div>
                                @endif
            
                                <div class="form-group">
                                    <label for="subsidiary_id" class="col-sm-4 control-label">Sucursal</label>
                                    <div class="col-sm-6">
                                      <select class="form-control" id="subsidiary_id" name="subsidiary_id">
                                          <option value="">TODOS</option>
                                          @foreach($subsidiaries as $subsidiary)
                                            <option @if($subsidiary->id == $service->subsidiary_id) selected="selected" @endif value="{{$subsidiary->id}}">{{$subsidiary->name}}</option>
                                          @endforeach
                                      </select>
                                    </div>
                                </div>
            
                                <div class="form-group">
                                    <label for="include_in_diaries_discount" class="col-sm-4 control-label">Incluir en descuento por cita?</label>
                                    <div class="col-sm-6">
                                      <select class="form-control" id="include_in_diaries_discount" name="include_in_diaries_discount" required="required">
                                        <option value="1" @if($service->include_in_diaries_discount) selected="selected" @endif>SI</option>
                                        <option value="0" @if(!$service->include_in_diaries_discount) selected="selected" @endif>NO</option>
                                      </select>
                                    </div>
                                </div>
            
                                <div class="form-group">
                                    <label for="include_in_birthday_discount" class="col-sm-4 control-label">Incluir en descuento por cumpleaños?</label>
                                    <div class="col-sm-6">
                                      <select class="form-control" id="include_in_birthday_discount" name="include_in_birthday_discount" required="required">
                                        <option value="1" @if($service->include_in_birthday_discount) selected="selected" @endif>SI</option>
                                        <option value="0" @if(!$service->include_in_birthday_discount) selected="selected" @endif>NO</option>
                                      </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="show_for_customer_diaries" class="col-sm-4 control-label">Mostrar en citas (Clientes)?</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" id="show_for_customer_diaries" name="show_for_customer_diaries">
                                        <option @if($service->show_for_customer_diaries) selected="selected" @endif value="1">SI</option>
                                        <option @if(!$service->show_for_customer_diaries) selected="selected" @endif value="0">NO</option>
                                        </select>
                                    </div>
                                </div>
            
                                @if($currentSubsidiary != null && $currentSubsidiary->is_laundry)
                                <div class="form-group">
                                    <label for="type" class="col-sm-4 control-label">Tipo</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" id="type" name="type">
                                            <option>Sin tipo</option>
                                            <option value="1" @if($currentSubsidiary->type == 1) selected="selected" @endif>Kilo</option>
                                            <option value="2" @if($currentSubsidiary->type == 2) selected="selected" @endif>Pieza</option>
                                            <option value="3" @if($currentSubsidiary->type == 3) selected="selected" @endif>Renta</option>
                                        </select>
                                    </div>
                                </div>
                                @endif
            
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-6">
                                        <button type="submit" class="btn btn-default">Guardar</button>
                                    </div>
                                </div>
            
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="custom_prices">
                            <manage-service-prices
                            :current_service="{{$service->toJson()}}"
                            :subsidiaries="{{$subsidiaries->toJson()}}"
                            ></manage-service-prices>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
