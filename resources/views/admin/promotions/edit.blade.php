@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Promociones
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body" style="padding: 20px 20px;">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#info" aria-controls="products" role="tab" data-toggle="tab">Información general</a></li>
                        <li role="presentation"><a href="#services" aria-controls="services" role="tab" data-toggle="tab">Servicios asociados</a></li>
                        <li role="presentation"><a href="#products" aria-controls="products" role="tab" data-toggle="tab">Productos asociados</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="info">
                            <form action="{{route('promotions.update', $promotion->id)}}" method="POST" class="form-horizontal">
                                {{{ csrf_field() }}}
                                {{{ method_field('PUT') }}}
        
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label for="name" class="control-label">Nombre *</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ $promotion->name }}">
                                    </div>
                                
                                    <div class="col-sm-6">
                                        <label for="name" class="control-label" style="padding-bottom: 5px;">Días de actividad *</label>
                                        <br>
                                        <label><input type="checkbox" name="monday" value="1" @if($promotion->monday) checked @endif/> Lun</label>&nbsp;
                                        <label><input type="checkbox" name="tuesday" value="1" @if($promotion->tuesday) checked @endif/> Mar</label>&nbsp;
                                        <label><input type="checkbox" name="wednesday" value="1" @if($promotion->wednesday) checked @endif/> Mie</label>&nbsp;
                                        <label><input type="checkbox" name="thursday" value="1" @if($promotion->thursday) checked @endif/> Jue</label>&nbsp;
                                        <label><input type="checkbox" name="friday" value="1" @if($promotion->friday) checked @endif/> Vie</label>&nbsp;
                                        <label><input type="checkbox" name="saturday" value="1" @if($promotion->saturday) checked @endif/> Sab</label>&nbsp;
                                        <label><input type="checkbox" name="sunday" value="1" @if($promotion->sunday) checked @endif/> Dom</label>
                                    </div>
                                </div>
        
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="start">Promoción válida del: </label>
                                        <div class="input-group input-daterange">
                                            <input type="text" class="form-control" name="start_date" value="{{$promotion->start_date}}">
                                            <span class="input-group-addon">al</span>
                                            <input type="text" class="form-control" name="end_date" value="{{$promotion->end_date}}">
                                        </div>
                                    </div>
                                </div>
        
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label for="start">Horario de : </label>
                                        <div class="input-group bootstrap-timepicker timepicker">
                                            <input type="text" class="form-control timeinput" style="text-align: center;" name="start_time" value="{{$promotion->start_time}}">
                                            <span class="input-group-addon">a</span>
                                            <input type="text" class="form-control timeinput" style="text-align: center;" name="end_time" value="{{$promotion->end_time}}">
                                        </div>
                                    </div>
                                </div>
        
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label for="must_buy_a_service" class="control-label">Requiere la compra de un servicio? *</label>
                                        <select class="form-control" id="must_buy_a_service" name="must_buy_a_service" required="required">
                                            <option value="">Seleccione uno por favor...</option>
                                            <option @if($promotion->must_buy_a_service) selected="selected" @endif value="1">Si</option>
                                            <option @if(!$promotion->must_buy_a_service) selected="selected" @endif value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="must_buy_a_product" class="control-label">Requiere la compra de un producto? *</label>
                                        <select class="form-control" id="must_buy_a_product" name="must_buy_a_product" required="required">
                                            <option value="">Seleccione uno por favor...</option>
                                            <option @if($promotion->must_buy_a_product) selected="selected" @endif value="1">Si</option>
                                            <option @if(!$promotion->must_buy_a_product) selected="selected" @endif value="0">No</option>
                                        </select>
                                    </div>
                                </div>
        
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label for="email" class="control-label">Sucursal *</label>
                                        <select class="form-control" name="subsidiary_id" id="subsidiary_id">
                                            <option value="0">Todas</option>
                                            @foreach($subsidiaries as $subsidiary)
                                                <option @if($promotion->subsidiary_id == $subsidiary->id) selected="selected" @endif
                                                    value="{{$subsidiary->id}}">
                                                    {{ $subsidiary->name }}
                                                </option>
                                            @endforeach 
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="is_permanent" class="control-label">Es permanente? *</label>
                                        <select class="form-control" id="is_permanent" name="is_permanent" required="required">
                                            <option value="">Seleccione uno por favor...</option>
                                            <option @if($promotion->is_permanent) selected="selected" @endif value="1">Si</option>
                                            <option @if(!$promotion->is_permanent) selected="selected" @endif value="0">No</option>
                                        </select>
                                    </div>
                                </div>
        
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label for="discount_percentaje" class="control-label">Porcentaje de descuento de servicios (si aplica)</label>
                                        <input type="number" min="0" max="100" class="form-control" id="discount_percentaje" name="discount_percentaje" value="{{ $promotion->discount_percentaje }}">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="cash_only" class="control-label">Aplica solo con pago en efectivo? *</label>
                                        <select class="form-control" id="cash_only" name="cash_only">
                                            <option @if($promotion->cash_only) selected="selected" @endif value="1">Si</option>
                                            <option @if(!$promotion->cash_only) selected="selected" @endif value="0">No</option>
                                        </select>
                                    </div>
                                </div>
        
                                <hr>
                                <div class="form-group">
                                    <div class="col-md-9">
                                    </div>
                                    <div class="col-md-3">
                                        <input type="submit" class="btn btn-primary" value="Guardar">
                                        <a href="{{route('promotions.index')}}" class="btn btn-default" >Cancelar</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="services">
                            <manage-promotion-service-prices
                                :current_promotion="{{$promotion->toJson()}}"
                                :services="{{$services->toJson()}}"
                            ></manage-promotion-service-prices>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="products">
                            <manage-promotion-product-prices
                                :current_promotion="{{$promotion->toJson()}}"
                                :products="{{$products->toJson()}}"
                            ></manage-promotion-product-prices>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection