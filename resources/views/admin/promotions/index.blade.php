@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Promociones
                    @if(Auth::user()->isA('super-admin', 'horarios', 'subsidiary-admin'))
                        <div class="pull-right">
                            <a href="{{route('promotions.create')}}">Agregar promociones</a>
                        </div>
                    @endif
                </div>

                <div class="panel-body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#actives" aria-controls="actives" role="tab" data-toggle="tab">Activos</a></li>
                        @if(!Auth::user()->isA('subsidiary-admin'))
                            <li role="presentation"><a href="#inactives" aria-controls="inactives" role="tab" data-toggle="tab">Inactivos</a></li>
                        @endif
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="actives">
                            <table class="table table-striped datatables">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Periodo</th>
                                        <th>Horario</th>
                                        <th>Días</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($active_promotions as $promotion)
                                        <tr>
                                            <td>{{ $promotion->name }}</td>
                                            <td>{{ "$promotion->start_date - $promotion->end_date" }}</td>
                                            <td>{{ "$promotion->start_time - $promotion->end_time" }}</td>
                                            <td>
                                                @if($promotion->monday) lun, @endif
                                                @if($promotion->tuesday) mar, @endif
                                                @if($promotion->wednesday) mie, @endif
                                                @if($promotion->thursday) jue, @endif
                                                @if($promotion->friday) vie, @endif
                                                @if($promotion->saturday) sab, @endif
                                                @if($promotion->sunday) dom, @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('promotions.edit', $promotion->id) }}">Editar</a>
                                                @if(Auth::user()->isA('super-admin'))
                                                    |
                                                    <a href="#" data-toggle="modal" data-target="#modal-delete-{{$promotion->id}}">
                                                        Dar de baja
                                                    </a>
                                                    @include('admin.promotions.partials.delete_modal', compact('promotion'))
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="inactives">
                            <table class="table table-striped datatables">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Periodo</th>
                                        <th>Horario</th>
                                        <th>Días</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($inactive_promotions as $promotion)
                                        <tr>
                                            <td>{{ $promotion->name }}</td>
                                            <td>{{ "$promotion->start_date - $promotion->end_date" }}</td>
                                            <td>{{ "$promotion->start_time - $promotion->end_time" }}</td>
                                            <td>
                                                @if($promotion->monday) lun, @endif
                                                @if($promotion->tuesday) mar, @endif
                                                @if($promotion->wednesday) mie, @endif
                                                @if($promotion->thursday) jue, @endif
                                                @if($promotion->friday) vie, @endif
                                                @if($promotion->saturday) sab, @endif
                                                @if($promotion->sunday) dom, @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('promotions.edit', $promotion->id) }}">Editar</a>
                                                @if(Auth::user()->isA('super-admin'))
                                                    |
                                                    <a href="#" onclick="document.getElementById('form-active-{{$promotion->id}}').submit()">
                                                        Dar de alta
                                                    </a>
                                                    <form action="{{route('promotions.active', $promotion->id)}}" method="post" id="form-active-{{$promotion->id}}">
                                                      {{ csrf_field() }}
                                                      {{ method_field('PUT') }}
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
