@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Promociones
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body" style="padding: 0px 50px;">
                    <form action="{{route('promotions.store')}}" method="POST" class="form-horizontal">
                        {{{ csrf_field() }}}

                        <div class="form-group">
                            <div class="col-sm-6">
                                <label for="name" class="control-label">Nombre *</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                        
                            <div class="col-sm-6">
                                <label for="name" class="control-label" style="padding-bottom: 5px;">Días de actividad *</label>
                                <br>
                                <label><input type="checkbox" name="monday" value="1" /> Lun</label>&nbsp;
                                <label><input type="checkbox" name="tuesday" value="1" /> Mar</label>&nbsp;
                                <label><input type="checkbox" name="wednesday" value="1" /> Mie</label>&nbsp;
                                <label><input type="checkbox" name="thursday" value="1" /> Jue</label>&nbsp;
                                <label><input type="checkbox" name="friday" value="1" /> Vie</label>&nbsp;
                                <label><input type="checkbox" name="saturday" value="1" /> Sab</label>&nbsp;
                                <label><input type="checkbox" name="sunday" value="1" /> Dom</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="start">Promoción válida del: </label>
                                <div class="input-group input-daterange">
                                    <input type="text" class="form-control" name="start_date">
                                    <span class="input-group-addon">al</span>
                                    <input type="text" class="form-control" name="end_date">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="start">Horario de : </label>
                                <div class="input-group bootstrap-timepicker timepicker">
                                    <input type="text" class="form-control timeinput" style="text-align: center;" name="start_time">
                                    <span class="input-group-addon">a</span>
                                    <input type="text" class="form-control timeinput" style="text-align: center;" name="end_time">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6">
                                <label for="must_buy_a_service" class="control-label">Requiere la compra de un servicio? *</label>
                                <select class="form-control" id="must_buy_a_service" name="must_buy_a_service" required="required">
                                    <option value="">Seleccione uno por favor...</option>
                                    <option value="1">Si</option>
                                    <option value="0" selected>No</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label for="must_buy_a_product" class="control-label">Requiere la compra de un producto? *</label>
                                <select class="form-control" id="must_buy_a_product" name="must_buy_a_product" required="required">
                                    <option value="">Seleccione uno por favor...</option>
                                    <option value="1">Si</option>
                                    <option value="0" selected>No</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6">
                                <label for="email" class="control-label">Sucursal *</label>
                                <select class="form-control" name="subsidiary_id" id="subsidiary_id">
                                    <option value="0">Todas</option>
                                    @foreach($subsidiaries as $subsidiary)
                                        <option value="{{$subsidiary->id}}">{{ $subsidiary->name }}</option>
                                    @endforeach 
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label for="is_permanent" class="control-label">Es permanente? *</label>
                                <select class="form-control" id="is_permanent" name="is_permanent" required="required">
                                    <option value="">Seleccione uno por favor...</option>
                                    <option value="1">Si</option>
                                    <option value="0" selected>No</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6">
                                <label for="discount_percentaje" class="control-label">Porcentaje de descuento de servicios (si aplica)</label>
                                <input type="number" min="0" max="100" class="form-control" id="discount_percentaje" name="discount_percentaje">
                            </div>
                            <div class="col-sm-6">
                                <label for="cash_only" class="control-label">Aplica solo con pago en efectivo? *</label>
                                <select class="form-control" id="cash_only" name="cash_only">
                                    <option value="1">Si</option>
                                    <option value="0" selected>No</option>
                                </select>
                            </div>
                        </div>

                        <hr>
                        <div class="form-group">
                            <div class="col-md-9">
                            </div>
                            <div class="col-md-3">
                                <input type="submit" class="btn btn-primary" value="Guardar">
                                <a href="{{route('promotions.index')}}" class="btn btn-default" >Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection