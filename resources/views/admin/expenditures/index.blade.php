@extends('layouts.app_blank_print_header')

@section('content')

<table class="table show-in-print" style="font-size: 11px; padding: 2px 3px;">
    <thead>
        <tr>
            <th style="width: 80px;">Fecha</th>
            <th>Sucursal</th>
            <th style="width: 200px;">Cuenta</th>
            <th>Concepto</th>
            <th style="width: 80px;">Importe</th>
        </tr>
    </thead>
    <tbody>
        @foreach($expenditures as $expenditure)
        <tr>
            <td>{{ $expenditure->date }}</td>
            <td>{{ $expenditure->subsidiary->name }}</td>
            <td>{{ $expenditure->account->name }}</td>
            <td>{{ $expenditure->description }}</td>
            <td>$ {{ $expenditure->total }}</td>
        </tr>
        @endforeach
        <tr>
            <td colspan="4">Total:</td>
            <td colspan="2">$ {{ $expenditures->sum('total') }}</td>
        </tr>
    </tbody>
</table>

<div class="container hide-in-print">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Erogaciones
                    <div class="pull-right">
                        <a href="{{route('expenditures.create')}}">Agregar nuevo</a>&nbsp;&nbsp;
                        <button type="button" class="btn btn-primary btn-sm" title="imprimir"
                            onclick="window.print();"><i class="fa fa-print"></i></button>
                        </p>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4 text-left">
                                <a href="{{ route('expenditures.index', ['date' => $start->copy()->subDay()->format('Y-m-d'), 'filter' => $filter]) }}"
                                    class="pull-left">{{$start->copy()->subDay()->format('M')}}</a>
                            </div>
                            <div class="col-md-4">
                                <p class="text-center">Mes actual: <strong>{{$date->copy()->format('M')}}</strong></p>
                            </div>
                            <div class="col-md-4">
                                <a href="{{ route('expenditures.index', ['date' => $end->copy()->addDay()->format('Y-m-d'), 'filter' => $filter]) }}"
                                    class="pull-right">{{$end->copy()->addDay()->format('M')}}</a>
                            </div>
                        </div>
                        <form action="{{ route('expenditures.index')}}" method="get">
                            <input type="hidden" name="date" value="{{$date->copy()->format('Y-m-d')}}">
                            <div class="form-group">
                                <label for="filter">Filtar por cuenta</label>
                                <select name="filter" id="filter" class="form-control select2">
                                    <option value="all">Todas las cuentas</option>
                                    @foreach($accounts as $account)
                                    <option @if($account->id == $filter) selected="selected" @endif
                                        value="{{$account->id}}">{{$account->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Filtrar</button>
                            </div>
                        </form>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Sucursal</th>
                                    <th>Cuenta</th>
                                    <th>Concepto</th>
                                    <th>Importe</th>
                                    <th class="hide-in-print">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($expenditures as $expenditure)
                                <tr>
                                    <td>{{ $expenditure->date }}</td>
                                    <td>{{ $expenditure->subsidiary->name }}</td>
                                    <td>{{ $expenditure->account->name }}</td>
                                    <td>{{ $expenditure->description }}</td>
                                    <td>$ {{ $expenditure->total }}</td>
                                    <td class="hide-in-print">
                                        <a href="{{ route('expenditures.edit', $expenditure->id) }}"
                                            class="btn btn-link">Editar</a>
                                        @if(Auth::user()->isA('super-admin'))
                                        <form action="{{route('expenditures.destroy', $expenditure->id)}}"
                                            method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <input type="submit" class="btn btn-link" value="Eliminar">
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="4">Total:</td>
                                    <td colspan="2">$ {{ $expenditures->sum('total') }}</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-4 text-left">
                                <a href="{{ route('expenditures.index', ['date' => $start->copy()->subDay()->format('Y-m-d'), 'filter' => $filter]) }}"
                                    class="pull-left">{{$start->copy()->subDay()->format('M')}}</a>
                            </div>
                            <div class="col-md-4">
                                <p class="text-center">Mes actual: <strong>{{$date->copy()->format('M')}}</strong></p>
                            </div>
                            <div class="col-md-4">
                                <a href="{{ route('expenditures.index', ['date' => $end->copy()->addDay()->format('Y-m-d'), 'filter' => $filter]) }}"
                                    class="pull-right">{{$end->copy()->addDay()->format('M')}}</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @endsection