@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Erogaciones <a href="{{route('expenditures.create')}}">Nuevo</a>
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{route('expenditures.update', $expenditure->id)}}" enctype="multipart/form-data" method="POST" class="form-horizontal">
                        {{{ csrf_field() }}}
                        {{{ method_field('PUT') }}}

                        <div class="form-group">
                            <label for="date" class="col-sm-4 control-label">Fecha</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control datepicker" id="date" name="date" value="{{ old('date', $expenditure->date) }}" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="subsidiary_id" class="col-sm-4 control-label">Sucursal*</label>
                            <div class="col-sm-6">
                                <select name="subsidiary_id" id="subsidiary_id" class="form-control" required="required">
                                    @foreach(App\Subsidiary::all() as $subsidiary)
                                        <option @if($expenditure->subsidiary_id == $subsidiary->id) selected="selected" @endif value="{{$subsidiary->id}}">{{$subsidiary->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="account_id" class="col-sm-4 control-label">Cuenta*</label>
                            <div class="col-sm-6">
                                <select name="account_id" id="account_id" class="form-control" required="required">
                                    @foreach(App\Account::all() as $account)
                                        <option @if($expenditure->account_id == $account->id) selected="selected" @endif value="{{$account->id}}">{{$account->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description" class="col-sm-4 control-label">Concepto*</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="description" name="description" required="required" value="{{ old('description', $expenditure->description) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="total" class="col-sm-4 control-label">Importe*</label>
                            <div class="col-sm-6">
                              <input type="number" step="any" class="form-control" id="total" name="total" required="required" value="{{ old('total', $expenditure->total) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="image" class="col-sm-4 control-label">Foto</label>
                            <div class="col-sm-6">
                                <input type="file" class="form-control" id="image" name="image">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-4">
                                <button type="submit" class="btn btn-default">Guardar</button>
                                <a class="btn btn-default" href="{{ route('expenditures.create') }}">Agregar nuevo</a>
                            </div>
                            @if($expenditure->image)
                                <div class="col-sm-2">
                                    <a href="{{ $expenditure->image->path }}">Ver archivo</a>
                                </div>
                            @endif
                            @if($expenditure->movement != null)
                            <div class="col-sm-2">
                                <a href="{{route('warehouse.movements.print', $expenditure->movement->id)}}">Ver folio</a>
                            </div>
                            @endif
                        </div>


                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
