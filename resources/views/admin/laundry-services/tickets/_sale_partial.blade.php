<p><strong>Fecha:</strong> {{ $laundry_service->date }}</p>
<p>
    <strong>Cliente:</strong> {{ $customer_subsidiary->name }}
</p>

<table width="100%">
    <tr>
        <th>Servicio</th>
        <th>Importe</th>
    </tr>
    <tr>
        <td align="center">
            <span>
            Lavanderia
            </span>
        </td>
        <td align="center">
            <span>
                {{ $laundry_service->amount }}
            </span>
        </td>
    </tr>    
    <tr>
        <td colspan="2" align="right"><strong>Efectivo</strong></td>
        <td>$ {{ $laundry_service->amount }} mxn</td>
    </tr>
    <tr>
        <td colspan="2" align="right"><strong>Subtotal</strong></td>
        <td>$ {{ $laundry_service->amount }} mxn</td>
    </tr> 
    <tr>
        <td colspan="2" align="right"><strong>A pagar</strong></td>
        <td>$ {{ $laundry_service->amount }} mxn</td>
    </tr>
    <tr>
        <td colspan="2" align="right"><strong>Tipo de cambio</strong></td>
        <td>$ {{$laundry_service->cashRegister->exchangeRate->rate}}</td>
    </tr>
</table>

<p><strong>Cajero:</strong> {{ $laundry_service->cashRegister->employee->short_name }}</p>

<h2><strong>Folio:</strong> {{$subsidiary->key}} - {{ $laundry_service->folio }}</h2>
<p><strong>{{ $text }}</strong></p>
@if($laundry_service->canceled_by)
    <p><strong>Cancelado</strong></p>
    <p><strong>Fecha de cancelacion:</strong> {{ $laundry_service->canceled_at }} </p>
    <p><strong>Cancelado por:</strong> {{ $laundry_service->canceler->name }}</p>
@endif

<p>{{ $configuration->text_footer }}</p>
<p>SUGERENCIAS EN www.bocsiyowbarbershop.com.mx/sugerencias</p>

<hr>
