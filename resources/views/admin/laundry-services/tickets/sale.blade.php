@extends('layouts.tickets')

@section('subsidiary')
    {{ $subsidiary->name }}
@endsection

@section('fecha')
    {{ $laundry_service->created_at->format('d-m-Y h:i a')}}
@endsection

@section('address')
    {{ $subsidiary->address }}
@endsection

@section('content')
    @php
        $configuration = App\Configuration::first();
        // $imagepath = $configuration->image->path;
        if($subdomain && $subsidiary->configuration_id != null){
            $configuration = App\Configuration::find($subsidiary->configuration_id);
        } 
        $imagepath = $configuration->image->path;
    @endphp
    @include('admin.laundry-services.tickets._sale_partial', ['text' => 'Original'])
    
    <img src="{{ $imagepath }}" alt="" style="width: 100%;">
    <p class="text-center">@yield('title', 'BocsiYow')</p>
    <p>{{ $configuration->razon_social }}<br>
    {{ $configuration->rfc }}<br>
    {{ $configuration->curp }}<br>
    {{ $configuration->regimen }}<br>
    {{ $configuration->address }}<br>
    Lugar y fecha de expedición: <br>
    {{ $configuration->created_in }} - {{ $subsidiary->created_at->format('d-m-Y h:i a')}}</p>
    <p class="text-center"> Sucursal "{{ $subsidiary->name }}" </p>
    <p class="text-center"> "{{ $subsidiary->address }}" </p>
    
    @include('admin.laundry-services.tickets._sale_partial', ['text' => 'Copia cliente'])
@endsection
