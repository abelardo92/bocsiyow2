@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Menu</div>

                <div class="panel-body">
                    @if(Auth::user()->isA('super-admin', 'cashier-admin'))
                        <ul class="list-unstyled">
                            <li><a href="{{route('products.index')}}">Productos</a></li>
                            <li><a href="{{route('services.index')}}">Servicios</a></li>
                            @if(isset($subsidiary) && ($subsidiary->id == 7 || $subsidiary->is_laundry))
                            <li><a href="{{route('packages.index')}}">Paquetes</a></li>
                            @endif

                            @if(Auth::user()->isA('super-admin', 'cashier-admin'))
                                <li><a href="{{ route('inventories.activos.insumos', ["type" => 'insumo']) }}">Inventario de insumos</a></li>
                                    <li><a href="{{ route('inventories.activos.insumos', ["type" => 'activo']) }}">Inventario de activos</a></li>
                            @endif
                        </ul>
                    @endif
                    @if($subsidiary)
                        <ul class="list-unstyled">
                            <li>
                                <a href="/home/inventories/entries" @click="showAuthModal">Entradas</a>
                            </li>
                            <li>
                                <a href="/home/inventories/departures" @click="showAuthModal">Salidas</a>
                            </li>
                            <li>
                                <a href="/home/inventories/adjustments" @click="showAdminAuthModal">Ajustes</a>
                            </li>
                            <li>
                                <a href="/home/inventories/stocks" @click="showAuthModal">Existencias</a>
                            </li>
                        </ul>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-9">
            @include('admin.inventories.entries.index')
            @include('admin.inventories.departures.index')
            @include('admin.inventories.adjustments.index')
        </div>
    </div>
</div>
@endsection

@section('scripts')
    @if(session()->has('ticket_url'))
        <script>
            (function () {
                window.open('{{session('ticket_url')}}', '_blank');
            })();
        </script>
    @endif
@endsection
