@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            @include('admin.warehouse.movement_menu')
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Ajustes 
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="/home/inventories/adjustments" method="POST" v-on:submit.prevent class="form">
                        {{{ csrf_field() }}}
                        <warehouse-adjustments
                        :warehouse_products="{{$warehouse_products->toJson()}}"
                        ></warehouse-adjustments>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
