@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Salidas 
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="/home/inventories/departures" method="POST"v-on:submit.prevent class="form">
                        {{{ csrf_field() }}}

                        <div class="form-group">
                            <label for="concept_id">Concepto</label>
                            <select name="concept_id" id="concept_id" class="form-control" required="required" v-model="concept_id">
                                <option value="">Seleccione uno por favor...</option>
                                @foreach($concepts as $concept)
                                    <option value="{{$concept->id}}">{{$concept->name}}</option>
                                @endforeach
                                <option value="new">Crear concepto</option>
                            </select>
                        </div>

                        <div class="form-group" v-if="concept_id == 'new'">
                            <label for="concept">Nuevo concepto</label>
                            <input type="text" class="form-control" name="concept" id="concept">
                        </div>

                        <departures></departures>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
