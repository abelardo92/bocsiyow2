@extends('layouts.tickets')

@section('title')
    {{$title}}
@endsection

@section('subsidiary')
    {{$model && $model->subsidiary ? $model->subsidiary->name : '' }}
@endsection

@section('content')
    <p><strong>Folio:</strong> {{$model->folio}}</p>
    <p><strong>Fecha:</strong> {{$model->created_at->format('d-m-Y') }}</p>
    <p><strong>Hora:</strong> {{$model->created_at->format('H:i a') }}</p>
    <p><strong>Ajusto:</strong> {{$model->user->name}}</p>

    <table width="100%">
        <tr>
            <th>Codigo</th>
            <th colspan="2">Producto</th>
        </tr>
        <tr>
            <th>Existencia</th>
            <th>Ajuste</th>
            <th>Real</th>
        </tr>
        @foreach($models as $adjustment)
            <tr>
                <td align="center">{{$adjustment->product->key}}</td>
                <td align="center" colspan="2">{{$adjustment->product->name}}</td>
            </tr>
            <tr>
                <td align="center">{{$adjustment->current_existence}}</td>
                <td align="center">{{$adjustment->qty}}</td>
                <td align="center">{{$adjustment->real_existence}}</td>
            </tr>
        @endforeach
    </table>

    <div class="sign text-area">
        @if($model->employee)
            <p>{{$model->employee->name}}</p>
        @else
            <p>Firma</p>
        @endif
    </div>
@endsection
