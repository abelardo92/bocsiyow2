@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Ajustes 
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="/home/inventories/adjustments" method="POST"v-on:submit.prevent class="form">
                        {{{ csrf_field() }}}

                        <adjustments></adjustments>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
