@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Promociones
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body" style="padding: 0px 50px;">
                    <form action="{{route('alerts.store')}}" method="POST" class="form-horizontal">
                        {{{ csrf_field() }}}

                        <div class="form-group">
                            <div class="col-sm-6">
                                <label for="name" class="control-label">Nombre *</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>

                            <div class="col-sm-6">
                                <label for="message" class="control-label">Mensaje *</label>
                                <input type="text" class="form-control" id="message" name="message">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label for="name" class="control-label" style="padding-bottom: 5px;">Días donde se mostrará *</label>
                                <br>
                                <label><input type="checkbox" name="monday" value="1" /> Lun</label>&nbsp;
                                <label><input type="checkbox" name="tuesday" value="1" /> Mar</label>&nbsp;
                                <label><input type="checkbox" name="wednesday" value="1" /> Mie</label>&nbsp;
                                <label><input type="checkbox" name="thursday" value="1" /> Jue</label>&nbsp;
                                <label><input type="checkbox" name="friday" value="1" /> Vie</label>&nbsp;
                                <label><input type="checkbox" name="saturday" value="1" /> Sab</label>&nbsp;
                                <label><input type="checkbox" name="sunday" value="1" /> Dom</label>
                            </div>
                            <div class="col-sm-6">
                                <label for="start">Hora del mensaje : </label>
                                <div class="bootstrap-timepicker timepicker">
                                    <input type="text" class="form-control timeinput" style="text-align: center;" name="time">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label for="email" class="control-label">Sucursal *</label>
                                <select class="form-control" name="subsidiary_id" id="subsidiary_id">
                                    <option value="0">Todas</option>
                                    @foreach($subsidiaries as $subsidiary)
                                        <option value="{{$subsidiary->id}}">{{ $subsidiary->name }}</option>
                                    @endforeach 
                                </select>
                            </div>
                        </div>

                        <hr>
                        <div class="form-group">
                            <div class="col-md-9">
                            </div>
                            <div class="col-md-3">
                                <input type="submit" class="btn btn-primary" value="Guardar">
                                <a href="{{route('promotions.index')}}" class="btn btn-default" >Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection