@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Alertas
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body" style="padding: 0px 50px;">
                    <form action="{{route('alerts.update', $alert->id)}}" method="POST" class="form-horizontal">
                        {{{ csrf_field() }}}
                        {{{ method_field('PUT') }}}

                        <div class="form-group">
                            <div class="col-sm-6">
                                <label for="name" class="control-label">Nombre *</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ $alert->name }}">
                            </div>

                            <div class="col-sm-6">
                                <label for="message" class="control-label">Mensaje *</label>
                                <input type="text" class="form-control" id="message" name="message" value="{{ $alert->message }}">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label for="name" class="control-label" style="padding-bottom: 5px;">Días donde se mostrará *</label>
                                <br>
                                <label><input type="checkbox" name="monday" value="1" @if($alert->monday) checked @endif/> Lun</label>&nbsp;
                                <label><input type="checkbox" name="tuesday" value="1" @if($alert->tuesday) checked @endif/> Mar</label>&nbsp;
                                <label><input type="checkbox" name="wednesday" value="1" @if($alert->wednesday) checked @endif/> Mie</label>&nbsp;
                                <label><input type="checkbox" name="thursday" value="1" @if($alert->thursday) checked @endif/> Jue</label>&nbsp;
                                <label><input type="checkbox" name="friday" value="1" @if($alert->friday) checked @endif/> Vie</label>&nbsp;
                                <label><input type="checkbox" name="saturday" value="1" @if($alert->saturday) checked @endif/> Sab</label>&nbsp;
                                <label><input type="checkbox" name="sunday" value="1" @if($alert->sunday) checked @endif/> Dom</label>
                            </div>
                            <div class="col-sm-6">
                                <label for="start">Hora del mensaje : </label>
                                <div class="bootstrap-timepicker timepicker">
                                    <input type="text" class="form-control timeinput" style="text-align: center;" name="time" value="{{ $alert->time }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label for="email" class="control-label">Sucursal *</label>
                                <select class="form-control" name="subsidiary_id" id="subsidiary_id">
                                    <option value="0">Todas</option>
                                    @foreach($subsidiaries as $subsidiary)
                                        <option @if($alert->subsidiary_id == $subsidiary->id) selected="selected" @endif 
                                            value="{{$subsidiary->id}}">{{ $subsidiary->name }}</option>
                                    @endforeach 
                                </select>
                            </div>
                        </div>

                        <hr>
                        <div class="form-group">
                            <div class="col-md-9">
                            </div>
                            <div class="col-md-3">
                                <input type="submit" class="btn btn-primary" value="Guardar">
                                <a href="{{route('alerts.index')}}" class="btn btn-default" >Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection