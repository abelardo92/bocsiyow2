@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Alertas
                    <div class="pull-right">
                        <a href="{{route('alerts.create')}}">Agregar alerta</a>
                    </div>
                </div>

                <div class="panel-body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#actives" aria-controls="actives" role="tab" data-toggle="tab">Activas</a></li>
                        <li role="presentation"><a href="#inactives" aria-controls="inactives" role="tab" data-toggle="tab">Inactivas</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="actives">
                            <table class="table table-striped datatables">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Sucursal</th>
                                        <th>Horario</th>
                                        <th>Días</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($alerts as $alert)
                                        <tr>
                                            <td>{{ $alert->name }}</td>
                                            <td>
                                                @if($alert->subsidiary)
                                                    {{ $alert->subsidiary->name }}
                                                @else
                                                    TODAS
                                                @endif
                                            </td>
                                            <td>{{ $alert->time }}</td>
                                            <td>
                                                @if($alert->monday) lun, @endif
                                                @if($alert->tuesday) mar, @endif
                                                @if($alert->wednesday) mie, @endif
                                                @if($alert->thursday) jue, @endif
                                                @if($alert->friday) vie, @endif
                                                @if($alert->saturday) sab, @endif
                                                @if($alert->sunday) dom, @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('alerts.edit', $alert->id) }}">Editar</a>
                                                @if(Auth::user()->isA('super-admin'))
                                                    |
                                                    <a href="#" data-toggle="modal" data-target="#modal-delete-{{$alert->id}}">
                                                        Dar de baja
                                                    </a>
                                                    @include('admin.alerts.partials.delete_modal', compact('alert'))
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="inactives">
                            <table class="table table-striped datatables">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Sucursal</th>
                                        <th>Horario</th>
                                        <th>Días</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($inactive_alerts as $alert)
                                        <tr>
                                            <td>{{ $alert->name }}</td>
                                            <td>
                                                @if($alert->subsidiary)
                                                    {{ $alert->subsidiary->name }}
                                                @else
                                                    TODAS
                                                @endif
                                            </td>
                                            <td>{{ $alert->time }}</td>
                                            <td>
                                                @if($alert->monday) lun, @endif
                                                @if($alert->tuesday) mar, @endif
                                                @if($alert->wednesday) mie, @endif
                                                @if($alert->thursday) jue, @endif
                                                @if($alert->friday) vie, @endif
                                                @if($alert->saturday) sab, @endif
                                                @if($alert->sunday) dom, @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('alerts.edit', $alert->id) }}">Editar</a>
                                                @if(Auth::user()->isA('super-admin'))
                                                    |
                                                    <a href="#" onclick="document.getElementById('form-active-{{$alert->id}}').submit()">
                                                        Dar de alta
                                                    </a>
                                                    <form action="{{route('alerts.active', $alert->id)}}" method="post" id="form-active-{{$alert->id}}">
                                                        {{ csrf_field() }}
                                                        {{ method_field('PUT') }}
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection