@extends('layouts.app_customers')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">

        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Solicitud de empleo
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{route('employee-requests.update', $employee_request->id)}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                        {{{ csrf_field() }}}
                        {{{ method_field('PUT') }}}

                        <div class="col-12">
                            <h4 class="my-4"><b>DATOS PERSONALES</b></h4>
                        </div>

                        <div class="form-group">
                            <label for="status" class="col-sm-2 control-label">Estatus *</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="status" name="status">
                                    <option value="pendiente" {{ $employee_request->status == 'pendiente' ? 'selected' : '' }}>Pendiente</option>
                                    <option value="completado" {{ $employee_request->status == 'completado' ? 'selected' : '' }}>Completado</option>
                                    <option value="rechazado" {{ $employee_request->status == 'rechazado' ? 'selected' : '' }}>Rechazado</option>
                                </select>
                                @include('errors.field',['field' => 'status'])
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Nombre completo *</label>
                            <div class="col-sm-4">
                                <input type="text" placeholder="Nombre" class="form-control" id="name" name="name" value="{{ $employee_request->name }}">
                                @include('errors.field',['field' => 'name'])
                            </div>
                            <label for="address" class="col-sm-2 control-label">Dirección *</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="address" name="address" value="{{ $employee_request->address }}">
                                @include('errors.field',['field' => 'address'])
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-sm-2 control-label">Teléfono *</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="phone" name="phone" value="{{ $employee_request->phone }}">
                                @include('errors.field',['field' => 'phone'])
                            </div>
                            <label for="email" class="col-sm-2 control-label">Correo electrónico *</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="email" name="email" value="{{ $employee_request->email }}">
                                @include('errors.field',['field' => 'email'])
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="marital_status" class="col-sm-2 control-label">Estado civil *</label>
                            <div class="col-sm-4">
                                <select name="marital_status" id="marital_status" class="form-control">
                                    <option value="">Seleccione uno por favor...</option>
                                    <option value="Soltero" {{ $employee_request->marital_status == 'Soltero' ? 'selected' : '' }}>Soltero</option>
                                    <option value="Casado" {{ $employee_request->marital_status == 'Casado' ? 'selected' : '' }}>Casado</option>
                                    <option value="Divorciado" {{ $employee_request->marital_status == 'Divorciado' ? 'selected' : '' }}>Divorciado</option>
                                    <option value="Viudo" {{ $employee_request->marital_status == 'Viudo' ? 'selected' : '' }}>Viudo</option>
                                    <option value="Union Libre" {{ $employee_request->marital_status == 'Union Libre' ? 'selected' : '' }}>Union Libre</option>
                                </select>
                                @include('errors.field',['field' => 'marital_status'])
                            </div>
                            <label for="gender" class="col-sm-2 control-label">Genero *</label>
                            <div class="col-sm-4">
                                <select name="gender" id="gender" class="form-control">
                                    <option value="Masculino" {{ $employee_request->gender == 'Masculino' ? 'selected' : '' }}>Masculino</option>
                                    <option value="Femenino" {{ $employee_request->gender == 'Femenino' ? 'selected' : '' }}>Femenino</option>
                                </select>
                                @include('errors.field',['field' => 'gender'])
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-sm-2 control-label">Sucursal en la que desea trabajar *</label>
                            <div class="col-sm-4">
                                <select class="form-control" id="subsidiary_id" name="subsidiary_id" required="required">
                                    <option value="">Seleccione uno por favor...</option>
                                    @foreach($subsidiaries as $subsidiary)
                                        <option value="{{$subsidiary->id}}" {{ $employee_request->subsidiary_id == $subsidiary->id ? 'selected' : '' }}>{{$subsidiary->name}}</option>
                                    @endforeach
                                </select>
                                @include('errors.field',['field' => 'phone'])
                            </div>
                            <label for="birth_date" class="col-sm-2 control-label">Fecha de nacimiento *</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control datepicker" id="birth_date" name="birth_date" value="{{ $employee_request->birth_date }}">
                                @include('errors.field',['field' => 'birth_date'])
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="nationality" class="col-sm-2 control-label">Nacionalidad *</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="nationality" name="nationality" value="{{ $employee_request->nationality }}">
                                @include('errors.field',['field' => 'nationality'])
                            </div>
                            <label for="state" class="col-sm-2 control-label">Estado *</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="state" name="state" value="{{ $employee_request->state }}">
                                @include('errors.field',['field' => 'state'])
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="city" class="col-sm-2 control-label">Ciudad *</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="city" name="city" value="{{ $employee_request->city }}">
                                @include('errors.field',['field' => 'city'])
                            </div>
                        </div>

                        <div class="col-12">
                            <h4 class="my-4"><b>DOCUMENTACIÓN</b></h4>
                        </div>

                        <div class="form-group">
                            <label for="rfc" class="col-sm-2 control-label">RFC *</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="rfc" name="rfc" value="{{ $employee_request->rfc }}">
                                @include('errors.field',['field' => 'rfc'])
                            </div>
                            <label for="curp" class="col-sm-2 control-label">CURP *</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="curp" name="curp" value="{{ $employee_request->curp }}">
                                @include('errors.field',['field' => 'curp'])
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="nss" class="col-sm-2 control-label">No. de seguridad social (NSS) *</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="nss" name="nss" value="{{ $employee_request->nss }}">
                                @include('errors.field',['field' => 'nss'])
                            </div>
                            <label for="name" class="col-sm-2 control-label">Documentación relevante (Ej: certificados)</label>
                            <div class="col-sm-4">
                                <input type="file" class="form-control" id="image[]" multiple name="image[]">
                                @include('errors.field',['field' => 'image'])
                            </div>
                        </div>

                        <div class="col-12">
                            <h4 class="my-4"><b>ESTADO DE SALUD</b></h4>
                        </div>

                        <div class="form-group">
                            <label for="health_condition" class="col-sm-2 control-label">Como considera su salud actual? *</label>
                            <div class="col-sm-4">
                                <select name="health_condition" id="health_condition" class="form-control">
                                    <option value="Bueno" {{ $employee_request->health_condition == 'Bueno' ? 'selected' : '' }}>Bueno</option>
                                    <option value="Regular" {{ $employee_request->health_condition == 'Regular' ? 'selected' : '' }}>Regular</option>
                                    <option value="Malo" {{ $employee_request->health_condition == 'Malo' ? 'selected' : '' }}>Malo</option>
                                </select>
                                @include('errors.field',['field' => 'health_condition'])
                            </div>
                            <label for="suffer_some_illness" class="col-sm-2 control-label">Padece alguna enfermedad? si es asi, cual?</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="suffer_some_illness" name="suffer_some_illness" value="{{ $employee_request->suffer_some_illness }}">
                                @include('errors.field',['field' => 'suffer_some_illness'])
                            </div>
                        </div>

                        <div class="col-12">
                            <h4 class="my-4"><b>DATOS FAMILIARES</b></h4>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12" style="padding: 0px 30px;">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Nombre</th>
                                            <th>Vive</th>
                                            <th>Domicilio</th>
                                            <th>Ocupación</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Padre</td>
                                            <td>
                                                <input type="text" class="form-control" id="father_name" name="father_name" value="{{ $employee_request->father_name }}">
                                                @include('errors.field',['field' => 'father_name'])
                                            </td>
                                            <td>
                                                <select name="father_alive" id="father_alive" class="form-control">
                                                    <option value="1" {{ $employee_request->father_alive == '1' ? 'selected' : '' }}>SI</option>
                                                    <option value="0" {{ $employee_request->father_alive == '0' ? 'selected' : '' }}>NO</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="father_address" name="father_address" value="{{ $employee_request->father_address }}">
                                                @include('errors.field',['field' => 'father_address'])
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="father_occupation" name="father_occupation" value="{{ $employee_request->father_occupation }}">
                                                @include('errors.field',['field' => 'father_occupation'])
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Madre</td>
                                            <td>
                                                <input type="text" class="form-control" id="mother_name" name="mother_name" value="{{ $employee_request->mother_name }}">
                                                @include('errors.field',['field' => 'mother_name'])
                                            </td>
                                            <td>
                                                <select name="mother_alive" id="mother_alive" class="form-control">
                                                    <option value="1" {{ $employee_request->mother_alive == '1' ? 'selected' : '' }}>SI</option>
                                                    <option value="0" {{ $employee_request->mother_alive == '0' ? 'selected' : '' }}>NO</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="mother_address" name="mother_address" value="{{ $employee_request->mother_address }}">
                                                @include('errors.field',['field' => 'mother_address'])
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="mother_occupation" name="mother_occupation" value="{{ $employee_request->mother_occupation }}">
                                                @include('errors.field',['field' => 'mother_occupation'])
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Esposa(o) o conyugue</td>
                                            <td>
                                                <input type="text" class="form-control" id="spouse_name" name="spouse_name" value="{{ $employee_request->spouse_name }}">
                                                @include('errors.field',['field' => 'spouse_name'])
                                            </td>
                                            <td>
                                                <select name="spouse_alive" id="spouse_alive" class="form-control">
                                                    <option value="1" {{ $employee_request->spouse_alive == '1' ? 'selected' : '' }}>SI</option>
                                                    <option value="0" {{ $employee_request->spouse_alive == '0' ? 'selected' : '' }}>NO</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="spouse_address" name="spouse_address" value="{{ $employee_request->spouse_address }}">
                                                @include('errors.field',['field' => 'spouse_address'])
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="spouse_occupation" name="spouse_occupation" value="{{ $employee_request->spouse_occupation }}">
                                                @include('errors.field',['field' => 'spouse_occupation'])
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12 d-flex">
                                <button type="submit" class="btn btn-primary ml-auto">Realizar solicitud de empleo</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
