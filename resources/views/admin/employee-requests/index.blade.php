@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Solicitudes de empleo</div>

                    <div class="panel-body">
                        <a href="/solicitar-empleo">Agregar solicitud</a>
                        <table class="table datatables">
                            <thead>
                                <tr>
                                    <th>Fecha de solicitud</th>
                                    <th>Nombre</th>
                                    <th>Sucursal</th>
                                    <th>Estatus</th>
                                    <th>Ver archivo(s)</th>
                                    @if(Auth::user()->isA('super-admin'))
                                    <th>Acciones</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($employee_requests as $employee_request)
                                    <tr>
                                        <td>{{ $employee_request->created_at }}</td>
                                        <td>{{ $employee_request->name }}</td>
                                        <td>{{ $employee_request->subsidiary ? $employee_request->subsidiary->name : '' }}</td>
                                        <td>
                                            @php
                                                $status = $employee_request->status;
                                                switch ($status) {
                                                    case 'pendiente':
                                                        $status_class = 'label label-warning';
                                                        break;
                                                    case 'completado':
                                                        $status_class = 'label label-success';
                                                        break;
                                                    case 'rechazado':
                                                        $status_class = 'label label-danger';
                                                        break;
                                                    default:
                                                        $status_class = 'label label-warning';
                                                        break;
                                                }
                                            @endphp
                                            <span class="{{ $status_class }}">{{ strtoupper($status) }}</span>
                                        </td>
                                        <td>
                                            @foreach($employee_request->images as $file)
                                                <a href="{{ $file->path }}">{{ $file->name }}</a><br>
                                            @endforeach
                                        </td>
                                        @if(Auth::user()->isA('super-admin'))
                                        <td class="row" style="width: 80px !important; display: flex;">
                                            <div class="col-sm-4 index-edit-button-container">
                                                <a href="{{ route('employee-requests.edit', $employee_request->id) }}" class="btn btn-link index-edit-button">
                                                    <i class="fa fa-pencil-square-o fa-2x"></i>
                                                </a>
                                            </div>
                                            <div class="col-sm-4 index-edit-button-container" style="margin-left: 10px;">
                                                <a href="{{ route('employees.from-request', $employee_request->id) }}" class="btn btn-link index-edit-button" style="padding-top: 3px;">
                                                    <i class="fa fa-user fa-2x"></i>
                                                </a>
                                            </div>

                                            @if(Auth::user()->isA('super-admin'))
                                            <div class="col-sm-4 index-delete-button-container">
                                                <form action="{{route('employee-requests.destroy', $employee_request->id)}}" method="post" class="delete-form">
                                                    {{{csrf_field()}}}
                                                    {{{method_field('DELETE')}}}
                                                    <button type="submit" class="btn btn-link index-delete-button">
                                                        <i class="fa fa-times fa-2x"></i>
                                                    </button>
                                                </form>
                                            </div>
                                            @endif
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(".delete-form").on('submit', function (e) {
        if (confirm("Desea eliminar este registro?") == false) {
            e.preventDefault();
        }
    });
</script>
@endsection
