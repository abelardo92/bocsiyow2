@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Productos
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <div class="">
                        <div class="">
                            <form class="form-horizontal" action="{{route('warehouse.movements.update', $warehouseMovement->id)}}" method="POST">
                                {{{ csrf_field() }}}

                                <div class="container" style="width:100%;">
                                    <div class="row">
                                        <div class="col-md-6 container">
                                            <div class="row">
                                            </div>
                                            <div class="row">
                                                <label class="control-label"> Lista de productos de pedido (Folio {{ $warehouseMovement->request->folio }}, Sucursal {{ $warehouseMovement->subsidiary->name }})</label>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Codigo</th>
                                                            <th>Nombre</th>
                                                            <th>Cantidad</th>
                                                            <th>P. unitario</th>
                                                            <th>Precio</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @php
                                                        $total = 0;
                                                    @endphp
                                                    @foreach($warehouseMovement->request->products as $warehouseProduct)
                                                    <tr>
                                                        <td>{{ $warehouseProduct->product->code }}</td>
                                                        <td>{{ $warehouseProduct->product->name }}</td>
                                                        <td>{{ $warehouseProduct->quantity }}</td>
                                                        <td>{{ $warehouseProduct->amount }}</td>
                                                        <td>{{ $warehouseProduct->amount * $warehouseProduct->quantity }}</td>
                                                    </tr>
                                                    @php
                                                        $total += $warehouseProduct->amount * $warehouseProduct->quantity;
                                                    @endphp
                                                    @endforeach
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td><b>Total:</b></td>
                                                            <td>{{ $total }}</td>
                                                        </tr>
                                                    </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 container">
                                            <div class="row">
                                                <label class="control-label col-md-5"> Agregar productos</label>
                                                <div class="col-sm-5">
                                                    <select name="product" id="product" class="select2 form-control">
                                                    @foreach($warehouseProducts as $warehouseProduct)
                                                        <option value="{{$warehouseProduct->id}}" data-value="{{$warehouseProduct->amount}}"
                                                        data-code="{{$warehouseProduct->code}}"
                                                        @if($warehouseMovement->containsProduct($warehouseProduct->id))
                                                            disabled="disabled"
                                                        @endif
                                                        >{{$warehouseProduct->name}}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-sm-1">
                                                    <input type="button" class="btn btn-success" id="addProduct" v-on:click="addProduct" value="Añadir"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <table class="table table-striped" id="productsTable">
                                                        <thead>
                                                            <tr>
                                                                <th>Cod</th>
                                                                <th>Nombre</th>
                                                                <th>Cantidad</th>
                                                                <th>P.unit</th>
                                                                <th>Precio</th>
                                                                <th>Opciones</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @php
                                                            $total2 = 0;
                                                        @endphp
                                                        @foreach($warehouseMovement->products as $product)
                                                        <tr id='product{{ $product->product->id }}'>
                                                            <td>{{ $product->product->code }}</td>
                                                            <td>
                                                                <input type='hidden' id='product_ids' name='product_ids[]' value='{{ $product->product->id }}' />
                                                                <input type='hidden' id='product_amounts' name='product_amounts[]' value='{{ $product->amount }}' />
                                                                {{ $product->product->name }}
                                                            </td>
                                                            <td>
                                                                <input class='form-control product_quantities' type='number' min='1' id='product_quantities' name='product_quantities[]' class='form-control' value='{{ $product->quantity }}'/>
                                                            </td>
                                                            <td class="amounts">{{ $product->amount }}</td>
                                                            <td class="subtotals">{{ $product->amount * $product->quantity }}</td>
                                                            <td>
                                                                <input type="button" class="btn btn-danger" onClick='deleteProduct({{ $product->product->id }},{{ $product->id }})' value="Borrar"/>
                                                            </td>
                                                        </tr>
                                                        @php
                                                            $total2 += $product->amount * $product->quantity;
                                                        @endphp
                                                        @endforeach
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td><b>Total:</b></td>
                                                            <td id="movementTotal">{{ $total2 }}</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" name="request_id" value="{{$warehouseMovement->request->id}}">

                                <div class="form-group">
                                    <div class="col-sm-offset-9 col-sm-1">
                                        <button type="submit" name="submit" class="btn btn-default" value="save">Guardar</button>
                                    </div>
                                    <div class="col-sm-1">
                                        <button type="submit" name="submit" class="btn btn-primary" value="finalize">Finalizar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.4/vue.js"></script>-->
    <script>

    $("#product").val("");

    function refreshTotals()
    {
        var total = 0;
        $('.amounts').each(function (index) {
            var quantity = $(this).closest('tr').find('.product_quantities').val();
            var amount = $(this).html();
            var subtotal = parseFloat(amount * quantity).toFixed(2);  
            $(this).next("td").text(subtotal);
            total = total + parseFloat(subtotal);
        });
        $("#movementTotal").text(parseFloat(total).toFixed(2));
    }
    $('body').on('change', '.product_quantities', function() {
        refreshTotals();
    });

    //import Vue from 'vue'
    //new Vue({
    /*
    var app = new Vue({
        el: '.panel-body',
        data: {
            message: "hola",
            products: [
                {
                        id : $("#product").val(),
                        name : $("#product option:selected").text(),
                        quantity: 1,
                }
            ]
        },
    });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': Laravel.csrfToken
            }
        });

        $('#addProduct').on('click', function (evt) {
            alert(JSON.stringify(app.products));
            if($("#product").val() != null) {
                app.products.push({
                    id : $("#product").val(),
                    name : $("#product option:selected").text(),
                    quantity: 1,
                });
                $('#product option:selected').attr('disabled','disabled');
                $("#product").val("");
                $('#product').select2();
            }
        });

        function removeProduct(index, id){
            app.products.splice(index, 1);
            $('#product option[value="' + id + '"]').removeAttr('disabled');
            $('#product').select2();
        }
        */
        
        $('#addProduct').on('click', function (evt) {
            if($("#product").val() != null){
                var value = $("#product").val();
                var name = $("#product option:selected").text();
                var amount = $("#product option:selected").data('value');
                var code = $("#product option:selected").data('code');

                var idInput = "<input type='hidden' id='product_ids' name='product_ids[]' value='"+value+"' /><input type='hidden' id='product_amounts' name='product_amounts[]' value='"+amount+"' />";
                var quantityInput = "<input class='form-control product_quantities' type='number' min='1' id='product_quantities' name='product_quantities[]' class='form-control' value='1'/> ";
                var removeButton = "<input type='button' class='btn btn-danger' onClick='removeProduct("+value+")' value='Borrar'></input>";
                var htmlString = "<tr id='product"+value+"'><td>"+code+"</td><td>"+idInput+name+"</td><td>"+quantityInput+"</td><td class='amounts'>"+amount+"</td><td class='subtotals'>"+amount+"</td><td>"+removeButton+"</td></tr>";
                if($("#productsTable >tbody >tr").length > 1) {
                    $("#productsTable > tbody").find('tr:last').prev().after(htmlString);
                } else {
                    $(htmlString).insertBefore('#productsTable > tbody > tr:first');
                }
                
                $('#product option:selected').attr('disabled','disabled');
                $("#product").val("");
                $('#product').select2();
            }
            refreshTotals();
        });
        
        function removeProduct(id){
            $('#product' + id).remove();
            $('#product option[value="' + id + '"]').removeAttr('disabled');
            $('#product').select2();
            refreshTotals();
        }

        function deleteProduct(productId, id) {
            $.ajax({
                type: 'get',
                dataType: 'json',
                url: '/home/warehouse/movements/'+id+'/destroyProduct',
                success: function(data){
                    if(data.is_deleted){
                        removeProduct(productId);
                    }    
                },
                error: function (error) {
                    alert('No se pudo eliminar el producto');
                }
            });
        }
        
    </script>
@endsection