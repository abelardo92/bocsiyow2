@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            @include('admin.warehouse.movement_menu')
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Entradas
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form class="form-horizontal" action="{{route('warehouse.movements.storeEntry')}}" method="POST" enctype="multipart/form-data">
                        {{{ csrf_field() }}}
                        <warehouse-movement-table-form
                            :warehouse_products="{{$warehouseProducts->toJson()}}"
                            :products_str="[]"
                        ></warehouse-movement-table-form>
                        <div class="form-group">
                            <div class="col-sm-offset-10 col-sm-2">
                                <button type="submit" class="btn btn-default">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.4/vue.js"></script>
    <script>
    //import Vue from 'vue'
    //new Vue({
    /*
    var app = new Vue({
        el: '.panel-body',
        data: {
            message: "hola",
            products: [
                {
                        id : $("#product").val(),
                        name : $("#product option:selected").text(),
                        quantity: 1,
                }
            ]
        },
    });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': Laravel.csrfToken
            }
        });

        $('#addProduct').on('click', function (evt) {
            alert(JSON.stringify(app.products));
            if($("#product").val() != null) {
                app.products.push({
                    id : $("#product").val(),
                    name : $("#product option:selected").text(),
                    quantity: 1,
                });
                $('#product option:selected').attr('disabled','disabled');
                $("#product").val("");
                $('#product').select2();
            }
        });

        function removeProduct(index, id){
            app.products.splice(index, 1);
            $('#product option[value="' + id + '"]').removeAttr('disabled');
            $('#product').select2();
        }
        */
        
        $('#addProduct').on('click', function (evt) {
            if($("#product").val() != null){
                var value = $("#product").val();
                var name = $("#product option:selected").text();
                var amountValue = $("#product option:selected").data('value');
                var codeValue = $("#product option:selected").data('code');
                var idInput = "<input type='hidden' id='product_ids' name='product_ids[]' value='"+value+"' /><input type='hidden' id='product_amounts' name='product_amounts[]' value='"+amountValue+"' />";
                var priceInput = "<input class='form-control' type='number' step='any' min='0' id='product_prices' name='product_prices[]' value='0'/> ";
                var quantityInput = "<input class='form-control' type='number' min='1' id='product_quantities' name='product_quantities[]' value='1'/> ";
                var removeButton = "<button class='btn btn-danger' onClick='removeProduct("+value+")'>Borrar</button>";
                $("#productsTable > tbody").append("<tr id='product"+value+"'><td>"+codeValue+"</td><td>"+idInput+name+"</td><td>"+quantityInput+"</td><td>"+priceInput+"</td><td>"+removeButton+"</td></tr>");
                $('#product option:selected').attr('disabled','disabled');
                $("#product").val("");
                $('#product').select2();
            }
        });
        
        function removeProduct(id) {
            $('#product' + id).remove();
            $('#product option[value="' + id + '"]').removeAttr('disabled');
            $('#product').select2();
        }
        
    </script>
@endsection