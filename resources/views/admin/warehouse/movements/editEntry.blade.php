@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            @include('admin.warehouse.movement_menu')
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Entradas
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form class="form-horizontal" action="{{route('warehouse.movements.updateEntry', $warehouseMovement->id)}}" method="POST" enctype="multipart/form-data">
                        {{{ csrf_field() }}}
                        <warehouse-movement-table-form
                            :warehouse_products="{{$warehouseProducts->toJson()}}"
                            :products_str="{{$warehouseMovement->products->toJson()}}"
                        ></warehouse-movement-table-form>
                        <div class="form-group">
                            <div class="col-sm-offset-10 col-sm-2">
                                <button type="submit" class="btn btn-default">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
