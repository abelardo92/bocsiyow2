<p><strong>Fecha:</strong> {{ $warehouseMovement->created_at->format('d-m-Y') }}</p>
@if($warehouseMovement->subsidiary != null)
<p><strong>Para sucursal:</strong> {{ $warehouseMovement->subsidiary->name }}</p>
@endif
<p>
    <strong>Folio de 
        @if($warehouseMovement->type == 2)
            salida: 
        @else
            entrada: 
        @endif
    </strong> 
{{ $warehouseMovement->folio }}</p>
@if($warehouseMovement->request != null)
<p><strong>Folio de pedido:</strong> {{ $warehouseMovement->request->folio }}</p>
@endif
<h4><strong>
    @if($warehouseMovement->type == 2)
        SALIDAS 
    @else
        ENTRADAS 
    @endif
    DE ALMACEN (PRODUCTOS NO SURTIDOS)
</strong></h4>

<table width="100%">
    <tr>
        <th>#</th>
        <th>Código</th>
        <th>Area</th>
        <th>Concepto</th>
        <th>Unidades</th>
        <th>Importe unitario</th>
        <th>Total</th>
    </tr>
    <?php 
        $total = 0;
        $suministros = 0;
        $aseo = 0;
        $ventas = 0;
        $papeleria = 0;
    ?>
    @foreach($warehouseMovement->request->products as $index => $product)
        <?php 
            $showProduct = false;
            $subtrackMovementQuantity = false; 
        ?>
        @if($movementProduct = $warehouseMovement->products->where('warehouse_product_id', $product->product->id)->first()) 
            @if($product->quantity > $movementProduct->quantity)
                <?php 
                    $showProduct = true; 
                    $subtrackMovementQuantity = true;
                ?>
            @endif
        @else
            <?php $showProduct = true; ?>
        @endif
        @if($showProduct)
            @if($subtrackMovementQuantity)
                <?php $quantity = $product->quantity - $movementProduct->quantity; ?>
            @else
                <?php $quantity = $product->quantity; ?>
            @endif
        <tr>
            <td align="center"> {{ $index + 1 }} </td>
            <td align="center"> {{ $product->product->code }} </td>
            <td align="center"> {{ $uses[$product->product->use] }} </td>
            <td align="center"> {{ $product->product->name }} </td>
            <td align="center"> {{ $quantity }}</td>
            <td align="center"> {{ $product->amount }} </td>
            <td align="center">
             {{ $quantity * $product->amount }} 
            </td>
        </tr>
        @php 
            if($product->product->accounting == 1) {
                $suministros += $quantity * $product->amount;
            } else
            if($product->product->accounting == 2) {
                $aseo += $quantity * $product->amount;
            } else
            if($product->product->accounting == 4) {
                $ventas += $quantity * $product->amount;
            } else
            if($product->product->accounting == 5) {
                $papeleria += $quantity * $product->amount;
            }
            $total += $quantity * $product->amount;
        @endphp
        @endif
    @endforeach
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td align="center"><b>Suministros:</b></td>
        <td align="center"> {{ $suministros }}</td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td align="center"><b>Aseo y limpieza:</b></td>
        <td align="center"> {{ $aseo }}</td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td align="center"><b>Ventas:</b></td>
        <td align="center"> {{ $ventas }}</td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td align="center"><b>Papeleria:</b></td>
        <td align="center"> {{ $papeleria }}</td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td align="center"><b>Total:</b></td>
        <td align="center"> {{ $total }}</td>
        <td></td>
    </tr>
</table>
<br/><br/><br/>
<table width="100%">
    <tr>
        <td><strong>Surtió la nota</strong></td>
        <td>_____________________________<br>
        @if($warehouseMovement->createdBy != null)
            {{ $warehouseMovement->createdBy->name }}
        @else
            Nombre y firma
        @endif
        </td>
        <td><strong>Recibió</strong></td>
        <td>_____________________________<br>Nombre y firma</td>
    </tr>
</table>
