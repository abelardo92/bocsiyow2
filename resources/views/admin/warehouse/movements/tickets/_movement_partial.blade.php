<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('images/logo.jpeg') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link href="/css/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.1/fullcalendar.min.css" />
    <link href="/css/app.css" rel="stylesheet"/>
    <style>
        table, th, td {
            text-align: center;
            margin-left: 15px;
            align: top;
        }
</style>
</head>
<body>
    <table style="width: 1200px;" >
        <tr>
            <td width="60%" >
                <table width="100%">
                    <tr>
                        <td width="50%">
                            <p><strong>Fecha:</strong> {{ $warehouseMovement->created_at->format('d-m-Y') }}</p>
                        </td>
                        <td width="50%">
                            @if($warehouseMovement->subsidiary != null)
                                <p><strong>Para sucursal:</strong> {{ $warehouseMovement->subsidiary->name }}</p>
                            @endif
                            @if($warehouseMovement->type != 2)
                                <p><strong>Proveedor: </strong> {{ $warehouseMovement->provider }}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><strong>No. de nota: </strong> {{ $warehouseMovement->note_folio }}</p>
                        </td>
                        <td>
                            @if($warehouseMovement->createdBy != null)
                                <p><strong>Capturó: </strong> {{ $warehouseMovement->createdBy->name }}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <strong>Folio de 
                                    @if($warehouseMovement->type == 2)
                                        salida: 
                                    @else
                                        entrada: 
                                    @endif
                                </strong> 
                                {{ $warehouseMovement->folio }}
                            </p>
                        </td>
                        @if($warehouseMovement->request != null)
                            <td>
                                <p><strong>Folio de pedido:</strong> {{ $warehouseMovement->request->folio }}</p>  
                            </td>
                        @endif
                    </tr>
                    @if($warehouseMovement->request != null)
                    <tr>
                        <td>
                            <p>
                                <strong>Solicitado por:</strong> {{ $warehouseMovement->request->createdBy->name }}
                            </p>
                        </td>
                        <td>
                            <p>
                                <strong>F. Solicitud:</strong> {{ $warehouseMovement->request->created_at->format('d-m-Y H:i:s') }}
                            </p>  
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <strong>Confirmado por:</strong> {{ $warehouseMovement->request->confirmedBy->name }}
                            </p>
                        </td>
                        <td>
                            <p>
                                <strong>F. Confirmacion:</strong> {{ $warehouseMovement->request->confirmed_at->format('d-m-Y H:i:s') }}
                            </p>  
                        </td>
                    </tr>
                    @endif
                    <tr>
                        <td>
                            <p>
                                <strong>
                                @if($warehouseMovement->type == 2)
                                    SALIDAS 
                                @else
                                    ENTRADAS 
                                @endif
                                    DE ALMACEN
                                </strong>
                            </p>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <th>COD</th>
                        <th>CONCEPTO</th>
                        <th>UNIDADES</th>
                        <th>P. UNITARIO</th>
                        <th>IMP TOTAL</th>
                    </tr>
                    <?php 
                        $total = 0;
                        $suministros = 0;
                        $aseo = 0;
                        $ventas = 0;
                        $papeleria = 0;
                    ?>
                    @foreach($warehouseMovement->products as $index => $product)
                        <?php 
                            $amount = $product->amount;
                            if($product->provider_price != null){
                                $amount = $product->provider_price;
                            }
                        ?>
                        <tr>
                            <td align="center"> {{ $product->product->code }} </td>
                            <td align="center"> {{ $product->product->name }} </td>
                            <td align="center"> {{ $product->quantity }} </td>
                            <td align="center"> {{ $amount }} </td>
                            <td align="center"> {{ $product->quantity * $amount }} </td>
                        </tr>
                        @php 
                            if($product->product->accounting == 1) {
                                $suministros += $product->quantity * $amount;
                            } else
                            if($product->product->accounting == 2) {
                                $aseo += $product->quantity * $amount;
                            } else
                            if($product->product->accounting == 4) {
                                $ventas += $product->quantity * $amount;
                            } else
                            if($product->product->accounting == 5) {
                                $papeleria += $product->quantity * $amount;
                            }
                            $total += $product->quantity * $amount;
                        @endphp
                    @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="center"><b>Suministros:</b></td>
                            <td align="center"> {{ $suministros }}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="center"><b>Aseo y limpieza:</b></td>
                            <td align="center"> {{ $aseo }}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="center"><b>Ventas:</b></td>
                            <td align="center"> {{ $ventas }}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="center"><b>Papeleria:</b></td>
                            <td align="center"> {{ $papeleria }}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="center"><b>Subtotal:</b></td>
                            <td align="center"> {{ $total }}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="center"><b>Descuento:</b></td>
                            <td align="center"> {{ $warehouseMovement->discount }}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="center"><b>Total:</b></td>
                            <td align="center"> {{ $total - $warehouseMovement->discount }}</td>
                            <td></td>
                        </tr>
                    </table>
                    <br/><br/><br/>
                    <table width="100%">
                        <tr>
                            <td><strong>Surtió la nota</strong></td>
                            <td>_____________________________<br>
                                @if($warehouseMovement->createdBy != null)
                                    {{ $warehouseMovement->createdBy->name }}
                                @else
                                    Nombre y firma
                                @endif
                            </td>
                            <td><strong>Recibió</strong></td>
                            <td>_____________________________<br>Nombre y firma</td>
                        </tr>
                    </table>
                </td>
                <td width="40%">
                    @if($image = $warehouseMovement->image()->get()->first())
                        <img src="{{$image->path}}" alt="{{$image->name}}" style="margin-top: -100px; width: 260px;">
                    @endif
                </td>
            </tr>
        </table> 
    </body>
</html>
