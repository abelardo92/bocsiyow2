@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            @include('admin.warehouse.movement_menu')
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Productos de almacen (Paquetes)</div>

                <div class="panel-body">
                    <form method="GET" action="{{ url('home/warehouse/products') }}">
                        <div class="pull-right">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" name="filter" placeholder="buscar...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Nombre</th>
                                <th>Uso</th>
                                <th>Monto</th>
                                <th>Contabilidad</th>
                                <th>En almacen</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td>{{ $product->code }}</td>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $uses[$product->use] }}</td>
                                    <td>{{ $product->amount }}</td>
                                    <td>{{ $accountings[$product->accounting] }}</td>
                                    <td>{{ $product->in_storage }}</td>
                                    <td>
                                        <a href="{{ route('warehouse.products.edit', $product->id) }}" class="btn btn-link btn-xs">Editar</a>
                                        @if(Auth::user()->isA('super-admin'))
                                        <form action="{{route('warehouse.product.destroy', $product->id)}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <input type="submit" class="btn btn-link btn-xs" value="Eliminar">
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $products->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
