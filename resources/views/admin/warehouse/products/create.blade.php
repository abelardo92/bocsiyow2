@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            @include('admin.warehouse.movement_menu')
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Productos
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <div class="">
                        <div class="">
                            <form action="{{route('warehouse.products.store')}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                {{{ csrf_field() }}}

                                <div class="form-group">
                                    <label for="code" class="col-sm-2 control-label">Codigo *</label>
                                    <div class="col-sm-3">
                                      <input type="text" class="form-control" id="code" name="code" required="required" value="{{ old('code') }}">
                                    </div>

                                    <label for="name" class="col-sm-2 control-label">Nombre *</label>
                                    <div class="col-sm-3">
                                      <input type="text" class="form-control" id="name" name="name" required="required" value="{{ old('name') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="use" class="col-sm-2 control-label">Uso *</label>
                                    <div class="col-sm-3">
                                      <select type="text" class="form-control" id="use" name="use" >
                                        @foreach( App\WarehouseProduct::getEnum('uses') as $key => $value)
                                           <option value="{{ $key }}">{{ $value }}</option> 
                                        @endforeach
                                      </select>
                                    </div>
                                    <label for="amount" class="col-sm-2 control-label">Monto</label>
                                    <div class="col-sm-3">
                                      <input type="number" min="0" step="any" class="form-control" id="amount" name="amount">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="accounting" class="col-sm-2 control-label">Contabilidad *</label>
                                    <div class="col-sm-3">
                                      <select class="form-control" id="accounting" name="accounting">
                                        @foreach( App\WarehouseProduct::getEnum('accountings') as $key => $value)
                                           <option value="{{ $key }}">{{ $value }}</option> 
                                        @endforeach
                                      </select>
                                    </div>
                                    <label for="product_id" class="col-sm-2 control-label">Producto correspondiente</label>
                                    <div class="col-sm-3">
                                      <select class="form-control" id="product_id" name="product_id">
                                        <option value="">Sin producto</option>
                                        @foreach( $products as $product)
                                           <option value="{{ $product->id }}">{{ $product->name }}</option> 
                                        @endforeach
                                      </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="type" class="col-sm-2 control-label">Producto para *</label>
                                    <div class="col-sm-3">
                                      <select class="form-control" id="type" name="type">
                                        @foreach( App\WarehouseProduct::getEnum('types') as $key => $value)
                                           <option value="{{ $key }}">{{ $value }}</option> 
                                        @endforeach
                                      </select>
                                    </div>

                                    <label for="image" class="col-sm-2 control-label">Foto(s)</label>
                                    <div class="col-sm-3">
                                      <input type="file" class="form-control" id="image" name="image[]" multiple="multiple">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-9 col-sm-2">
                                        <button type="submit" class="btn btn-default">Guardar</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
