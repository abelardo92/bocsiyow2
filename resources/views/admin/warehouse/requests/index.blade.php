@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Solicitudes a almacen
                    <div class="pull-right">
                        <a href="{{route('warehouse.requests.create')}}">Agregar solicitud</a>

                    </div>
                </div>

                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Folio</th>
                                <th>Solicitado por</th>
                                <th>Fecha</th>
                                <th>Ver información</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($requests as $request)
                                <tr>
                                    <td>{{ $request->folio }}</td>
                                    <td>
                                        @if($request->employee != null)
                                            {{ $request->employee->name }}    
                                        @endif
                                    </td>
                                    <td>{{ $request->created_at }}</td>
                                    <td><a href="{{ route('warehouse.requests.show', $request->id) }}">Ver información</a></td>
                                    <td>
                                        @if($request->movements->isEmpty())
                                            @if(!$request->is_confirmed)
                                            <a href="{{ route('warehouse.requests.edit', $request->id) }}" class="btn btn-link">Editar</a>
                                            <form action="{{ route('warehouse.requests.destroy', [$request->id]) }}" method="post">
                                                {{{csrf_field()}}}
                                                {{{method_field('DELETE')}}}
                                                <input type="submit" class="btn btn-link" value="Eliminar">
                                            </form>
                                            @endif
                                        @else
                                            @php
                                                $movement = $request->movements->first();
                                            @endphp
                                            @if($movement->is_confirmed)
                                                <a href="{{ route('warehouse.movements.printSubsidiary', $movement->id) }}">Imprimir pedido</a>
                                            @else
                                                <a href="{{ route('warehouse.movements.confirm', $movement->id) }}">Autorizar pedido</a>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
