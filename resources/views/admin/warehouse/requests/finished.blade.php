@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            @include('admin.warehouse.movement_menu')
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Pedidos finalizados
                </div>

                <div class="panel-body">
                    <form method="GET" action="{{ url('home/warehouse/requests/finished') }}">
                        <div class="row">
                            <label for="code" class="col-sm-1 control-label">Sucursal</label>
                            <div class="col-sm-3">
                                <select class="form-control" id="subsidiary_id" name="subsidiary_id">
                                    @if($user->access_susidiary_id != null)
                                        <option value="">TODOS</option>
                                    @endif
                                    @foreach($subsidiaries as $subsidiary)
                                    <option @if($subsidiary_id == $subsidiary->id) selected="selected" @endif value="{{$subsidiary->id}}">{{$subsidiary->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <label for="start" class="col-sm-1">Fecha: </label>
                            <div class="input-group input-daterange col-sm-5">
                                <input type="text" class="form-control" name="start" value="{{$start}}">
                                <span class="input-group-addon">al</span>
                                <input type="text" class="form-control" name="end" value="{{$end}}">
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Sucursal</th>
                                <th>Folio</th>
                                <th>Solicitado por</th>
                                <th>Fecha</th>
                                <th>Ver información</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($requests as $request)
                                <tr>
                                    <td>{{ $request->subsidiary->name }}</td>
                                    <td>{{ $request->folio }}</td>
                                    <td>
                                        @if($request->employee != null)
                                            {{ $request->employee->short_name }}    
                                        @endif
                                    </td>
                                    <td>{{ $request->created_at }}</td>
                                    <td><a href="{{ route('warehouse.requests.show', $request->id) }}">Ver información</a></td>
                                    <td>
                                        @if($request->movements == null || count($request->movements) == 0)
                                            <a href="{{ route('warehouse.movements.create', $request->id) }}">Realizar pedido</a>
                                        @else
                                            @php
                                                $movement = $request->movements->first();
                                            @endphp
                                            @if(!$movement->is_confirmed)
                                                <a href="{{ route('warehouse.movements.edit', $movement->id) }}">Editar pedido</a><br/>
                                            @endif
                                            <a href="{{ route('warehouse.movements.print', $movement->id) }}">Imprimir</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
