@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Catalogo de empleados
                    <p class="pull-right">
                        <button type="button" class="btn btn-primary btn-sm hide-in-print" title="imprimir" onclick="window.print();"><i class="fa fa-print"></i></button>
                    </p>
                </div>

                <div class="panel-body">
                    <form action="{{url('/home/catalogs/employees')}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}
                        <div class="form-group">
                            <label for="status">Estatus: </label>
                            <select name="status" id="status" class="form-control">
                                <option @if($status == 'all') selected="selected" @endif value="all">Todos</option>
                                <option @if($status == 'active') selected="selected" @endif value="active">Activos</option>
                                <option @if($status == 'inactive') selected="selected" @endif value="inactive">De baja</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default hide-in-print" value="Buscar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @foreach($employees->chunk(2) as $employees_chunk)
        <div class="row">
            @foreach($employees_chunk as $employee)
                <div class="col-md-6 col-xs-6 col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {{$employee->short_name}} - {{$employee->key}}
                            <p class="pull-right">
                                {{$employee->active ? 'activo' : "baja - {$employee->inactived_at}"}}
                            </p>
                        </div>
                        <div class="panel-body">
                            <p class="pull-left"><strong>{{$employee->name}}</strong></p>
                            <p class="pull-right"><strong>RFC: </strong> {{$employee->rfc}}</p>
                            <div class="clearfix"></div>
                            <p class="pull-left"><strong>Fecha ingreso: </strong> {{$employee->entry_date}}</p>
                            <p class="pull-right"><strong>Salario diario: </strong> $ {{$employee->salary}}</p>

                            <div class="clearfix"></div>
                            <p class="pull-left"><strong>CURP: </strong> {{$employee->curp}}</p>
                            <p class="pull-right"><strong>NSS: </strong> {{$employee->nss}}</p>
                            <div class="clearfix"></div>
                            <p class="pull-left"><strong>Dirección: </strong> {{$employee->address}}</p>
                            <div class="clearfix"></div>
                            <p class="pull-left"><strong>Genero: </strong> {{$employee->gender}}</p>
                            <p class="pull-right"><strong>Estado civil: </strong> {{$employee->marital_status}}</p>
                            <div class="clearfix"></div>
                            <p class="pull-left"><strong>Salario IMSS: </strong> {{$employee->imss_salary}}</p>
                            <p class="pull-right"><strong>Infonavit: </strong> {{$employee->infonavit_number}}</p>
                            <div class="clearfix"></div>
                            <p class="pull-left"><strong>Correo: </strong> {{$employee->email}}</p>
                            <p class="pull-right"><strong>Telefono: </strong> {{$employee->phone}}</p>
                            <div class="clearfix"></div>
                            <p class="pull-left"><strong>Clave empleado: </strong> {{$employee->code}}</p>
                            
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endforeach
</div>
@endsection
