@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Nominas</div>

                <div class="panel-body">
                    <table class="table table-striped datatables">
                        <thead>
                            <tr>
                                <th>Periodo</th>
                                <th>Empleados</th>
                                <th>Total</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($paysheets as $paysheets_groups)
                                    <tr>
                                        <td>{{ $paysheets_groups->first()->key }}</td>
                                        <td>{{ $paysheets_groups->count() }}</td>
                                        <td>{{ $paysheets_groups->sum('neto') }}</td>
                                        <td>
                                            <a href="{{ route('paysheets.show', $paysheets_groups->first()->key) }}">Ver</a>
                                        </td>
                                    </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
