@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Clientes
                    <div class="pull-right">
                        <a href="{{route('customers.create')}}">Agregar cliente</a>
                    </div>
                </div>

                <div class="panel-body">
                <form method="GET" action="{{ url('home/customers') }}">
                <div class="pull-right">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" name="filter" placeholder="buscar...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <a href="{{route('customers.excel')}}" class="btn btn-success">Exportar excel</a>
                        </div>
                    </div>
                </div>
                </form>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Telefono</th>
                                <th>Correo</th>
                                <th>Facebook</th>
                                <th>Twitter</th>
                                <th>Cumpleaños</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($customers as $customer)
                                <tr>
                                    <td>{{ $customer->name }}</td>
                                    <td>{{ $customer->phone }}</td>
                                    <td>{{ $customer->email }}</td>
                                    <td>{{ $customer->facebook }}</td>
                                    <td>{{ $customer->twitter }}</td>
                                    <td>{{ $customer->birthday }}</td>
                                    <td class="row" style="width: 80px !important;">
                                        <div class="col-sm-6 index-edit-button-container">
                                            <a href="{{ route('customers.edit', $customer->id) }}" class="btn btn-link index-edit-button">
                                                <i class="fa fa-pencil-square-o fa-2x"></i>
                                            </a>
                                        </div>
                                        
                                        @if(Auth::user()->isA('super-admin'))
                                        <div class="col-sm-6 index-delete-button-container">
                                            <form action="{{route('customers.deactivate', $customer->id)}}" method="post" class="">
                                                {{{csrf_field()}}}
                                                <button type="submit" class="btn btn-link index-delete-button">
                                                    <i class="fa fa-times fa-2x"></i>
                                                </button>
                                            </form>
                                        </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if(isset($filter))
                        {{ $customers->appends(['filter' => $filter])->links() }}
                    @else
                        {{ $customers->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
