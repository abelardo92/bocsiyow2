@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Clientes
                    <p class="pull-right help-block">Campos con * son obligatorios.</p>
                </div>

                <div class="panel-body">
                    <form action="{{route('customers.update', $customer->id)}}" method="POST" class="form-horizontal">
                        {{{ csrf_field() }}}
                        {{{ method_field('PUT') }}}

                        <div class="form-group">
                            <label for="wallet_number" class="col-sm-4 control-label">Monedero *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="wallet_number" name="wallet_number" value="{{ $customer->wallet_number }}" @if($customer->wallet_number) disabled @endif>
                                <replace-wallet
                                    current="{{ $customer->wallet_number }}"
                                    customer_id="{{ $customer->id }}"
                                ></replace-wallet>
                            </div>
                        </div>

                        @if($customer->last_wallet_number)
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Monedero anterior *</label>
                                <div class="col-sm-6">
                                  <input type="text" class="form-control" value="{{ $customer->last_wallet_number }}" disabled>
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <label for="name" class="col-sm-4 control-label">Nombre*</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="name" name="name" required="required" value="{{ old('name', $customer->name) }}" @if(!Auth::user()->isA('super-admin')) disabled @endif>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="identify_number" class="col-sm-4 control-label">Numero de identificación</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="identify_number" name="identify_number" value="{{ old('identify_number', $customer->identify_number) }}">
                              <span class="help-block">IFE, INE, Licencia conducir, Pasaporte, etc.</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-sm-4 control-label">Telefono *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone', $customer->phone) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="birthday" class="col-sm-4 control-label">Cumpleaños *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control datepicker" id="birthday" name="birthday" value="{{ old('birthday', $customer->birthday) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-sm-4 control-label">Correo</label>
                            <div class="col-sm-6">
                              <input type="email" class="form-control" id="email" name="email" value="{{ old('email', $customer->email) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="facebook" class="col-sm-4 control-label">Facebook</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="facebook" name="facebook"  value="{{ old('facebook', $customer->facebook) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="twitter" class="col-sm-4 control-label">Twitter</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="twitter" name="twitter"  value="{{ old('twitter', $customer->twitter) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="residence_place" class="col-sm-4 control-label">Lugar de residencia *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="residence_place" name="residence_place" required="required" value="{{ old('residence_place', $customer->residence_place) }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="visits" class="col-sm-4 control-label">Numero de visitas *</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" id="visits" name="visits" disabled="disabled" value="{{ $customer->visits }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Servicios gratis</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" disabled="disabled" value="{{ $customer->services_free }}">
                            </div>
                        </div>

                        @if($user->isA('super-admin'))

                        <div class="form-group">
                            <label for="has_courtesy" class="col-sm-4 control-label">Cuenta con cortesia?</label>
                            <div class="col-sm-6">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="has_courtesy" value="1" @if($customer->has_courtesy) checked @endif> Si
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="has_courtesy" value="0" @if(!$customer->has_courtesy) checked @endif> No
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="service_id" class="col-sm-4 control-label">Servicio(s) con cortesia</label>
                            <div class="col-sm-6">
                                <select name="service_id[]" id="service_id" multiple="multiple" class="select2 form-control">
                                    <option value="0">Seleccionar uno o mas por favor...</option>
                                    @foreach($services as $service)
                                        @php 
                                        $courtesyService = $customer->courtesyServices->where('service_id',$service->id)->first();
                                        @endphp
                                        <option value="{{$service->id}}" @if($courtesyService) selected @endif>{{$service->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                            <div class="form-group">
                                <label for="residence_place" class="col-sm-4 control-label">Asociado a:</label>
                                <div class="col-sm-6">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="related_brand" value="1" @if($customer->related_brand == 1) checked @endif> Bocsiyow
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="related_brand" value="2" @if($customer->related_brand == 2) checked @endif> Sevilla
                                        </label>
                                    </div>
                                </div>
                            </div>
                        @else
                            <input type="hidden" name="related_brand" value="{{$customer->related_brand}}"/>
                        @endif

                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-6">
                                <button type="submit" class="btn btn-default">Guardar</button>
                                <div class="pull-right">
                                @if($customer->creator)
                                    Creado por: {{ $customer->creator->getName() }}<br/>
                                @endif
                                    Fecha de  creación: {{ $customer->created_at }}
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Monederos para referencia
                    <referral-modal
                        customer_id="{{ $customer->id }}"
                    ></referral-modal>
                </div>

                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Monedero</th>
                                <th>Cliente</th>
                                <th>Usado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($customer->referrals()->paginate(10) as $referral)
                                <tr>
                                    <td>{{ $referral->wallet_number }}</td>
                                    <td>{{ $referral->owner ? $referral->owner->name : '' }}</td>
                                    <td>{{ $referral->used ? 'Si' : 'No' }}</td>
                                    <td>
                                        <form action="{{route('customers.referrals.destroy', [$customer->id, $referral->id])}}" method="post">
                                            {{csrf_field()}}
                                            {{method_field('DELETE')}}
                                            <input type="submit" class="btn btn-danger btn-sm" value="Eliminar">
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{$customer->referrals()->paginate(10)->links()}}
                </div>

            </div>

        </div>
    </div>
</div>
@endsection
