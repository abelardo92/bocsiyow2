<div class="modal fade" id="modal-auth" tabindex="-1" role="dialog" aria-labelledby="modal-authLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-authLabel">Verifica tu identidad.</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" v-if="auth_error">
          @{{auth_error}}
        </div>
        <form action="/home/subsidiary/auth" method="post" @submit="makeAuth">
          {{{ csrf_field() }}}

          <div class="form-group">
            <label for="code" class="control-label">Clave empleado</label>
            <input type="text" class="form-control" id="code" name="code" required="required" v-model="auth.code">
          </div>

          <div class="form-group">
            <label for="password" class="control-label">Contraseña *</label>
            <input type="password" class="form-control" id="password" name="password" required="required" v-model="auth.password">
          </div>

          <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Enviar">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>