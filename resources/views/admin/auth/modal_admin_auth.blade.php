<div class="modal fade" id="modal-admin-auth" tabindex="-1" role="dialog" aria-labelledby="modal-admin-authLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-admin-authLabel">Verifica tu identidad.</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" v-if="auth_error">
          @{{auth_error}}
        </div>
        <form action="/home/subsidiary/auth/admin" method="post" @submit="makeAdminAuth">
          {{{ csrf_field() }}}

          <div class="form-group">
            <label for="email" class="control-label">Correo electronico *</label>
            <input type="email" class="form-control" id="email" name="email" required="required" v-model="admin.email">
          </div>

          <div class="form-group">
            <label for="password" class="control-label">Contraseña *</label>
            <input type="password" class="form-control" id="password" name="password" required="required" v-model="admin.password">
          </div>

          <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Enviar">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>