<div class="row">
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                Ventas del dia
                <p class="pull-right">
                    {{ Carbon\Carbon::now()->formatLocalized('%d %B') }}
                </p>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Sucursal</th>
                            <th>Importe</th>
                            <th>Servicios</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $total = 0;
                        $total_services = 0;
                        ?>
                        @foreach($subsidiaries as $subsidiary)
                            <tr>
                                <td>{{$subsidiary->name}}</td>
                                <td>$ 
                                @if($subsidiary->id == 11)
                                    <?php 
                                        $subtotal = $subsidiary->originLaundryServices()->today()->notCanceled()->sum('amount');
                                    ?>
                                @else
                                    <?php $subtotal = 0; ?>
                                    @if($subsidiary->is_laundry)
                                    <?php
                                        $cash_registerss = $subsidiary->cashRegisters()->today()->get();
                                        foreach ($cash_registerss as $cr) {
                                            $subtotal += $cr->totalLaundry();
                                        }
                                    ?>
                                    @else
                                        <?php 
                                        $subsidiary_sales = $subsidiary->todaySales;
                                        $subtotal += $subsidiary_sales->sum('subtotal');
                                        ?>
                                    @endif
                                @endif
                                {{ $subtotal }}
                                <?php $total += $subtotal; ?>
                                </td>
                                <td>
                                    @if($subtotal > 0)
                                        <?php  
                                        $today_services = $subsidiary->getServicesToday();
                                        $total_services += $today_services; 
                                        ?>
                                        @if($today_services > 0)
                                            <a href="#" data-toggle="modal" data-target="#modal-subsidiary-sales-{{$subsidiary->id}}">
                                                {{ $today_services }}
                                            </a>
                                        @else 
                                            0 
                                        @endif
                                    @else
                                        0
                                    @endif
                                </td>
                            </tr>
                            <div class="modal fade" id="modal-subsidiary-sales-{{$subsidiary->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-subsidiary-sales-{{$subsidiary->id}}-Label">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="modal-subsidiary-sales-{{$subsidiary->id}}-Label">Resumen de ventas de {{$subsidiary->name}}</h4>
                                        </div>
                                        <div class="modal-body services-table">
                                            <div class="row">
                                                <div class="col-sm-1"><b>Ticket#</b></div>
                                                <div class="col-sm-4"><b>Cliente</b></div>
                                                <div class="col-sm-4"><b>Barbero</b></div>
                                                <div class="col-sm-3"><b>Servicios</b></div>
                                            </div>
                                            @foreach ($subsidiary_sales as $subsidiary_sale)
                                                @if($subsidiary_sale->services)
                                                <div class="row">
                                                    <div class="col-sm-1">
                                                        {{$subsidiary_sale->folio}}
                                                    </div>
                                                    <div class="col-sm-4">
                                                        @if($subsidiary_sale->customer)
                                                            {{$subsidiary_sale->customer->name}}
                                                        @endif
                                                    </div>
                                                    <div class="col-sm-4">
                                                        @if($subsidiary_sale->employee)
                                                            {{$subsidiary_sale->employee->name}}
                                                        @endif
                                                    </div>
                                                    <div class="col-sm-3">
                                                        @foreach ($subsidiary_sale->services as $subsidiary_service)
                                                            @if($subsidiary_service->service_id != 19)
                                                                {{$subsidiary_service->service->name}} <b>({{$subsidiary_service->qty}})</b><br>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <tr>
                            <td>Total:</td>
                            <td>$ {{ $total }}</td>
                            <td>{{ $total_services }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                Ventas por turno
                <p class="pull-right">
                    {{ Carbon\Carbon::now()->formatLocalized('%d %B') }}
                </p>
            </div>
            <div class="table-wrapper-scroll-y">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Sucursal</th>
                            <th>Importe</th>
                            <th>Turno</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total = 0; ?>
                        @foreach($cash_registers as $cash_register)
                            <tr>
                                <td>{{$cash_register->subsidiary->name}}</td>
                                <td>
                                <?php
                                if($cash_register->subsidiary->id != 11) {
                                    $subtotal = 0;
                                    if($cash_register->subsidiary->is_laundry) {
                                        $subtotal += $cash_register->totalLaundry();
                                    } else {
                                        $subtotal += $cash_register->sales()->today()
                                        ->finished()->notCanceled()->sum('subtotal');
                                    }    
                                } else {
                                    $subtotal = $cash_register->laundryServices()->notCanceled()->get()->sum('amount');
                                }
                                ?>
                                $ {{$subtotal}}
                                </td>
                                <td>{{$cash_register->turn->name}}</td>
                                
                                <?php 
                                    $total += $subtotal;
                                ?>
                            </tr>
                        @endforeach
                        <tr>
                            <td>Total:</td>
                            <td>$ {{ $total }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="panel panel-default">
            <div class="panel-heading">
                Pendientes
            </div>
            <div class="table-wrapper-scroll-y">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Sucursal</th>
                            <th>Asunto</th>
                            <th>Ver</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total = 0; ?>
                        @foreach($pendings as $pending)
                            <tr>
                                <td>{{$pending->date}}</td>
                                <td>
                                    @if($pending->subsidiary != null)
                                        {{ $pending->subsidiary->name }}
                                    @endif
                                </td>
                                <td>{{$pending->subject}}</td>
                                <td>
                                    <a class="btn btn-link" href="{{ route('pendings.show', $pending->id) }}">Ver</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                Horarios por sucursal
                <p class="pull-right">
                    {{ Carbon\Carbon::now()->formatLocalized('%A %d %B') }} -
                    @if(Carbon\Carbon::now()->format('H:i:s') <= '15:45:00')
                        Turno Matutino
                    @else
                        Turno Vespertino
                    @endif
                </p>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Sucursal</th>
                            <th>Puesto</th>
                            <th>Nombre</th>
                            <th>Asistencia</th>
                            <th>Hora entrada</th>
                            <th>Doble</th>
                            <th>¿Acceso?</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $curr_subsidiary = 0; ?>
                        @foreach($subsidiaries as $subsidiary)

                            <?php $schedules = $subsidiary->schedules()->with(['employee', 'turn'])->today()->currentTurn($subsidiary->id)->get(); ?>
                            @if($schedules->count() > 0)
                                @foreach($schedules as $schedule)
                                    @if(!$schedule->employee->active)
                                        @continue
                                    @endif
                                    <tr
                                    @if($curr_subsidiary % 2 == 0)
                                        class="active"
                                    @endif
                                    >
                                    <?php
                                        $todayAttendances = $schedule->employee->attendances()->where('subsidiary_id', $subsidiary->id)->today()->get();
                                    ?>
                                        <td>{{$subsidiary->name}}</td>
                                        <td>{{$schedule->employee->job}}</td>
                                        <td>{{$schedule->employee->short_name}}</td>
                                        <td>
                                            @if($todayAttendances->count())
                                                <i class="fa fa-check"></i>
                                            @else
                                                <i class="fa fa-close"></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if($todayAttendances->count())
                                                {{$todayAttendances->first()->getTime()}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($schedule->turn->identifier == 3)
                                                <i class="fa fa-check"></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if($todayAttendances->count())
                                                <i class="fa fa-unlock"></i>
                                            @else
                                                @if($schedule->can_check)
                                                    <i class="fa fa-unlock"></i>
                                                @else
                                                    @if($schedule->canCheck())
                                                        <i class="fa fa-unlock"></i>   
                                                    @else
                                                        <form action="{{route('employees.schedules.unlock', $schedule->id)}}" method="post">
                                                            {{ csrf_field() }}
                                                            {{ method_field('PUT') }}
                                                            <button class="btn btn-link">
                                                                <i class="fa fa-lock"></i>
                                                            </button>
                                                        </form>
                                                    @endif
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{route('employees.edit', $schedule->employee->id)}}">Editar</a>
                                        </td>
                                   </tr>
                                @endforeach
                                <?php $curr_subsidiary++; ?>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @if($user->isA('super-admin','super-admin-restringido'))
        <log-in-to-subsidiary
        :subsidiaries="{{$subsidiaries->toJson()}}"
        ></log-in-to-subsidiary>
    @endif
    <reprint-cash-cut
    :subsidiaries="{{$subsidiaries->toJson()}}"
    :turns="{{$turns->toJson()}}"
    :exchange_rate="{{$exchange_rate->rate}}"
    ></reprint-cash-cut>
    @if($user->isA('super-admin','super-admin-restringido'))
        <cancel-admin-sale
        :subsidiaries="{{$subsidiaries->toJson()}}"
        role="{{$user->roles->first()->name}}"
        ></cancel-admin-sale>
    @endif
</div>