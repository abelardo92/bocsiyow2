<?php
    if(isset($sale) && $sale->subsidiary->configuration_id != null) {
        $configuration = App\Configuration::find($sale->subsidiary->configuration_id);
    } else {
        $configuration = App\Configuration::first();
    }
    $imagepath = $configuration->image->path;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Tickets</title>

    <style>
        html, body {
            max-width: 100%;
            margin: 0 auto;
            font-size: 8.5px;
            padding: 0px;
            float: left;
            font-family: Arial, Helvetica, sans-serif;
            /*font-size: 7px;*/
            font-style: normal;
            line-height: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            color: #000;
            max-width: 400px;
        }
        table th {
            border-bottom: 1px solid black;
        }

        .text-center{
            text-align: center;
        }

        .sign {
            width: 50%;
            margin: 100px auto 0;
            display: block;
        }
        .sign p {
            border-top: 1px solid black;
            width: 100%;
            text-align: center;
        }

        .background{
            position:absolute;
            z-index:-1;
            background:white;
            color:yellow;
        }

        .content{
            position:absolute;
            z-index:1;
        }

        .bg-text
        {
            color:lightgrey;
            font-size:44px;
            transform:rotate(330deg);
            -webkit-transform:rotate(330deg);
        }

        @page{
           margin: 0;
        }
    </style>

</head>
<body>
    <img src="{{ $imagepath }}" alt="" style="width: 100%;">
    @if(isset($sale) && $sale->subsidiary->related_brand == 1)
    <p class="text-center">@yield('title', 'BocsiYow')</p>
    @endif
    <p>
    @if(isset($sale) && $sale->subsidiary->barber_can_sell)
        {{ $sale->employee->name }}<br>
        RFC: {{ $sale->employee->rfc }}<br>
        CURP: {{ $sale->employee->curp }}<br>
    @else
        {{ $configuration->razon_social }}<br>
        {{ $configuration->rfc }}<br>
        {{ $configuration->curp }}<br>
    @endif
    {{ $configuration->regimen }}<br>
    {{ $configuration->address }}<br>
    Lugar y fecha de expedición: <br>
    {{ $configuration->created_in }} - @yield('fecha', '')</p>

    <?php if($subdomain && $subsidiary->key != "07"){ ?>
        <p class="text-center">
        <?php if($subsidiary->key != "08"){
            echo "Sucursal";
        }?>
        "@yield('subsidiary', '')"</p>
        <p class="text-center">"@yield('address', '')"</p>
    <?php } ?>
    
    @yield('content')

    {{-- <p>{{ $configuration->text_footer }}</p> --}}

    @yield('barber')
</body>
</html>
