@php
    $user = Auth::user();
@endphp
@if(!$user->isA('employee') && ($subdomain || $user->isA('super-admin', 'super-admin-restringido', 'manager')) && (!$subdomain || ($subdomain && $current_subsidiary->id != 11)) && !$user->isA('subsidiary-admin', 'supervisor', 'cashier-admin'))
<li><a href="{{route('customers.index')}}">Clientes</a></li>
@endif

<!-- @if($user->isA('subsidiary-admin'))
<li><a href="{{route('inventories.index')}}">Inventarios</a></li>
@endif -->

@if((!$user->isA('employee','supervisor') && ($subdomain || $user->isA('super-admin', 'super-admin-restringido', 'manager', 'cashier-admin', 'accountant')) && (!$subdomain || ($subdomain && $current_subsidiary->id != 11))) || $user->isA('subsidiary-admin') || $user->id == 75 || $user->id == 87 || $user->id == 77)
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        Inventarios
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <li><a href="{{route('inventories.index')}}">Inventarios</a></li>
        @if($subdomain)
            <li><a href="{{route('warehouse.requests.index')}}">Solicitudes a almacen</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager', 'subsidiary-admin', 'cashier-admin'))
            <li><a href="{{route('warehouse.products.adjust-must-haves')}}">Ajustar inventario deseado (Almacen)</a></li>
        @endif
        <!-- Acceso a Gerardo Reyes, Miguel, Brisa, egresos -->
        @if($user->isA('super-admin', 'super-admin-restringido', 'subsidiary-admin', 'cashier-admin', 'manager', 'accountant') || $user->id == 88 || $user->id == 75 || $user->id == 87)

            @if(!$user->isA('subsidiary-admin', 'cashier-admin'))
            <li><a href="{{route('warehouse.products.index')}}">Administrar productos de almacen</a></li>
            @endif
            <li><a href="{{route('warehouse.requests.pending')}}">Solicitudes realizadas de almacen</a></li>
        @endif
        @if($user->isA('super-admin'))
            <li><a href="{{route('promotions.index')}}">Promociones</a></li>
        @endif
    </ul>
</li>
@endif

@if($user->isA('super-admin', 'super-admin-restringido', 'subsidiary-admin', 'manager', 'cashier-admin'))
<?php //dd($user); ?>
    <li><a href="{{url('/home/admin/sales')}}">Ventas activas</a></li>
@endif

<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        Empleados
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        @if($user->isA('super-admin', 'super-admin-restringido', 'horarios', 'nomina', 'manager', 'cashier-admin', 'subsidiary-admin'))
            <li><a href="{{route('employees.index')}}">Administrar Empleados</a></li>
        @endif
        {{-- ! SHOWING TO ADMON FOR NOW --}}
        @if($user->isA('super-admin', 'super-admin-restringido') && isset($current_subsidiary) && $current_subsidiary->id == 7)
            <li><a href="{{url('/home/sales/employees/create')}}">Vender producto(s) a empleado</a></li>
            @if(!isset($current_subsidiary) || $current_subsidiary->id == 7)
            <li><a href="{{url('/home/sales/employees/list')}}">Producto(s) vendidos a empleado</a></li>
            @endif
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager', 'supervisor'))
            <li><a href="{{url('/home/catalogs/employees')}}">Resumen Empleados</a></li>
            <li><a href="{{url('/home/issues/create2')}}">Crear incidencia</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido'))
            <li><a href="{{route('employee-requests.index')}}">Solicitudes de empleo</a></li>
        @endif
    </ul>
</li>

@if($user->isA('super-admin', 'super-admin-restringido', 'horarios', 'nomina', 'subsidiary-admin', 'accountant', 'supervisor', 'cashier-admin', 'manager') || $user->id == 88 || $user->id == 75
        || $user->id == 87 || $user->id == 77)
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            Reportes
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu" style="max-height: 550px; overflow: scroll;">
        @if($user->isA('super-admin', 'super-admin-restringido', 'horarios', 'nomina', 'manager', 'cashier-admin'))
            <li class="active"><a href="javascript:void(0);">Reporte de nomina</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido'))
            <li><a href="{{url('/home/reports/attendances')}}">Checadas del dia</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager', 'cashier-admin'))
            <li><a href="{{url('/home/reports/attendance-card')}}">Tarjeta de asistencia</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager', 'cashier-admin'))
            <li><a href="{{url('/home/reports/issues')}}">Incidencias</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'horarios', 'supervisor', 'cashier-admin', 'manager'))
            <li><a href="{{url('/home/reports/schedules')}}">Horarios</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager', 'cashier-admin', 'supervisor'))
            <li><a href="{{url('/home/reports/weekly/paysheet/barber')}}">Semanal por barbero</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager', 'supervisor'))
            <li><a href="{{url('/home/reports/weekly/paysheet/employee')}}">Semanal por empleado</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'nomina', 'manager', 'supervisor', 'cashier-admin'))
            <li><a href="{{url('/home/reports/signatureByEmployee')}}">Firma por empleado</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'nomina', 'manager'))
            <li><a href="{{url('/home/reports/weekly/paysheet/resume')}}">Resumen</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager'))
            <li><a href="{{url('/home/paysheets')}}">Nominas</a></li>
            <li><a href="{{url('/home/deposits')}}">Depósitos</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'accountant'))
            <li><a href="{{url('/home/reports/cashCutsByDate')}}">Movimientos de caja por fecha</a></li>
            <li><a href="{{url('/home/reports/cashierMovementsResume')}}">Resumen de movimientos de caja</a></li>
        @endif

        @if($user->isA('super-admin', 'super-admin-restringido', 'subsidiary-admin', 'supervisor'))
        <li><a href="{{url('/home/reports/weekly/barbers/excellence')}}">Empleados con excelencia</a></li>
        <li><a href="{{url('/home/reports/weekly/barbers/without-excellence')}}">Empleados sin excelencia</a></li>
        @endif

        @if($user->isA('super-admin', 'super-admin-restringido', 'subsidiary-admin', 'manager', 'cashier-admin', 'accountant'))
            <li class="active"><a href="javascript:void(0);">Reporte de ventas</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'accountant', 'subsidiary-admin', 'cashier-admin'))
            <li><a href="{{url('/home/reports/daily-cuts')}}">Cierres de caja</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'subsidiary-admin', 'manager'))
            <li><a href="{{url('/home/reports/sales/canceled')}}">Tickets y servicios cancelados</a></li>
            <li><a href="{{url('/home/reports/flow')}}">Flujo</a></li>
            <li><a href="{{url('/home/reports/daily')}}">Diario</a></li>
            <li><a href="{{url('/home/reports/accumulated-daily')}}">Diario acumulado</a></li>
            <li><a href="{{url('/home/reports/daily-turns')}}">Diario por turno</a></li>
            @if($user->isA('super-admin', 'super-admin-restringido'))
            <li><a href="{{url('/home/reports/daily-laundry-services')}}">Diario por servicios de lavandería</a></li>
            @endif
            <li><a href="{{url('/home/reports/subsidiary')}}">Por sucursal</a></li>
            <li><a href="{{url('/home/reports/subsidiaries')}}">Por sucursales</a></li>
            <li><a href="{{url('/home/reports/barber')}}">Por barbero</a></li>
            <li><a href="{{url('/home/reports/weekly/barbers')}}">Semanal por barbero</a></li>
            <li><a href="{{url('/home/reports/subsidiaries/turns')}}">Por sucursales por turno</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'subsidiary-admin', 'manager', 'cashier-admin'))
            <li><a href="{{url('/home/reports/sales/date')}}">Status por fecha</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'subsidiary-admin', 'manager'))
            <li><a href="{{url('/home/reports/sales/commission/status')}}">Status de comisiones</a></li>
            <li><a href="{{url('/home/reports/sales/commissions/products')}}">Comisión por productos</a></li>
            <li><a href="{{url('/home/reports/weekly/sold-products')}}">Productos vendidos</a></li>
            <li><a href="{{url('/home/reports/sales/tips')}}">Propinas</a></li>
            <li><a href="{{url('/home/reports/weekly/sales/commision/resume')}}">Comisiones por ventas</a></li>
            <li><a href="{{url('/home/reports/diaries')}}">Citas</a></li>
            <li><a href="{{url('/home/reports/promos')}}">Promociones</a></li>
        @endif
            @if($user->isA('super-admin', 'super-admin-restringido', 'subsidiary-admin', 'manager', 'cashier-admin'))
            <li><a href="{{url('/home/reports/sales/promotions')}}">Promociones por fecha</a></li>
            @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'subsidiary-admin', 'manager'))
            <li><a href="{{url('/home/reports/calendar')}}">Calendario</a></li>
            <li><a href="{{url('/home/reports/sales/repairs')}}">Descuento por reparaciones</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager', 'accountant'))
            <li class="active"><a href="javascript:void(0);">Reporte de pedidos</a></li>
            <li><a href="{{url('/home/reports/weekly/subsidiary-analysis')}}">Análisis de sucursales</a></li>
            <li><a href="{{url('/home/reports/warehouse/requests/unsupplied_orders')}}">Pedidos no surtidos</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido','subsidiary-admin', 'manager'))
            <li class="active"><a href="javascript:void(0);">Reporte de productos y servicios</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager'))
            <li><a href="{{url('/home/reports/services-subsidiaries')}}">Servicios por sucursal</a></li>
            <li><a href="{{url('/home/reports/services-employees')}}">Promedio de servicios por barbero</a></li>
            <li><a href="{{url('/home/reports/weekly/services/barbers')}}">Semanal por barbero</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido','subsidiary-admin', 'manager'))
            <li><a href="{{url('/home/reports/services-subsidiary')}}">Promedio de servicios por sucursal</a></li>
            <li><a href="{{url('/home/reports/services-per-time')}}">Promedio de servicios por tiempo</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager'))
            <li><a href="{{url('/home/reports/products-subsidiary')}}">Productos vendidos por sucursal</a></li>
            <li><a href="{{url('/home/reports/products-employees')}}">Productos vendidos por empleados</a></li>
            <li><a href="{{url('/home/reports/services/subsidiaries')}}">Resumen de servicios</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'nomina', 'manager'))
            <li class="active"><a href="javascript:void(0);">Reporte de inventarios</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager'))
            <li><a href="{{url('/home/reports/inventories/entries')}}">Entradas</a></li>
            <li><a href="{{url('/home/reports/inventories/departures')}}">Salidas</a></li>
            <li><a href="{{url('/home/reports/inventories/adjustments')}}">Ajustes</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'nomina', 'manager', 'cashier-admin'))
            <li><a href="{{url('/home/reports/inventories/stocks')}}">Existencias</a></li>
        @endif
        <!-- Acceso a Gerardo Reyes y Brisa -->
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager', 'accountant') || $user->id == 75 || $user->id == 87 || $user->id == 77)
            <li class="active"><a href="javascript:void(0);">Reporte de inventarios (Almacen)</a></li>
            <li><a href="{{url('/home/reports/inventories/warehouse/stocks')}}">Existencias</a></li>
            <li><a href="{{url('/home/reports/inventories/warehouse/productsToOrder')}}">Mercancía a comprar</a></li>
            <li><a href="{{url('/home/reports/inventories/warehouse/movementsByProduct')}}">Movimientos por producto</a></li>
        @endif
        @if($user->isA('super-admin', 'super-admin-restringido', 'manager'))
            <li class="active"><a href="javascript:void(0);">Marketing</a></li>
            <li><a href="{{url('/home/reports/sales/notFrequentCustomers')}}">Clientes no frecuentes</a></li>
            <li><a href="{{url('/home/reports/sales/byCustomer')}}">Ventas por cliente</a></li>
            <li><a href="{{url('/home/reports/customers/byBarber')}}">Clientes por barbero</a></li>

            <li class="active"><a href="javascript:void(0);">Encuestas</a></li>
            <li><a href="{{url('/home/reports/survey/subsidiary')}}">Por Sucursal</a></li>
            <li><a href="{{url('/home/reports/survey/barber')}}">Por Barbero</a></li>
        @endif
        </ul>
    </li>
@endif

@if(!$user->isA('employee') && ($user->isA('super-admin', 'super-admin-restringido', 'agenda', 'cashier-admin', 'subsidiary-admin', 'cashier-admin', 'manager') || $subdomain) && (!$subdomain || ($subdomain && $current_subsidiary->id != 11)) && !$user->isA('supervisor'))
<li><a href="{{route('diary.index')}}">Agenda</a></li>
<li><a href="{{route('admin.sales.activeSales')}}">Pantalla</a></li>
@endif
@if($user->isA('super-admin', 'super-admin-restringido', 'accountant'))
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            Erogaciones
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="{{route('expenditures.index')}}">Listado</a></li>
            <li><a href="{{route('accounts.index')}}">Cuentas</a></li>

            <li class="active"><a href="javascript:void(0);">Reportes</a></li>
            <li><a href="{{url('/home/reports/expenditures/subsidiary')}}">Gastos por Sucursal</a></li>
            <li><a href="{{url('/home/reports/expenditures/monthly')}}">Gastos mensuales</a></li>
        </ul>
    </li>
@endif
@if($user->isA('super-admin', 'super-admin-restringido','subsidiary-admin'))
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            Encuestas
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            @if($user->isA('super-admin', 'super-admin-restringido'))
            <li><a href="{{route('questions.index')}}">Preguntas</a></li>
            @endif
            <li><a href="{{route('admin.surveys.index')}}">Encuestas</a></li>
        </ul>
    </li>
@endif
<?php //dd("hola"); ?>
@if($subdomain && $user->isA('cashier'))
    <?php
        $issues_total = App\Issue::unreadedCashierMessagesBySubsidiary($current_subsidiary->id, $user)->count();
        $pendings_total = App\Pending::unreadedMessagesBySubsidiary($current_subsidiary->id, $user)->count();
    ?>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            Notificaciones
            @if($issues_total > 0 || $pendings_total > 0)
                <span class="badge badge-error">{{ $issues_total + $pendings_total }}</span>
            @endif
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li>
                <a href="{{route('issues.messages.received')}}">Mensajes
                @if($issues_total > 0)
                    <span class="badge badge-error">{{ $issues_total }}</span>
                @endif
                </a>
            </li>
            <li>
                <a href="{{route('pendings.nonFinished')}}">Pendientes
                @if($pendings_total > 0)
                    <span class="badge badge-error">{{ $pendings_total }}</span>
                @endif
                </a>
            </li>
        </ul>
    </li>
@endif

@if($user->isA('super-admin', 'super-admin-restringido', 'supervisor', 'agenda', 'cashier-admin', 'accountant','subsidiary-admin'))
    <?php
        $cashier_messages_total = App\Issue::unreadedCashierMessagesByAdmon($user)->count();
        $employee_messages_total = App\Issue::unreadedEmployeeMessagesByAdmon($user)->count();
        $pendings_total = App\Pending::unreadedMessages($user)->notAdmin()->count();
        $pendings_admin_total = App\Pending::unreadedMessages($user)->admin()->count();
        if($user->isA('super-admin', 'super-admin-restringido', 'agenda', 'cashier-admin')) {
            $complains_total = App\Complain::unreadedComplains()->count();
        } else {
            $complains_total = 0;
        }
    ?>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            Notificaciones
            @if($cashier_messages_total > 0 || $complains_total > 0 || $pendings_total > 0 || $pendings_admin_total > 0)
            &nbsp;<span class="badge badge-error">{{ $cashier_messages_total + $employee_messages_total + $complains_total + $pendings_total + $pendings_admin_total }}</span>
            @endif
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li>
                <a href="{{route('issues.messages.received')}}">Mensajes (Caja)
                    @if($cashier_messages_total > 0)
                    &nbsp;<span class="badge badge-error">{{ $cashier_messages_total }}</span>
                    @endif
                </a>
            </li>

            <li>
                <a href="{{route('issues.messages.employees.received')}}">Mensajes (Empleados)
                    @if($employee_messages_total > 0)
                    &nbsp;<span class="badge badge-error">{{ $employee_messages_total }}</span>
                    @endif
                </a>
            </li>

            @if(Auth::user()->isA('super-admin', 'super-admin-restringido','supervisor', 'cashier-admin', 'subsidiary-admin'))
                <li>
                    <a href="{{route('exchanges.index')}}">Intercambio de horarios</a>
                </li>
                <li>
                    <a href="{{route('pendings.nonFinished')}}">Pendientes
                        @if($pendings_total > 0)
                            &nbsp;<span class="badge badge-error">{{ $pendings_total }}</span>
                        @endif
                    </a>
                </li>
                @if(Auth::user()->isA('super-admin', 'super-admin-restringido'))
                <li>
                    <a href="{{route('pendings.admin.nonFinished')}}">Pendientes (Oficina)
                        @if($pendings_admin_total > 0)
                            &nbsp;<span class="badge badge-error">{{ $pendings_admin_total }}</span>
                        @endif
                    </a>
                </li>
                @endif
            @endif
            @if(Auth::user()->isA('super-admin', 'super-admin-restringido','agenda', 'cashier-admin', 'subsidiary-admin'))
                <li>
                    <a href="{{route('complains.index')}}">Quejas y sugerencias
                        @if($complains_total > 0)
                            <span class="badge badge-error">{{ $complains_total }}</span>
                        @endif
                    </a>
                </li>
            @endif
            @if(Auth::user()->isA('super-admin'))
                <li><a href="{{route('alerts.index')}}">Alertas</a></li>
            @endif
        </ul>
    </li>
@endif

@if($subdomain && !$user->isA('supervisor', 'employee', 'cashier-admin'))
    @if($current_subsidiary->id != 11)
        <li><a href="{{route('eats.index', $current_subsidiary->key)}}">Comidas</a></li>
    @endif
    <li>
        @if($current_subsidiary->devices->count() == 0)
            <a href="#" data-toggle="modal" data-target="#modal-clock-attendance">
                <i class="fa fa-clock-o fa-2x"></i>
            </a>
        @else
            <a href="#" data-toggle="modal" data-target="#modal-clock-attendance-device">
                <i class="fa fa-clock-o fa-2x"></i>
            </a>
        @endif
    </li>

    <li>
        <a
        @if($current_subsidiary->id != 11)
            @if($current_subsidiary->is_laundry)
                href="/home/sales/createLaundry"
            @else
                href="/home/sales/create"
            @endif
        @else
            href="/home/laundry-services/create"
        @endif
        title="Agregar nuevo servicio">
            <i class="fa fa-cart-plus fa-2x"></i>
        </a>
    </li>
    @if($user->isA('super-admin', 'super-admin-restringido') && $current_subsidiary->id == 11)
    <li>
        <a href="{{route('laundry-services.create2')}}" title="Agregar servicios antiguos">
            <i class="fa fa-plus-circle fa-2x"></i>
        </a>
    </li>
    @endif
    @if($current_subsidiary->id != 11)
    <li>
        <a href="#" data-toggle="modal" data-target="#modal-schedules" title="Imprimir horarios del empleado">
            <i class="fa fa-calendar fa-2x"></i>
        </a>
    </li>
    <li>
        <a href="#" data-toggle="modal" data-target="#modal-schedules-barber" title="Imprimir horarios del empleado">
            <i class="fa fa-calendar-check-o fa-2x"></i>
        </a>
    </li>
    @endif
@endif
