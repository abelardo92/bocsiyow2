<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="{{ asset('images/logo.jpeg') }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link href="{{ asset('css/survey.css') }}" rel="stylesheet" type="text/css">

        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </head>
    <body>
        <header>
            <div class="row">
                <div class="col-xs-4 col-sm-4 col-md-4">
                    <div class="text-left">
                        <img src="{{ asset('images/logo.jpeg') }}">
                    </div>
                </div>
            </div>
        </header>

        <div class="container mid-width">
            @yield('content')
        </div>
        
        <script src="{{asset('js/socket.io.js')}}"></script>
        <script src="{{ asset('js/survey.js') }}"></script>
        @yield('scripts')
    </body>
</html>
