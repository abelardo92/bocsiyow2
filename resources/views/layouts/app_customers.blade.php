<?php
$configuration = App\Configuration::first();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('images/logo.jpeg') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    {{-- <link href="/css/bootstrap-timepicker.min.css" rel="stylesheet"> --}}
    <link href="/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    {{-- <link rel="stylesheet" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"> --}}
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.1/fullcalendar.min.css" /> --}}
    <link href="/css/app.css" rel="stylesheet">
    @yield('css')

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
            'subsidiaries' => [7, 10, 5, 6, 9, 4]
        ]); ?>
    </script>

</head>
<body>


    @if(session()->has('success'))
        <div class="alert alert-info alert-dismissible" role="alert" style="right: 50px; top: 100px; z-index: 1031; position: absolute;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">X</span></button>
            {{session('success')}}
        </div>
    @endif

    @if(session()->has('error'))
        @php
        $extra_margin = 'margin-top: 320px;';
        @endphp
        <div class="alert alert-danger alert-dismissible" role="alert" style="{{$extra_margin}} right: 70px; z-index: 1031; position: absolute;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">X</span></button>
            {!! session('error') !!}
        </div>
    @endif

    <div class="container show-in-print">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <p>@yield('title', 'BocsiYow')</p>
                <p>{{ $configuration->razon_social }}</p>
                <p>{{ $configuration->rfc }}</p>
                <p>{{ $configuration->address }}</p>
                <p>Fecha de impresión: {{ Carbon\Carbon::now()->formatLocalized('%d de %B del %Y') }}</p>
            </div>
        </div>
    </div>

    <div id="app">
        @yield('content')
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.2/moment-with-locales.min.js"></script>
    <script src="/js/app.js?v=<?=strtotime('now')?>"></script>
    <script src="/js/bootstrap-timepicker.min.js"></script>
    <script src="/js/bootstrap-datepicker.min.js"></script>
    <script src="/js/bootstrap-datepicker.es.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.1/fullcalendar.min.js"></script>
    <script src="/js/enter2tab.js"></script>
    <script>
        $(function () {
            $("fieldset").enableEnterToTab({ captureTabKey: true });
            $('.timeinput').timepicker({
                icons: {
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down'
                }
            });
            $('.input-daterange').datepicker({
                language: "es",
                format: 'yyyy-mm-dd'
            });
            $('.datepicker').datepicker({
                language: "es",
                format: 'yyyy-mm-dd'
            });
            $('.select2').select2({placeholder: "Selecciona uno por favor..."});
            $('[data-toggle="tooltip"]').tooltip();
            $.extend( true, $.fn.dataTable.defaults, {
                "ordering": true
            } );
            $('.datatables').DataTable();
            $('#calendar').fullCalendar({
                lang: 'es',
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                events: {
                    url: '/home/reports/calendars',
                    type: 'GET',
                    textColor: '#ffffff',
                    data: function() { // a function that returns an object
                        return {
                            subsidiary_id: $('#subsidiary_id').val()
                        };
                    }
                }
            });
        });
    </script>
    @yield('scripts')
</body>
</html>
