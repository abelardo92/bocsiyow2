<button type="button" class="btn btn-link" data-toggle="modal" data-target="#time-modal-{{$attendance->id}}">Editar hora</button>
<div class="modal fade" tabindex="-1" role="dialog" id="time-modal-{{$attendance->id}}">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Actualizar hora</h4>
      </div>
      <div class="modal-body">
        <form action="{{route('time.edit', $attendance->id)}}" method="POST">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="time">Hora</label>
            <input type="time" class="form-control" name="time" id="time" value="{{$attendance->time}}">
          </div>

          <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Actualizar">
          </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->