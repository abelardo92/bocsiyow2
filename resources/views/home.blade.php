@extends('layouts.app')

@section('content')
    @if($subdomain && (!$user->isA('supervisor', 'Barbero', 'employee')))

        @if($current_subsidiary->is_laundry)
            <laundry-cash-register 
            imprest="{{$current_subsidiary->imprest}}"
            :services="{{$services->toJson()}}"
            :products="{{$current_subsidiary->products()->with('image')->get()->toJson()}}"
            :packages="{{$packages->toJson()}}"
            <?php $minicut = $current_subsidiary->miniCutPetitions()->latest()->first(); ?>
            @if($minicut != null)
                minicut="{{$minicut->datetime}}"
            @else
                minicut=""
            @endif
            :turns="{{$turns->toJson()}}"
            :barbers="{{$employees->toJson()}}"
            :sales_own="{{$sales->toJson()}}"
            exchange_rate_id="{{$exchange_rate->id}}"
            exchange_rate="{{$exchange_rate->rate}}"
            subsidiary="{{$current_subsidiary->toJson()}}"
            user="{{$user->toJson()}}"
            :cuts="{{$cuts->toJson()}}"
            >
            </laundry-cash-register>
        @else
            <cash-register 
            imprest="{{$current_subsidiary->imprest}}"
            :services="{{$services->toJson()}}"
            :products="{{$current_subsidiary->products()->with('image', 'subsidiaryPrices')->get()->toJson()}}"
            <?php $minicut = $current_subsidiary->miniCutPetitions()->latest()->first(); ?>
            @if($minicut != null)
                minicut="{{$minicut->datetime}}"
            @else
                minicut=""
            @endif
            :turns="{{$turns->toJson()}}"
            :barbers="{{$employees->toJson()}}"
            :sales_own="{{$sales->toJson()}}"
            exchange_rate_id="{{$exchange_rate->id}}"
            exchange_rate="{{$exchange_rate->rate}}"
            subsidiary_str="{{$current_subsidiary->toJson()}}"
            subsidiaries_str="{{$subsidiaries->toJson()}}"
            user_str="{{$user->toJson()}}"
            time_average="{{$time_average}}"
            @if($laundry_services != null)
                laundry_services="{{$laundry_services->toJson()}}"
            @endif
            @if($current_subsidiary->id != 11)
                :cuts="{{$cuts->toJson()}}"
            @endif
            @if($current_promotion != null)
                :current_promotion="{{$current_promotion->toJson()}}"
            @else
                {{-- :current_promotion="{{$current_promotion}}" --}}
            @endif
            >
            </cash-register>
        @endif
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Empleados del turno
                            @if(Carbon\Carbon::now()->format('H:i:s') <= '16:00:00')
                                matutino
                            @else
                                vespertino
                            @endif
                        </div>
                        <div class="panel-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Puesto</th>
                                        <th>Nombre</th>
                                        <th>Hora entrada</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($current_subsidiary->schedules()->today()->currentTurn($current_subsidiary->id)->get() as $schedule)
                                        <?php
                                            $todayAttendances = $schedule->employee->attendances()->where('subsidiary_id', $current_subsidiary->id)->today()->get();
                                        ?>
                                        @if(!$todayAttendances->count())
                                            <?php continue; ?>
                                        @endif
                                        @if($schedule->employee->has_rest)
                                            @if($todayAttendances->count() == 4)
                                                <?php continue; ?>
                                            @endif
                                        @else
                                            @if($todayAttendances->count() == 2)
                                                <?php continue; ?>
                                            @endif
                                        @endif
                                        <tr>
                                            <td>{{$schedule->employee->job}}</td>
                                            <td>{{$schedule->employee->short_name}}</td>
                                            <td>
                                                @if($todayAttendances->count())
                                                    {{$todayAttendances->first()->getTime()}}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                @if($current_subsidiary->id != 11)
                    <reprint-cuts :cuts="{{$cuts->toJson()}}"> </reprint-cuts>
                    <repair-service :barbers="{{$employees->toJson()}}"> </repair-service>
                    <reprint-out-ticket> </reprint-out-ticket>
                    <see-issues> </see-issues>
                @endif

            </div>
        </div>
    @endif
    
    @if($user->isA('supervisor'))
        <div class="container">
            @include('supervisor.dashboard')
        </div>
    @endif

    @if($user->isA('cashier-admin'))
        <div class="container">
            @include('cashier-admin.dashboard')
        </div>
    @endif

    @if($user->isA('super-admin', 'super-admin-restringido', 'manager'))
        <div class="container">
            @include('admin.dashboard')
        </div>
    @endif
    
    @if($user->isA('subsidiary-admin'))
        <div class="container">
            @include('subsidiary-admin.dashboard')
        </div>
    @endif

    @if((!$subdomain && $user->isA('employee', 'cashier', 'barber', 'Barbero')) || ($subdomain && $user->employee != null && !$user->isA('cashier'))))
        <div class="container">
            @include('employee_dashboard')
        </div>
    @endif

@endsection
