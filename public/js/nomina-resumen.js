
function updatePaysheet() {
    $('#paysheet-error').hide();
    $('#paysheet-success').hide();
    const paysheet_id = $("#paysheet_id").val();
    const paysheetData = getPaysheetData(paysheet_id);

    $.ajax({
        type: 'post',
        dataType: 'json',
        data: paysheetData,
        url: '/home/paysheets/updateById',
        success: function({success, message, paysheet_id}) {
            if(success) {
                $("#paysheet_id").val(paysheet_id);
                $('#paysheet-success').show();
                $('#paysheet-success').html(message);
            }
        },
        error: function (error) {
            $('#paysheet-error').show();
            $('#paysheet-error').html(JSON.stringify(error));
        }
    });
}

function archivePaysheet(paysheet_id) {
    $('#paysheet-error').hide();
    $('#paysheet-success').hide();
    const data = getPaysheetData(paysheet_id);
    $.ajax({
        type: 'post',
        dataType: 'json',
        data,
        url: '/home/paysheets/archiveById',
        success: function(data) {
            if(data.success) {
                $('#paysheet-success').show();
                $('#paysheet-success').html(data.message);
            }
        },
        error: function (error) {
            $('#paysheet-error').show();
            $('#paysheet-error').html(JSON.stringify(error));
        }
    });
}

function getPaysheetData(paysheet_id) {
    const data = {
        _token: $('input[name="_token"]').val(),
        id: paysheet_id,
        start: $('#original_start').val(),
        end: $('#original_end').val(),
        employee_id: $('#original_employee_id').val(),
        total: Number($('#salarios-text').html().replace(',','')),
        asistencia: Number($('#attendance').val().replace(',','')),
        asistencia_pay: $('#check-attendance').is(':checked') ? 1 : 0,
        puntualidad: Number($('#punctuality').val().replace(',','')),
        puntualidad_pay: $('#check-punctuality').is(':checked') ? 1 : 0,
        productividad: Number($('#productivity').val().replace(',','')),
        productividad_pay: $('#check-productivity').is(':checked') ? 1 : 0,
        sunday: 0,
        sunday_pay: 0,
        // sunday: Number($('#sunday').val().replace(',','')),
        // sunday_pay: $('#check-sunday').is(':checked') ? 1 : 0,
        excellence: Number($('#excellence').val().replace(',','')),
        excellence_pay: $('#check-excellence').is(':checked') ? 1 : 0,
        extra: Number($('#extraday').val().replace(',','')),
        extra_pay: $('#check-extraday').is(':checked') ? 1 : 0,
        prestamo: Number($('#prestamo-text').html()),
        uniforme: Number($('#uniform-text').html()),
        infonavit: Number($('#infonavit-text').html()),
        depositado: Number($('#depositado').val()),
        note: $("#note").val(),
    };
    return data;
}

$(function() {

    submitEvent("deducciones");
    submitEvent("percepciones");

    function submitEvent(section) {
        $(`#${section}_form`).submit(function(e){
            e.preventDefault();

            $(`#${section}_error`).hide();
            $(`#${section}_success`).hide();

            $.ajax({
                type: 'post',
                dataType: 'json',
                data: {
                    _token: $('input[name="_token"]').val(),
                    start: $("#original_start").val(),
                    end: $("#original_end").val(),
                    employee_id: $("#original_employee_id").val(),
                    paysheet_id: $("#paysheet_id").val(),
                    text: $(`#${section}_form #text`).val(),
                    amount: $(`#${section}_form #amount`).val(),
                    date: $(`#${section}_form #date`).val(),
                    note: $("#note").val(),
                },
                url: `/home/${section}`,
                success: function({success, message, paysheet_id}) {
                    if(success) {
                        $(`#${section}_success`).html(message);
                        $(`#${section}_success`).show();
                        setTimeout(function () {
                            $(".modal").modal('hide');
                            $("#search").trigger("click");
                        } , 2000);
                    }
                },
                error: function (error) {
                    $(`#${section}_error`).show();
                    $(`#${section}_error`).html(JSON.stringify(error));
                }
            });
        });
    }

    function updateTotales() {
        const total = Number($('#total-text').html().replace(',',''));
        const total_deducciones = Number($('#total-deducciones-text').html().replace(',',''));
        const neto = total - total_deducciones;
        const depositado = Number($('#depositado').val());
        $('#neto-text').text(parseFloat(neto).toFixed(2));
        $('#diferencia-text').text(parseFloat(neto - depositado).toFixed(2));
    }

    function updateIngresos() {
        const total = Number($('#salarios-text').html().replace(',',''));
        const asistencia = Number($('#attendance-text').html().replace(',',''));
        const puntualidad = Number($('#punctuality-text').html().replace(',',''));
        const productividad = Number($('#productivity-text').html().replace(',',''));
        const sunday = 0;
        // const sunday = Number($('#sunday-text').html().replace(',',''));
        const excellence = Number($('#excellence-text').html().replace(',',''));
        const extra = Number($('#extraday-text').html().replace(',',''));
        const tips = Number($('#tips-text').html().replace(',',''));
        const commission = Number($('#commission-text').html().replace(',',''));
        const perceptions = Number($('#perceptions-text').html().replace(',',''));
        const sum = total + asistencia + puntualidad + productividad + sunday + excellence + extra + tips + commission + perceptions;
        //alert(sum);
        $('#total-text').text(parseFloat(sum).toFixed(2));
        updateTotales();
    }

    function updateDeducciones() {
        const prestamo = Number($('#prestamo').val());
        // const faltas = Number($('#faults-text').html().replace(',',''));
        const uniforme = Number($('#uniform').val());
        const infonavit = Number($('#infonavit').val());
        const commission = Number($('#commissiontip-text').html().replace(',',''));
        const deductions = Number($('#deductions-text').html().replace(',',''));
        // const sum = prestamo + faltas + uniforme + infonavit + commission + deductions;
        const sum = prestamo + uniforme + infonavit + commission + deductions;
        $('#total-deducciones-text').text(parseFloat(sum).toFixed(2));
        updateTotales();
    }

    $('#check-attendance, #check-punctuality, #check-productivity, #check-excellence, #check-extraday, #check-sunday').on('change', function (e) {
        const $this = $(e.currentTarget);
        const input = $this.attr('id').split('-')[1];

        const val = $this.is(':checked') ? parseFloat($('#'+input).val().replace(',','.')).toFixed(2) : "0.00";

        $(`#${input}-text`).text(val);
        updateIngresos();
    });

    $("#check-attendance").trigger("change");
    $("#check-punctuality").trigger("change");
    $("#check-productivity").trigger("change");
    $("#check-sunday").trigger("change");
    $("#check-excellence").trigger("change");
    $("#check-extraday").trigger("change");

    // $('#check-faults').on('change', function (e) {
    //     const $this = $(e.currentTarget);
    //     const input = $this.attr('id').split('-')[1];
    //     const val = 0;
    //     if ($this.is(':checked')) {
    //         val = $('#'+input).val();
    //     }
    //     $('#'+input+'-text').text(val+'.00');
    //     updateDeducciones();
    // });

    $('#prestamo, #uniform, #infonavit').on('change', function (e) {
        updateDeducciones();
    });

    $('#depositado').on('change', function (e) {
        updateTotales();
    });
});
