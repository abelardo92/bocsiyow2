<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseMovementProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_movement_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_movement_id')->unsigned();
            $table->foreign('warehouse_movement_id')->references('id')->on('warehouse_movements');
            $table->integer('warehouse_product_id')->unsigned();
            $table->foreign('warehouse_product_id')->references('id')->on('warehouse_products');
            $table->integer('quantity')->unsigned();
            $table->decimal('amount', 12, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_movement_products');
    }
}
