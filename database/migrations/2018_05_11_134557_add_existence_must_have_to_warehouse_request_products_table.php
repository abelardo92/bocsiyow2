<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExistenceMustHaveToWarehouseRequestProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('warehouse_request_products', function (Blueprint $table) {
            $table->integer('existence')->default(0);
            $table->integer('must_have')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('warehouse_request_products', function (Blueprint $table) {
            $table->dropColumn('existence');
            $table->dropColumn('must_have');
        });
    }
}
