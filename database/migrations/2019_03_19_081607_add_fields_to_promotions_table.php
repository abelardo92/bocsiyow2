<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToPromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promotions', function (Blueprint $table) {
            $table->integer('must_buy_specific_product')->unsigned()->nullable();
            $table->foreign('must_buy_specific_product')->references('id')->on('products');
            $table->integer('must_buy_specific_service')->unsigned()->nullable();
            $table->foreign('must_buy_specific_service')->references('id')->on('services');
            $table->boolean('is_permanent')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promotions', function (Blueprint $table) {
            $table->dropForeign(['must_buy_specific_service']);
            $table->dropColumn('must_buy_specific_service');
            $table->dropForeign(['must_buy_specific_product']);
            $table->dropColumn('must_buy_specific_product');
            $table->dropColumn('is_permanent');
        });
    }
}
