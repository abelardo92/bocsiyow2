<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCanceledToSaleProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sale_products', function (Blueprint $table) {
            $table->integer('canceled_by')->nullable()->unsigned();
            $table->foreign('canceled_by')->references('id')->on('users');
            $table->datetime('canceled_at')->nullable();
            // $table->string('canceled_description');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sale_products', function (Blueprint $table) {
            $table->dropForeign(['canceled_by']);
            $table->dropColumn('canceled_by');
            $table->dropColumn('canceled_at');
            // $table->dropColumn('canceled_description');
        });
    }
}
