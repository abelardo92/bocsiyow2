<?php

use App\Expenditure;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTotalToExpendituresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (Expenditure::all() as $expenditure) {
            $expenditure->update([
                'total' => floatval(str_replace(',', '', $expenditure->total))
            ]);
        }
        
        // Schema::table('expenditures', function (Blueprint $table) {
        //     $table->decimal('total', 16, 2)->change();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('expenditures', function (Blueprint $table) {
        //     $table->string('total')->change();
        // });
    }
}
