<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserSubsidiaryToIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('issues', function (Blueprint $table) {
            $table->integer('origin_user_id')->unsigned()->nullable();
            $table->foreign('origin_user_id')->references('id')->on('users');
            $table->integer('origin_subsidiary_id')->unsigned()->nullable();
            $table->foreign('origin_subsidiary_id')->references('id')->on('subsidiaries');
            $table->integer('destiny_subsidiary_id')->unsigned()->nullable();
            $table->foreign('destiny_subsidiary_id')->references('id')->on('subsidiaries');
            $table->string('subject');
            //$table->integer('employee_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('issues', function (Blueprint $table) {
            $table->dropForeign(['origin_user_id']);
            $table->dropColumn('origin_user_id');
            $table->dropForeign(['origin_subsidiary_id']);
            $table->dropColumn('origin_subsidiary_id');
            $table->dropForeign(['destiny_subsidiary_id']);
            $table->dropColumn('destiny_subsidiary_id');
            $table->dropColumn('subject');
            //$table->integer('employee_id')->nullable(false)->change();
        });
    }
}
