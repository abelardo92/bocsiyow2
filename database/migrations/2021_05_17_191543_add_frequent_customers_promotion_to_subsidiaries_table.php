<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddFrequentCustomersPromotionToSubsidiariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subsidiaries', function (Blueprint $table) {
            $table->boolean('frequent_customers_discount')->default(false);
        });
        // Activar la promoción para TULUM únicamente
        DB::statement("UPDATE subsidiaries SET frequent_customers_discount = 1 WHERE id = '6' LIMIT 1;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subsidiaries', function (Blueprint $table) {
            $table->dropColumn('frequent_customers_discount');
        });
    }
}
