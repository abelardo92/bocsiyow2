<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseAdjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_adjustments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('folio')->unsigned();
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('warehouse_product_id')->unsigned()->nullable();
            $table->foreign('warehouse_product_id')->references('id')->on('warehouse_products');
            $table->integer('current_existence')->default(0);
            $table->integer('qty')->default(0);
            $table->integer('real_existence')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_adjustments');
    }
}
