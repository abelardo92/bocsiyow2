<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaysheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paysheets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key');
            $table->date('start');
            $table->date('end');
            $table->integer('employee_id');
            $table->decimal('total', 12, 2);
            $table->decimal('asistencia', 12, 2);
            $table->boolean('asistencia_pay');
            $table->decimal('puntualidad', 12, 2);
            $table->boolean('puntualidad_pay');
            $table->decimal('productividad', 12, 2);
            $table->boolean('productividad_pay');
            $table->decimal('extra', 12, 2);
            $table->boolean('extra_pay');
            $table->decimal('total_ingreso', 12, 2);
            $table->decimal('prestamo', 12, 2);
            $table->decimal('faltas', 12, 2);
            $table->boolean('faltas_pay');
            $table->decimal('uniforme', 12, 2);
            $table->decimal('infonavit', 12, 2);
            $table->decimal('otras', 12, 2);
            $table->decimal('total_deducciones', 12, 2);
            $table->decimal('neto', 12, 2);
            $table->decimal('depositado', 12, 2);
            $table->decimal('diferencia', 12, 2);
            $table->string('status')->default('save');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paysheets');
    }
}
