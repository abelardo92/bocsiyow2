<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddDiscountPercentajeToSaleServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sale_services', function (Blueprint $table) {
            $table->integer('discount_percentaje')->unsigned()->nullable();
        });

        $query = "UPDATE sale_services ss 
        INNER JOIN sales s on s.id = ss.sale_id 
        SET ss.discount_percentaje = 20, ss.price = ss.price * 0.8
        WHERE WEEKDAY(ss.created_at) = 1 AND s.subsidiary_id = 10 AND ss.created_at > '2020-01-18'";
        DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sale_services', function (Blueprint $table) {
            $table->dropColumn('discount_percentaje');
        });
    }
}
