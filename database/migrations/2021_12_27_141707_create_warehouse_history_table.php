<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->date('date');
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });

        Schema::create('warehouse_history_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_history_id')->unsigned()->nullable();
            $table->foreign('warehouse_history_id')->references('id')->on('warehouse_histories');
            $table->integer('warehouse_product_id')->unsigned()->nullable();
            $table->foreign('warehouse_product_id')->references('id')->on('warehouse_products');
            $table->string('code');
            $table->string('name');
            $table->integer('use')->unsigned();
            $table->decimal('amount', 12, 2);
            $table->integer('accounting')->unsigned();
            $table->integer('in_storage')->unsigned();
            $table->integer('product_id')->unsigned()->nullable();
            $table->foreign('product_id')->references('id')->on('products');
            $table->integer('type')->unsigned()->default(1);
            $table->integer('warehouse_desired_existence')->default(0);
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_history_products');
        Schema::dropIfExists('warehouse_histories');
    }
}
