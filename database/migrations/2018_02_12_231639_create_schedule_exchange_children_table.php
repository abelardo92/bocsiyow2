<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleExchangeChildrenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_exchange_children', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('employee_id')->unsigned();
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->integer('employee2_id')->unsigned();
            $table->foreign('employee2_id')->references('id')->on('employees');
            $table->boolean('employee_authorized')->nullable();
            $table->boolean('employee2_authorized')->nullable();
            $table->integer('schedule_exchange_id')->unsigned();
            $table->foreign('schedule_exchange_id')->references('id')->on('schedule_exchanges');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_exchange_children');
    }
}
