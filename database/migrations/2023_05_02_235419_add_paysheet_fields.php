<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaysheetFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paysheets', function (Blueprint $table) {
            $table->decimal('sunday', 12, 2);
            $table->boolean('sunday_pay');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paysheets', function (Blueprint $table) {
            $table->dropColumn('sunday');
            $table->dropColumn('sunday_pay');
        });
    }
}
