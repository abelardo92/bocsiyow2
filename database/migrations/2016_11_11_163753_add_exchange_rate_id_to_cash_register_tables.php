<?php

use App\Sale;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExchangeRateIdToCashRegisterTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cash_registers', function (Blueprint $table) {
            $table->integer('exchange_rate_id')->nullable();
        });

        foreach (Sale::groupBy('cash_register_id')->get() as $sale) {
            if($cash_register = $sale->cashRegister){
                $cash_register->exchange_rate_id = $sale->exchange_rate_id;
                $cash_register->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cash_registers', function (Blueprint $table) {
            $table->dropColumn('exchange_rate_id');
        });
    }
}
