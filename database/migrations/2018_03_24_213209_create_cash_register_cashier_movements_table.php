<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashRegisterCashierMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_register_cashier_movements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fiftycents')->nullable();
            $table->integer('onepeso')->default(0);
            $table->integer('twopesos')->default(0);
            $table->integer('fivepesos')->default(0);
            $table->integer('tenpesos')->default(0);
            $table->integer('twentypesos')->default(0);
            $table->integer('fiftypesos')->default(0);
            $table->integer('hundredpesos')->default(0);
            $table->integer('twohundredpesos')->default(0);
            $table->integer('fivehundredpesos')->default(0);
            $table->integer('onethousandpesos')->default(0);
            $table->integer('usd')->default(0);
            $table->decimal('exchange_rate', 12, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_register_cashier_movements');
    }
}
