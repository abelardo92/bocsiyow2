<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncludeInBirthdayToServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->boolean('include_in_birthday_discount')->default(true);
        });
        Schema::table('sale_services', function (Blueprint $table) {
            $table->boolean('include_in_birthday_discount')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->dropColumn('include_in_birthday_discount');
        });
        Schema::table('sale_services', function (Blueprint $table) {
            $table->dropColumn('include_in_birthday_discount');
        });
    }
}
