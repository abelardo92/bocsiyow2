<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncludeInDiariesDiscountToSaleServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sale_services', function (Blueprint $table) {
            $table->boolean('include_in_diaries_discount')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sale_services', function (Blueprint $table) {
            $table->dropColumn('include_in_diaries_discount');
        });
    }
}
