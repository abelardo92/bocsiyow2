<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateSaleServicePrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // updates sales with desired cost
        // NICHUPTE 4
        // TULUM = 6
        // HUAYACAN = 9
        $update_costs = [
            // corte
            ['service_id' => '1', 'cost' => '57.22', 'subsidiary_id' => '9'],
            // barba
            ['service_id' => '2', 'cost' => '57.22', 'subsidiary_id' => '9'],

            // ['service_id' => '25', 'cost' => '50', 'subsidiary_id' => '9'],
            // ['service_id' => '54', 'cost' => '50', 'subsidiary_id' => '9'],

            // masage...
            ['service_id' => '16', 'cost' => '57.22', 'subsidiary_id' => '9'],
            ['service_id' => '39', 'cost' => '57.22', 'subsidiary_id' => '9'],
            ['service_id' => '51', 'cost' => '57.22', 'subsidiary_id' => '9'],
            ['service_id' => '53', 'cost' => '57.22', 'subsidiary_id' => '9'],

            // cejas...
            ['service_id' => '4', 'cost' => '57.22', 'subsidiary_id' => '9'],

            // aplicacion de tinte
            ['service_id' => '15', 'cost' => '50', 'subsidiary_id' => '9'],

            // depilacion fosas nasales
            ['service_id' => '30', 'cost' => '15', 'subsidiary_id' => '9'],

            // paquete premium
            ['service_id' => '35', 'cost' => '228.89', 'subsidiary_id' => '9'],
        ];

        foreach($update_costs as $cost) {
            DB::statement("UPDATE sale_services ss 
            INNER JOIN sales s ON s.id = ss.sale_id 
            SET cost = '{$cost['cost']}' 
            WHERE ss.service_id = '{$cost['service_id']}' AND s.subsidiary_id = '{$cost['subsidiary_id']}' AND s.date >= '2022-08-11';");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
