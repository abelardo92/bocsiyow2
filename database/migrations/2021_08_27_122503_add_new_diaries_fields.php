<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewDiariesFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('diaries', function (Blueprint $table) {
            $table->string('customer_name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->integer('suggested_customer_id')->unsigned()->nullable();
            $table->foreign('suggested_customer_id')->references('id')->on('customers');
            $table->boolean('created_by_customer')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('diaries', function (Blueprint $table) {
            $table->dropColumn('customer_name');
            $table->dropColumn('email');
            $table->dropColumn('phone');
            $table->dropForeign(['suggested_customer_id']);
            $table->dropColumn('suggested_customer_id');
            $table->dropColumn('created_by_customer');
        });
    }
}
