<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncludeToDiariesDiscountToPromotionServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promotion_services', function (Blueprint $table) {
            $table->boolean('include_in_diaries_discount')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promotion_services', function (Blueprint $table) {
            $table->dropColumn('include_in_diaries_discount');
        });
    }
}
