<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinalizedByToWarehouseMovements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('warehouse_movements', function (Blueprint $table) {
            $table->boolean('is_finalized')->default(false);
            $table->integer('finalized_by')->unsigned()->nullable();
            $table->foreign('finalized_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('warehouse_movements', function (Blueprint $table) {
            $table->dropColumn('is_finalized');
            $table->dropForeign(['finalized_by']);
            $table->dropColumn('finalized_by');
        });
    }
}
