<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelatedIssueToIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('issues', function (Blueprint $table) {
            $table->integer('opened_by')->unsigned()->nullable();
            $table->foreign('opened_by')->references('id')->on('users');
            $table->integer('related_issue_id')->unsigned()->nullable();
            $table->foreign('related_issue_id')->references('id')->on('issues');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('issues', function (Blueprint $table) {
            $table->dropForeign(['opened_by']);
            $table->dropColumn('opened_by');
            $table->dropForeign(['related_issue_id']);
            $table->dropColumn('related_issue_id');
        });
    }
}
