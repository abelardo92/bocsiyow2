<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCourtesyCodeToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->boolean('has_courtesy')->default(false);
            $table->string('courtesy_code')->default(null);
            $table->datetime('courtesy_code_sent_at')->nullable();
            $table->boolean('courtesy_active')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('has_courtesy');
            $table->dropColumn('courtesy_code');
            $table->dropColumn('courtesy_code_sent_at');
            $table->dropColumn('courtesy_active');
        });
    }
}
