<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleExchangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_exchanges', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('employee_id')->unsigned();
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->integer('employee2_id')->unsigned();
            $table->foreign('employee2_id')->references('id')->on('employees');
            $table->boolean('employee_authorized')->default(true);
            $table->boolean('employee2_authorized')->nullable();
            $table->boolean('admin_authorized')->nullable();
            $table->integer('user_id')->nullable()->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedule_exchanges', function (Blueprint $table) {
            Schema::dropIfExists('schedule_exchanges');
        });
    }
}
