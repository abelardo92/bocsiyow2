<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('address');
            $table->string('email');
            $table->string('phone');
            $table->string('marital_status');
            $table->string('gender');
            $table->date('birth_date');
            $table->integer('subsidiary_id')->unsigned();
            $table->foreign('subsidiary_id')->references('id')->on('subsidiaries');
            $table->string('nationality');
            $table->string('state');
            $table->string('city');
            $table->string('rfc');
            $table->string('curp');
            $table->string('nss');
            $table->string('health_condition');
            $table->string('suffer_some_illness');
            $table->string('father_name');
            $table->string('father_alive');
            $table->string('father_address');
            $table->string('father_occupation');
            $table->string('mother_name');
            $table->string('mother_alive');
            $table->string('mother_address');
            $table->string('mother_occupation');
            $table->string('spouse_name');
            $table->string('spouse_alive');
            $table->string('spouse_address');
            $table->string('spouse_occupation');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_requests');
    }
}
