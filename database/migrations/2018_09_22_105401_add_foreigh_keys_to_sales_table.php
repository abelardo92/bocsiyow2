<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeighKeysToSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales', function (Blueprint $table) {
            //$table->foreign('customer_id')->references('id')->on('customers');
            //-$table->foreign('subsidiary_id')->references('id')->on('subsidiaries');
            //-$table->foreign('turn_id')->references('id')->on('turns');
            //$table->foreign('exchange_rate_id')->references('id')->on('exchange_rates');
            //$table->foreign('cash_register_id')->references('id')->on('cash_registers');
            //$table->foreign('canceled_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales', function (Blueprint $table) {
            //$table->dropForeign(['customer_id']);
            //$table->dropForeign(['subsidiary_id']);
            //$table->dropForeign(['turn_id']);
            //$table->dropForeign(['exchange_rate_id']);
            //$table->dropForeign(['cash_register_id']);
            //$table->dropForeign(['canceled_by']);
        });
    }
}
