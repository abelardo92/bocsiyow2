<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKardexTypeToInventoryArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventory_articles', function (Blueprint $table) {
            $table->string('kardex_type')->nullable();
            $table->integer('folio')->nullable();
            $table->string('concepto')->nullable();
            $table->integer('current_existence')->nullable();
            $table->integer('real_existence')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventory_articles', function (Blueprint $table) {
            $table->dropColumn('kardex_type');
            $table->dropColumn('folio');
            $table->dropColumn('concepto');
            $table->dropColumn('current_existence');
            $table->dropColumn('real_existence');
        });
    }
}
