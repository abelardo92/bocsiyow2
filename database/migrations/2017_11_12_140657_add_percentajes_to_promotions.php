<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPercentajesToPromotions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promotions', function (Blueprint $table) {
            $table->integer('services_discount_percentaje')->default(0)->unsigned();
            $table->integer('products_discount_percentaje')->default(0)->unsigned();
            $table->boolean('must_buy_a_service')->default(false);
            $table->boolean('must_buy_a_product')->default(false);
            $table->integer('subsidiary_id')->nullable()->unsigned();
            $table->foreign('subsidiary_id')->references('id')->on('subsidiaries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promotions', function (Blueprint $table) {
            $table->dropForeign(['subsidiary_id']);
            $table->dropColumn('subsidiary_id');
            $table->dropColumn('products_discount_percentaje');
            $table->dropColumn('services_discount_percentaje');
            $table->dropColumn('must_buy_a_product');
            $table->dropColumn('must_buy_a_service');
        });
    }
}
