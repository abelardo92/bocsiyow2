<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveToEmployeeRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_requests', function (Blueprint $table) {
            $table->boolean('is_active')->default(true);
        });
        Schema::table('employees', function (Blueprint $table) {
            $table->integer('employee_request_id')->unsigned()->nullable();
            $table->foreign('employee_request_id')->references('id')->on('employee_requests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_requests', function (Blueprint $table) {
            $table->dropColumn('is_active');
        });
        Schema::table('employees', function (Blueprint $table) {
            $table->dropForeign(['employee_request_id']);
            $table->dropColumn('employee_request_id');
        });
    }
}
