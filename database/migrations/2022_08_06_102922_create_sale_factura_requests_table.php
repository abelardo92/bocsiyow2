<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateSaleFacturaRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('iso');
            $table->timestamps();
        });

        $query = "INSERT INTO 
            states (name, iso) 
            VALUES 
            ('Aguascalientes','AGS'),
            ('Baja California','BCN'),
            ('Baja California Sur','BCS'),
            ('Campeche','CAM'),
            ('Chiapas','CHP'),
            ('Chihuahua','CHI'),
            ('Ciudad de México','DIF'),
            ('Coahuila','COA'),
            ('Colima','COL'),
            ('Durango','DUR'),
            ('Guanajuato','GTO'),
            ('Guerrero','GRO'),
            ('Hidalgo','HGO'),
            ('Jalisco','JAL'),
            ('México','MEX'),
            ('Michoacán','MIC'),
            ('Morelos','MOR'),
            ('Nayarit','NAY'),
            ('Nuevo León','NLE'),
            ('Oaxaca','OAX'),
            ('Puebla','PUE'),
            ('Querétaro','QRO'),
            ('Quintana Roo','ROO'),
            ('San Luis Potosí','SLP'),
            ('Sinaloa','SIN'),
            ('Sonora','SON'),
            ('Tabasco','TAB'),
            ('Tamaulipas','TAM'),
            ('Tlaxcala','TLX'),
            ('Veracruz','VER'),
            ('Yucatán','YUC'),
            ('Zacatecas','ZAC')";

        DB::statement($query);

        Schema::create('cfdi_uses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('shortcut');
            $table->timestamps();
        });

        $query = "INSERT INTO 
            cfdi_uses (name, shortcut) 
            VALUES 
            ('Adquisición de mercancías','G01'),
            ('Devoluciones, descuentos o bonificaciones','G02'),
            ('Gastos en general','G03'),
            ('Construcciones','I01'),
            ('Mobiliario y equipo de oficina por inversiones','I02'),
            ('Equipo de transporte','I03'),
            ('Equipo de cómputo y accesorios','I04'),
            ('Dados, troqueles, moldes, matrices y herramental','I05'),
            ('Comunicaciones telefónicas','I06'),
            ('Comunicaciones satelitales','I07'),
            ('Otra maquinaria y equipo','I08'),
            ('Honorarios médicos, dentales y gastos hospitalarios.','D01'),
            ('Gastos médicos por incapacidad o discapacidad','D02'),
            ('Gastos funerales','D03'),
            ('Donativos','D04'),
            ('Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación)','D05'),
            ('Aportaciones voluntarias al SAR','D06'),
            ('Primas por seguros de gastos médicos','D07'),
            ('Gastos de transportación escolar obligatoria','D08'),
            ('Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones','D09'),
            ('Pagos por servicios educativos (colegiaturas)','D10'),
            ('Pagos','CP01'),
            ('Nómina','CN01'),
            ('Sin Efectos Fiscales','S01')";

        DB::statement($query);

        Schema::create('tax_regimes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        $query = "INSERT INTO 
            tax_regimes (name) 
            VALUES 
            ('Régimen Simplificado de Confianza'),
            ('Sueldos y salarios e ingresos asimilados a salarios'),
            ('Régimen de Actividades Empresariales y Profesionales'),
            ('Régimen de Incorporación Fiscal'),
            ('Enajenación de bienes'),
            ('Régimen de Actividades Empresariales con ingresos a través de Plataformas Tecnológicas'),
            ('Régimen de Arrendamiento'),
            ('Intereses'),
            ('Obtención de premios'),
            ('Dividendos'),
            ('Demás ingresos')";

        DB::statement($query);

        Schema::create('sale_factura_requests', function (Blueprint $table) {
            $table->increments('id');

            // sale information that should  match.
            $table->integer('subsidiary_id')->unsigned();
            $table->foreign('subsidiary_id')->references('id')->on('subsidiaries');
            $table->integer('folio')->unsigned();
            $table->date('folio_date');
            $table->decimal('subtotal', 16, 2)->nullable();
            // $table->string('customer_email')->nullable();

            // FACTURA information
            $table->string('rfc');
            $table->string('full_name');

            // Address information
            $table->string('postal_code');
            // $table->string('road_type');
            $table->string('road_name');
            $table->string('external_number');
            $table->string('internal_number');
            $table->string('suburb_name');
            $table->string('locality_name');
            $table->string('municipality_name');
            $table->integer('state_id')->unsigned();
            $table->foreign('state_id')->references('id')->on('states');
            $table->string('between_street_1');
            $table->string('between_street_2');

            $table->string('factura_email');
            $table->string('phone');
            $table->string('payment_type');
            $table->integer('cfdi_use_id')->unsigned();
            $table->foreign('cfdi_use_id')->references('id')->on('cfdi_uses');
            $table->integer('tax_regime_id')->unsigned();
            $table->foreign('tax_regime_id')->references('id')->on('tax_regimes');

            $table->string('status'); // 

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_factura_requests');
        Schema::dropIfExists('tax_regimes');
        Schema::dropIfExists('cfdi_uses');
        Schema::dropIfExists('states');
    }
}
