<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddSubsidiaryCostToSubsidiaryPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subsidiary_service_prices', function (Blueprint $table) {
            $table->decimal('cost', 16, 2)->default(0);
        });

        // default cost values...
        DB::statement("UPDATE services SET sell_price = '240', cost = '50' WHERE id = '1' LIMIT 1;");
        DB::statement("UPDATE services SET sell_price = '190', cost = '50' WHERE id = '2' LIMIT 1;");
        DB::statement("UPDATE services SET sell_price = '190', cost = '50' WHERE id IN ('25','54') LIMIT 1;");
        DB::statement("UPDATE services SET sell_price = '190', cost = '50' WHERE id IN ('16','39','51','53');");
        DB::statement("UPDATE services SET sell_price = '80', cost = '15' WHERE id = '4' LIMIT 1;");
        DB::statement("UPDATE services SET sell_price = '110', cost = '50' WHERE id = '15' LIMIT 1;");
        DB::statement("UPDATE services SET sell_price = '80', cost = '15' WHERE id = '30' LIMIT 1;");
        DB::statement("UPDATE services SET sell_price = '755', cost = '200' WHERE id = '35' LIMIT 1;");
        DB::statement("UPDATE services SET sell_price = '590', cost = '150' WHERE id = '36' LIMIT 1;");
        DB::statement("UPDATE services SET sell_price = '590', cost = '150' WHERE id = '37' LIMIT 1;");
        DB::statement("UPDATE services SET sell_price = '350', cost = '100' WHERE id = '38' LIMIT 1;");
        DB::statement("UPDATE services SET sell_price = '170', cost = '50' WHERE id = '52' LIMIT 1;");

        // updates sales with desired cost
        // NICHUPTE 4
        // TULUM = 6
        // HUAYACAN = 9
        $update_costs = [
            ['service_id' => '1', 'cost' => '50', 'subsidiary_id' => '4'],
            ['service_id' => '2', 'cost' => '50', 'subsidiary_id' => '4'],
            ['service_id' => '25', 'cost' => '50', 'subsidiary_id' => '4'],
            ['service_id' => '54', 'cost' => '50', 'subsidiary_id' => '4'],
            ['service_id' => '16', 'cost' => '50', 'subsidiary_id' => '4'],
            ['service_id' => '39', 'cost' => '50', 'subsidiary_id' => '4'],
            ['service_id' => '51', 'cost' => '50', 'subsidiary_id' => '4'],
            ['service_id' => '53', 'cost' => '50', 'subsidiary_id' => '4'],
            ['service_id' => '4', 'cost' => '15', 'subsidiary_id' => '4'],
            ['service_id' => '15', 'cost' => '50', 'subsidiary_id' => '4'],
            ['service_id' => '30', 'cost' => '15', 'subsidiary_id' => '4'],
            ['service_id' => '35', 'cost' => '200', 'subsidiary_id' => '4'],
            ['service_id' => '36', 'cost' => '150', 'subsidiary_id' => '4'],
            ['service_id' => '37', 'cost' => '150', 'subsidiary_id' => '4'],
            ['service_id' => '38', 'cost' => '100', 'subsidiary_id' => '4'],
            ['service_id' => '52', 'cost' => '50', 'subsidiary_id' => '4'],

            ['service_id' => '1', 'cost' => '50', 'subsidiary_id' => '6'],
            ['service_id' => '2', 'cost' => '50', 'subsidiary_id' => '6'],
            ['service_id' => '25', 'cost' => '50', 'subsidiary_id' => '6'],
            ['service_id' => '54', 'cost' => '50', 'subsidiary_id' => '6'],
            ['service_id' => '16', 'cost' => '50', 'subsidiary_id' => '6'],
            ['service_id' => '39', 'cost' => '50', 'subsidiary_id' => '6'],
            ['service_id' => '51', 'cost' => '50', 'subsidiary_id' => '6'],
            ['service_id' => '53', 'cost' => '50', 'subsidiary_id' => '6'],
            ['service_id' => '4', 'cost' => '15', 'subsidiary_id' => '6'],
            ['service_id' => '15', 'cost' => '50', 'subsidiary_id' => '6'],
            ['service_id' => '30', 'cost' => '15', 'subsidiary_id' => '6'],
            ['service_id' => '35', 'cost' => '200', 'subsidiary_id' => '6'],
            ['service_id' => '36', 'cost' => '150', 'subsidiary_id' => '6'],
            ['service_id' => '37', 'cost' => '150', 'subsidiary_id' => '6'],
            ['service_id' => '38', 'cost' => '100', 'subsidiary_id' => '6'],
            ['service_id' => '52', 'cost' => '50', 'subsidiary_id' => '6'],

            ['service_id' => '1', 'cost' => '55.56', 'subsidiary_id' => '9'],
            ['service_id' => '2', 'cost' => '55.56', 'subsidiary_id' => '9'],
            // ['service_id' => '25', 'cost' => '50', 'subsidiary_id' => '9'],
            // ['service_id' => '54', 'cost' => '50', 'subsidiary_id' => '9'],
            ['service_id' => '16', 'cost' => '55.56', 'subsidiary_id' => '9'],
            ['service_id' => '39', 'cost' => '55.56', 'subsidiary_id' => '9'],
            ['service_id' => '51', 'cost' => '55.56', 'subsidiary_id' => '9'],
            ['service_id' => '53', 'cost' => '55.56', 'subsidiary_id' => '9'],
            ['service_id' => '4', 'cost' => '55.56', 'subsidiary_id' => '9'],
            ['service_id' => '15', 'cost' => '50', 'subsidiary_id' => '9'],
            ['service_id' => '30', 'cost' => '15', 'subsidiary_id' => '9'],
            ['service_id' => '35', 'cost' => '222.22', 'subsidiary_id' => '9'],
        ];

        foreach($update_costs as $cost) {
            DB::statement("UPDATE sale_services ss 
            INNER JOIN sales s ON s.id = ss.sale_id 
            SET cost = '{$cost['cost']}' 
            WHERE ss.service_id = '{$cost['service_id']}' AND s.subsidiary_id = '{$cost['subsidiary_id']}' AND s.date >= '2022-08-04';");
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subsidiary_service_prices', function (Blueprint $table) {
            $table->dropColumn('cost');
        });
    }
}
