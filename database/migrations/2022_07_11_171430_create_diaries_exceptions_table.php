<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateDiariesExceptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diaries_exceptions', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(true);
            $table->integer('subsidiary_id')->unsigned();
            $table->foreign('subsidiary_id')->references('id')->on('subsidiaries');
            $table->boolean('monday')->default(false);
            $table->time('monday_from_time');
            $table->time('monday_to_time');
            $table->boolean('tuesday')->default(false);
            $table->time('tuesday_from_time');
            $table->time('tuesday_to_time');
            $table->boolean('wednesday')->default(false);
            $table->time('wednesday_from_time');
            $table->time('wednesday_to_time');
            $table->boolean('thursday')->default(false);
            $table->time('thursday_from_time');
            $table->time('thursday_to_time');
            $table->boolean('friday')->default(false);
            $table->time('friday_from_time');
            $table->time('friday_to_time');
            $table->boolean('saturday')->default(false);
            $table->time('saturday_from_time');
            $table->time('saturday_to_time');
            $table->boolean('sunday')->default(false);
            $table->time('sunday_from_time');
            $table->time('sunday_to_time');
            $table->timestamps();
        });


        $query = "INSERT INTO diaries_exceptions (subsidiary_id, monday, monday_from_time, monday_to_time
        , tuesday, tuesday_from_time, tuesday_to_time, wednesday, wednesday_from_time, wednesday_to_time
        , thursday, thursday_from_time, thursday_to_time, friday, friday_from_time, friday_to_time
        , saturday, saturday_from_time, saturday_to_time, sunday, sunday_from_time, sunday_to_time) VALUES (
        4, 0, '9:00:00', '12:00:00', 1, '9:00:00', '12:00:00', 1, '9:00:00', '12:00:00', 0, '9:00:00', '12:00:00'
        , 0, '9:00:00', '12:00:00', 0, '9:00:00', '12:00:00', 0, '9:00:00', '12:00:00'
        )";

        DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diaries_exceptions');
    }
}
