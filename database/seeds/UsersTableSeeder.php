<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->users() as $user) {
            if (!User::where('email', $user['email'])->first()) {
                $user = User::create($user);

                $user->assign('super-admin');
            }
        }
    }

    private function users()
    {
        return [
            [
                'name' => 'Jorge',
                'email' => 'admin@bocsiyowbarbers.com',
                'password' => bcrypt('Admin.010'),
                'api_token' => str_random(60),
            ],
            [
                'name' => 'Jorge Arguelles',
                'email' => 'arguellesyasociadossc@live.com.mx',
                'password' => bcrypt('Admin.010'),
                'api_token' => str_random(60),
            ],
        ];
    }
}
